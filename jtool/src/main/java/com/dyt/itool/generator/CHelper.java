package com.dyt.itool.generator;

import com.dyt.itool.model.IProperty;
import com.dyt.itool.model.impl.BasicProperty;
import com.dyt.itool.model.impl.ListProperty;
import com.dyt.itool.model.impl.MapProperty;

public class CHelper {


	public String getTypeName( IProperty property ){
		
		if( property.getType() == IProperty.PropertyType.BASIC )
		{
			BasicProperty basicProperty = ( BasicProperty)property;
			if( "string".equalsIgnoreCase( basicProperty.getTypeName() ) )
			{
				return "std::string";
			}
		}
		
		if( property.getType() == IProperty.PropertyType.LIST ){
			ListProperty listProperty = (ListProperty)property;
			
			return "std::vector<" + getTypeName( listProperty.getElementType() ) + ">";
		}
		
		if( property.getType() == IProperty.PropertyType.MAP ){
			MapProperty mapProperty = ( MapProperty)property;
			
			return "std::map<" + getTypeName( mapProperty.getKeyType()) + "," + getTypeName( mapProperty.getValueType( ) ) + ">";
		}
		
		return property.getTypeName( );
	}
	

}
