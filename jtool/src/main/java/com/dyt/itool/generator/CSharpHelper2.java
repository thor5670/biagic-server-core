package com.dyt.itool.generator;

import java.util.*;
import com.dyt.itool.model.*;
import com.dyt.itool.model.impl.*;

public class CSharpHelper2 {

	public static final String NEWLINE = "\n";

	private Map<String, String> basicType = new HashMap<String, String>();
	private Map<String, String> basicWrapType = new HashMap<String, String>();

	private Map<String, String> basictTypeReadStringMap = new HashMap<String, String>();
	
	public CSharpHelper2() {
		
		basicType.put("bool", "bool");
		basicType.put("byte", "byte");
		basicType.put("short", "short");
		basicType.put("int", "int");
		basicType.put("long", "long");
		basicType.put("float", "float");
		basicType.put("double", "double");
		basicType.put("string", "string");

		
		basicWrapType.put("bool", "Boolean");
		basicWrapType.put("byte", "byte");
		basicWrapType.put("short", "Short");
		basicWrapType.put("int", "int");
		basicWrapType.put("long", "Long");
		basicWrapType.put("float", "Float");
		basicWrapType.put("double", "Double");
		basicWrapType.put("string", "String");
		
		
		
		basictTypeReadStringMap.put("bool", "Boolean");
		basictTypeReadStringMap.put("byte", "Byte");
		basictTypeReadStringMap.put("short", "Int16");
		basictTypeReadStringMap.put("int", "Int32");
		basictTypeReadStringMap.put("long", "Int64");
		basictTypeReadStringMap.put("float", "Single");
		basictTypeReadStringMap.put("double", "Double");
		basictTypeReadStringMap.put("string", "String");

		
	}

	public String getTypeName(IProperty property) {

		if (property.getType() == IProperty.PropertyType.BASIC) {
			return basicType.get(property.getTypeName());
		}

		if (property.getType() == IProperty.PropertyType.LIST) {
			ListProperty listProperty = (ListProperty) property;

			return getWrapTypeName(listProperty.getElementType()) + "[]";
		}

		if (property.getType() == IProperty.PropertyType.MAP) {
			MapProperty mapProperty = (MapProperty) property;

			return "Dictionary<" + getWrapTypeName(mapProperty.getKeyType()) + ","
					+ getWrapTypeName(mapProperty.getValueType()) + ">";
		}

		return property.getTypeName();
	}
	
	public String getListTypeName(IProperty property,String size)
	{
		if (property.getType() == IProperty.PropertyType.LIST) {
			ListProperty listProperty = (ListProperty) property;

			return getWrapTypeName(listProperty.getElementType()) + "[" + size + "]";
		}

		return property.getTypeName();
	}

	public String getWrapTypeName(IProperty property) {
		if (property.getType() == IProperty.PropertyType.BASIC) {
			return basicWrapType.get(property.getTypeName());
		}

		if (property.getType() == IProperty.PropertyType.LIST) {
			ListProperty listProperty = (ListProperty) property;

			return getWrapTypeName(listProperty.getElementType()) + "[]";
		}

		if (property.getType() == IProperty.PropertyType.MAP) {
			MapProperty mapProperty = (MapProperty) property;

			return "Dictionary<" + getWrapTypeName(mapProperty.getKeyType()) + ","
					+ getWrapTypeName(mapProperty.getValueType()) + ">";
		}

		return property.getTypeName();
	}

	private String getReadString(String basicType) {
		return "Read" + this.basictTypeReadStringMap.get( basicType );
	}

	private String getWriteString(String basicType) {
		return "Write";
	}


	public String getVarDefine(IProperty property) {
		String define = "public " + getTypeName(property) + " "
				+ property.getVarName();

//		if (property.getType() != IProperty.PropertyType.BASIC) {
//			define += " = new " + getTypeName(property) + " ( )";
//		}
		define += ";";

		return define;
	}

	public String getVarPackageDefine(IProperty property) {

		StringBuilder sb = new StringBuilder();
		sb.append(NEWLINE);
		sb.append("	public " + getTypeName(property));

		if (property.getTypeName().equals("boolean")
				|| property.getTypeName().equals("Boolean")) {
			sb.append(" is");
		} else {
			sb.append(" get");
		}
		sb.append(Character.toUpperCase(property.getVarName().charAt(0))
				+ property.getVarName().substring(1) + "() {");
		sb.append(NEWLINE);
		sb.append("		return " + property.getVarName() + ";");
		sb.append(NEWLINE);
		sb.append("	}");
		sb.append(NEWLINE);

		sb.append(NEWLINE);
		sb.append("	public void set");
		sb.append(Character.toUpperCase(property.getVarName().charAt(0))
				+ property.getVarName().substring(1) + "("
				+ getTypeName(property) + " " + property.getVarName() + ") {");
		sb.append(NEWLINE);
		sb.append("		this." + property.getVarName() + " = "
				+ property.getVarName() + ";");
		sb.append(NEWLINE);
		sb.append("	}");

		return sb.toString();
	}

	public String getReaderString(IProperty property) {

		if (property.getType() == IProperty.PropertyType.USERTYPE) {
			StringBuilder sb = new StringBuilder();
			sb.append("if( buffer.ReadBoolean( ) ){");
			sb.append(NEWLINE);
			sb.append("			" + property.getVarName() + " = new " +  getTypeName(property) + "();" );			
			sb.append(NEWLINE);
			sb.append("			" + property.getVarName() + ".readFromBuffer( buffer );");
			sb.append(NEWLINE);
			sb.append("		}else{");
			sb.append(NEWLINE);
			sb.append("			" + property.getVarName() + " = null;");
			sb.append(NEWLINE);
			sb.append("		}");
			sb.append(NEWLINE);
			return sb.toString();

		} else if (property.getType() == IProperty.PropertyType.LIST) {

			ListProperty listProperty = (ListProperty) property;

			StringBuilder sb = new StringBuilder();
			sb.append("{");
			sb.append(NEWLINE);
			sb.append("    		int size = buffer.ReadInt32();");
			sb.append(NEWLINE);
			sb.append("			" + property.getVarName() + " = new " + getListTypeName(property,"size") + ";");
			sb.append(NEWLINE);
			sb.append("    		for( int i = 0; i < size; i++ ){");
			sb.append(NEWLINE);
			if (listProperty.getElementType().getType() == IProperty.PropertyType.BASIC) {
				sb.append("	       		"
						+ property.getVarName()
						+ "[i] = buffer."
						+ getReadString(listProperty.getElementType()
								.getTypeName()) + "();");
				sb.append(NEWLINE);
			} else {
				sb.append("        		"
						+ listProperty.getElementType().getTypeName()
						+ " model = new "
						+ listProperty.getElementType().getTypeName() + "( );");
				sb.append(NEWLINE);
				sb.append("        		model.readFromBuffer( buffer );");
				sb.append(NEWLINE);
				sb.append("				" + property.getVarName() + "[i] =  model ; ");
				sb.append(NEWLINE);
			}
			sb.append("    		}");
			sb.append(NEWLINE);
			sb.append("		}");
			return sb.toString();

		} else if (property.getType() == IProperty.PropertyType.MAP) {

			MapProperty mapProperty = (MapProperty) property;

			StringBuilder sb = new StringBuilder();
			sb.append("{");
			sb.append(NEWLINE);
			sb.append("    		int size = buffer.ReadInt32();");
			sb.append(NEWLINE);
			sb.append("    		for( int i = 0; i < size; i++ ){");
			sb.append(NEWLINE);
			if (mapProperty.getKeyType().getType() == IProperty.PropertyType.BASIC) {
				sb.append("        		"
						+ getWrapTypeName(mapProperty.getKeyType())
						+ " key = buffer."
						+ getReadString(mapProperty.getKeyType().getTypeName())
						+ "( );");
				sb.append(NEWLINE);
			} else {
				sb.append("        		"
						+ getWrapTypeName(mapProperty.getKeyType())
						+ " key = new "
						+ mapProperty.getKeyType().getTypeName() + "( );");
				sb.append(NEWLINE);
				sb.append("        		key.readFromBuffer( buffer );");
				sb.append(NEWLINE);
			}

			if (mapProperty.getValueType().getType() == IProperty.PropertyType.BASIC) {
				sb.append("        		"
						+ getWrapTypeName(mapProperty.getValueType())
						+ " value = buffer."
						+ getReadString(mapProperty.getValueType()
								.getTypeName()) + "( );");
				sb.append(NEWLINE);
			} else {
				sb.append("        		"
						+ getWrapTypeName(mapProperty.getValueType())
						+ " value = new "
						+ mapProperty.getValueType().getTypeName() + "( );");
				sb.append(NEWLINE);
				sb.append("        		value.readFromBuffer( buffer );");
				sb.append(NEWLINE);
			}

			sb.append("        		" + property.getVarName()
					+ ".Add( key, value ); ");
			sb.append(NEWLINE);
			sb.append("    			}");
			sb.append(NEWLINE);
			sb.append("		}");

			return sb.toString();
		} else {

			return property.getVarName() + " = buffer."
					+ getReadString(property.getTypeName()) + "( );";

		}
	}

	public String getWriterString(IProperty property) {

		if (property.getType() == IProperty.PropertyType.USERTYPE) {

			StringBuilder sb = new StringBuilder();
			sb.append("if( " + property.getVarName() + " != null ){");
			sb.append(NEWLINE);
			sb.append("			buffer.Write( true );");
			sb.append(NEWLINE);
			sb.append("			" + property.getVarName()
					+ ".writeToBuffer( buffer );");
			sb.append(NEWLINE);
			sb.append("		}else{");
			sb.append(NEWLINE);
			sb.append("			buffer.Write( false );");
			sb.append(NEWLINE);
			sb.append("		}");
			sb.append(NEWLINE);
			return sb.toString();

		} else if (property.getType() == IProperty.PropertyType.LIST) {

			ListProperty listProperty = (ListProperty) property;

			StringBuilder sb = new StringBuilder();
			sb.append("{");
			sb.append(NEWLINE);
			sb.append("    		int size = " + property.getVarName() + ".Length;");
			sb.append(NEWLINE);
			sb.append("	    	buffer.Write( size ); ");
			sb.append(NEWLINE);
			sb.append("    		for( int i = 0; i < size; i++ ){");
			sb.append(NEWLINE);
			if (listProperty.getElementType().getType() == IProperty.PropertyType.BASIC) {
				sb.append("	       		buffer."
						+ getWriteString(listProperty.getElementType()
								.getTypeName()) + "( " + property.getVarName()
						+ "[i]); ");
				sb.append(NEWLINE);
			} else {
				sb.append("        		"
						+ listProperty.getElementType().getTypeName()
						+ " model = " + property.getVarName() + "[i];");
				sb.append(NEWLINE);
				sb.append("        		model.writeToBuffer( buffer );");
				sb.append(NEWLINE);
			}
			sb.append("    		}");
			sb.append(NEWLINE);
			sb.append("		}");
			return sb.toString();

		} else if (property.getType() == IProperty.PropertyType.MAP) {

			MapProperty mapProperty = (MapProperty) property;

			StringBuilder sb = new StringBuilder();
			sb.append("{");
			sb.append(NEWLINE);
			sb.append("    Set<" + getWrapTypeName(mapProperty.getKeyType())
					+ "> keySet = " + property.getVarName() + ".keySet();");
			sb.append(NEWLINE);
			sb.append("    buffer.writeInt( keySet.size( ) );");
			sb.append(NEWLINE);
			sb.append("    for( " + getTypeName(mapProperty.getKeyType())
					+ " key  : keySet ){");
			sb.append(NEWLINE);
			if (mapProperty.getKeyType().getType() == IProperty.PropertyType.BASIC) {
				sb.append("        buffer."
						+ getWriteString(mapProperty.getKeyType().getTypeName())
						+ "(  key );");
			} else {
				sb.append("        key.writeToBuffer( buffer );");
			}
			sb.append(NEWLINE);

			if (mapProperty.getValueType().getType() == IProperty.PropertyType.BASIC) {
				sb.append("        buffer."
						+ getWriteString(mapProperty.getValueType()
								.getTypeName()) + "( "
						+ mapProperty.getVarName() + ".[i]);");
			} else {
				sb.append("        " + mapProperty.getVarName()
						+ ".get( key ).writeToBuffer( buffer );");
			}
			sb.append(NEWLINE);

			sb.append("    }");
			sb.append(NEWLINE);

			sb.append("}");

			return sb.toString();

		} else {

			return "buffer." + getWriteString(property.getTypeName()) + "( "
					+ property.getVarName() + ");";
		}
	}
}
