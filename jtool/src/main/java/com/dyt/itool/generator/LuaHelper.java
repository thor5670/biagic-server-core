package com.dyt.itool.generator;

import com.dyt.itool.model.IProperty;
import com.dyt.itool.model.impl.BasicProperty;
import com.dyt.itool.model.impl.ListProperty;


public class LuaHelper {
	
	
	public static final String NEWLINE = "\r";

	public String getDefaultValue( IProperty property ){
		
		if( property.getType() == IProperty.PropertyType.BASIC )
		{
			BasicProperty basicProperty = ( BasicProperty)property;
			if( "string".equalsIgnoreCase( basicProperty.getTypeName() ) )
			{
				return "\"\"";
			}
			else if( "bool".equalsIgnoreCase( basicProperty.getTypeName() ) )
			{
				return "NO";
			}
			else{
				return "0";
			}
		}
		else if( property.getType() == IProperty.PropertyType.USERTYPE ){
			return property.getTypeName() + ":create()";
		}else{
			return "{ }";
		}
		
	}
	
	public String getReadString( IProperty property ){
		
		if( property.getType() == IProperty.PropertyType.USERTYPE ){
			
			
			StringBuilder sb = new StringBuilder( );
			sb.append( "if buffer:readBool( )  then");
			sb.append( NEWLINE );
			sb.append( "			self." + property.getVarName() + ":readFromBuffer( buffer )" );
			sb.append( NEWLINE );
			sb.append( "	else" );
			sb.append( NEWLINE );
			sb.append( "			self." + property.getVarName() + " = nil" );
			sb.append( NEWLINE );
			sb.append( "	end");
			sb.append( NEWLINE );
			return sb.toString();
			
			
		}else if( property.getType() == IProperty.PropertyType.LIST ){
			
			ListProperty listProperty = (ListProperty)property;
			
			StringBuilder sb = new StringBuilder( );
			sb.append( "do");
			sb.append( NEWLINE );
			sb.append("    		local size = buffer:readInt()" );
			sb.append( NEWLINE );
			sb.append("    		for i = 1, size do" );
			sb.append( NEWLINE );
			if( listProperty.getElementType().getType() == IProperty.PropertyType.BASIC  ){
				sb.append("	       		table.insert( self." + property.getVarName() + ",  buffer:"  +  getReadString( listProperty.getElementType( ).getTypeName( ) ) + "() );" );
			}else{
				sb.append("        		local model = " + listProperty.getElementType().getTypeName() + ":create() " );
				sb.append( NEWLINE );
				sb.append("        		model:readFromBuffer( buffer );" );
				sb.append( NEWLINE );
				sb.append("        		table.insert( self."  +  property.getVarName() + ", model ) " );
			}
			sb.append( NEWLINE );
			sb.append("    		end" );
			sb.append( NEWLINE );
			sb.append("	end" );
			return sb.toString( );
		
		}else{
			
			return "self." + property.getVarName() + " = buffer:" + getReadString( property.getTypeName( ) ) + "( )"; 
			
		}
		
	}
	
	
	public  String getWriteString( IProperty property ){
		
		
		if( property.getType() == IProperty.PropertyType.USERTYPE ){
			
			StringBuilder sb = new StringBuilder( );
			sb.append( "if self." + property.getVarName()+ " ~= nil  then");
			sb.append( NEWLINE );
			sb.append( "			buffer:writeBool( true )"  );
			sb.append( NEWLINE );
			sb.append( "			self." + property.getVarName() + ":writeToBuffer( buffer )" );
			sb.append( NEWLINE );
			sb.append( "	else" );
			sb.append( NEWLINE );
			sb.append( "			buffer:writeBool( false );" );
			sb.append( NEWLINE );
			sb.append( "	end");
			sb.append( NEWLINE );
			return sb.toString();
			
		}else if( property.getType() == IProperty.PropertyType.LIST ){
			
			ListProperty listProperty = (ListProperty)property;
			
			StringBuilder sb = new StringBuilder( );
			sb.append( "do");
			sb.append( NEWLINE );
			sb.append("    		local size = table.getn( self." + property.getVarName() + " ) " );
			sb.append( NEWLINE );
			sb.append("    		buffer:writeInt( size ) " );
			sb.append( NEWLINE );
			sb.append("    		for  i = 1, size do" );
			sb.append( NEWLINE );
			if( listProperty.getElementType().getType() == IProperty.PropertyType.BASIC  ){
				sb.append("	       		buffer:" + getWriteString( listProperty.getElementType().getTypeName( ) ) + "( self." + property.getVarName() + "[ i ] ) " );
				sb.append( NEWLINE );
			}else{
				sb.append("        		local  model = self." + property.getVarName() + "[ i ] " );
				sb.append( NEWLINE );
				sb.append("        		model:writeToBuffer( buffer )" );
				sb.append( NEWLINE );
			}
			sb.append("    		end" );
			sb.append( NEWLINE );
			sb.append("	end" );
			return sb.toString( );
			
		}else{
			
			return  "buffer:" + getWriteString( property.getTypeName( ) ) + "( self." + property.getVarName() + " )"; 
		}
	}
	


	private String getReadString( String basicType ){
		return getIoString( "read", basicType );
	}
	
	private String getWriteString( String basicType ){
		return getIoString( "write", basicType );
	}
	
	
	public String getIoString( String suffix, String basicType ){
		if( "string".equals( basicType ) ){
			suffix += "UTF8";
		}
		else {
			char[] chars =  basicType.toCharArray();
			chars[ 0 ] = Character.toUpperCase( chars[ 0 ] );
			suffix += new String( chars );
		}
		return suffix;
	}
	
	
	
	/*
	public String getVarnameList( Struct model ){
		StringBuffer buffer = new StringBuffer();
		
		for( int i = 0; i < model.getPropertyList().size(); i++ ){
			
			IProperty p = model.getPropertyList().get( i );
			buffer.append( "self." + p.getVarName()  );
			
			if( i < model.getPropertyList().size() - 1 ){
				buffer.append( "," );
			}
			
		}
		
		return buffer.toString( );
	}
	
	public String getDecodeFMT( Struct model ){
		StringBuffer buffer = new StringBuffer();
		
		for( IProperty p : model.getPropertyList() ){
			buffer.append( getPropertyDecodeFMT( p ) );
		}
		
		return buffer.toString( );
	}
	
	
	public String getPropertyDecodeFMT( IProperty p ){
		if( p.getType( ) != IProperty.PropertyType.BASIC ){
			throw new RuntimeException( "lua property type error" );
		}
		
		if( "string".equalsIgnoreCase( p.getTypeName() ) )
		{
			return ">s";
		}
		else if( "bool".equalsIgnoreCase( p.getTypeName() ) )
		{
			return ">b";
		}
		else{
			return ">i";
		}
	}
	
	*/
	
}
