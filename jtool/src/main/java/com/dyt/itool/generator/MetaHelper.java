package com.dyt.itool.generator;

import com.dyt.itool.model.IProperty;
import com.dyt.itool.model.Struct;

import java.util.*;

public class MetaHelper {

    private Map<String, List<String>> structIdsMap = new HashMap<String, List<String>>();
    private Map<String, String> structDictNamesMap = new HashMap<String, String>();

    public void addStructMetaInfo(Struct struct) {

        String name = null;
        String dictName = null;
        List<String> ids = new ArrayList<String>();
        for (IProperty property : struct.getPropertyList()) {

            if (property.getVarName().equals("excel")) {
                name = property.getValue().toString().trim();
            } else if (property.getVarName().startsWith("id")) {
                String value = property.getValue().toString();
                if (value.trim().length() > 0) {
                    ids.add(value);
                }
            } else if (property.getVarName().equals("dictName")) {
                dictName = property.getValue().toString();
            }
        }

        if (name != null && ids.size() > 0) {
            structIdsMap.put(name, ids);
        }
        if (name != null && dictName.trim().length() > 0) {
            structDictNamesMap.put(name, dictName);
        }

    }


    public boolean containsType(String excelName) {
        return structDictNamesMap.containsKey(excelName);
    }

    public Map<String, String> getDictNames() {
        return structDictNamesMap;
    }

    public Set<String> getDictNamesSet() {
        Set<String> set = new HashSet<>();
        set.addAll(structDictNamesMap.values());
        return set;
    }
    public String getString(String str){
        return str;
    }

    public boolean containsId(String excelName) {
        return structIdsMap.containsKey(excelName);
    }

    public String getIds(String excelName) {
        List<String> id_list = structIdsMap.get(excelName);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < id_list.size(); i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(id_list.get(i).split(" ")[1]);
        }
        return sb.toString();
    }

    public String getDictIds(String excelName) {
        List<String> id_list = structIdsMap.get(excelName);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < id_list.size(); i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append("dict." + id_list.get(i).split(" ")[1]);
        }
        return sb.toString();
    }

    public List<String> getIdList(String excelName) {
        List<String> id_list = new ArrayList<String>();
        for (String idAndType : structIdsMap.get(excelName)) {
            id_list.add(idAndType.split(" ")[1]);
        }
        return id_list;
    }

    public String getIdAndTypes(String excelName) {
        List<String> id_list = structIdsMap.get(excelName);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < id_list.size(); i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(id_list.get(i));
        }
        return sb.toString();
    }

}
