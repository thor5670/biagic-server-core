package com.dyt.itool.model;

public class HttpCommand extends Struct{

	protected String commandId;
	protected String moduleId;
	protected String moduleName;
	
	protected Struct request = new Struct("reqeust");
	protected Struct response = new Struct("response");
	
	
	public HttpCommand( String moduleId, String moduleName, String commandId, String commandName  ){
		super( commandName );
		
		this.commandId = commandId;
		
		this.moduleId = moduleId;
		this.moduleName = moduleName;
	}

	public String getCommandId() {
		return commandId;
	}

	
	public String getCommandName( ){
		return structName;
	}
	
	public String getModuleId(){
		return moduleId;
	}
	
	public String getModuleName() {
		return moduleName;
	}
	
	public Struct getRequest( ){
		return request;
	}
	
	public Struct getResponse( ){
		return response;
	}
	
	
}
