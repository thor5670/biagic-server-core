package com.dyt.itool.model;

public interface IProperty {
	
	public enum PropertyType
	{
		BASIC,
		LIST,
		MAP,
		USERTYPE
	}
	
	 public static final String  BOOL 	= "bool";
	 public static final String  CHAR 	= "char";
	 
	 public static final String  BYTE 	= "byte";
	 public static final String  SHORT 	= "short";
	 public static final String  INT 	= "int";
	 public static final String  LONG 	= "long";
	 
	 public static final String  FLOAT = "float";
	 
	 public static final String  STRING = "string";
	 public static final String  BYTES 	= "bytes";
	 public static final String  LIST 	= "list";
	 public static final String MAP = "map";
	
	/***
	 * @return 
	 */
	PropertyType getType( );
	
	/***
	 * @return 属性类型名称
	 */
	String getTypeName( );
	
	/***
	 * @return 变量命名
	 */
	String getVarName( );
	
	void setValue( Object value );
	/***
	 * @return 变量值
	 */
	Object getValue( );


	/**
	 * 是否是可选的
	 * @return
	 */
	boolean isOptinal( );


	/**
	 * 克隆
	 * @return
	 */
	IProperty clone( );

}
