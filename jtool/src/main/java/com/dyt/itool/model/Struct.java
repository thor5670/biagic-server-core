package com.dyt.itool.model;


import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Struct {

    protected String pkgName;

    protected String structName;

    protected List<IProperty> propertyList = new ArrayList<IProperty>();


    /*
     * 父结构体，如果没有为null
     */
    protected Struct supperStruct = null;

    public Struct(String structName) {
        this.structName = structName;
    }

    public Struct clone() {
        Struct struct = new Struct(structName);

        for (int i = 0; i < propertyList.size(); i++) {

            IProperty prop = propertyList.get(i);

            if (prop == null) {
                throw new RuntimeException("struct " + structName + " index= " +i+", prop is null ");
            }

            struct.propertyList.add(prop.clone());
        }

        return struct;
    }

    public void addProperty(IProperty property) {
        this.propertyList.add(property);
    }


    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }


    public String getPkgName() {
        return pkgName;
    }

    public String getStructName() {
        return structName;
    }

    public List<IProperty> getPropertyList() {
        return propertyList;
    }

    /**
     * 是否有父类
     * @return
     */
    public boolean hasSupperStruct(){
        return supperStruct != null;
    }

    /**
     * 获得不在父类中的属性
     * @return
     */
    public List<IProperty> getPropertyListNotInSupper() {
        if( !hasSupperStruct() ){
            return this.propertyList;
        }

        List<IProperty> properties = new ArrayList<>( );
        for( IProperty property : propertyList ){
            if( !supperStruct.containsProperty( property.getVarName() ) ){
                properties.add( property );
            }
        }
        return properties;
    }

    /**
     * 是否包含指定名称的属性
     * @param propName
     * @return
     */
    private boolean containsProperty( String propName ){

        for( IProperty property : propertyList ){
            if( property.getVarName().equals( propName ) ){
                return true;
            }
        }

        return false;
    }


    /**
     * 获取父类结构体
     * @return
     */
    public Struct getSupperStruct() {
        return supperStruct;
    }

    /**
     * 获取父类结构体
     * @param supperStruct
     */
    public void setSupperStruct(Struct supperStruct) {
        this.supperStruct = supperStruct;
    }


    @Override
    public String toString() {
        return structName;
    }

    public String getMd5() {
        StringBuffer struct = new StringBuffer();

        for (IProperty property : propertyList) {
            struct.append(property.getTypeName()).append(property.getVarName());
        }

        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(struct.toString().getBytes());
            return new BigInteger(1, digest.digest()).toString(16);

        } catch (NoSuchAlgorithmException e) {
            System.out.println("没有找到相应的加密算法,加密失败");
            System.exit(0);
            return null;
        }
    }

}
