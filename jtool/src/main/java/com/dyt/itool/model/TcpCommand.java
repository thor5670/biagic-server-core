package com.dyt.itool.model;


public class TcpCommand extends Struct{
	public static final String TYPE_CS = "cs";
	public static final String TYPE_SC =  "sc";
	
	protected String commandId;
	protected String commandType;
	protected String moduleId;
	protected String moduleName;
	
	public TcpCommand( String moduleId, String moduleName, String commandId, String commandName , String commandType ){
		super( commandName );
		this.commandId = commandId;
		this.commandType = commandType;
		
		this.moduleId = moduleId;
		this.moduleName = moduleName;
	}

	public String getCommandId() {
		return commandId;
	}

	public String getCommandName( ){
		return structName;
	}
	
	public String getCommandType() {
		return commandType;
	}
	
	public String getModuleId( ){
		return moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}
	
	
}
