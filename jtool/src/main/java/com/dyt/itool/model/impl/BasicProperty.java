package com.dyt.itool.model.impl;

import com.dyt.itool.model.IProperty;

public class BasicProperty implements IProperty{


	protected String typeName;
	protected String varName;
	protected PropertyType type;
	protected Object value;

	/**
	 * 是否是可选的, 默认每个属性都是必须的
	 */
	protected boolean optinal = false ;
	
	public BasicProperty( String typeName, String varName )
	{
		this.typeName = typeName;
		this.varName = varName;
		this.type = PropertyType.BASIC;
	}
	
	public BasicProperty( String typeName, String varName, Object value )
	{
		this.typeName = typeName;
		this.varName = varName;
		this.value = value;
		
		this.type = PropertyType.BASIC;
	}
	
	public PropertyType getType() {
		return type;
	}

	public String getTypeName() {
		return typeName;
	}

	public String getVarName() {
		return varName;
	}
	
	public void setValue( Object value ){
		this.value = value;
	}
	
	public Object getValue( ){
		return value;
	}
	
	public IProperty clone( ){
		return new BasicProperty( typeName, varName );
	}

	public void setOptinal(boolean optinal) {
		this.optinal = optinal;
	}

	public boolean isOptinal() {
		return optinal;
	}

}
