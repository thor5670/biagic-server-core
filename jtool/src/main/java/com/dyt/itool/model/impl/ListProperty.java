package com.dyt.itool.model.impl;

import com.dyt.itool.model.IProperty;


public class ListProperty extends BasicProperty {
	
	protected IProperty elementType;
	
	public ListProperty( String typeName, String varName, IProperty elementType )
	{
		super( typeName, varName );
		
		this.elementType = elementType;
		this.type = PropertyType.LIST;
		
	}
	
	public IProperty getElementType( ){
		return elementType;
	}
	
	public String getTypeName( ){
		return typeName + "<" + elementType.getTypeName( ) + ">" ;
	}
	
	
	public IProperty clone( ){
		return new ListProperty( typeName, varName, elementType );
	}
	
}
