package com.dyt.itool.model.impl;

import com.dyt.itool.model.IProperty;

public class MapProperty extends BasicProperty {

	protected IProperty keyType;
	protected IProperty valueType;

	public MapProperty(String typeName, String varName, IProperty keyType,
			IProperty valueType) {
		super(typeName, varName);

		this.keyType = keyType;
		this.valueType = valueType;
		this.type = PropertyType.MAP;
	}

	public String getTypeName() {
		return typeName + "<" + keyType.getTypeName() + ","
				+ valueType.getTypeName() + ">";
	}

	public IProperty getKeyType() {
		return keyType;
	}

	public IProperty getValueType() {
		return valueType;
	}

	public IProperty clone() {
		return new MapProperty(typeName, varName, keyType, valueType);
	}

}
