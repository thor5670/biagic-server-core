package com.dyt.itool.model.impl;

import com.dyt.itool.model.IProperty;
import com.dyt.itool.model.Struct;

public class StructProperty extends BasicProperty {

	protected Struct struct;
	
	
	public StructProperty( Struct struct, String varName ){
		super( struct.getStructName(), varName );
		
		this.struct = struct;
		this.type = PropertyType.USERTYPE;
	}
	
	public Struct getStruct( ){
		return struct;
	}

	public IProperty clone( ){
		return new StructProperty( struct, varName );
	}
}
