package com.dyt.itool.parser;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractParser {
	
	protected Map<String,String> basicTypeMap = new HashMap<String,String >( );
	
	public AbstractParser( ){
		initBasicType( );
	}
	
	public abstract void  parse( String filename );
	
	
	private void initBasicType( ){
		
		basicTypeMap.put( "bool", "bool" );
		basicTypeMap.put( "byte", "byte" );
		basicTypeMap.put( "sbyte", "byte" ); //sbyte -> byte
		basicTypeMap.put( "short", "short" );
		
		basicTypeMap.put( "int", "int" );
		basicTypeMap.put( "long", "long" );
		basicTypeMap.put( "float", "float" );
		basicTypeMap.put( "double", "double" );
		
		basicTypeMap.put("string", "string" );
	}
	
	

}
