package com.dyt.itool.parser;

import java.io.*;
import java.util.*;

import com.dyt.itool.generator.VelocityUtils;
import com.dyt.itool.model.*;
import com.dyt.itool.model.impl.BasicProperty;


public class Cocos2dResParser {
	
	private List< IProperty > fileNames = new ArrayList<IProperty>( );
	private List< IProperty > spriteFrameNames = new ArrayList<IProperty>( );
	

	public void parse(  String dirName , String prefix ){
		parsePlist( dirName );
		parseFileNames( dirName, prefix );
	}
	   @SuppressWarnings("unchecked")
	public void parsePlist( String dirName ){
		File dir = new File( dirName );
		if( !dir.exists( ) || !dir.isDirectory() )
			return;
		
	   File[] files = dir.listFiles( );
	   for( File f : files ){
		   if( f.isFile( ) && f.getName().endsWith( ".plist" ) && !f.getAbsolutePath().contains( "particle") ) {

			HashMap<String, Object> dict  = ( HashMap<String,Object> )( PlistParser.parsePlist( f.getAbsolutePath( ) ) );
			   if( dict != null )
			   {
				   HashMap<String,Object> frames = (HashMap<String,Object>) dict.get( "frames" );
				   for( String key : frames.keySet() ){
					  
					   String varName = transforFilePath( key );
					   BasicProperty p = new BasicProperty( "string", varName );
					   p.setValue( key );
					   spriteFrameNames.add( p );
				   }
			   }
		   }
	   }
	   
	   for( File f : files ) {
		   
		   if( f.isDirectory( )  && !f.getName().startsWith( "." ) ){
			   parsePlist( f.getAbsolutePath() );
		   }
	   }
	   
	}
	
	public void parseFileNames(  String dirName , String prefix ){
		File dir = new File( dirName );
		if( !dir.exists( ) || !dir.isDirectory() )
			return;
		
	   File[] files = dir.listFiles( );
	   for( File f : files ){
		   
		   if( f.isFile( ) && !f.getName().startsWith( "." ) ) {
			   
			   String resPath = prefix + f.getName( );
			   String resName = transforFilePath( resPath );
			   
			   BasicProperty property = new BasicProperty( "string", resName );
			   property.setValue( resPath );
			   fileNames.add( property  );
		   }
	   }
		
	   for( File f : files ) {
		   
		   if( f.isDirectory( )  && !f.getName().startsWith( "." ) ){
			   
			   parseFileNames( f.getAbsolutePath( ) , getNewPrefix( prefix , f.getName( ) ) );
		   }
	   }
	}
	
	
	public void generateCode(  Map<String,String> templateOutputMap  )
	{
		
		for( String templateFile : templateOutputMap.keySet() ){
			
			String outputFile = templateOutputMap.get( templateFile );
			
			try{
				
				Map<String, Object> dataMap = new HashMap<String,Object >( );
				dataMap.put( "fileNameList", fileNames );
				dataMap.put( "spriteFrameList", spriteFrameNames );
				
				VelocityUtils.generateFromVelocity( templateFile , outputFile,  dataMap );
				
				Writer standOut = new OutputStreamWriter( System.out);
				VelocityUtils.generateFromVelocity( templateFile, standOut, dataMap );
				standOut.flush();
				
			}catch( Exception e ){
				e.printStackTrace();
			}
		}
	}

	
	private String transforFilePath( String filePath ){
		return filePath.replaceAll( "/", "_" ).replaceAll( "\\.", "_" );
	}
	
	private String getNewPrefix( String prefix, String appendDir ){
	
			return prefix  + appendDir + "/";
	}

}
