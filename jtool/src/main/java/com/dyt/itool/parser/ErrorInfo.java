package com.dyt.itool.parser;

public class ErrorInfo {
	
	private String errorid ;
	private String name;
	private String desc;
	
	
	public String getErrorid() {
		return errorid;
	}
	
	public void setErrorid(String errorid) {
		this.errorid = errorid;
	}

	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
