# hotfix 热更新模块说明

## 配置
	1. 将jdk tools.jar添加到maven本地仓库
		mvn install:install-file -Dfile=/Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home/lib/tools.jar -DgroupId=com.sun -DartifactId=tools -Dversion=1.8.0 -Dpackaging=jar
		
	2. 使用hotfix功能的地方依赖rts-hotfix模块
		
	2. 项目入口pom文件中指定代理类入口, Agent-Class
		<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<archive>
						<manifest>
							<addClasspath>true</addClasspath>
						</manifest>
						<manifestEntries>
							<Can-Redefine-Classes>
								true
							</Can-Redefine-Classes>
							<Premain-Class>
								com.rtsapp.agent.AgentLoader
							</Premain-Class>
							<Agent-Class>
								com.rtsapp.agent.AgentLoader
							</Agent-Class>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>
			

		
		
## 热修复功能使用说明
    

## 下一阶段准备添加的功能
    

    1. 热修复执行完成后, 将更新失败的文件用日志输出出来 (已完成)


# 目前处于开发阶段, 功能随时会有变动.  如果项目中需要使用, 请与李旺沟通


## 使用 

1.  执行一次更新,更新指定文件:
    参数输入指定类:  path/path/path/file.class
    参数输入包名:  path/path/path* --- 注意以*号结尾
    
	