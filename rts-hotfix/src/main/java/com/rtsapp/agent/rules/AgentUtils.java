package com.rtsapp.agent.rules;

/**
 * Created by liwang on 17-3-23.
 */
public final class AgentUtils {

    public static <T> boolean isEmpty( T[] ts ) {
        return ts == null || ts.length <= 0;
    }

    public static boolean isEmpty( byte[] bytes ) {
        return bytes == null || bytes.length <= 0;
    }

    public static boolean isEmpty( String s ) {
        return s == null || s.trim( ).isEmpty( );
    }


}
