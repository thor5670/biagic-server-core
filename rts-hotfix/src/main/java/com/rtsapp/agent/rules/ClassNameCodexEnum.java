package com.rtsapp.agent.rules;

/**
 * Created by liwang on 17-3-21.
 */
public enum ClassNameCodexEnum {

    ALL( "更新所有.class", 1 ),
    BY_NAMES( "更新指定名字的.class (包含文件后缀)", 2 ),
    BY_PACKAGES( "更新指定包下的所有.class", 3 ),;

    private String desc;
    private int id;


    ClassNameCodexEnum( String desc, int id ) {
        this.desc = desc;
        this.id = id;
    }

    /**
     * 获取 hotfix规则的描述
     * <p>
     * 例如: 在控制台输入hotfix规则, 方便手动更新时选择
     *
     * @return
     */
    public static String getInstruction( ) {

        StringBuilder instruction = new StringBuilder( "\n" );

        for ( ClassNameCodexEnum e : ClassNameCodexEnum.values( ) ) {
            instruction.append( e.getId( ) ).append( " :  " ).append( e.getDesc( ) );
        }
        return instruction.append( "\n" ).toString( );
    }


    public String getDesc( ) {
        return desc;
    }

    public int getId( ) {
        return id;
    }

}
