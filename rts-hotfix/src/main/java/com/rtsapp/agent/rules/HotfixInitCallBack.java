package com.rtsapp.agent.rules;

/**
 * Created by liwang on 17-3-31.
 */
public interface HotfixInitCallBack {

    void callback( boolean initSuccess );

}
