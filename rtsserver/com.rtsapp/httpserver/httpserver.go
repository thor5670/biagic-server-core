package httpserver

import (
	"fmt"
	"net/http"
)


type HttpServer struct{
	port string
	routers map[string]HttpRouter
}

type HttpRouter interface{
	ServeHTTP(http.ResponseWriter, *http.Request)
}

func ( s *HttpServer) UrlMapping( url string, r HttpRouter ) {
	s.routers[ url ] = r
}


func ( s *HttpServer) Start() error{
	http.Handle( "/static/", http.StripPrefix("/static/", http.FileServer( http.Dir("./static" ) ) ) )
	for key, value := range s.routers {
		http.Handle( key, value )
	}
	ipport := fmt.Sprintf("0.0.0.0:%s", s.port )
	fmt.Println( ipport )
	return http.ListenAndServe( ipport, nil )
}




func NewHttpServer( port string ) (*HttpServer) {
	return &HttpServer{ port, make(map[string]HttpRouter) }
}

