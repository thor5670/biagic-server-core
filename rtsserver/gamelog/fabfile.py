#coding:utf-8
from fabric.api import *

import sys;


def mvn_clean_install():
	local("pwd")
	local("mvn clean install")
	local("mvn install")


def mvn_install_projects( projectdirs ):
	for pdir in projectdirs :
		with lcd( pdir ):
			mvn_clean_install()


@task
def mvn_gamelog():
	projects = (
      "../rtsserver",
      "gamelog-api", 
      "gamelog-cmd",
      "gamelog-client",
      "gamelog-server",
      "gamelog-analyze", )
	mvn_install_projects( projects )
	print('ok')

