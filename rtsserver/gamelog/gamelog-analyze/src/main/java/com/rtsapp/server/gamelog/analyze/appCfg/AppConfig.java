package com.rtsapp.server.gamelog.analyze.appCfg;

import java.util.Properties;

/**
 * Created by admin on 16-11-17.
 */
public class AppConfig {
    private static final AppConfig appConfig = new AppConfig();
    private static PropertyCfg cfg;
    private static Properties properties;
    public static AppConfig getInstance() {
        return appConfig;
    }
    static {
        cfg = new PropertyCfg("cfg/app.properties");
    }

    public String getItemId() {
        return cfg.getString("itemId");
    }
}
