package com.rtsapp.server.gamelog.analyze.appCfg;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * Created by admin on 16-11-17.
 */
public class PropertyCfg {
    private Properties properties;

    public PropertyCfg(String stream) {
        properties = new Properties();
        try {
            properties.load(new InputStreamReader(new FileInputStream(stream), "UTF-8"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getString(String key) {
        return properties.getProperty(key);
    }
}
