package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.ActionDao;
import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.stat.ChannelUtils;
import com.rtsapp.server.gamelog.analyze.stat.DateUtils;
import com.rtsapp.server.gamelog.analyze.stat.action.ActionParam;
import com.rtsapp.server.gamelog.analyze.stat.action.ActionParams;
import com.rtsapp.server.gamelog.analyze.stat.action.ActionTypeInfo;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by admin on 16-11-1.
 */
@Controller
@RequestMapping("/action/")
public class ActionController {

    @Autowired
    private ActionDao actionDao;
    @Autowired
    private CommonDao commonDao;

    @RequestMapping("index")
    public ModelAndView index() {

        List<ActionParams> actionTypeInfos = actionDao.selectActionParams();

        ModelAndView modelAndView = new ModelAndView("action/action_index");

        modelAndView.addObject("actionTypes", actionTypeInfos);

        return modelAndView;
    }

    @RequestMapping("search")
    public ModelAndView search(HttpServletRequest request) throws ParseException {
        ModelAndView andView = new ModelAndView("action/action_search");

        ParamServerChannelDate param = ParamUtils.parseServerChannelDate(request, commonDao);
        if (!ChannelUtils.isChannelsValid(param.getChannelNames())) {
            return andView;
        }

        String[] serverIds = request.getParameterValues("serverIds");
        String[] actionTypes = request.getParameterValues("actionTypes");
        String startDay = request.getParameter("startDay");
        String endDay = request.getParameter("endDay");

        Timestamp startTimestamp = null;
        Timestamp endTimestamp = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if (startDay.trim().length() > 0) {
            startTimestamp = new Timestamp(sdf.parse(startDay).getTime());
        }
        if (endDay.trim().length() > 0) {
            endTimestamp = new Timestamp(sdf.parse(endDay).getTime() + DateUtils.DAY_IN_MILLIS);
        }

        ActionParam actionParam = new ActionParam(serverIds, param.getChannelNames(), actionTypes, startTimestamp, endTimestamp);
        //开始日期前总的注册人数
        int totalRegisterCount = actionDao.selectRoleInit(param);

        List<ActionParams> actionParamses = actionDao.selectActionParam(actionParam);

        List<ActionTypeInfo> actionTypeInfos = getActionInfos(totalRegisterCount, actionParamses);

        if (actionTypeInfos != null && actionTypeInfos.size() > 0) {
            andView.addObject("actionTypeInfos", actionTypeInfos);
        }

        return andView;
    }

    private List<ActionTypeInfo> getActionInfos(int totalRegisterCount, List<ActionParams> actionParamses) {
        List<ActionTypeInfo> actionTypeInfos = new ArrayList<>();
        for (ActionParams params : actionParamses) {
            ActionTypeInfo actionTypeInfo = new ActionTypeInfo();
            actionTypeInfo.setActionParam(params.getActionParam());

            if (params.getUserId() == null || "".equals(params.getUserId())) {
                params.setUserId("0");
            }

            actionTypeInfo.setUserNum(String.valueOf(totalRegisterCount));
            actionTypeInfo.setFinishNum(params.getUserId());

            if (!"0".equals(params.getUserId()) && totalRegisterCount != 0) {
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                float v = Float.parseFloat(params.getUserId()) / (float)totalRegisterCount * 100;
                actionTypeInfo.setFinishRate(decimalFormat.format(v) + "%");
            } else {
                actionTypeInfo.setFinishNum("0");
                actionTypeInfo.setFinishRate("NaN%");
            }

            actionTypeInfos.add(actionTypeInfo);
        }
        return actionTypeInfos;
    }
}
