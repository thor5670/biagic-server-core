package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.ActivityDao;
import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.stat.ChannelUtils;
import com.rtsapp.server.gamelog.analyze.stat.DateUtils;
import com.rtsapp.server.gamelog.analyze.stat.activity.ActivityParam;
import com.rtsapp.server.gamelog.analyze.stat.activity.ActivityTypeInfo;
import com.rtsapp.server.gamelog.analyze.stat.activity.ActivityViewData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
@RequestMapping( "/activity/")
public class ActivityController {

    @Autowired
    private ActivityDao activityDao;
    @Autowired
    private CommonDao commonDao;


    @RequestMapping( "search" )
    public ModelAndView search( HttpServletRequest request ) {

        ModelAndView mav = new ModelAndView( "activity/activity_search" );

        String[] channelNames = ChannelUtils.getChannels( commonDao, request, "channelNames" );
        if ( !ChannelUtils.isChannelsValid( channelNames ) ) {
            return mav;
        }

        String[] serverIds = request.getParameterValues( "serverIds" );
        String[] activityTypes = request.getParameterValues( "activityTypes" );
        String startDay = request.getParameter( "startDay" );
        String endDay = request.getParameter( "endDay" );


        Timestamp startTimestamp = null;
        Timestamp endTimestamp = null;
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );

        try {
            if ( startDay.trim( ).length( ) > 0 ) {
                startTimestamp = new Timestamp( sdf.parse( startDay ).getTime( ) );
            }
            if ( endDay.trim( ).length( ) > 0 ) {
                endTimestamp = new Timestamp( sdf.parse( endDay ).getTime( ) + DateUtils.DAY_IN_MILLIS );
            }

        } catch ( ParseException e ) {
            e.printStackTrace( );
        }

//        if ( channelNames == null ) {
//            channelNames = new String[]{ "1", "3" };
//        }

        ActivityParam param = new ActivityParam( activityTypes, startTimestamp, endTimestamp, serverIds, channelNames );

        List<ActivityViewData> list = activityDao.selectByTime( param );

        mav.addObject( "list", list );

        return mav;
    }

    @RequestMapping( "index" )
    public ModelAndView index( ) {

        List<ActivityTypeInfo> activityTypes = activityDao.selectActivityTypeInfo( );

        ModelAndView modelAndView = new ModelAndView( "activity/activity_index" );

        modelAndView.addObject( "activityTypes", activityTypes );

        return modelAndView;
    }


}
