package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.stat.ChannelUtils;
import com.rtsapp.server.gamelog.analyze.stat.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/common/")
public class CommonController {

    @Autowired
    private CommonDao dao;

    @RequestMapping("param")
    public ModelAndView param(HttpServletRequest request) {

//        List<ServerInfo> serverList = new ArrayList<ServerInfo>();
//        serverList.add( new ServerInfo( "1", "S1-百战百胜" ) );
//        serverList.add( new ServerInfo( "2", "S2-百战百胜" ) );

        List<Integer> serverList = dao.selectServerIds();

//        List<String> channelList = dao.selectChannelNames( );
        List<String> channelList = ChannelUtils.getChannelsByUserName(dao, request);
        if (channelList == null || channelList.isEmpty()) {
            channelList = dao.selectChannelNames();
        }


        String startDay = DateUtils.convertDateToString(new Date());


        ModelAndView mav = new ModelAndView("param");
        mav.addObject("serverList", serverList);
        mav.addObject("channelList", channelList);
        mav.addObject("startDay", startDay);
        mav.addObject("endDay", startDay);

        return mav;
    }


}
