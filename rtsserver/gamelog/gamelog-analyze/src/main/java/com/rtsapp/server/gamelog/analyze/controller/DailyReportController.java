package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.dao.ReportDao;
import com.rtsapp.server.gamelog.analyze.stat.ChannelUtils;
import com.rtsapp.server.gamelog.analyze.stat.DateUtils;
import com.rtsapp.server.gamelog.analyze.stat.daily.*;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;


@Controller
@RequestMapping("/daily/")
public class DailyReportController {

    @Autowired
    private ReportDao reportDao;
    @Autowired
    private CommonDao commonDao;

    @RequestMapping( "index" )
    public String index(){
        return "daily/daily_index";
    }


    @RequestMapping( "search" )
    public ModelAndView search( HttpServletRequest request ) {
        ModelAndView mav = new ModelAndView( "daily/daily_search" );

        ParamServerChannelDate param = ParamUtils.parseServerChannelDate( request, commonDao );
        if ( !ChannelUtils.isChannelsValid( param.getChannelNames( ) ) ) {
            return mav;
        }

        Date startDay = null;
        if ( param.getStartTime( ) == null ) {
            startDay = new Date( DateUtils.getDayStartTime_MS( System.currentTimeMillis( ) ) );
        } else {
            startDay = new Date( param.getStartTime().getTime() );
        }

        Date endDay = null;
        if ( param.getEndTime( ) == null ) {
            endDay = new Date( startDay.getTime( ) + DateUtils.DAY_IN_MILLIS );
        } else {
            endDay = new Date( param.getEndTime( ).getTime( ) );
        }



        //开始日期前总的注册人数
        int totalRegisterCount = reportDao.selectRegisterCount(param);

        //区间内注册信息
        List<DataRegister> registerList = reportDao.selectRegister(param);

        //区间内充值信息
        List<DataRecharge > rechargeList = reportDao.selectRecharge( param );

        //区间内 + 留存对应的天 的所有活跃记录
        ParamServerChannelDatestr paramServerChannelDatestr = new ParamServerChannelDatestr( param.getServerIds(), param.getChannelNames(),  DailyReportUtils.getRetentionActiveDays( startDay, endDay )  );
        List<DataActive> activeList = reportDao.selectActive( paramServerChannelDatestr );

        // 4. 计算新手引导通过率
        DailyReportUtils utils = new DailyReportUtils( startDay , endDay , totalRegisterCount,  registerList, activeList, rechargeList );
        List<DailyReport> reportList = utils.stat();

        //计算出新手引导数据
        mav.addObject( "reportList", reportList );
        return mav;

    }

}  
