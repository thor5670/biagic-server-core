package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.appCfg.AppConfig;
import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.dao.GoldcostDao;
import com.rtsapp.server.gamelog.analyze.stat.ChannelUtils;
import com.rtsapp.server.gamelog.analyze.stat.DateUtils;
import com.rtsapp.server.gamelog.analyze.stat.goldcost.CostParam;
import com.rtsapp.server.gamelog.analyze.stat.goldcost.GoldItems;
import com.rtsapp.server.gamelog.analyze.stat.goldcost.GoldcostViewData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by liwang on 16-8-1.
 */
@Controller
@RequestMapping( "/goldcost" )
public class GoldcostController {

    @Autowired
    private GoldcostDao goldcostDao;
    @Autowired
    private CommonDao commonDao;


    @RequestMapping( "/index" )
    public ModelAndView index( ) {
        ModelAndView modelAndView = new ModelAndView("goldcost/goldcost_index");
        String itemIds = AppConfig.getInstance().getItemId();
        List<GoldItems> items = getItemIds(itemIds);
        modelAndView.addObject("items", items);
        return modelAndView;
    }

    @RequestMapping( "/search" )
    public ModelAndView search( HttpServletRequest request ) {
        ModelAndView mav = new ModelAndView( "goldcost/goldcost_search" );

        CostParam param = new CostParam();
        param.setChannelNames( ChannelUtils.getChannels( commonDao, request, "channelNames" ) );
        if ( !ChannelUtils.isChannelsValid( param.getChannelNames( ) ) ) {
            return mav;
        }
        param.setServerIds( request.getParameterValues( "serverIds" ) );

        String startDay = request.getParameter( "startDay" );
        String endDay = request.getParameter( "endDay" );

        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
        long startTime_MS = 0;
        long endTime_MS = 0;
        try {
            if ( startDay != null && !startDay.trim( ).isEmpty( ) ) {
                startTime_MS = sdf.parse( startDay ).getTime( );
            }

            if ( endDay != null && !endDay.trim( ).isEmpty( ) ) {
                endTime_MS = sdf.parse( endDay ).getTime( ) + DateUtils.DAY_IN_MILLIS;
            }
        } catch ( ParseException e ) {
            e.printStackTrace( );

            startTime_MS = DateUtils.getDayStartTime_MS( System.currentTimeMillis( ) );
            endTime_MS = startTime_MS + DateUtils.DAY_IN_MILLIS;
        }

        param.setStartTime( startTime_MS > 0 ? new Timestamp( startTime_MS ) : null );
        param.setEndTime( endTime_MS > 0 ? new Timestamp( endTime_MS ) : null );

        String[] items = request.getParameterValues("item");

        if (items != null) {
            param.setItemIds(items);
        }

        List<GoldcostViewData> list = goldcostDao.selectCostInfo(param);
        if ( list == null ) {
            list = new ArrayList<>( );
        }

        mav.addObject( "list", list );
        return mav;
    }

    private List<GoldItems> getItemIds(String itemId) {
        List<GoldItems> list = new ArrayList<>();
        String[] itemIds = itemId.split("#");
        for (int i = 0; i < itemIds.length; i++) {
            GoldItems goldItems = new GoldItems();
            String[] strings = itemIds[i].split(",");
            goldItems.setItemId(strings[0]);
            goldItems.setItem(strings[1]);
            list.add(goldItems);
        }
        return list;
    }

}
