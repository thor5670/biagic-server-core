package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.dao.GuideDao;
import com.rtsapp.server.gamelog.analyze.stat.ChannelUtils;
import com.rtsapp.server.gamelog.analyze.stat.guide.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
@RequestMapping("/guide/")
public class GuideController {

    @Autowired
    private GuideDao guideDao;
    @Autowired
    private CommonDao commonDao;

    @RequestMapping( "index" )
    public String index(){
        return "guide/guide_index";
    }



    @RequestMapping( "search" )
    public ModelAndView search( HttpServletRequest request ) {
        ModelAndView mav = new ModelAndView( "guide/guide_search" );

        ParamServerChannelDate param = ParamUtils.parseServerChannelDate( request, commonDao );
        if ( !ChannelUtils.isChannelsValid( param.getChannelNames( ) ) ) {
            return mav;
        }

        // 1. 取出每个服务器, 当前注册的角色ID
        List<DataRoleInit> roleInitList = guideDao.selectRoleInit( param );

        // 2. 取出每个服务器, 今天通过的新手引导ID
        List<DataGuide> firstDayGuideList = guideDao.selectGuide( param );

        // 3. 取得所有新手引导ID名称列表
        List<StatGuide> allGuides = guideDao.selectGuideType( param );

        // 4. 计算新手引导通过率
        GuideStatUtil statService = new GuideStatUtil( roleInitList, firstDayGuideList, allGuides );
        List<StatGuide> guideList = statService.statGuidePass();


        //计算出新手引导数据
        mav.addObject( "guideList", guideList );
        return mav;

    }



}  
