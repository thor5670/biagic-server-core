package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.IncomeDao;
import com.rtsapp.server.gamelog.analyze.stat.RequestUtils;
import com.rtsapp.server.gamelog.analyze.stat.StringUtils;
import com.rtsapp.server.gamelog.analyze.stat.income.IncomeDataMgr;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by liwang on 17-01-10.
 * <p>
 * 营收及留存查询
 */
@Controller
@RequestMapping( "/income" )
public class IncomeController implements InitializingBean {

    @Autowired
    private IncomeDao incomeDao;

    private IncomeDataMgr incomeDataMgr = null;

    @RequestMapping( "/index" )
    public String index( ) {
        return "income/income_index";
    }

    @RequestMapping( "/incomesearch" )
    public ModelAndView incomesearch( HttpServletRequest request ) {
        System.out.println( "\n开始执行收入查询" );

        ModelAndView mav = new ModelAndView( "income/income_search" );
        mav.addObject( "list", incomeDataMgr.getIncomeViewDataList( ) );
        return mav;
    }

    @RequestMapping( "/usersearch" )
    public ModelAndView usersearch( HttpServletRequest request ) {
        System.out.println( "\n开始执行用户查询" );

        ModelAndView mav = new ModelAndView( "income/user_search" );
        mav.addObject( "list", incomeDataMgr.getUserViewDataList( ) );
        return mav;
    }


    @RequestMapping( "/targetset" )
    public void targetset( HttpServletRequest request, HttpServletResponse response ) {

        String gameId = request.getParameter( "gameId" );
        if ( StringUtils.isEmpty( gameId ) ) {
            return;
        }

        String areaId = request.getParameter( "areaId" );
        if ( StringUtils.isEmpty( areaId ) ) {
            return;
        }

        // 修改项目名称
        String gameName = RequestUtils.getStringParam( request, "gameName", null );
        if ( !StringUtils.isEmpty( gameName ) ) {
            if ( incomeDao.selectGameInfoByGameIdAndAreaId( gameId, areaId ) == null ) {
                incomeDao.insertGameInfo( gameId, areaId, gameName, null, null, null );
            }
            incomeDao.updateGameName( gameId, gameName );
        }

        // 修改地区版本名称
        String areaName = RequestUtils.getStringParam( request, "areaName", null );
        if ( areaName != null && !incomeDao.updateAreaName( gameId, areaId, areaName ) ) {
            incomeDao.insertGameInfo( gameId, areaId, null, areaName, null, null );
        }

        // 修改月指标
        float monthIncomeTarget = RequestUtils.getFloatParam( request, "monthIncomeTarget", RequestUtils.INVALID_FLOAT );
        String monthIncomeTargetStr = null;
        if ( monthIncomeTarget != RequestUtils.INVALID_FLOAT ) {

            Calendar cal = Calendar.getInstance( );
            monthIncomeTargetStr = cal.get( Calendar.YEAR ) + String.valueOf( cal.get( Calendar.MONTH ) + 1 ) + "#" + monthIncomeTarget;
        } else {
//            try {
//                response.getWriter( ).print( "monthIncomeTargetError" );
//            } catch ( IOException e ) {
//                e.printStackTrace( );
//            }
        }
        if ( monthIncomeTargetStr != null && !incomeDao.updateMonthIncomeTarget( gameId, areaId, monthIncomeTargetStr ) ) {
            incomeDao.insertGameInfo( gameId, areaId, null, null, monthIncomeTargetStr, null );
        }


        // 修改汇率
        float exchangeRate = RequestUtils.getFloatParam( request, "exchangeRate", RequestUtils.INVALID_FLOAT );
        if ( exchangeRate != RequestUtils.INVALID_DOUBLE ) {
            if ( !incomeDao.updateExchangeRate( gameId, areaId, String.valueOf( exchangeRate ) ) ) {
                incomeDao.insertGameInfo( gameId, areaId, null, null, null, String.valueOf( exchangeRate ) );
            }
        }


        // 刷新页面
        try {
            response.sendRedirect( "/income/admin?bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb" );
        } catch ( IOException e ) {
            e.printStackTrace( );
        }

    }

    /**
     * 设置
     *
     * @param request
     * @return
     */
    @RequestMapping( "/admin" )
    public ModelAndView incomeAdmin( HttpServletRequest request ) {

        ModelAndView mav = new ModelAndView( "/income/income_admin" );

        Calendar cal = Calendar.getInstance( );

        mav.addObject( "list", incomeDao.selectGameInfo( ) );
        return mav;
    }

    @RequestMapping( "/incomeTargetIndex" )
    public ModelAndView incomeTargetIndex( HttpServletRequest request, HttpServletResponse response ) throws IOException {
        ModelAndView mav = new ModelAndView( "/income/income_target_admin" );

        String gameId = RequestUtils.getStringParam( request, "gameId", null );
        String areaId = RequestUtils.getStringParam( request, "areaId", null );
        if ( StringUtils.isEmpty( gameId ) || StringUtils.isEmpty( areaId ) ) {
            response.sendRedirect( "/income/admin?bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb" );
            return null;
        }

        int currYear = Calendar.getInstance( ).get( Calendar.YEAR );

        mav.addObject( "year", currYear );
        mav.addObject( "gameInfo", incomeDao.selectGameInfoByGameIdAndAreaId( gameId, areaId ) );

        ConcurrentMap<Integer, Integer> incomeTargetMap = incomeDataMgr.getIncomeTargetByYear( gameId, areaId, currYear );
        mav.addObject( "incomeTargetMap", incomeTargetMap );

        return mav;
    }

    @RequestMapping( "/incomeTargetUpdate" )
    public void incomeTargetUpdate( HttpServletRequest request, HttpServletResponse response ) throws IOException {

        String gameId = RequestUtils.getStringParam( request, "gameId", null );
        String areaId = RequestUtils.getStringParam( request, "areaId", null );
        int year = RequestUtils.getIntParam( request, "year", RequestUtils.INVALID_INT );
        int month = RequestUtils.getIntParam( request, "month", RequestUtils.INVALID_INT );
        int incomeTarget = RequestUtils.getIntParam( request, "incomeTarget", RequestUtils.INVALID_INT );

        if ( !( StringUtils.isEmpty( gameId ) || StringUtils.isEmpty( areaId ) || year == RequestUtils.INVALID_INT || month == RequestUtils.INVALID_INT || incomeTarget == RequestUtils.INVALID_INT ) ) {
            incomeDataMgr.updateIncomeTarget( gameId, areaId, year, month, incomeTarget );
        }

        response.sendRedirect( "/income/incomeTargetIndex?bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb&gameId="+gameId+"&areaId="+areaId );
    }

    /**
     * spring依赖注入完成后的行为
     *
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet( ) throws Exception {

        this.incomeDataMgr = new IncomeDataMgr( incomeDao );

    }
}
