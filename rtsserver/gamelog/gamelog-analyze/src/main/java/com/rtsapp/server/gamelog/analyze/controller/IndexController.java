package com.rtsapp.server.gamelog.analyze.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by liwang on 16-7-28.
 * <p>
 * 首页
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String first(HttpServletRequest request) {
        return index(request);
    }

    @RequestMapping("/index")
    public String index(HttpServletRequest request) {

        // 如果已经登录, 直接到菜单页
        Object userName = request.getSession().getAttribute(LoginController.SESSION_ATTR);
        if (userName != null && !userName.toString().trim().isEmpty()) {
            return "navigation";
        }

        return "index";
    }

    @RequestMapping("/navigation")
    public String navigation(HttpServletRequest request) {
        return "navigation";
    }

//    @RequestMapping( "/" )
//    public String index( ) {
//
//
//        return "index";
//    }


}
