package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.dao.LevelRunOffDao;
import com.rtsapp.server.gamelog.analyze.stat.levelrunoff.LevelRunOffLevelNum;
import com.rtsapp.server.gamelog.analyze.stat.levelrunoff.LevelRunOffParam;
import com.rtsapp.server.gamelog.analyze.stat.levelrunoff.LevelRunOffParamUtils;
import com.rtsapp.server.gamelog.analyze.stat.levelrunoff.LevelRunOffViewData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 等级流失分布
 * Created by admin on 16/11/10.
 */
@Controller
@RequestMapping("/levelrunoff/")
public class LevelRunOffController {

    @Autowired
    private LevelRunOffDao levelRunOffDao;
    @Autowired
    private CommonDao commonDao;

    @RequestMapping("index")
    public String index() {
        return "levelrunoff/levelrunoff_index";
    }

    @RequestMapping("search")
    private ModelAndView getLevelRunOffCounts(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("levelrunoff/levelrunoff_search");

        //double格式化工具(保留两位小数)
        DecimalFormat df = new DecimalFormat("###0.00");

        //构建展示的list
        List<LevelRunOffViewData> dataList = new ArrayList<>();

        //构建所需参数
        LevelRunOffParam param = LevelRunOffParamUtils.getLevelRunOffParam(request, commonDao);

        //获取服务器总人数
        double sum = levelRunOffDao.selectSum(param);
        if (sum == 0) {
            return null;
        }

        //获取1级的人数信息
        LevelRunOffViewData firstLevel_data = new LevelRunOffViewData();
        final int firstLevelCounts = levelRunOffDao.selectFirstLevelCount(param);
        firstLevel_data.setLevel(LevelRunOffParamUtils.FIRSTLEVEL);
        firstLevel_data.setCounts(firstLevelCounts);

        //1级流失率,1级累计流失率,两值相等
        double firstLevelRatio = (firstLevelCounts / sum) * LevelRunOffParamUtils.PERCENTAGE;
        //格式化为2位小数
        double formatFirstLevelRatio = Double.parseDouble(df.format(firstLevelRatio));
        firstLevel_data.setRunOffRatio(formatFirstLevelRatio);
        firstLevel_data.setAccumulateRunOffRatio(formatFirstLevelRatio);
        dataList.add(firstLevel_data);

        //获取2至最高级的等级及人数
        List<LevelRunOffLevelNum> levelNum = levelRunOffDao.selectLevelNum(param);
        //计算累计人数
        int accumulateNum = firstLevelCounts;
        for (int i = 0; i < levelNum.size(); i++) {

            LevelRunOffViewData viewData = new LevelRunOffViewData();
            LevelRunOffLevelNum levelNumResult = levelNum.get(i);

            viewData.setLevel(levelNumResult.getLevel());

            //本级人数
            int runOffNum = levelNumResult.getNumber();
            viewData.setCounts(runOffNum);
            accumulateNum += runOffNum;

            //本级流失率
            double runOffRatio = (runOffNum / sum) * LevelRunOffParamUtils.PERCENTAGE;
            //格式化本级流失率
            double formatRunOffRatio = Double.parseDouble(df.format(runOffRatio));
            viewData.setRunOffRatio(formatRunOffRatio);

            //本级累计流失率
            double accumulateRunOffRatio = (accumulateNum / sum) * LevelRunOffParamUtils.PERCENTAGE;
            //格式化本级流失率
            double formatAccumulateRunOffRatio = Double.parseDouble(df.format(accumulateRunOffRatio));
            viewData.setAccumulateRunOffRatio(formatAccumulateRunOffRatio);

            dataList.add(viewData);

        }

        mav.addObject("dataList", dataList);
        return mav;

    }


}
