package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.LoginDao;
import com.rtsapp.server.gamelog.analyze.stat.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by liwang on 16-7-28.
 * <p>
 * 首页
 */
@Controller
@RequestMapping( "/login" )
public class LoginController {

    private static final Byte registerLock = new Byte( ( byte ) 0 );

    public static final String SESSION_ATTR = "userName";

    @Autowired
    private LoginDao loginDao;


    @RequestMapping( "/userNameCheck" )
    public void userNameCheck( HttpServletRequest request, HttpServletResponse response ) throws IOException {

        String userName = request.getParameter( "userName" );

        boolean b = loginDao.isUserRegister( userName );

        response.getWriter( ).print( b );
    }

    @RequestMapping( "/login" )
    public void login( HttpServletRequest request, HttpServletResponse response ) throws IOException {

        String userName = request.getParameter( "userName" );
        if ( userName == null || userName.trim( ).isEmpty( ) ) {
            response.getWriter( ).print( "用户名不能为空!" );
            return;
        }
        userName = userName.trim( );

        String passWord = request.getParameter( "passWord" );
        if ( passWord == null || passWord.trim( ).isEmpty( ) ) {
            response.getWriter( ).print( "密码不能为空!" );
            return;
        }
        passWord = MD5.getMD5( passWord.trim( ) );

        boolean b = loginDao.login( userName, passWord );

        if ( b ) {
            request.getSession( ).setAttribute( SESSION_ATTR, userName );
        }

        response.getWriter( ).print( b );
    }

//    @RequestMapping( "/register" )
    public void register( HttpServletRequest request, HttpServletResponse response ) throws IOException {

        String userName = request.getParameter( "userName" );
        if ( userName == null || userName.trim( ).isEmpty( ) ) {
            response.getWriter( ).print( "用户名不能为空!" );
            return;
        }
        userName = userName.trim( );

        String passWord = request.getParameter( "passWord" );
        if ( passWord == null || passWord.trim( ).isEmpty( ) ) {
            response.getWriter( ).print( "密码不能为空!" );
            return;
        }
        passWord = MD5.getMD5( passWord.trim( ) );

        synchronized ( registerLock ) {
            if ( loginDao.isUserRegister( userName ) ) {
                response.getWriter( ).print( "该用户名已经被注册!" );
                return;
            }

            try {
                loginDao.register( userName, passWord );
            } catch ( Throwable e ) {
                e.printStackTrace( );
                response.getWriter( ).print( "该用户名已经被注册!" );
            }
        }

        response.getWriter( ).print( "注册成功!" );
    }


    @RequestMapping( "/logout" )
    public String logOut( HttpServletRequest request ) {

        request.getSession().invalidate();

        return "index";
    }

}
