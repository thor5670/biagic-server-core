package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.stat.ChannelUtils;
import com.rtsapp.server.gamelog.analyze.stat.DateUtils;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by admin on 16-7-27.
 */
public class ParamUtils {


    public static ParamServerChannelDate parseServerChannelDate( HttpServletRequest request, final CommonDao dao ) {

        ParamServerChannelDate param = new ParamServerChannelDate( );

        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );

        try {
            Date startDay = sdf.parse( request.getParameter( "startDay" ) );
            param.setStartTime( new Timestamp( startDay.getTime( ) ) );

        } catch ( ParseException e ) {
            e.printStackTrace( );
        }

        try {
            Date endDay = sdf.parse( request.getParameter( "endDay" ) );
            if ( endDay != null ) {
                endDay = DateUtils.increaseDate( endDay, 1 );
                param.setEndTime( new Timestamp( endDay.getTime( ) ) );
            }
        } catch ( ParseException e ) {
            e.printStackTrace( );
        }


        String[] serverIds = request.getParameterValues( "serverIds" );
        String[] channelNames = ChannelUtils.getChannels( dao, request, "channelNames" );

        if ( serverIds != null && serverIds.length > 0 ) {
            param.setServerIds( serverIds );
        }

        if ( channelNames != null && channelNames.length > 0 ) {
            param.setChannelNames( channelNames );
        }

        return param;


    }
}
