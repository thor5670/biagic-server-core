package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.dao.PaymentDao;
import com.rtsapp.server.gamelog.analyze.stat.payment.PaymentDailyViewData;
import com.rtsapp.server.gamelog.analyze.stat.payment.PaymentDailyUtil;
import com.rtsapp.server.gamelog.analyze.stat.payment.PaymentIntervalUtil;
import com.rtsapp.server.gamelog.analyze.stat.payment.PaymentIntervalViewData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by liwang on 16-7-29.
 */
@Controller
@RequestMapping( "/payment" )
public class PaymentController {

    @Autowired
    private PaymentDao paymentDao;
    @Autowired
    private CommonDao commonDao;


    @RequestMapping( "/index" )
    public String index( ) {
        return "payment/payment_index";
    }

    @RequestMapping( "/daily_search" )
    public ModelAndView searchDailyRecharge( HttpServletRequest request ) {

        final long timeNow_MS = System.currentTimeMillis( );

        PaymentDailyViewData viewData1 = PaymentDailyUtil.buildViewData( commonDao, paymentDao, request, PaymentDailyUtil.TODAY, timeNow_MS );
        PaymentDailyViewData viewData2 = PaymentDailyUtil.buildViewData( commonDao, paymentDao, request, PaymentDailyUtil.YESTERDAY_SAME_TIME, timeNow_MS );
        PaymentDailyViewData viewData3 = PaymentDailyUtil.buildViewData( commonDao, paymentDao, request, PaymentDailyUtil.YESTERDAY_WHOLE_DAY, timeNow_MS );


        ModelAndView modelAndView = new ModelAndView( "/payment/payment_daily_search" );

        List<PaymentDailyViewData> list = new ArrayList<>( );
        list.add( viewData1 );
        if ( viewData2 != null ) {
            list.add( viewData2 );
        }
        list.add( viewData3 );

        modelAndView.addObject( "list", list );
        return modelAndView;
    }

    @RequestMapping( "interval_search" )
    public ModelAndView searchPaymentInterval( HttpServletRequest request ) {

        final List<PaymentIntervalViewData> list = PaymentIntervalUtil.buildViewData( paymentDao, commonDao, request );

        ModelAndView modelAndView = new ModelAndView( "/payment/payment_interval_search" );
        modelAndView.addObject( "list", list );
        return modelAndView;
    }

}
