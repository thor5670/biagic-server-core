package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.dao.RechargeDao;
import com.rtsapp.server.gamelog.analyze.stat.recharge.DataRecharge;
import com.rtsapp.server.gamelog.analyze.stat.daily.DataRegister;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;
import com.rtsapp.server.gamelog.analyze.stat.recharge.RechargeUtils;
import com.rtsapp.server.gamelog.analyze.stat.recharge.RechargeViewData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by admin on 16/10/31.
 */
@Controller
@RequestMapping("/recharge/")
public class RechargeController {

    @Autowired
    private RechargeDao rechargeDao;
    @Autowired
    private CommonDao commonDao;


    @RequestMapping("index")
    public String index() {
        return "recharge/recharge_index";
    }

    @RequestMapping("recharge_search")
    public ModelAndView searchRechargeByDate(HttpServletRequest request) {

        ModelAndView mav = new ModelAndView("recharge/recharge_search");

        ParamServerChannelDate param = ParamUtils.parseServerChannelDate(request,commonDao);

        List<DataRegister> dataRegisterList = rechargeDao.selectRegister(param);
        List<DataRecharge> dataRechargelist = rechargeDao.selectRechargeInfo(param);

        RechargeUtils rechargeUtils = new RechargeUtils(param.getStartTime(),param.getEndTime(),dataRegisterList,dataRechargelist);
        List<RechargeViewData> rechargeViewDataList = rechargeUtils.stat();

        mav.addObject("rechargeViewDataList", rechargeViewDataList);

        return mav;
    }

}
