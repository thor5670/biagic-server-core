package com.rtsapp.server.gamelog.analyze.controller;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.dao.RechargeDetailDao;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;
import com.rtsapp.server.gamelog.analyze.stat.rechargedetail.RechargeDetail;
import com.rtsapp.server.gamelog.analyze.stat.rechargedetail.RechargeDetailViewData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/12/9.
 */
@Controller
@RequestMapping("/rechargedetail/")
public class RechargeDetailController {

    @Autowired
    RechargeDetailDao rechargeDetailDao;
    @Autowired
    CommonDao commonDao;

    @RequestMapping("index")
    public String index() {
        return "/rechargedetail/recharge_detail_index";
    }

    @RequestMapping("search")
    public ModelAndView searchRechargeDetail(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView("rechargedetail/rechargedetail_search");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        ParamServerChannelDate param = ParamUtils.parseServerChannelDate(request, commonDao);
        List<RechargeDetail> rechargeDetailList = rechargeDetailDao.selectRechargeDetail(param);

        List<RechargeDetailViewData> rechargeDetailViewDataList = new ArrayList<>();
        //将查出来的数据导入展示类中,并将充值时间格式化
        for (int i = 0; i < rechargeDetailList.size(); i++) {
            RechargeDetail detail = rechargeDetailList.get(i);
            RechargeDetailViewData rechargeDetailViewData = new RechargeDetailViewData();

            String logTime = sdf.format(detail.getLogTime());
            rechargeDetailViewData.setLogTime(logTime);

            rechargeDetailViewData.setServerId(detail.getServerId());
            rechargeDetailViewData.setUserId(detail.getUserId());
            rechargeDetailViewData.setMoney(detail.getMoney());

            rechargeDetailViewDataList.add(rechargeDetailViewData);
        }

        mav.addObject("rechargeDetailViewDataList", rechargeDetailViewDataList);

        return mav;
    }
}
