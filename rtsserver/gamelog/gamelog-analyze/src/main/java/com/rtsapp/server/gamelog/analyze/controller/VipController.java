package com.rtsapp.server.gamelog.analyze.controller;


import com.rtsapp.server.gamelog.analyze.dao.VipDao;
import com.rtsapp.server.gamelog.analyze.stat.vip.VipParam;
import com.rtsapp.server.gamelog.analyze.stat.vip.VipParamUtils;
import com.rtsapp.server.gamelog.analyze.stat.vip.VipUserId;
import com.rtsapp.server.gamelog.analyze.stat.vip.VipViewData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 16/11/2.
 */
@Controller
@RequestMapping( "/vip/" )
public class VipController {

    @Autowired
    private VipDao vipDao;

    @RequestMapping( "index" )
    public String index( ) {
        return "vip/vip_index";
    }

    @RequestMapping( "vip_search" )
    public ModelAndView getVipCount( HttpServletRequest request ) {
        ModelAndView mav = new ModelAndView( "vip/vip_search" );

        //构建显示的list
        List<VipViewData> vipViewDatas = new ArrayList<>( );

        //vip0:默认将vip0与vip1合并
        //vip1
        VipViewData vip1data = new VipViewData( );
        vip1data.setVipLevel( 1 );
        VipParam vip1Param = VipParamUtils.getVipParam( request, 1 );
        vip1data.setCounts( vipDao.selectVip1( vip1Param ) );
        vipViewDatas.add( vip1data );

        for ( int i = 2; i <= 15; i++ ) {
            VipViewData data = new VipViewData( );
            //构建vip查询参数(从vip2-vip15)
            VipParam param = VipParamUtils.getVipParam( request, i );
            //构建vip展示形式
            List<VipUserId> vipUserIds = vipDao.selectUserIdByVIP( param );
            data.setVipLevel( i );
            if ( vipUserIds != null ) {
                data.setCounts( vipUserIds.size( ) );
            }
            vipViewDatas.add( data );
        }

        mav.addObject( "vipViewDatas", vipViewDatas );
        return mav;
    }


}
