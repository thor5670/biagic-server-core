package com.rtsapp.server.gamelog.analyze.dao;

import com.rtsapp.server.gamelog.analyze.stat.action.ActionParam;
import com.rtsapp.server.gamelog.analyze.stat.action.ActionParams;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;

import java.util.List;

/**
 * Created by admin on 16-11-1.
 */
public interface ActionDao  {
    List<ActionParams> selectActionParams();

    List<ActionParams> selectActionParam(ActionParam param);

    int selectRoleInit(ParamServerChannelDate param);
}
