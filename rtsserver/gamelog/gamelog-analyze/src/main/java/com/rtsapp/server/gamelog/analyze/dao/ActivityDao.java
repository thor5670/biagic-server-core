package com.rtsapp.server.gamelog.analyze.dao;


import com.rtsapp.server.gamelog.analyze.stat.activity.ActivityParam;
import com.rtsapp.server.gamelog.analyze.stat.activity.ActivityTypeInfo;
import com.rtsapp.server.gamelog.analyze.stat.activity.ActivityViewData;

import java.util.List;

public interface ActivityDao {

    List<ActivityViewData> selectByTime( ActivityParam param );

    List<ActivityTypeInfo> selectActivityTypeInfo( );

}
