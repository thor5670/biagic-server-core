package com.rtsapp.server.gamelog.analyze.dao;

import java.util.List;

/**
 * Created by admin on 16-7-28.
 */
public interface CommonDao {

    List<Integer> selectServerIds();

    List<String> selectChannelNames();

    String selectChannelNamesStrByUserName( String userName );

}
