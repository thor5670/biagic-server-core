package com.rtsapp.server.gamelog.analyze.dao;

import com.rtsapp.server.gamelog.analyze.stat.goldcost.GoldcostViewData;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by liwang on 16-7-29.
 */
public interface GoldcostDao {

    List<GoldcostViewData> selectCostInfo(ParamServerChannelDate param);

}
