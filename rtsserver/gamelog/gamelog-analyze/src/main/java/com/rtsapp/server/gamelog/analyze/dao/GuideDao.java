package com.rtsapp.server.gamelog.analyze.dao;

import com.rtsapp.server.gamelog.analyze.stat.guide.DataGuide;
import com.rtsapp.server.gamelog.analyze.stat.guide.DataRoleInit;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;
import com.rtsapp.server.gamelog.analyze.stat.guide.StatGuide;

import java.util.List;

/**
 * Created by admin on 16-7-26.
 */
public interface GuideDao {

    List<DataRoleInit> selectRoleInit( ParamServerChannelDate param );

    List<DataGuide> selectGuide( ParamServerChannelDate param );

    // 3. 取得所有新手引导ID名称列表
    List<StatGuide> selectGuideType( ParamServerChannelDate param );

}
