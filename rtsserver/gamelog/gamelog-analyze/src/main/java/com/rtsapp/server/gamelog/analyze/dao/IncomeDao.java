package com.rtsapp.server.gamelog.analyze.dao;

import com.rtsapp.server.gamelog.analyze.stat.income.model.IncomeQueryParam;
import com.rtsapp.server.gamelog.analyze.stat.income.IncomeViewData;
import com.rtsapp.server.gamelog.analyze.stat.income.UserViewData;
import com.rtsapp.server.gamelog.analyze.stat.income.model.GameIdAndAreaIdInfo;
import com.rtsapp.server.gamelog.analyze.stat.income.model.IncomeTargetInfo;

import java.util.List;

/**
 * Created by liwang on 16-7-29.
 */
public interface IncomeDao {

    // 查询指定时间区间的收入总额
    float selectIncome( IncomeQueryParam params );

    // 查询指定时间区间的新增用户数
    int selectUserIncrement( IncomeQueryParam params );

    // 查询指定时间区间的DAU(登录用户数)
    int selectDAU( IncomeQueryParam params );

    // 查询指定时间区间的APA(付费玩家数)
    int selectAPA( IncomeQueryParam params );


    List< Integer > selectUserCreate( IncomeQueryParam params );

    List< Integer > selectUserLogin( IncomeQueryParam params );

    String selectMonthTarget( String gameId, String areaId );

    boolean updateGameName( String gameId, String gameName );

    IncomeViewData selectGameInfoByGameIdAndAreaId( String gameId, String areaId );

    boolean updateAreaName( String gameId, String areaId, String areaName );

    boolean updateExchangeRate( String gameId, String areaId, String exchangeRate );
    boolean insertGameInfo( String gameId, String areaId, String gameName, String areaName, String monthIncomeTarget, String exchangeRate );

    List< IncomeViewData > selectGameInfo( );

    String getCurrencyByGameIdAndAreaId( String gameId, String areaId );

    // 查询汇率
    String selectExchangeRate( String gameId, String areaId );

    // 查询收入指标
    List<IncomeTargetInfo> selectIncomeTargetInfo( );

    // 修改收入指标
    boolean updateMonthIncomeTarget( String gameId, String areaId, String monthIncomeTarget );

    // 查询出所有的gameId和areaId
    List<GameIdAndAreaIdInfo> selectAllGameIdAndAreaIdFromLoginLog( );

    List<GameIdAndAreaIdInfo> selectAllGameIdAndAreaIdFromGameInfo( );

    List< UserViewData > selectGameIdGameNameAreaIdAreaNameFromGameInfo( );

    List<IncomeViewData> selectIncomeViewDataFromGameInfo( );

}
