package com.rtsapp.server.gamelog.analyze.dao;

import com.rtsapp.server.gamelog.analyze.stat.levelrunoff.LevelRunOffLevelNum;
import com.rtsapp.server.gamelog.analyze.stat.levelrunoff.LevelRunOffParam;

import java.util.List;

/**
 * Created by admin on 16/11/10.
 */
public interface LevelRunOffDao {

    int selectSum(LevelRunOffParam param);

    List<LevelRunOffLevelNum> selectLevelNum(LevelRunOffParam param);

    int selectFirstLevelCount(LevelRunOffParam param);

}
