package com.rtsapp.server.gamelog.analyze.dao;

/**
 * Created by liwang on 16-8-2.
 */
public interface LoginDao {

    boolean isUserRegister( String userName );

    boolean login( String userName, String passWord );

    boolean register( String userName, String passWord );


}
