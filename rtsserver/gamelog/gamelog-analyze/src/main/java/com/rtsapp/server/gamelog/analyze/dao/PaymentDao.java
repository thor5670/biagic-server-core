package com.rtsapp.server.gamelog.analyze.dao;

import com.rtsapp.server.gamelog.analyze.stat.payment.PaymentDailyViewData;
import com.rtsapp.server.gamelog.analyze.stat.payment.PaymentIntervalViewData;
import com.rtsapp.server.gamelog.analyze.stat.payment.PaymentQueryParam;

/**
 * Created by liwang on 16-7-29.
 */
public interface PaymentDao {

    PaymentDailyViewData selectPayNum( PaymentQueryParam param );

    Integer selectDAU( PaymentQueryParam param );
    Integer selectAPA( PaymentQueryParam param );
    Integer selectPayGoldNum( PaymentQueryParam param );
    Integer selectSystemGoldNum( PaymentQueryParam param );
    Integer selectCostNum( PaymentQueryParam param );
    Integer selectCostPlayerNum( PaymentQueryParam param );
    Integer selectPCU( PaymentQueryParam param );


    PaymentIntervalViewData selectPayIntervalInfo( PaymentQueryParam param );
}
