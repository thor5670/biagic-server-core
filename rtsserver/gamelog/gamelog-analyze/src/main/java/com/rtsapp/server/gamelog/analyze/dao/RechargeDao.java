package com.rtsapp.server.gamelog.analyze.dao;


import com.rtsapp.server.gamelog.analyze.stat.recharge.DataRecharge;
import com.rtsapp.server.gamelog.analyze.stat.daily.DataRegister;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;

import java.util.List;

/**
 * Created by admin on 16/10/31.
 */
public interface RechargeDao {

    List<DataRegister> selectRegister(ParamServerChannelDate param );

    List<DataRecharge> selectRechargeInfo(ParamServerChannelDate param );


}
