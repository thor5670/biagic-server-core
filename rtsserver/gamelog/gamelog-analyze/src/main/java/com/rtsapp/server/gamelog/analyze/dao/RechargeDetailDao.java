package com.rtsapp.server.gamelog.analyze.dao;

import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;
import com.rtsapp.server.gamelog.analyze.stat.rechargedetail.RechargeDetail;

import java.util.List;

/**
 * Created by admin on 16/12/9.
 */
public interface RechargeDetailDao {

    List<RechargeDetail> selectRechargeDetail(ParamServerChannelDate param);

}
