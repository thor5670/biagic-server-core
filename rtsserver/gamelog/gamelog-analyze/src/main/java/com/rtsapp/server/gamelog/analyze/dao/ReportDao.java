package com.rtsapp.server.gamelog.analyze.dao;

import com.rtsapp.server.gamelog.analyze.stat.daily.DataActive;
import com.rtsapp.server.gamelog.analyze.stat.daily.DataRecharge;
import com.rtsapp.server.gamelog.analyze.stat.daily.DataRegister;
import com.rtsapp.server.gamelog.analyze.stat.daily.ParamServerChannelDatestr;
import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by admin on 16-7-28.
 */
public interface ReportDao {


    int selectRegisterCount(ParamServerChannelDate param);

    List<DataRegister> selectRegister(ParamServerChannelDate param);


    List<DataRecharge> selectRecharge(ParamServerChannelDate param);


    List<DataActive> selectActive( ParamServerChannelDatestr param );


}
