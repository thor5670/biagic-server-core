package com.rtsapp.server.gamelog.analyze.dao;

import com.rtsapp.server.gamelog.analyze.stat.vip.VipParam;
import com.rtsapp.server.gamelog.analyze.stat.vip.VipUserId;

import java.util.List;

/**
 * Created by admin on 16/11/2.
 */
public interface VipDao {

    List<VipUserId> selectUserIdByVIP( VipParam param );

    int selectVip1( VipParam param );

}
