package com.rtsapp.server.gamelog.analyze.stat;

/**
 * Created by liwang on 16-7-29.
 */
public class DoubleUtils {


    /**
     * 将double数值转换为百分比, 小数保留1位
     *
     * @param d
     * @return
     */
    public static String doubleToPercentKeep2( double d ) {

        return String.format( "%.2f", d * 100 ) + "%";

    }

    public static String floatToPercentKeep2( float f ) {
        String result = String.format( "%.2f", f * 100 );
        return "NaN".equals( result ) ? "" : ( result + "%" );

    }

    public static String doubleKeep2( double d ) {

        return String.format( "%.2f", d );

    }

    public static void main( String[] sdfs ) {
    }

}
