package com.rtsapp.server.gamelog.analyze.stat;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * Created by liwang on 17-1-19.
 */
public class MailUtils {

    // 发送邮件的邮箱
    private static final String emailFrom = "liwang@rtsapp.com";
    // 邮件发送人
    private static final String nameShow = "rtsapp-gamelog";
    // 发送邮箱的密码
    private static final String pwd = "yuanyou343743885";

    // 邮件服务器参数配置
    private final Properties props;
    private volatile Address[] addresses = null;


    private static final MailUtils instance = new MailUtils( );

    private MailUtils( ) {

        // 邮件服务器配置信息
        props = new Properties( );
        props.setProperty( "mail.smtp.auth", "true" );
        props.setProperty( "mail.host", "smtp.exmail.qq.com" );
        props.setProperty( "mail.transport.protocol", "smtp" );

        // 添加收件人
        addAddress( "liwang@rtsapp.com" );

    }

    public static final MailUtils getInstance( ) {
        return instance;
    }


    /**
     * 发送邮件
     *
     * @param mailTitle   邮件标题
     * @param mailContent 邮件内容
     * @throws UnsupportedEncodingException
     * @throws MessagingException
     */
    public void sendMail( String mailTitle, String mailContent ) {
        if ( mailContent == null ) {
            System.out.println( "MailUtils.sendMail : 邮件内容不能为null!" );
            return;
        }

        Session session = Session.getInstance( props );
        Message msg = new MimeMessage( session );

        try {
            msg.setFrom( new InternetAddress( emailFrom, nameShow ) );
            msg.setSubject( mailTitle );
            msg.setContent( mailContent, "text/html;charset=UTF-8" );
            msg.saveChanges( );

            Transport transport = null;
            transport = session.getTransport( );
            transport.connect( emailFrom, pwd );
            transport.sendMessage( msg, addresses );
            transport.close( );
        } catch ( MessagingException e ) {
            e.printStackTrace( );
        } catch ( UnsupportedEncodingException e ) {
            e.printStackTrace( );
        }

    }


    /**
     * 添加一个收件人
     *
     * @param addressStr email地址 TODO 格式验证
     */
    public void addAddress( String addressStr ) {
        if ( StringUtils.isEmpty( addressStr ) ) {
            System.out.println( "MailUtils.addAddress : email地址为空, 不能添加!" );
            return;
        }

        Address[] oldAddresses = this.addresses;

        int newLength = oldAddresses == null ? 1 : ( oldAddresses.length + 1 );
        Address[] newAddresses = new InternetAddress[ newLength ];

        if ( newLength > 1 ) {
            System.arraycopy( oldAddresses, 0, newAddresses, 0, newLength - 1 );
        }

        try {
            newAddresses[ newLength - 1 ] = new InternetAddress( addressStr );
        } catch ( AddressException e ) {
            System.out.println( "MailUtils.addAddress : 添加email地址失败!" );
            e.printStackTrace( );
        }

        this.addresses = newAddresses;
    }


    public static void main( String[] kkk ) throws UnsupportedEncodingException, MessagingException {
        instance.sendMail( null, "" );
//        Address address = new InternetAddress( "88990" );
//        System.out.println( address.toString( ) );
    }

}
