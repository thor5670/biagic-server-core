package com.rtsapp.server.gamelog.analyze.stat;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by liwang on 17-1-13.
 */
public class RequestUtils {

    public static final int INVALID_INT = -1;
    public static final float INVALID_FLOAT = -1;
    public static final float INVALID_DOUBLE = -1;


    public static String getStringParam( HttpServletRequest request, String paramName, String defaultValue ) {
        String value = request.getParameter( paramName );
        return StringUtils.isEmpty( value ) ? defaultValue : value;
    }

    public static float getFloatParam( HttpServletRequest request, String paramName, float defaultValue ) {
        try {
            return Float.parseFloat( request.getParameter( paramName ) );
        } catch ( Throwable e ) {
            return defaultValue;
        }
    }

    public static double getDoubleParam( HttpServletRequest request, String paramName, double defaultValue ) {
        try {
            return Float.parseFloat( request.getParameter( paramName ) );
        } catch ( Throwable e ) {
            return defaultValue;
        }
    }

    public static int getIntParam( HttpServletRequest request, String paramName, int defaultValue ) {
        try {
            return Integer.parseInt( request.getParameter( paramName ) );
        } catch ( Throwable e ) {
            return defaultValue;
        }
    }

}
