package com.rtsapp.server.gamelog.analyze.stat;

/**
 * Created by liwang on 17-1-13.
 */
public class StringUtils {

    public static boolean isEmpty( String str) {
        return str == null || str.trim( ).isEmpty( );
    }
}
