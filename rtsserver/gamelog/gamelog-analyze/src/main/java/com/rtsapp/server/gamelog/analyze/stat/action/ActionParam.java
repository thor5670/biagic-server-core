package com.rtsapp.server.gamelog.analyze.stat.action;

import java.sql.Timestamp;

/**
 * Created by admin on 16-11-1.
 */
public class ActionParam {

    // 服务器组
    private String[] serverIds;

    // 渠道组
    private String[] channelNames;

    // 行为类型组
    private String[] actions;

    private Timestamp startTime;

    private Timestamp endTime;

    public ActionParam(String[] serverIds, String[] channelNames, String[] actions, Timestamp startTime, Timestamp endTime) {
        this.serverIds = serverIds;
        this.channelNames = channelNames;
        this.actions = actions;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String[] getServerIds() {
        return serverIds;
    }

    public void setServerIds(String[] serverIds) {
        this.serverIds = serverIds;
    }

    public String[] getChannelNames() {
        return channelNames;
    }

    public void setChannelNames(String[] channelNames) {
        this.channelNames = channelNames;
    }

    public String[] getActions() {
        return actions;
    }

    public void setActions(String[] actions) {
        this.actions = actions;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }
}
