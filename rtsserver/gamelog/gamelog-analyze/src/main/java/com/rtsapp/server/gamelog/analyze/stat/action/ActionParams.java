package com.rtsapp.server.gamelog.analyze.stat.action;

/**
 * Created by admin on 16-11-1.
 */
public class ActionParams {
    private String action;
    private String actionParam;
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getActionParam() {
        return actionParam;
    }

    public void setActionParam(String actionParam) {
        this.actionParam = actionParam;
    }
}
