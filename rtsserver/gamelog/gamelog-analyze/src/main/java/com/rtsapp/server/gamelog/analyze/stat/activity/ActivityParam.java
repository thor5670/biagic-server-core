package com.rtsapp.server.gamelog.analyze.stat.activity;

import java.sql.Timestamp;

/**
 * Created by liwang on 16-7-27.
 */
public class ActivityParam {

    // 服务器组
    private String[] serverIds;

    // 渠道组
    private String[] channelNames;

    // 活动类型组
    private String[] activitys;

    private Timestamp startTime;

    private Timestamp endTime;


    public ActivityParam( ) {

    }

    public ActivityParam( String[] activitys, Timestamp startTime, Timestamp endTime, String[] serverIds, String[] channelNames ) {
        this.activitys = activitys;
        this.startTime = startTime;
        this.endTime = endTime;
        this.serverIds = serverIds;
        this.channelNames = channelNames;
    }


    public String[] getChannelNames( ) {
        return channelNames;
    }

    public void setChannelNames( String[] channelNames ) {
        this.channelNames = channelNames;
    }

    public String[] getServerIds( ) {
        return serverIds;
    }

    public void setServerIds( String[] serverIds ) {
        this.serverIds = serverIds;
    }

    public String[] getActivitys( ) {
        return activitys;
    }

    public void setActivitys( String[] activitys ) {
        this.activitys = activitys;
    }

    public Timestamp getStartTime( ) {
        return startTime;
    }

    public void setStartTime( Timestamp startTime ) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime( ) {
        return endTime;
    }

    public void setEndTime( Timestamp endTime ) {
        this.endTime = endTime;
    }
}
