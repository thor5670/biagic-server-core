package com.rtsapp.server.gamelog.analyze.stat.activity;

/**
 * Created by liwang on 16-7-28.
 * <p>
 * 用于记录活动Id及活动名称
 */
public class ActivityTypeInfo {

    private String activityType;
    private String activityTypeName;


    public String getActivityType( ) {
        return activityType;
    }

    public void setActivityType( String activityType ) {
        this.activityType = activityType;
    }

    public String getActivityTypeName( ) {
        return activityTypeName;
    }

    public void setActivityTypeName( String activityTypeName ) {
        this.activityTypeName = activityTypeName;
    }
}
