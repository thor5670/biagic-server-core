package com.rtsapp.server.gamelog.analyze.stat.activity;

/**
 * Created by liwang on 16-7-27.
 */
public class ActivityViewData {

    private String yearMonthDay;

    private String activityTypeName;

    private String activityCondition;

    private String activityConditionName;

    private String count;


    public String getActivityCondition( ) {
        return activityCondition;
    }

    public void setActivityCondition( String activityCondition ) {
        this.activityCondition = activityCondition;
    }

    public String getActivityTypeName( ) {
        return activityTypeName;
    }

    public void setActivityTypeName( String activityTypeName ) {
        this.activityTypeName = activityTypeName;
    }

    public String getActivityConditionName( ) {
        return activityConditionName;
    }

    public void setActivityConditionName( String activityConditionName ) {
        this.activityConditionName = activityConditionName;
    }

    public String getYearMonthDay( ) {
        return yearMonthDay;
    }

    public void setYearMonthDay( String yearMonthDay ) {
        this.yearMonthDay = yearMonthDay;
    }

    public String getCount( ) {
        return count;
    }

    public void setCount( String count ) {
        this.count = count;
    }
}
