package com.rtsapp.server.gamelog.analyze.stat.daily;

import com.rtsapp.server.gamelog.analyze.stat.DateUtils;
import com.rtsapp.server.gamelog.analyze.stat.DoubleUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by admin on 16-7-27.
 */
public class DailyReport {

    private Date date;

    //总用户
    private int totalRegister;
    //新增注册
    private int dayRegister;
    //DAU
    private int DAU;


    //注册用户付费人数
    private int registerPayUser;
    //注册用户付费金额
    private float registerPayMoney;

    //首充人数
    private int firstPayUser;
    //首充总金额
    private float firstPayMoney;

    //总付费人数
    private int totalPayUser;
    //总付费金额
    private float totalPayMoney;

    //1,2,3,4,5,6,7,14,30日留存
    private int[] retentions;


    public DailyReport( Date date ) {
        this.date = date;
    }

    public Date getDate( ) {
        return date;
    }

    public String getDateString( ) {
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
        return sdf.format( this.date );
    }


    public String getWeekString( ) {
        return DateUtils.getWeekOfDate( this.date );
    }


    /**
     * 付费率
     * 总付费人数 / DAU
     * @return
     */
    public String getPayPercent( ) {
        return DoubleUtils.doubleToPercentKeep2( totalPayUser * 1.0 / DAU );
    }

    /**
     * 平均每个用户付费
     * 总付费金额 / DAU
     * @return
     */
    public String getARPU( ) {
        return DoubleUtils.doubleKeep2( totalPayMoney * 1.0 / DAU );
    }

    /**
     * 平均每个付费用户付费
     *
     * @return
     */
    public String getARPPU( ) {
        return DoubleUtils.doubleKeep2( totalPayMoney * 1.0 / totalPayUser );
    }


    public int getTotalRegister( ) {
        return totalRegister;
    }

    public void setTotalRegister( int totalRegister ) {
        this.totalRegister = totalRegister;
    }


    public int getDayRegister( ) {
        return dayRegister;
    }

    public void setDayRegister( int dayRegister ) {
        this.dayRegister = dayRegister;
    }

    public int getDAU( ) {
        return DAU;
    }

    public void setDAU( int DAU ) {
        this.DAU = DAU;
    }

    public int getRegisterPayUser( ) {
        return registerPayUser;
    }

    public void setRegisterPayUser( int registerPayUser ) {
        this.registerPayUser = registerPayUser;
    }

    public float getRegisterPayMoney( ) {
        return registerPayMoney;
    }

    public void setRegisterPayMoney( float registerPayMoney ) {
        this.registerPayMoney = registerPayMoney;
    }

    public int getFirstPayUser( ) {
        return firstPayUser;
    }

    public void setFirstPayUser( int firstPayUser ) {
        this.firstPayUser = firstPayUser;
    }

    public float getFirstPayMoney( ) {
        return firstPayMoney;
    }

    public void setFirstPayMoney( float firstPayMoney ) {
        this.firstPayMoney = firstPayMoney;
    }

    public int getTotalPayUser( ) {
        return totalPayUser;
    }

    public void setTotalPayUser( int totalPayUser ) {
        this.totalPayUser = totalPayUser;
    }

    public float getTotalPayMoney( ) {
        return totalPayMoney;
    }

    public void setTotalPayMoney( float totalPayMoney ) {
        this.totalPayMoney = totalPayMoney;
    }


    public int[] getRetentions( ) {
        return retentions;
    }

    public void setRetentions( int[] retentions ) {
        this.retentions = retentions;
    }

    /**
     * 留存1日
     *
     * @return
     */
    public int getRetentionUser1( ) {
        return retentions[ 0 ];
    }

    public String getRetentionPercent1( ) {
        return DoubleUtils.doubleToPercentKeep2( retentions[ 0 ] * 1.0 / dayRegister );
    }

    public int getRetentionUser2( ) {
        return retentions[ 1 ];
    }

    public String getRetentionPercent2( ) {
        return DoubleUtils.doubleToPercentKeep2( retentions[ 1 ] * 1.0 / dayRegister );
    }


    public int getRetentionUser3( ) {
        return retentions[ 2 ];
    }

    public String getRetentionPercent3( ) {
        return DoubleUtils.doubleToPercentKeep2( retentions[ 2 ] * 1.0 / dayRegister );
    }


    public int getRetentionUser4( ) {
        return retentions[ 3 ];
    }

    public String getRetentionPercent4( ) {
        return DoubleUtils.doubleToPercentKeep2( retentions[ 3 ] * 1.0 / dayRegister );
    }


    public int getRetentionUser5( ) {
        return retentions[ 4 ];
    }

    public String getRetentionPercent5( ) {
        return DoubleUtils.doubleToPercentKeep2( retentions[ 4 ] * 1.0 / dayRegister );
    }


    public int getRetentionUser6( ) {
        return retentions[ 5 ];
    }

    public String getRetentionPercent6( ) {
        return DoubleUtils.doubleToPercentKeep2( retentions[ 5 ] * 1.0 / dayRegister );
    }


    public int getRetentionUser7( ) {
        return retentions[ 6 ];
    }

    public String getRetentionPercent7( ) {
        return DoubleUtils.doubleToPercentKeep2( retentions[ 6 ] * 1.0 / dayRegister );
    }


    public int getRetentionUser14( ) {
        return retentions[ 7 ];
    }

    public String getRetentionPercent14( ) {
        return DoubleUtils.doubleToPercentKeep2( retentions[ 7 ] * 1.0 / dayRegister );
    }

    public int getRetentionUser30( ) {
        return retentions[ 8 ];
    }

    public String getRetentionPercent30( ) {
        return DoubleUtils.doubleToPercentKeep2( retentions[ 8 ] * 1.0 / dayRegister );
    }
}
