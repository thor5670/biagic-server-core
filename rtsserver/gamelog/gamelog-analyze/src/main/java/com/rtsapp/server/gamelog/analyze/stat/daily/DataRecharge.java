package com.rtsapp.server.gamelog.analyze.stat.daily;

import java.util.Date;

/**
 * Created by admin on 16-7-27.
 */
public class DataRecharge {


    private Date logTime;

    private int userId;

    private float money;

    private int payTimes;


    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public int getPayTimes() {
        return payTimes;
    }

    public void setPayTimes(int payTimes) {
        this.payTimes = payTimes;
    }
}
