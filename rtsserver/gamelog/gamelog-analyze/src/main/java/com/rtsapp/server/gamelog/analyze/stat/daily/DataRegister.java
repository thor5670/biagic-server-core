package com.rtsapp.server.gamelog.analyze.stat.daily;

import java.util.Date;

/**
 * Created by admin on 16-7-27.
 */
public class DataRegister {

    private Date logTime;

    private int userId;


    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
