package com.rtsapp.server.gamelog.analyze.stat.daily;

import java.util.Set;


public class ParamServerChannelDatestr {


    //服务器ID组, 空代表全部
    private String[] serverIds;


    //渠道组, 空代表全部
    private String[] channelNames;

    //
    private Set<String> allDayStrs;

    public ParamServerChannelDatestr(String[] serverIds, String[] channelNames, Set<String> allDayStrs) {
        this.serverIds = serverIds;
        this.channelNames = channelNames;
        this.allDayStrs = allDayStrs;
    }



    public String[] getServerIds() {
        return serverIds;
    }

    public void setServerIds(String[] serverIds) {
        this.serverIds = serverIds;
    }

    public String[] getChannelNames() {
        return channelNames;
    }

    public void setChannelNames(String[] channelNames) {
        this.channelNames = channelNames;
    }


    public Set<String> getAllDayStrs() {
        return allDayStrs;
    }


    public void setAllDayStrs(Set<String> allDayStrs) {
        this.allDayStrs = allDayStrs;
    }


}
