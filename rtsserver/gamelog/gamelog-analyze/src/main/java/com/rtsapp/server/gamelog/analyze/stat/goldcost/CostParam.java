package com.rtsapp.server.gamelog.analyze.stat.goldcost;

import com.rtsapp.server.gamelog.analyze.stat.guide.ParamServerChannelDate;

/**
 * Created by admin on 16-11-17.
 */
public class CostParam extends ParamServerChannelDate {
    private String[] itemIds;

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }
}
