package com.rtsapp.server.gamelog.analyze.stat.goldcost;

/**
 * Created by admin on 16-11-17.
 */
public class GoldItems {
    private String itemId;
    private String item;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
