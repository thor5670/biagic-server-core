package com.rtsapp.server.gamelog.analyze.stat.goldcost;

/**
 * Created by liwang on 16-8-1.
 */
public class GoldcostViewData {

    private String yearMonthDay;

    private String behavior;

    private int totalCost;

    private int costTimes;

    private int costPlayerNum;


    public String getYearMonthDay( ) {
        return yearMonthDay;
    }

    public void setYearMonthDay( String yearMonthDay ) {
        this.yearMonthDay = yearMonthDay;
    }

    public String getBehavior( ) {
        return behavior;
    }

    public void setBehavior( String behavior ) {
        this.behavior = behavior;
    }

    public int getTotalCost( ) {
        return totalCost;
    }

    public void setTotalCost( Integer totalCost ) {
        this.totalCost = totalCost == null ? 0 : totalCost;
    }

    public int getCostTimes( ) {
        return costTimes;
    }

    public void setCostTimes( Integer costTimes ) {
        this.costTimes = costTimes == null ? 0 : costTimes;
    }

    public int getCostPlayerNum( ) {
        return costPlayerNum;
    }

    public void setCostPlayerNum( Integer costPlayerNum ) {
        this.costPlayerNum = costPlayerNum == null ? 0 : costPlayerNum;
    }
}
