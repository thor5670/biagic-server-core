package com.rtsapp.server.gamelog.analyze.stat.guide;

/**
 * Created by admin on 16-7-26.
 */
public class DataGuide {

    private String serverId;
    private String roleId;
    private int guideId;

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public int getGuideId() {
        return guideId;
    }

    public void setGuideId(int guideId) {
        this.guideId = guideId;
    }
}
