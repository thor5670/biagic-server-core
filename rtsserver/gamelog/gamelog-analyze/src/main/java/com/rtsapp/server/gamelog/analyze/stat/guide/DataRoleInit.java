package com.rtsapp.server.gamelog.analyze.stat.guide;

/**
 * 角色初始化
 */
public class DataRoleInit {

    private String serverId;

    private String roleId;


    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
}
