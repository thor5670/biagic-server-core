package com.rtsapp.server.gamelog.analyze.stat.guide;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 16-7-26.
 */
public class GuideStatUtil {

    //1. 角色注册的信息
    private final List<DataRoleInit> roleInitList ;

    // 2. 取出每个服务器, 今天通过的新手引导ID
    private final List<DataGuide> firstDayGuideList ;

    // 3. 取得所有新手引导ID名称列表
    private final List<StatGuide> allGuides ;
    private final Map<Integer,StatGuide> guideMap = new HashMap<Integer, StatGuide>();

    public GuideStatUtil(List<DataRoleInit> roleInitList, List<DataGuide> firstDayGuideList, List<StatGuide> allGuides) {
        this.allGuides = allGuides;
        this.firstDayGuideList = firstDayGuideList;
        this.roleInitList = roleInitList;


        for( StatGuide statGuide : allGuides ){
            guideMap.put( statGuide.getGuideId(), statGuide );
        }

        Collections.sort(allGuides);
    }

    public List<StatGuide> statGuidePass(){

        //重置

        //统计新手ID通过次数
        int totalRoleCount = roleInitList.size();

        for( DataGuide guide : firstDayGuideList ){
            if( isNewRoleGuide( guide ) ) {
                increaseGuide(guide);
            }
        }

        //计算通过率
        calculatePassPercent(totalRoleCount);

        //最前面追加注册信息
        StatGuide regGuide = new StatGuide( 0, "注册人数", totalRoleCount, 1 );
        allGuides.add( 0, regGuide );

        return allGuides;
    }

    private void calculatePassPercent( float totalRoleCount) {

        for( StatGuide statGuide : allGuides ){
            statGuide.setPassPercent( statGuide.getPassCount() / totalRoleCount  );
        }
    }


    /**
     * 是否是新角色的新手引导记录
     * @param guide
     * @return
     */
    private boolean isNewRoleGuide( DataGuide guide ){

        for( DataRoleInit roleInit : roleInitList ){
            if( guide.getServerId() != null && guide.getRoleId() != null &&
                   guide.getServerId().equals( roleInit.getServerId()) && guide.getRoleId().equals( roleInit.getRoleId() ) ){
                return true;
            }
        }

        return false;
    }

    private void increaseGuide( DataGuide guide ){
        StatGuide stat =  guideMap.get(guide.getGuideId());
        if( stat != null ){
            stat.setPassCount(stat.getPassCount() + 1);
        }else{
            //TODO error print
        }
    }

}
