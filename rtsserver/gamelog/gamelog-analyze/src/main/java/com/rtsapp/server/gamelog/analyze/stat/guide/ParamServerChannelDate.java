package com.rtsapp.server.gamelog.analyze.stat.guide;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by admin on 16-7-26.
 */
public class ParamServerChannelDate {


    //服务器ID组, 空代表全部
    private String[] serverIds;


    //渠道组, 空代表全部
    private String[] channelNames;


    //开始时间
    private Timestamp startTime;

    //结束时间
    private Timestamp endTime;

    public String[] getServerIds() {
        return serverIds;
    }

    public void setServerIds(String[] serverIds) {
        this.serverIds = serverIds;
    }

    public String[] getChannelNames() {
        return channelNames;
    }

    public void setChannelNames(String[] channelNames) {
        this.channelNames = channelNames;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }
}
