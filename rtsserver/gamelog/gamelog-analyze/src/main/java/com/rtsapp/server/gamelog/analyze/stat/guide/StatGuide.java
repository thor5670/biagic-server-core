package com.rtsapp.server.gamelog.analyze.stat.guide;

import com.rtsapp.server.gamelog.analyze.stat.DoubleUtils;

/**
 * Created by admin on 16-7-26.
 */
public class StatGuide implements Comparable<StatGuide>{

    //新手引导ID
    private int guideId;

    //新手引导名称
    private String guideName;

    //第一天通过人数
    private int passCount;

    private float passPercent;

    public StatGuide() {
    }

    public StatGuide(int guideId, String guideName, int passCount, float passPercent) {
        this.guideId = guideId;
        this.guideName = guideName;
        this.passCount = passCount;
        this.passPercent = passPercent;
    }

    public int getGuideId() {
        return guideId;
    }

    public void setGuideId(int guideId) {
        this.guideId = guideId;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public int getPassCount() {
        return passCount;
    }

    public void setPassCount(int passCount) {
        this.passCount = passCount;
    }

    public String getPassPercent() {
        return DoubleUtils.floatToPercentKeep2( passPercent );
    }

    public void setPassPercent(float passPercent) {
        this.passPercent = passPercent;
    }

    public int compareTo(StatGuide o) {
        //先这么处理
        // 荣伟的新手引导100以内是大步骤，然后每个大步骤中的小步骤

        if(  o.guideId < 100 && this.guideId < 100  ){ // 都是大步骤
            return this.guideId - o.guideId;
        }else if( o.guideId >= 100 && this.guideId >= 100 ) { // 都是小步骤
            return this.guideId - o.guideId;
        }else{

            if( this.guideId < 100 ){
                int firstId = this.guideId;
                int secondId  = o.guideId / 100 ;

                if( firstId >= secondId ){
                    return 1;
                }else{
                    return -1;
                }
            }else{
                int firstId = this.guideId / 100 ;
                int secondId  = o.guideId ;

                if( firstId <= secondId ){
                    return -1;
                }else{
                    return 1;
                }
            }




        }

    }
}
