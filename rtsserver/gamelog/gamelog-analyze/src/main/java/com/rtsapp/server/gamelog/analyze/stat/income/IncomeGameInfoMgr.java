package com.rtsapp.server.gamelog.analyze.stat.income;

import com.rtsapp.server.gamelog.analyze.dao.IncomeDao;
import com.rtsapp.server.gamelog.analyze.stat.income.model.GameIdAndAreaIdInfo;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by liwang on 17-2-9.
 * <p>
 * 游戏信息管理
 */
public class IncomeGameInfoMgr {

    private final IncomeDao incomeDao;
    private final Timer timer;

    protected IncomeGameInfoMgr( IncomeDao incomeDao, Timer timer ) {
        this.incomeDao = incomeDao;
        this.timer = timer;

        initialize( );
    }

    private void initialize( ) {
        // 1小时同步一次
        timer.scheduleAtFixedRate( new TimerTask( ) {
            @Override
            public void run( ) {
                refreshGameInfoForDB( );
            }
        }, 10000, 20000 );
    }


    private void refreshGameInfoForDB( ) {

        List<GameIdAndAreaIdInfo> idList = incomeDao.selectAllGameIdAndAreaIdFromLoginLog( );
        List<GameIdAndAreaIdInfo> list = incomeDao.selectAllGameIdAndAreaIdFromGameInfo( );

        for ( int i = 0; i < idList.size( ); i++ ) {
            GameIdAndAreaIdInfo newIdInfo = idList.get( i );

            boolean include = false;
            for ( int m = 0; m < list.size( ); m++ ) {
                GameIdAndAreaIdInfo oldIdInfo = list.get( m );
                if ( oldIdInfo.gameId.equals( newIdInfo.gameId ) && oldIdInfo.areaId.equals( newIdInfo.areaId ) ) {
                    include = true;
                    break;
                }
            }

            if ( !include ) {
                incomeDao.insertGameInfo( newIdInfo.gameId, newIdInfo.areaId, null, null, null, "1" );
            }

        }

    }


}
