package com.rtsapp.server.gamelog.analyze.stat.income;

import com.rtsapp.server.gamelog.analyze.stat.StringUtils;

/**
 * Created by liwang on 17-1-10.
 */
public class IncomeViewData {

    // 游戏id
    private String gameId = null;
    // 游戏名称
    private String gameName;
    // 地区版本id
    private String areaId;
    // 地区版本名称
    private String areaName;

    // 今日实时收入 (实时查询)
    private int todayRealTime;
    // 昨日实时收入 (实时查询)
    private int yesterdayRealTime;
    // 昨日总计
    private int yesterdayTotal;
    // 前日总计
    private int dayBeforeYesterdayTotal;

    // 日均指标 (月指标/当月天数) (实时计算)
    private int dayAvgTarget;
    // 月指标 (人工设定) (实时查询)
    private int monthIncomeTarget;

    // 月累计 (不包括今日实时)
    private int monthTotal;

    // 汇率
    private double exchangeRate;

    // --------


    public String getGameId( ) {
        return gameId;
    }

    public void setGameId( String gameId ) {
        this.gameId = gameId;
    }

    public String getGameName( ) {
        return StringUtils.isEmpty( gameName ) ? gameId : gameName;
    }

    public void setGameName( String gameName ) {
        this.gameName = gameName;
    }

    public String getAreaId( ) {
        return areaId;
    }

    public void setAreaId( String areaId ) {
        this.areaId = areaId;
    }

    public String getAreaName( ) {
        return StringUtils.isEmpty( areaName ) ? areaId : areaName;
    }

    public void setAreaName( String areaName ) {
        this.areaName = areaName;
    }

    public int getTodayRealTime( ) {
        return todayRealTime;
    }

    public void setTodayRealTime( int todayRealTime ) {
        this.todayRealTime = todayRealTime;
    }

    public int getYesterdayRealTime( ) {
        return yesterdayRealTime;
    }

    public void setYesterdayRealTime( int yesterdayRealTime ) {
        this.yesterdayRealTime = yesterdayRealTime;
    }

    public int getYesterdayTotal( ) {
        return yesterdayTotal;
    }

    public void setYesterdayTotal( int yesterdayTotal ) {
        this.yesterdayTotal = yesterdayTotal;
    }

    public int getDayBeforeYesterdayTotal( ) {
        return dayBeforeYesterdayTotal;
    }

    public void setDayBeforeYesterdayTotal( int dayBeforeYesterdayTotal ) {
        this.dayBeforeYesterdayTotal = dayBeforeYesterdayTotal;
    }

    public int getDayAvgTarget( ) {
        return dayAvgTarget;
    }

    public void setDayAvgTarget( int dayAvgTarget ) {
        this.dayAvgTarget = dayAvgTarget;
    }

    public int getMonthIncomeTarget( ) {
        return monthIncomeTarget;
    }

    public void setMonthIncomeTarget( int monthIncomeTarget ) {
        this.monthIncomeTarget = monthIncomeTarget;
    }

    public int getMonthTotal( ) {
        return monthTotal;
    }

    public void setMonthTotal( int monthTotal ) {
        this.monthTotal = monthTotal;
    }

    public double getExchangeRate( ) {
        return exchangeRate;
    }

    public void setExchangeRate( double exchangeRate ) {
        this.exchangeRate = exchangeRate;
    }
}
