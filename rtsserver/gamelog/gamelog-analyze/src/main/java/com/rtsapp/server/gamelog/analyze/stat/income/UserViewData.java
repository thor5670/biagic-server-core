package com.rtsapp.server.gamelog.analyze.stat.income;

/**
 * Created by liwang on 17-1-10.
 */
public class UserViewData {

    // 游戏id
    private String gameId = null;
    // 游戏名称
    private String gameName;
    // 地区版本id
    private String areaId;
    // 地区版本名称
    private String areaName;

    // 今日新增 (实时查询)
    private int todayIncrement;
    // 月新增 (实时计算)
    private int monthIncrement;
    // 今日DAU (实时查询)
    private int todayDAU;
    // 昨日DAU总
    private int yesterdayDAU;
    // 今日APA (实时查询)
    private int todayAPA;
    // 昨日APA总
    private int yesterdayAPA;

    // 昨日次留
    private String yesterdayRetention1;


    // --------


    public String getGameId( ) {
        return gameId;
    }

    public void setGameId( String gameId ) {
        this.gameId = gameId;
    }

    public String getAreaId( ) {
        return areaId;
    }

    public void setAreaId( String areaId ) {
        this.areaId = areaId;
    }

    public int getTodayIncrement( ) {
        return todayIncrement;
    }

    public void setTodayIncrement( int todayIncrement ) {
        this.todayIncrement = todayIncrement;
    }

    public int getMonthIncrement( ) {
        return monthIncrement;
    }

    public void setMonthIncrement( int monthIncrement ) {
        this.monthIncrement = monthIncrement;
    }

    public int getTodayDAU( ) {
        return todayDAU;
    }

    public void setTodayDAU( int todayDAU ) {
        this.todayDAU = todayDAU;
    }

    public int getYesterdayDAU( ) {
        return yesterdayDAU;
    }

    public void setYesterdayDAU( int yesterdayDAU ) {
        this.yesterdayDAU = yesterdayDAU;
    }

    public int getTodayAPA( ) {
        return todayAPA;
    }

    public void setTodayAPA( int todayAPA ) {
        this.todayAPA = todayAPA;
    }

    public int getYesterdayAPA( ) {
        return yesterdayAPA;
    }

    public void setYesterdayAPA( int yesterdayAPA ) {
        this.yesterdayAPA = yesterdayAPA;
    }

    public String getYesterdayRetention1( ) {
        return yesterdayRetention1;
    }

    public void setYesterdayRetention1( String yesterdayRetention1 ) {
        this.yesterdayRetention1 = yesterdayRetention1;
    }

    public String getGameName( ) {
        return ( gameName == null || gameName.trim( ).isEmpty( ) ) ? gameId : gameName;
    }

    public void setGameName( String gameName ) {
        this.gameName = gameName;
    }

    public String getAreaName( ) {
        return ( areaName == null || areaName.trim( ).isEmpty( ) ) ? areaId : areaName;
    }

    public void setAreaName( String areaName ) {
        this.areaName = areaName;
    }


}
