package com.rtsapp.server.gamelog.analyze.stat.income.model;

/**
 * Created by liwang on 17-1-17.
 */
public class IncomeCacheData {

    // 游戏id
    private String gameId;
    // 地区版本id
    private String areaId;

    // 昨日总计
    private float yesterdayTotal;
    // 前日总计
    private float dayBeforeYesterdayTotal;


    // 月累计 (不包括今日实时)
    private float monthTotal;



    public String getGameId( ) {
        return gameId;
    }

    public void setGameId( String gameId ) {
        this.gameId = gameId;
    }

    public String getAreaId( ) {
        return areaId;
    }

    public void setAreaId( String areaId ) {
        this.areaId = areaId;
    }

    public float getYesterdayTotal( ) {
        return yesterdayTotal;
    }

    public void setYesterdayTotal( float yesterdayTotal ) {
        this.yesterdayTotal = yesterdayTotal;
    }

    public float getDayBeforeYesterdayTotal( ) {
        return dayBeforeYesterdayTotal;
    }

    public void setDayBeforeYesterdayTotal( float dayBeforeYesterdayTotal ) {
        this.dayBeforeYesterdayTotal = dayBeforeYesterdayTotal;
    }

    public float getMonthTotal( ) {
        return monthTotal;
    }

    public void setMonthTotal( float monthTotal ) {
        this.monthTotal = monthTotal;
    }
}
