package com.rtsapp.server.gamelog.analyze.stat.income.model;


import java.sql.Timestamp;

/**
 * Created by liwang on 17-1-11.
 */
public class IncomeQueryParam {

    String gameId;
    String areaId;
    Timestamp startTime;
    Timestamp endTime;


    public IncomeQueryParam( ) {
    }

    public IncomeQueryParam( String gameId, String areaId, Timestamp startTime, Timestamp endTime ) {
        this.gameId = gameId;
        this.areaId = areaId;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getGameId( ) {
        return gameId;
    }

    public void setGameId( String gameId ) {
        this.gameId = gameId;
    }

    public String getAreaId( ) {
        return areaId;
    }

    public void setAreaId( String areaId ) {
        this.areaId = areaId;
    }

    public Timestamp getStartTime( ) {
        return startTime;
    }

    public void setStartTime( Timestamp startTime ) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime( ) {
        return endTime;
    }

    public void setEndTime( Timestamp endTime ) {
        this.endTime = endTime;
    }
}
