package com.rtsapp.server.gamelog.analyze.stat.income.model;

/**
 * Created by liwang on 17-2-9.
 */
public class IncomeTargetInfo {

    public String gameId;
    public String areaId;
    public String year_Month_IncomeTarget_JsonStr;

}
