package com.rtsapp.server.gamelog.analyze.stat.income.model;

/**
 * Created by liwang on 17-1-17.
 */
public class UserCacheData {

    // 游戏id
    private String gameId;
    // 地区版本id
    private String areaId;

    // 月新增 (实时计算)
    private int monthIncrement;
    // 昨日DAU总
    private int yesterdayDAU;
    // 昨日APA总
    private int yesterdayAPA;

    // 昨日次留
    private float yesterdayRetention1;


    public String getGameId( ) {
        return gameId;
    }

    public void setGameId( String gameId ) {
        this.gameId = gameId;
    }

    public String getAreaId( ) {
        return areaId;
    }

    public void setAreaId( String areaId ) {
        this.areaId = areaId;
    }

    public int getMonthIncrement( ) {
        return monthIncrement;
    }

    public void setMonthIncrement( int monthIncrement ) {
        this.monthIncrement = monthIncrement;
    }

    public int getYesterdayDAU( ) {
        return yesterdayDAU;
    }

    public void setYesterdayDAU( int yesterdayDAU ) {
        this.yesterdayDAU = yesterdayDAU;
    }

    public int getYesterdayAPA( ) {
        return yesterdayAPA;
    }

    public void setYesterdayAPA( int yesterdayAPA ) {
        this.yesterdayAPA = yesterdayAPA;
    }

    public float getYesterdayRetention1( ) {
        return yesterdayRetention1;
    }

    public void setYesterdayRetention1( float yesterdayRetention1 ) {
        this.yesterdayRetention1 = yesterdayRetention1;
    }
}
