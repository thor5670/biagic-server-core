package com.rtsapp.server.gamelog.analyze.stat.levelrunoff;

/**
 * 查询出来的结构:level等级_number数量
 * Created by admin on 16/11/14.
 */
public class LevelRunOffLevelNum {
    private int level;
    private int number;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
