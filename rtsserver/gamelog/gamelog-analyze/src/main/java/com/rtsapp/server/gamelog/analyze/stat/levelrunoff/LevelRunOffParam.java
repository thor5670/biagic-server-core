package com.rtsapp.server.gamelog.analyze.stat.levelrunoff;

import java.sql.Timestamp;

/**
 * 等级流失分布的查询参数
 * Created by admin on 16/11/2.
 */
public class LevelRunOffParam {

    private Timestamp startTime;

    private Timestamp endTime;

    //服务器组ID,空代表全部
    private String[] serverIds;

    //渠道组, 空代表全部
    private String[] channelNames;

    public Timestamp getStartTime( ) {
        return startTime;
    }

    public void setStartTime( Timestamp startTime ) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime( ) {
        return endTime;
    }

    public void setEndTime( Timestamp endTime ) {
        this.endTime = endTime;
    }

    public String[] getServerIds( ) {
        return serverIds;
    }

    public void setServerIds( String[] serverIds ) {
        this.serverIds = serverIds;
    }

    public String[] getChannelNames() {
        return channelNames;
    }

    public void setChannelNames(String[] channelNames) {
        this.channelNames = channelNames;
    }
}
