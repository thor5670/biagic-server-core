package com.rtsapp.server.gamelog.analyze.stat.levelrunoff;

/**
 * 等级流失分布展示参数结构
 * Created by admin on 16/11/10.
 */
public class LevelRunOffViewData {

    //等级
    private int level;

    //玩家数
    private int counts;

    //流失率(此流失率会乘以100,因为展示时会在其后加上%)
    private Double runOffRatio;

    //累计流失率(此流失率会乘以100,因为展示时会在其后加上%)
    private Double accumulateRunOffRatio;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public Double getRunOffRatio() {
        return runOffRatio;
    }

    public void setRunOffRatio(Double runOffRatio) {
        this.runOffRatio = runOffRatio == null ? 0 : runOffRatio;
    }

    public Double getAccumulateRunOffRatio() {
        return accumulateRunOffRatio;
    }

    public void setAccumulateRunOffRatio(Double accumulateRunOffRatio) {
        this.accumulateRunOffRatio = accumulateRunOffRatio == null ? 0 : accumulateRunOffRatio;
    }
}
