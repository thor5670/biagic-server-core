package com.rtsapp.server.gamelog.analyze.stat.payment;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.dao.PaymentDao;
import com.rtsapp.server.gamelog.analyze.stat.ChannelUtils;
import com.rtsapp.server.gamelog.analyze.stat.DateUtils;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import static com.rtsapp.server.gamelog.analyze.stat.DateUtils.getDayStartTime_MS;

/**
 * Created by liwang on 16-7-29.
 */
public class PaymentDailyUtil {

    public static final byte TODAY = 1;
    public static final byte YESTERDAY_SAME_TIME = 2;
    public static final byte YESTERDAY_WHOLE_DAY = 3;

    public static PaymentDailyViewData buildViewData( final CommonDao dao, PaymentDao paymentDao, HttpServletRequest request, byte type, long timeNow_MS ) {

        PaymentQueryParam param = makeQueryParamFromRequest( dao, request, type, timeNow_MS );
        if ( param == null || !ChannelUtils.isChannelsValid( param.getChannelNames( ) ) ) {
            return null;
        }

        PaymentDailyViewData viewData = paymentDao.selectPayNum( param );
        if ( viewData == null ) {
            viewData = new PaymentDailyViewData( );
        }


        viewData.setDAU( paymentDao.selectDAU( param ) );
        viewData.setAPA( paymentDao.selectAPA( param ) );
        viewData.setPayGoldNum( paymentDao.selectPayGoldNum( param ) );
        viewData.setSystemGoldNum( paymentDao.selectSystemGoldNum( param ) );
        viewData.setCostGold( paymentDao.selectCostNum( param ) );
        viewData.setCostPlayerNum( paymentDao.selectCostPlayerNum( param ) );
        viewData.setPCU( paymentDao.selectPCU( param ) );

        setDateInfo( viewData, type, timeNow_MS, param );

        return viewData;
    }

    private static void setDateInfo( PaymentDailyViewData viewData, byte type, long timeNow_MS, PaymentQueryParam param ) {

        if ( type == TODAY ) {
            long startTime_MS = param.getStartTime( ).getTime( );
            if ( DateUtils.isSameDay( startTime_MS, timeNow_MS ) ) {
                viewData.setYearMonthDay( "今日实时" );
            } else {
                viewData.setYearMonthDay( new SimpleDateFormat( "yyyy-MM-dd" ).format( startTime_MS ) );
            }

        } else if ( type == YESTERDAY_SAME_TIME ) {
            viewData.setYearMonthDay( "昨日同期" );
        } else if ( type == YESTERDAY_WHOLE_DAY ) {
            viewData.setYearMonthDay( "昨日全天" );
        }
    }


    /**
     * 将request中的数据构建成查询数据库的参数PaymentQueryParam
     *
     * @param request
     * @return
     */
    public  static PaymentQueryParam makeQueryParamFromRequest( final CommonDao dao, HttpServletRequest request, byte type, long timeNow_MS ) {

        final PaymentQueryParam param = new PaymentQueryParam( );


        String selectTimeStr = request.getParameter( "startDay" );
        makeTimeParam( param, selectTimeStr, timeNow_MS, type );
        if ( param.getStartTime( ) == null ) {
            return null;
        }

        param.setServerIds( request.getParameterValues( "serverIds" ) );
        param.setChannelNames( ChannelUtils.getChannels( dao, request, "channelNames" ) );

//        Timestamp startTimestamp = getStartTimestamp( selectTimeStr, timeNow_MS, type );
//        Timestamp endTimestamp = getEndTimestamp( selectTimeStr, timeNow_MS, type );

        return param;

    }

    public static void makeTimeParam( PaymentQueryParam param, String selectTimeStr, final long timeNow_MS, final byte type ) {

        long startTime_MS = ( selectTimeStr == null || selectTimeStr.isEmpty( ) ) ? timeNow_MS : 0;
        if ( startTime_MS == 0 ) {
            try {
                startTime_MS = new SimpleDateFormat( "yyyy-MM-dd" ).parse( selectTimeStr ).getTime( );
            } catch ( ParseException e ) {
                e.printStackTrace( );
                startTime_MS = getDayStartTime_MS( timeNow_MS );
            }
        }

        long endTime_MS = 0;
        boolean selectTimeIsToday = DateUtils.isSameDay( startTime_MS, timeNow_MS );

        if ( type == TODAY ) {
            startTime_MS = getDayStartTime_MS( startTime_MS );
            endTime_MS = startTime_MS + DateUtils.DAY_IN_MILLIS;
        } else if ( type == YESTERDAY_SAME_TIME ) {
            if ( !selectTimeIsToday ) {
                return;
            }
            startTime_MS = getDayStartTime_MS( startTime_MS ) - DateUtils.DAY_IN_MILLIS;
            endTime_MS = startTime_MS + ( timeNow_MS + TimeZone.getDefault( ).getRawOffset( ) ) % DateUtils.DAY_IN_MILLIS;
        } else if ( type == YESTERDAY_WHOLE_DAY ) {
            endTime_MS = getDayStartTime_MS( startTime_MS );
            startTime_MS = endTime_MS - DateUtils.DAY_IN_MILLIS;
        }


        param.setStartTime( new Timestamp( startTime_MS ) );
        param.setEndTime( new Timestamp( endTime_MS ) );

    }

    /**
     * 根据所选时间获得开始时间
     *
     * @param selectTimeStr
     * @param timeNow_MS
     * @return
     */
    public static Timestamp getStartTimestamp( String selectTimeStr, long timeNow_MS, byte type ) {

        if ( type != TODAY ) {
            timeNow_MS -= DateUtils.DAY_IN_MILLIS;
        }

        // 未选择时间, 开始时间设为当天0点0时0分
        if ( selectTimeStr == null || selectTimeStr.isEmpty( ) ) {
            return new Timestamp( getDayStartTime_MS( timeNow_MS ) );
        }

        try {
            // 开始时间设为选择的时间点
            return new Timestamp( new SimpleDateFormat( "yyyy-MM-dd" ).parse( selectTimeStr ).getTime( ) );
        } catch ( ParseException e ) {
            e.printStackTrace( );

            // error. 格式化出错, 开始时间设为当天0点0时0分
            return new Timestamp( getDayStartTime_MS( timeNow_MS ) );
        }

    }

    /**
     * 根据所选时间获得结束时间
     *
     * @param selectTimeStr
     * @param timeNow_MS
     * @return
     */
    public static Timestamp getEndTimestamp( String selectTimeStr, long timeNow_MS, byte type ) {


        // 未选择时间, 结束时间设为当前时间
        if ( selectTimeStr == null || selectTimeStr.isEmpty( ) ) {
            return new Timestamp( timeNow_MS );
        }

        long selectTime = 0;
        try {
            selectTime = new SimpleDateFormat( "yyyy-MM-dd" ).parse( selectTimeStr ).getTime( );
        } catch ( ParseException e ) {
            e.printStackTrace( );

            // error. 格式化时间出错, 结束时间设为当前时间
            return new Timestamp( timeNow_MS );
        }

        // 选择的时间是今天, 结束时间设为当前时间
        if ( DateUtils.isSameDay( selectTime, timeNow_MS ) ) {
            return new Timestamp( timeNow_MS );
        }

        // 选择的时间不是今天, 结束时间设为选择的时间点24小时后
        return new Timestamp( selectTime + DateUtils.DAY_IN_MILLIS );
    }


}
