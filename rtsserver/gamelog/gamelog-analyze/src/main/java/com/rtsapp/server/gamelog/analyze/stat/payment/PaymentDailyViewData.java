package com.rtsapp.server.gamelog.analyze.stat.payment;

/**
 * Created by liwang on 16-7-29.
 */
public class PaymentDailyViewData {

    // 日期
    private String yearMonthDay;

    // 活跃用户
    private int DAU;

    // 充值金额
    private double payNum;

    // 活跃付费用户
    private int APA;

    // 充值获得的金币
    private int payGoldNum;

    // 系统赠送的金币
    private int systemGoldNum;

    // 消费的金币
    private int costGold;

    // 消费的人数
    private int costPlayerNum;

    // 最高同时在线人数
    private int PCU;

    // 金币增量合计
    public int getTotalGoldNum( ) {
        return payGoldNum + systemGoldNum;
    }

    public String getYearMonthDay( ) {
        return yearMonthDay;
    }

    public void setYearMonthDay( String yearMonthDay ) {
        this.yearMonthDay = yearMonthDay;
    }

    public int getDAU( ) {
        return DAU;
    }

    public void setDAU( Integer DAU ) {
        this.DAU = DAU == null ? 0 : DAU;
    }

    public double getPayNum( ) {
        return payNum;
    }

    public void setPayNum( Double payNum ) {
        this.payNum = payNum == null ? 0 : payNum;
    }

    public int getAPA( ) {
        return APA;
    }

    public void setAPA( Integer APA ) {
        this.APA = APA == null ? 0 : APA;
    }

    public int getPayGoldNum( ) {
        return payGoldNum;
    }

    public void setPayGoldNum( Integer payGoldNum ) {
        this.payGoldNum = payGoldNum == null ? 0 : payGoldNum;
    }

    public int getSystemGoldNum( ) {
        return systemGoldNum;
    }

    public void setSystemGoldNum( Integer systemGoldNum ) {
        this.systemGoldNum = systemGoldNum == null ? 0 : systemGoldNum;
    }

    public int getCostGold( ) {
        return costGold;
    }

    public void setCostGold( Integer costGold ) {
        this.costGold = costGold == null ? 0 : costGold;
    }

    public int getPCU( ) {
        return PCU;
    }

    public void setPCU( Integer PCU ) {
        this.PCU = PCU == null ? 0 : PCU;
    }

    public int getCostPlayerNum( ) {
        return costPlayerNum;
    }

    public void setCostPlayerNum( Integer costPlayerNum ) {
        this.costPlayerNum = costPlayerNum == null ? 0 : costPlayerNum;
    }
}
