package com.rtsapp.server.gamelog.analyze.stat.payment;

import com.rtsapp.server.gamelog.analyze.dao.CommonDao;
import com.rtsapp.server.gamelog.analyze.dao.PaymentDao;
import com.rtsapp.server.gamelog.analyze.stat.ChannelUtils;
import com.rtsapp.server.gamelog.analyze.stat.DateUtils;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import static com.rtsapp.server.gamelog.analyze.stat.DateUtils.getDayStartTime_MS;
import static com.rtsapp.server.gamelog.analyze.stat.payment.PaymentDailyUtil.*;

/**
 * Created by liwang on 16-7-29.
 */
public class PaymentIntervalUtil {

    private static final int[] intervalInfos = new int[]{ 0, 30, 100, 200, 500, 1000, 2000, 3000, 10000 };

    public static List<PaymentIntervalViewData> buildViewData( PaymentDao paymentDao, CommonDao commonDao, HttpServletRequest request ) {

        final List<PaymentIntervalViewData> list = new ArrayList<>( );
        final long timeNow_MS = System.currentTimeMillis( );
        int totalPlayerNum = 0;

        PaymentQueryParam param = makeQueryParamFromRequest( request, commonDao, timeNow_MS );
        if ( param == null || !ChannelUtils.isChannelsValid( param.getChannelNames( ) ) ) {
            return null;
        }

        for ( int i = 0; i < intervalInfos.length; i++ ) {
            param.setMin( intervalInfos[ i ] );
            param.setMax( ( i + 1 ) >= intervalInfos.length ? null : intervalInfos[ i + 1 ] );

            PaymentIntervalViewData viewData = paymentDao.selectPayIntervalInfo( param );
            if ( viewData != null ) {

                viewData.setPayInterval( intervalInfos[ i ] + ( ( i + 1 ) >= intervalInfos.length ? "以上" : ( "-" + intervalInfos[ i + 1 ] ) ) );

                list.add( viewData );
                totalPlayerNum += viewData.getPlayerNum( );
            }
        }

        for ( int i = 0; i < list.size( ); i++ ) {
            PaymentIntervalViewData viewData = list.get( i );
            viewData.setPlayerPercent( totalPlayerNum == 0 ? 0 : viewData.getPlayerNum( ) / ( double ) totalPlayerNum );
        }


        return list;
    }

    /**
     * 将request中的数据构建成查询数据库的参数PaymentQueryParam
     *
     * @param request
     * @return
     */
    private static PaymentQueryParam makeQueryParamFromRequest( HttpServletRequest request, final CommonDao commonDao, final long timeNow_MS ) {

        final PaymentQueryParam param = new PaymentQueryParam( );
//        param.setChannelNames( request.getParameterValues( "channelNames" ) );
        param.setChannelNames( ChannelUtils.getChannels( commonDao, request, "channelNames" ) );
        if ( !ChannelUtils.isChannelsValid( param.getChannelNames( ) ) ) {
            return null;
        }
        param.setServerIds( request.getParameterValues( "serverIds" ) );

        String selectTimeStr = request.getParameter( "startDay" );
        long startTime_MS = ( selectTimeStr == null || selectTimeStr.isEmpty( ) ) ? getDayStartTime_MS( timeNow_MS ) : 0;
        if ( startTime_MS == 0 ) {
            try {
                startTime_MS = new SimpleDateFormat( "yyyy-MM-dd" ).parse( selectTimeStr ).getTime( );
            } catch ( ParseException e ) {
                e.printStackTrace( );
                startTime_MS = getDayStartTime_MS( timeNow_MS );
            }
        }

        param.setStartTime( new Timestamp( startTime_MS ) );
        param.setEndTime( new Timestamp( startTime_MS + DateUtils.DAY_IN_MILLIS ) );

        return param;

    }

    public static void makeTimeParam( PaymentQueryParam param, String selectTimeStr, final long timeNow_MS, final byte type ) {

        long startTime_MS = ( selectTimeStr == null || selectTimeStr.isEmpty( ) ) ? timeNow_MS : 0;
        if ( startTime_MS == 0 ) {
            try {
                startTime_MS = new SimpleDateFormat( "yyyy-MM-dd" ).parse( selectTimeStr ).getTime( );
            } catch ( ParseException e ) {
                e.printStackTrace( );
                startTime_MS = getDayStartTime_MS( timeNow_MS );
            }
        }

        long endTime_MS = 0;
        boolean selectTimeIsToday = DateUtils.isSameDay( startTime_MS, timeNow_MS );

        if ( type == TODAY ) {
            startTime_MS = getDayStartTime_MS( startTime_MS );
            if ( selectTimeIsToday ) {
                endTime_MS = timeNow_MS;
            } else {
                endTime_MS = startTime_MS + ( timeNow_MS + TimeZone.getDefault( ).getRawOffset( ) ) % DateUtils.DAY_IN_MILLIS;
            }
        } else if ( type == YESTERDAY_SAME_TIME ) {
            if ( !selectTimeIsToday ) {
                return;
            }
            startTime_MS = getDayStartTime_MS( startTime_MS ) - DateUtils.DAY_IN_MILLIS;
            endTime_MS = startTime_MS + ( timeNow_MS + TimeZone.getDefault( ).getRawOffset( ) ) % DateUtils.DAY_IN_MILLIS;
        } else if ( type == YESTERDAY_WHOLE_DAY ) {
            endTime_MS = getDayStartTime_MS( startTime_MS );
            startTime_MS = endTime_MS - DateUtils.DAY_IN_MILLIS;
        }


        param.setStartTime( new Timestamp( startTime_MS ) );
        param.setEndTime( new Timestamp( endTime_MS ) );

    }

    /**
     * 根据所选时间获得开始时间
     *
     * @param selectTimeStr
     * @param timeNow_MS
     * @return
     */
    public static Timestamp getStartTimestamp( String selectTimeStr, long timeNow_MS, byte type ) {

        if ( type != TODAY ) {
            timeNow_MS -= DateUtils.DAY_IN_MILLIS;
        }

        // 未选择时间, 开始时间设为当天0点0时0分
        if ( selectTimeStr == null || selectTimeStr.isEmpty( ) ) {
            return new Timestamp( getDayStartTime_MS( timeNow_MS ) );
        }

        try {
            // 开始时间设为选择的时间点
            return new Timestamp( new SimpleDateFormat( "yyyy-MM-dd" ).parse( selectTimeStr ).getTime( ) );
        } catch ( ParseException e ) {
            e.printStackTrace( );

            // error. 格式化出错, 开始时间设为当天0点0时0分
            return new Timestamp( getDayStartTime_MS( timeNow_MS ) );
        }

    }

    /**
     * 根据所选时间获得结束时间
     *
     * @param selectTimeStr
     * @param timeNow_MS
     * @return
     */
    public static Timestamp getEndTimestamp( String selectTimeStr, long timeNow_MS, byte type ) {


        // 未选择时间, 结束时间设为当前时间
        if ( selectTimeStr == null || selectTimeStr.isEmpty( ) ) {
            return new Timestamp( timeNow_MS );
        }

        long selectTime = 0;
        try {
            selectTime = new SimpleDateFormat( "yyyy-MM-dd" ).parse( selectTimeStr ).getTime( );
        } catch ( ParseException e ) {
            e.printStackTrace( );

            // error. 格式化时间出错, 结束时间设为当前时间
            return new Timestamp( timeNow_MS );
        }

        // 选择的时间是今天, 结束时间设为当前时间
        if ( DateUtils.isSameDay( selectTime, timeNow_MS ) ) {
            return new Timestamp( timeNow_MS );
        }

        // 选择的时间不是今天, 结束时间设为选择的时间点24小时后
        return new Timestamp( selectTime + DateUtils.DAY_IN_MILLIS );
    }



}
