package com.rtsapp.server.gamelog.analyze.stat.payment;

import com.rtsapp.server.gamelog.analyze.stat.DoubleUtils;

/**
 * Created by liwang on 16-8-1.
 */
public class PaymentIntervalViewData {

    // 区间
    private String payInterval;

    // 充值人数
    private int playerNum;

    // 人数占比
    private double playerPercent;

    // 充值额
    private double payNum;


    public String getPayInterval( ) {
        return payInterval;
    }

    public void setPayInterval( String payInterval ) {
        this.payInterval = payInterval;
    }

    public int getPlayerNum( ) {
        return playerNum;
    }

    public void setPlayerNum( int playerNum ) {
        this.playerNum = playerNum;
    }

    public String getPlayerPercent( ) {
        return DoubleUtils.doubleToPercentKeep2( playerPercent );
    }

    public void setPlayerPercent( double playerPercent ) {
        this.playerPercent = playerPercent;
    }

    public double getPayNum( ) {
        return payNum;
    }

    public void setPayNum( double payNum ) {
        this.payNum = payNum;
    }
}
