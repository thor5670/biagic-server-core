package com.rtsapp.server.gamelog.analyze.stat.payment;

import java.sql.Timestamp;

/**
 * Created by liwang on 16-7-29.
 */
public class PaymentQueryParam {

    // 服务器组
    private String[] serverIds;

    // 渠道组
    private String[] channelNames;

    private Timestamp startTime;

    private Timestamp endTime;

    // 最小值
    private int min;

    // 最大值
    private Integer max;


    public PaymentQueryParam( ) {

    }

    public PaymentQueryParam( String[] serverIds, String[] channelNames, Timestamp startTime, Timestamp endTime ) {

        this.serverIds = serverIds;
        this.channelNames = channelNames;
        this.startTime = startTime;
        this.endTime = endTime;

    }


    public String[] getServerIds( ) {
        return serverIds;
    }

    public void setServerIds( String[] serverIds ) {
        this.serverIds = serverIds;
    }

    public String[] getChannelNames( ) {
        return channelNames;
    }

    public void setChannelNames( String[] channelNames ) {
        this.channelNames = channelNames;
    }

    public Timestamp getStartTime( ) {
        return startTime;
    }

    public void setStartTime( Timestamp startTime ) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime( ) {
        return endTime;
    }

    public void setEndTime( Timestamp endTime ) {
        this.endTime = endTime;
    }

    public int getMin( ) {
        return min;
    }

    public void setMin( int min ) {
        this.min = min;
    }

    public Integer getMax( ) {
        return max;
    }

    public void setMax( Integer max ) {
        this.max = max;
    }
}
