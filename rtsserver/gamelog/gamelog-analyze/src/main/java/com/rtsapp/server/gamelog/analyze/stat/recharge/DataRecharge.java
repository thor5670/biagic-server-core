package com.rtsapp.server.gamelog.analyze.stat.recharge;

import java.util.Date;

/**
 * Created by admin on 17/1/17.
 */
public class DataRecharge {
    private Date logTime;

    private int userId;

    private double productPrice;

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }
}
