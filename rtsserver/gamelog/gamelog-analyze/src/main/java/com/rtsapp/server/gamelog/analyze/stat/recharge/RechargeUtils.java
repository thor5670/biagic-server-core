package com.rtsapp.server.gamelog.analyze.stat.recharge;

import com.rtsapp.server.gamelog.analyze.stat.DateUtils;
import com.rtsapp.server.gamelog.analyze.stat.daily.DataRegister;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by admin on 17/1/16.
 */
public class RechargeUtils {

    //查询开始时间
    private final Date startDay;
    //查询结束时间
    private final Date endDay;
    //注册信息集
    private final Map<String, Set<Integer>> registerMap = new HashMap<>();
    //充值信息集
    private final Map<String, List<DataRecharge>> rechargeMap = new HashMap<>();

    public RechargeUtils(Date startDay, Date endDay, List<DataRegister> registerList, List<DataRecharge> rechargeList) {
        this.startDay = startDay;
        this.endDay = endDay;

        {
            //初始化注册信息
            for (Date date = startDay; DateUtils.isDateBefore(date, endDay); date = DateUtils.increaseDate(date, 1)) {
                //得到需要展示的日期
                registerMap.put(convertDateToString(date), new HashSet<>());
            }

            for (DataRegister register : registerList) {
                //循环遍历注册信息,将同一天注册的玩家放在一起
                Set<Integer> registerIds = registerMap.get(convertDateToString(register.getLogTime()));
                registerIds.add(register.getUserId());
            }

        }

        {
            //初始化充值信息
            for (Date date = startDay; DateUtils.isDateBefore(date, endDay); date = DateUtils.increaseDate(date, 1)) {
                rechargeMap.put(convertDateToString(date), new ArrayList<>());
            }

            for (DataRecharge recharge : rechargeList) {
                rechargeMap.get(convertDateToString(recharge.getLogTime())).add(recharge);
            }

        }

    }

    public List<RechargeViewData> stat() {

        //展示集
        List<RechargeViewData> rechargeViewDataList = new ArrayList<>();

        for (Date date = startDay; DateUtils.isDateBefore(date, endDay); date = DateUtils.increaseDate(date, 1)) {

            String _date = convertDateToString(date);
            RechargeViewData rechargeViewData = new RechargeViewData(_date);

            //今日新注册用户
            Set<Integer> registerIds = registerMap.get(_date);
            int registerNum = registerIds.size();
            rechargeViewData.setDNU(registerNum);
            if (registerNum == 0) {
                rechargeViewDataList.add(rechargeViewData);
                continue;
            }

            //1.3.7.30.累计日apa和应收金额
            setApaRec(rechargeViewData, registerIds, date);
            rechargeViewDataList.add(rechargeViewData);

        }
        return rechargeViewDataList;
    }

    //分别添加apa,rec的值
    private void setApaRec(RechargeViewData rechargeViewData, Set<Integer> registerIds, Date day) {

        //30天之后的日期
        Date afterDate30 = DateUtils.increaseDate(day, 30);
        //获取最远日期
        Date maxDate = DateUtils.isDateBefore(afterDate30, endDay) ? endDay : afterDate30;

        //第几天标识
        int dayth = 0;
        final Set<Integer> apaUserIds = new HashSet<>();
        double totalRecharge = 0.0;
        //计算今日的首日apa:rec,3日apa:rec,7日apa:rec,30日apa:rec,累计apa:rec
        for (Date date = day; DateUtils.isDateBefore(date, maxDate); date = DateUtils.increaseDate(date, 1)) {

            dayth++;

            List<DataRecharge> dataRechargeList = rechargeMap.get(convertDateToString(date));

            if (dataRechargeList != null && dataRechargeList.size() > 0) {
                for (DataRecharge dataRecharge : dataRechargeList) {
                    //今日注册用户今日充值
                    if (registerIds.contains(dataRecharge.getUserId())) {
                        //apa添加此用户
                        apaUserIds.add(dataRecharge.getUserId());
                        //累计消费金额
                        totalRecharge += dataRecharge.getProductPrice();
                    }
                }
            }

            config(dayth, apaUserIds, totalRecharge, rechargeViewData);

            if (DateUtils.isSameDay(date.getTime(), endDay.getTime()-1)) {
                rechargeViewData.setTotalAPA(apaUserIds.size());
                rechargeViewData.setTotalRec(formatDouble(totalRecharge / rechargeViewData.getDNU()));
            }

        }

    }

    //根据dayth适配并保存进相应的字段
    private void config(int dayth, Set<Integer> apaUserIds, double totalRecharge, RechargeViewData rechargeViewData) {
        if (dayth == 1) {
            rechargeViewData.setFirstDayAPA(apaUserIds.size());
            rechargeViewData.setFirstDayRec(formatDouble(totalRecharge / rechargeViewData.getDNU()));
        } else if (dayth == 3) {
            rechargeViewData.setThreeDayAPA(apaUserIds.size());
            rechargeViewData.setThreeDayRec(formatDouble(totalRecharge / rechargeViewData.getDNU()));
        } else if (dayth == 7) {
            rechargeViewData.setSevenDayAPA(apaUserIds.size());
            rechargeViewData.setSevenDayRec(formatDouble(totalRecharge / rechargeViewData.getDNU()));
        } else if (dayth == 30) {
            rechargeViewData.setThirtyDayAPA(apaUserIds.size());
            rechargeViewData.setThirtyDayRec(formatDouble(totalRecharge / rechargeViewData.getDNU()));
        }
    }

    private String convertDateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    private double formatDouble(double number){
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        return Double.parseDouble(decimalFormat.format(number));
    }
}
