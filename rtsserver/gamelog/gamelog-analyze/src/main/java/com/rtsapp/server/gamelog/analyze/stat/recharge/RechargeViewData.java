package com.rtsapp.server.gamelog.analyze.stat.recharge;

import java.util.Date;

/**
 * Created by admin on 16/10/31.
 */
public class RechargeViewData {

    private String date;

    private int DNU;

    private int firstDayAPA;

    private int threeDayAPA;

    private int sevenDayAPA;

    private int thirtyDayAPA;

    private int totalAPA;

    private Double firstDayRec;

    private Double threeDayRec;

    private Double sevenDayRec;

    private Double thirtyDayRec;

    private Double totalRec;

    public RechargeViewData(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDNU() {
        return DNU;
    }

    public void setDNU(int DNU) {
        this.DNU = DNU;
    }

    public int getFirstDayAPA() {
        return firstDayAPA;
    }

    public void setFirstDayAPA(int firstDayAPA) {
        this.firstDayAPA = firstDayAPA;
    }

    public int getThreeDayAPA() {
        return threeDayAPA;
    }

    public void setThreeDayAPA(int threeDayAPA) {
        this.threeDayAPA = threeDayAPA;
    }

    public int getSevenDayAPA() {
        return sevenDayAPA;
    }

    public void setSevenDayAPA(int sevenDayAPA) {
        this.sevenDayAPA = sevenDayAPA;
    }

    public int getThirtyDayAPA() {
        return thirtyDayAPA;
    }

    public void setThirtyDayAPA(int thirtyDayAPA) {
        this.thirtyDayAPA = thirtyDayAPA;
    }

    public int getTotalAPA() {
        return totalAPA;
    }

    public void setTotalAPA(int totalAPA) {
        this.totalAPA = totalAPA;
    }

    public Double getFirstDayRec() {
        return firstDayRec;
    }

    public void setFirstDayRec(Double firstDayRec) {
        this.firstDayRec = firstDayRec;
    }

    public Double getThreeDayRec() {
        return threeDayRec;
    }

    public void setThreeDayRec(Double threeDayRec) {
        this.threeDayRec = threeDayRec;
    }

    public Double getSevenDayRec() {
        return sevenDayRec;
    }

    public void setSevenDayRec(Double sevenDayRec) {
        this.sevenDayRec = sevenDayRec;
    }

    public Double getThirtyDayRec() {
        return thirtyDayRec;
    }

    public void setThirtyDayRec(Double thirtyDayRec) {
        this.thirtyDayRec = thirtyDayRec;
    }

    public Double getTotalRec() {
        return totalRec;
    }

    public void setTotalRec(Double totalRec) {
        this.totalRec = totalRec;
    }
}
