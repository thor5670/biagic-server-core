package com.rtsapp.server.gamelog.analyze.stat.rechargedetail;

import java.util.Date;

/**
 * Created by admin on 16/12/9.
 */
public class RechargeDetail {

    //服务器ID
    int serverId;
    //玩家ID
    int userId;
    //充值金额
    double money;
    //充值时间
    Date logTime;

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }
}
