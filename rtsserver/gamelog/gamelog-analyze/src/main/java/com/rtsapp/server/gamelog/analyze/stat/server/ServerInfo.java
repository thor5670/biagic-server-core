package com.rtsapp.server.gamelog.analyze.stat.server;

/**
 * Created by admin on 16-7-27.
 */
public class ServerInfo {

    private String serverId;
    private String serverName;


    public ServerInfo() {
    }

    public ServerInfo(String serverId, String serverName) {
        this.serverId = serverId;
        this.serverName = serverName;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }
}
