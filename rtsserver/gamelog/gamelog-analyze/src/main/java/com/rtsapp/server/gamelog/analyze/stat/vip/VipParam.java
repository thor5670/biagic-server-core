package com.rtsapp.server.gamelog.analyze.stat.vip;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by admin on 16/11/2.
 */
public class VipParam {

    private Timestamp startTime;
    private Timestamp endTime;
    private int vipLevel;
    private String[] serverIds;

    public Timestamp getStartTime( ) {
        return startTime;
    }

    public void setStartTime( Timestamp startTime ) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime( ) {
        return endTime;
    }

    public void setEndTime( Timestamp endTime ) {
        this.endTime = endTime;
    }

    public int getVipLevel( ) {
        return vipLevel;
    }

    public void setVipLevel( int vipLevel ) {
        this.vipLevel = vipLevel;
    }

    public String[] getServerIds( ) {
        return serverIds;
    }

    public void setServerIds( String[] serverIds ) {
        this.serverIds = serverIds;
    }
}
