package com.rtsapp.server.gamelog.analyze.stat.vip;

import com.rtsapp.server.gamelog.analyze.stat.DateUtils;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by admin on 16/11/2.
 */
public class VipParamUtils {

    //构建vip查询参数
    public static VipParam getVipParam( HttpServletRequest request, int vipLevel ) {

        VipParam param = new VipParam( );

        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );

        try {
            //统计的开始时间
            String _startDay = request.getParameter( "startDay" );
            Date startDay = sdf.parse( _startDay );
            param.setStartTime( new Timestamp( startDay.getTime( ) ) );
            //统计的结束时间,需要的是结束时间的23:59,所以需要加一天
            String _endDay = request.getParameter( "endDay" );
            Date tempEndDay = sdf.parse( _endDay );
            Date endDay = DateUtils.increaseDate( tempEndDay, 1 );
            param.setEndTime( new Timestamp( endDay.getTime( ) ) );

        } catch ( Exception e ) {
            e.printStackTrace( );
        }
        //服务器
        String[] serverIds = request.getParameterValues( "serverIds" );
        if ( serverIds != null && serverIds.length > 0 ) {
            param.setServerIds( serverIds );
        }

        //vip等级
        param.setVipLevel( vipLevel );

        return param;
    }
}
