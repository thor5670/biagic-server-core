package com.rtsapp.server.gamelog.analyze.stat.vip;

/**
 * Created by admin on 16/11/2.
 */
public class VipUserId {

    private String userId;
    private int vipLevel;

    public String getUserId( ) {
        return userId;
    }

    public void setUserId( String userId ) {
        this.userId = userId;
    }

    public int getVipLevel( ) {
        return vipLevel;
    }

    public void setVipLevel( int vipLevel ) {
        this.vipLevel = vipLevel;
    }
}
