package com.rtsapp.server.gamelog.analyze.stat.vip;

/**
 * Created by admin on 16/11/3.
 */
public class VipViewData {
    private int vipLevel;
    private int counts;


    public int getVipLevel( ) {
        return vipLevel;
    }

    public void setVipLevel( int vipLevel ) {
        this.vipLevel = vipLevel;
    }

    public int getCounts( ) {
        return counts;
    }

    public void setCounts( int counts ) {
        this.counts = counts;
    }
}
