<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

	<%@ include file="../include_top.jsp" %>


	<div class="searchParam" >
		<jsp:include page="/common/param" flush="true" />

		<div>
			<table border="0" width="1000">

				<tr>
					<td>
						行为选择:
						<c:forEach items="${actionTypes}" var="actionType" varStatus="state">
							<input name="actionTypes" type="checkbox" value="${actionType.action}" />
							<label> ${actionType.action} </label>
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>
						<input class="submit" id="submit" type="button" value="查 询"  onclick="search();">
					</td>
				</tr>
			</table>
		</div>
	</div>


	<div class="searchResult" id="searchResult" >
	</div>

	<script type="text/javascript">

		document.all.title.innerText = "玩家行为查询";
		document.all.nav_action.style.color = "red";

    		function search( ){

    			var actionTypes = $("input[name='actionTypes']:checked").serialize();
    			var startDay = $("#startDay").val();
    			var endDay = $("#endDay").val();

    			var postData = "startDay=" + startDay + "&endDay=" + endDay + "&" + getServerIds() + "&" + getChannelNames() + "&" + actionTypes;

    			$.ajax({
    				url:"/action/search",
    				type:"post",
    				data:postData,
					beforeSend: function () {
						$("#searchResult").html("正在查询...");
						$("#submit").attr("disabled","disabled");
					},
					success:function( result ){
						$("#searchResult").html( result );
						$("#submit").removeAttr("disabled");
					}
    			});

    		}

    </script>


<%@ include file="../include_bottom.jsp" %>