<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<body>

	<table  class="commonTable" >
		<thead>
		<tr>
			<th>行为参数</th>
			<th>总注册人数</th>
			<th>完成人次</th>
			<th>完成率</th>
		</tr>
		</thead>

		<tbody>
		<c:forEach items="${actionTypeInfos}" var="action">
			<tr>
				<td name="actionTypeName">${action.actionParam}</td>
				<td>${action.userNum}</td>
				<td>${action.finishNum}</td>
				<td>${action.finishRate}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</body>

<script type="text/javascript">

	distinct( "actionTypeName" );


	function distinct( docName ){

		var docs = document.getElementsByName( docName );
		var firstValue = "";
		for (var i = 0; i < docs.length; i++) {
			var doc = docs[i];
			if (doc.innerText != firstValue) {
				firstValue = doc.innerText;
			} else {
				doc.innerText = "";
			}
		}

	}

</script>

</html>
