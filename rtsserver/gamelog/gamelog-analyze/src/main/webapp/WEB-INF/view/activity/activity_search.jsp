<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<body>

	<table  class="commonTable" >
		<thead>
		<tr>
			<%-- <th>日期</th> --%>
			<th>活动名称</th>
			<th>奖励档位</th>
			<th>完成人次</th>
		</tr>
		</thead>

		<tbody>
		<c:forEach items="${list}" var="activityViewData">
			<tr>
				<%-- <td name="yearMonthDay">${activityViewData.yearMonthDay}</td> --%>
				<td name="activityTypeName">${activityViewData.activityTypeName}</td>
				<td>${activityViewData.activityConditionName}</td>
				<td>${activityViewData.count}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</body>

<script type="text/javascript">

	// distinct( "yearMonthDay" );
	distinct( "activityTypeName" );


	function distinct( docName ){

		var docs = document.getElementsByName( docName );
		var firstValue = "";
		for (var i = 0; i < docs.length; i++) {
			var doc = docs[i];
			if (doc.innerText != firstValue) {
				firstValue = doc.innerText;
			} else {
				doc.innerText = "";
			}
		}

	}

</script>

</html>
