<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>


<div>
	  <table class="commonTable" >
		<thead>
			<tr>
				<th >日期</th>
				<th >星期几</th>

				<th >总用户</th>
				<th >新用户</th>
				<th>DAU</th>

				<th>注册付费</th>
				<th>注册付费金额</th>
				<th>首充人数</th>
				<th>首充金额</th>
				<th>付费用户</th>
				<th>付费金额</th>

				<th>付费率</th>
				<th>ARPU</th>
				<th>ARPPU</th>

				<th>留存1日</th>
				<th>留存2日</th>
				<th>留存3日</th>
				<th>留存4日</th>
				<th>留存5日</th>
				<th>留存6日</th>
				<th>留存7日</th>
				<th>留存14日</th>
                <th>留存30日</th>
			</tr>
		</thead>

		<tbody>

			<c:forEach items="${reportList}" var="report" varStatus="status">
				 <c:if test="${status.index % 2 == 0 }" >
					<tr class="odd">
				</c:if>
				<c:if test="${status.index % 2 == 1 } ">
					<tr class="even">
				</c:if>

					<td>${ report.dateString }</td>
					<td>${ report.weekString }</td>

					<td>${ report.totalRegister }</td>
					<td>${ report.dayRegister }</td>
					<td>${ report.DAU }</td>

					<td>${ report.registerPayUser }</td>
					<td>${ report.registerPayMoney }</td>
					<td>${ report.firstPayUser }</td>
                    <td>${ report.firstPayMoney }</td>
                    <td>${ report.totalPayUser }</td>
                    <td>${ report.totalPayMoney }</td>

                    <td>${ report.payPercent }</td>
                    <!-- <td><fmt:formatNumber type="number" value="${ report.payPercent }" maxFractionDigits="1"/></td> -->
                    <td>${ report.ARPU }</td>
                    <td>${ report.ARPPU }</td>

					<td>${ report.retentionPercent1 } (${ report.retentionUser1 } )</td>
					<td>${ report.retentionPercent2 } (${ report.retentionUser2 } )</td>
					<td>${ report.retentionPercent3 } (${ report.retentionUser3 } )</td>
					<td>${ report.retentionPercent4 } (${ report.retentionUser4 } )</td>
					<td>${ report.retentionPercent5 } (${ report.retentionUser5 } )</td>
					<td>${ report.retentionPercent6 } (${ report.retentionUser6 } )</td>
					<td>${ report.retentionPercent7 } (${ report.retentionUser7 } )</td>
					<td>${ report.retentionPercent14 } (${ report.retentionUser14 } )</td>
					<td>${ report.retentionPercent30 } (${ report.retentionUser30 } )</td>

				</tr>

			 </c:forEach>

		</tbody>

	</table>

</div>