<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<title>日报查询</title>
<head>
	  <script src="/js/jquery-3.1.0.min.js"></script>
</head>
<body>
<h1><a href="/index">首页</a></h1>
	<div>
	<jsp:include page="/common/param" flush="true" />

		<div>
			<input type="button" id="submit" value="查询" onclick="search();" />
		</div>

	</div>
	<div id="searchResult" >

	</div>


</body>

<script type='text/javascript'>

	function search( ){

		var postData = "startDay=" + getStartDay() + "&endDay=" + getEndDay()+ "&" + getServerIds() + "&" +getChannelNames();

		$.ajax({
			url:"/daily/search",
			type:"post",
			data:postData,
			beforeSend: function () {
				$("#searchResult").html("正在查询...");
				$("#submit").attr("disabled","disabled");
			},
			success:function( result ){
				$("#searchResult").html( result );
				$("#submit").removeAttr("disabled");
			}
		});
	}

</script>
</html>