<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="../include_top.jsp" %>

	<div class="searchParam">
		<jsp:include page="/common/param" flush="true" />

		<div>
			<table border="0" width="1000">

				<tr>
					<td>
						重要信息:
						<c:forEach items="${items}" var="goldItem" varStatus="state">
							<input name="item" type="checkbox" value="${goldItem.itemId}" />
							<label> ${goldItem.item} </label>
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>
						<input class="submit" id="submit" type="button" value="查 询"  onclick="search();">
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="searchResult" id="searchResult" >

	</div>


	<script type="text/javascript">

		document.all.title.innerText = "消费信息查询";
		document.all.nav_goldcost.style.color = "red";

		function search(  ){

			var items = $("input[name='item']:checked").serialize();
			var startDay = $("#startDay").val();
			var endDay = $("#endDay").val();

			var postData = "startDay=" + startDay + "&endDay=" + endDay + "&" + getServerIds() + "&" +getChannelNames() + "&" + items ;

			$.ajax({
				url:"/goldcost/search",
				type:"post",
				data:postData,
				beforeSend: function () {
					$("#searchResult").html("正在查询...");
					$("#submit").attr("disabled","disabled");
				},
				success:function( result ){
					$("#searchResult").html( result );
					$("#submit").removeAttr("disabled");
				}
			});


		}


	</script>

<%@ include file="../include_bottom.jsp" %>
