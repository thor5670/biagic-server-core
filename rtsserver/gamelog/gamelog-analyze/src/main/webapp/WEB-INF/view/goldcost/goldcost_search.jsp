<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
    <body>

        <table  class="commonTable"  >

            <thead>
            <tr>
                <%-- <th>日期</th> --%>
                <th>消费类型</th>
                <th>消费总额</th>
                <th>消费人次</th>
                <th>消费人数</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${list}" var="viewData">
				<tr>
					<%-- <td name="yearMonthDay">${viewData.yearMonthDay}</td> --%>
					<td>${viewData.behavior}</td>
					<td>${viewData.totalCost}</td>
					<td>${viewData.costTimes}</td>
					<td>${viewData.costPlayerNum}</td>
				</tr>
			</c:forEach>
            </tbody>

        </table>

    </body>

    <%-- <script type="text/javascript">

        distinct( "yearMonthDay" );


        function distinct( docName ){

            var docs = document.getElementsByName( docName );
            var firstValue = "";
            for (var i = 0; i < docs.length; i++) {
                var doc = docs[i];
                if (doc.innerText != firstValue) {
                    firstValue = doc.innerText;
                } else {
                    doc.innerText = "";
                }
            }

        }

    </script> --%>

</html>
