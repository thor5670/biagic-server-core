<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<div>
	  <table  class="commonTable" >
		<thead>
			<tr>
				<th >新手引导ID</th>
				<th >新手引导备注</th>
				<th >通过的人数</th>
				<th >通过率</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${guideList}" var="guide" varStatus="status">
				<tr>
					<td>${ guide.guideId }</td>
					<td>${ guide.guideName }</td>
					<td>${ guide.passCount }</td>
					<td>${ guide.passPercent }</td>
				</tr>
			 </c:forEach>
		</tbody>
	</table>

</div>