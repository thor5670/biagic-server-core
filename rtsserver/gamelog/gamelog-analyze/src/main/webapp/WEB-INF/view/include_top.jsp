<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<title id="title"></title>
<head>
    <link href="/css/reset.css" rel="stylesheet" type="text/css"/>
    <link href="/css/container.css" rel="stylesheet" type="text/css"/>

    <script src="/js/jquery-3.1.0.min.js"></script>
</head>
<body>

<div class="container">

	<div class="nav">
		<a href="/daily/index" id="nav_daily">日报查询</a>
		<a href="/guide/index" id="nav_guide">新手引导查询</a>
		<a href="/activity/index" id="nav_activity">活动日志查询</a>
		<a href="/payment/index" id="nav_payment">充值信息查询</a>
		<a href="/goldcost/index" id="nav_goldcost">消费信息查询</a>
		<a href="/action/index" id="nav_action">玩家行为查询</a>
		<a href="/recharge/index" id="nav_recharge">LTV查询</a>
		<a href="/vip/index" id="nav_vip">VIP查询</a>
		<a href="/levelrunoff/index" id="nav_levelrunoff">等级流失分布</a>
		<a href="/rechargedetail/index" id="nav_rechargedetail">充值详情查询</a>
	</div>

</div>
