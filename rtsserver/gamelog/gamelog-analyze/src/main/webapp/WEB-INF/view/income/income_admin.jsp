<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<title id="title"></title>
<head>
        <link href="/css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="/css/container.css" rel="stylesheet" type="text/css"/>

        <script src="/js/jquery-3.1.0.min.js"></script>
</head>
<body>

<table class="commonTable" style="font-size: 24px;">

        <thead>
        <tr>
                <th colspan="9" align="center">
                        设置
                </th>
        </tr>
        <tr>
                <th>项目</th>
                <th>项目名称</th>
                <th>地区版本</th>
                <th>地区版本名称</th>
                <th>月指标</th>
                <th>汇率(1目标货币=?RMB)</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${list}" var="viewData">
                <tr>
                        <td>${viewData.gameId}</td>
                        <td>
                                <input type="text" value="${viewData.gameName}" style="font-size: 20px; width: 100px"
                                       onblur="valueSet( this.value, '${viewData.gameId}', '${viewData.areaId}', '&gameName=' );"/>

                        </td>
                        <td>${viewData.areaId}</td>
                        <td>
                                <input type="text" value="${viewData.areaName}" style="font-size: 20px; width: 100px"
                                       onblur="valueSet( this.value, '${viewData.gameId}', '${viewData.areaId}', '&areaName=' );"/>
                        </td>
                        <td>
                                <a href="javascript:incomeTargetAdmin('${viewData.gameId}', '${viewData.areaId}');">设置</a>
                                        <%--<input type="text" value="${viewData.monthIncomeTarget}" style="font-size: 20px; width: 100px"--%>
                                        <%--onblur="targetValueSet( this.value, '${viewData.gameId}', '${viewData.areaId}' );"/>--%>
                        </td>
                        <td>
                                <input type="text" name="exchangeRate" value="${viewData.exchangeRate}"
                                       onblur="valueSet( this.value, '${viewData.gameId}', '${viewData.areaId}', '&exchangeRate=' );"/>
                        </td>
                </tr>
        </c:forEach>
        </tbody>

</table>

<hr>
<hr>
<a href="/income/index?bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb" style="font-size: 40px">返回</a>

</body>

<script type="text/javascript">
    function incomeTargetAdmin( gameId, areaId ) {
        location.href = "/income/incomeTargetIndex?bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb&gameId=" + gameId + "&areaId=" + areaId;
    }

    function targetValueSet( value, gameId, areaId ) {


        if ( value != '' && parseFloat( value ) != value ) {

            alert( "输入不正确!" );
//            inp.value = parseFloat( value );
            return;
        }

        valueSet( value, gameId, areaId, '&monthIncomeTarget=' );
    }

    function valueSet( value, gameId, areaId, param ) {

        location.href = "/income/targetset?bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb&gameId=" + gameId + "&areaId=" + areaId + param + value;
//        $.ajax( {
//            url: "/income/targetset",
//            type: "post",
//            data: postData,
//            success: function ( result ) {
//                if ( result == "monthIncomeTargetError" ) {
////                    alert( "月指标数值输入错误!" );
//                }
//            }
//
//        } );


    }

</script>

</html>
