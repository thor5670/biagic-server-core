<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%--<%@ include file="../include_top.jsp" %>--%>
<html>
<title id="title"></title>
<head>
        <link href="/css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="/css/container.css" rel="stylesheet" type="text/css"/>
        <script src="/js/jquery-3.1.0.min.js"></script>
</head>
<body>
<div class="searchResult" id="incomeResult">
</div>

<hr>

<div class="searchResult" id="userResult">
</div>

<hr>
<hr>
<a href="/income/admin?bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb" style="font-size: 40px">设置</a>

</body>
<script type="text/javascript">

    document.all.title.innerText = "收入统计";


    $.ajax( {
        url: "/income/incomesearch",
        type: "post",
        data: "bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb",
        success: function ( result ) {
            $( "#incomeResult" ).html( result );
        }
    } );

    $.ajax( {
        url: "/income/usersearch",
        type: "post",
        data: "bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb",
        success: function ( result ) {
            $( "#userResult" ).html( result );
        }
    } );


</script>

</html>

<%--<%@ include file="../include_bottom.jsp" %>--%>
