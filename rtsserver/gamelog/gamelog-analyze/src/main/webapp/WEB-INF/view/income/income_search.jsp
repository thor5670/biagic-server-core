<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<body>

<table class="commonTable" style="font-size: 24px;">

        <thead>
        <tr>
                <th colspan="9" align="center">
                        收入统计
                </th>
        </tr>
        <tr>
                <th>项目</th>
                <th>地区版本</th>
                <th>今日实时</th>
                <th>昨日实时</th>
                <th>昨日总计</th>
                <th>前日总计</th>
                <th>日均指标</th>
                <th>月指标</th>
                <th>月累计</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${list}" var="viewData">
                <c:if test="${viewData.gameId == null}">
                        <tr>
                                <td>&nbsp;</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                        </tr>
                </c:if>
                <c:if test="${viewData.gameId != null}">
                        <tr>
                                <td>${viewData.gameName}</td>
                                <td>${viewData.areaName}</td>
                                <td>${viewData.todayRealTime}</td>
                                <td>${viewData.yesterdayRealTime}</td>
                                <td>${viewData.yesterdayTotal}</td>
                                <td>${viewData.dayBeforeYesterdayTotal}</td>
                                        <%--<td id="dayavg${viewData.gameId}${viewData.areaId}">${viewData.dayAvgTarget}</td>--%>
                                <td>${viewData.dayAvgTarget}</td>
                                        <%--<td><input type="text" value="${viewData.monthTarget}" style="font-size: 20px; width: 100px" oninput="targetInp( this, '${viewData.gameId}', '${viewData.areaId}' );"/></td>--%>
                                <td>${viewData.monthIncomeTarget}</td>
                                <td>${viewData.monthTotal}</td>
                        </tr>
                </c:if>

        </c:forEach>
        </tbody>

</table>

</body>

<script type="text/javascript">

    function targetInp( inp, gameId, areaId ) {
        var value = inp.value;

        if ( value != '' && parseFloat( value ) != value ) {

            alert( "输入不正确!" );
//            inp.value = parseFloat( value );
            return;
        }

        $.ajax( {
            url: "/income/targetset",
            type: "post",
            data: "bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb&gameId=" + gameId + "&areaId=" + areaId + "&targetValue=" + value,
            success: function ( result ) {
                document.getElementById( "dayavg" + gameId + areaId ).innerText = result;
            }

        } );


    }

</script>

</html>
