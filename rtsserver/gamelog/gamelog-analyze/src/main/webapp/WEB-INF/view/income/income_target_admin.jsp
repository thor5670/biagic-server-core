<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<title id="title"></title>
<head>
        <link href="/css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="/css/container.css" rel="stylesheet" type="text/css"/>

        <script src="/js/jquery-3.1.0.min.js"></script>
</head>
<body>

<table class="commonTable" style="font-size: 24px;">

        <thead>
        <tr>
                <th>
                        项目名称: ${gameInfo.gameName}
                </th>
        </tr>
        <tr>
                <th>
                        地区版本: ${gameInfo.areaName}
                </th>
        </tr>
        <tr>
                <th>
                        年份: ${year}
                </th>
        </tr>
        <tr>
                <th colspan="9" align="center">
                        收入指标设置
                </th>
        </tr>
        <tr>
                <th>月份</th>
                <th>收入指标(RMB)</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${incomeTargetMap}" var="incomeTarget">
                <tr>
                        <td>${incomeTarget.key + 1}</td>
                        <td><input type="text" value="${incomeTarget.value}"
                                   onblur="incomeTargetSet( this.value, '${gameInfo.gameId}', '${gameInfo.areaId}', ${year}, '${incomeTarget.key}' )"/>
                        </td>
                </tr>
        </c:forEach>
        </tbody>

</table>

<hr>
<hr>
<a href="/income/admin?bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb" style="font-size: 40px">返回</a>

</body>

<script type="text/javascript">

    function incomeTargetSet( value, gameId, areaId, year, month ) {

        location.href = "/income/incomeTargetUpdate?bdkey=b74b61cf6f08d5ccbd65328e1bfdedfb&gameId=" + gameId + "&areaId=" + areaId + "&year=" + year + "&month=" + month + "&incomeTarget=" + value;

    }

</script>

</html>
