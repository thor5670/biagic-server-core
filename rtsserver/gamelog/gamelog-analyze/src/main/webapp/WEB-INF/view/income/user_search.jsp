<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<body>

<table class="commonTable" style="font-size: 26px;">

        <thead>
        <tr>
                <th colspan="9" align="center">
                        用户统计
                </th>
        </tr>
        <tr>
                <th>项目</th>
                <th>地区版本</th>
                <th>今日新增</th>
                <th>月新增总</th>
                <th>今DAU</th>
                <th>昨DAU总</th>
                <th>今APA</th>
                <th>昨APA总</th>
                <th>昨次留</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${list}" var="viewData">
                <c:if test="${viewData.gameId == null}">
                        <tr>
                                <td>&nbsp;</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                        </tr>
                </c:if>
                <c:if test="${viewData.gameId !=null}">
                        <tr>
                                <td>${viewData.gameName}</td>
                                <td>${viewData.areaName}</td>
                                <td>${viewData.todayIncrement}</td>
                                <td>${viewData.monthIncrement}</td>
                                <td>${viewData.todayDAU}</td>
                                <td>${viewData.yesterdayDAU}</td>
                                <td>${viewData.todayAPA}</td>
                                <td>${viewData.yesterdayAPA}</td>
                                <td>${viewData.yesterdayRetention1}</td>
                        </tr>
                </c:if>
        </c:forEach>
        </tbody>

</table>

</body>

</html>
