<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- <%@ include file="include_top.jsp" %> -->
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
    <title>日报查询</title>
    <head>
        <link href="/css/reset.css" rel="stylesheet" type="text/css" />
        <link href="/css/container.css" rel="stylesheet" type="text/css" />

        <script src="/js/jquery-3.1.0.min.js"></script>
    </head>
    <body>


        <!-- <a id="change" onclick="change();">没有账号?点击这里注册</a><br><br> -->

        账号:
        <input type="text" id="userName" name="userName" oninput="userNameInput( this );" value="">
        <label id="userNameStatus" color="red"></label><br>

        密码:
        <input type="password" id="passWord" name="passWord" oninput="notNullInput( this );" onkeyup="checkLogin(event);" value=""><br>

        <input type="button" id="login" value="登录" onclick="login();">
        <!-- <input type="hidden" id="register" value="注册" onclick="register();"> -->

    </body>

    <script type="text/javascript">

            var ableText = "";
            var disableText = "不存在的用户名";


            function notNullInput( doc ){
                doc.value = doc.value.trim();
            }

            function login(){

                var userName = document.all.userName.value;
                var passWord = document.all.passWord.value;

                if ( !userNameAndPassWordCheck( userName, passWord ) ) {
                    return;
                }


                var postData = "userName=" + userName + "&passWord=" + passWord;
                $.ajax({
                    url:"/login/login",
                    type:"post",
                    data:postData,
                    success: function( result ){
                        if ( result == "true" ) {
                            document.location.href = "/navigation";
                        } else {
                            alert( "密码错误!" );
                            document.all.passWord.value = "";
                        }

                    }
                });


            }

            function userNameInput( doc ){

                notNullInput( doc );

                var userName = doc.value;
                var userNameStatus = document.all.userNameStatus;

                if ( userName != "" ) {
                    var postData = "userName=" + userName;
                    $.ajax({
                        url:"/login/userNameCheck",
                        type:"post",
                        data:postData,
                        success: function( result ){
                            userNameStatus.innerText = result.trim() == "true" ? ableText : disableText;
                        }
                    });
                } else {
                    userNameStatus.innerText = "";
                }


            }

            function register(){

                var userName = document.all.userName.value;
                var passWord = document.all.passWord.value;

                if ( !userNameAndPassWordCheck( userName, passWord ) ) {
                    return;
                }


                var postData = "userName=" + userName + "&passWord=" + passWord;

                $.ajax({

                    url:"/login/register",
                    type:"post",
                    data:postData,
                    success: function( result ){
                        result = result.trim();
                        alert(result);
                        if ( result == "注册成功!" ) {
                            disableRegister();
                        }

                    }

                });

            }

            function change(){
                var loginBut = document.all.login;
                var registerBut = document.all.register;
                var changeText = document.all.change;


                if ( loginBut.type == "hidden" ) {

                    loginBut.type = "button";
                    registerBut.type = "hidden";
                    changeText.innerText = "没有账号?点击这里注册";

                    ableText = "";
                    disableText = "不存在的用户名";

                } else {

                    loginBut.type = "hidden";
                    registerBut.type = "button";
                    changeText.innerText = "已有账号?点击这里登录";

                    ableText = "已被注册";
                    disableText = "";

                }

                document.all.userNameStatus.innerText = "";
                document.all.userName.value = "";
                document.all.passWord.value = "";


            }


            function userNameAndPassWordCheck( userName, passWord){

                if ( userName == "" ){
                    alert( "请输入用户名" );
                    return false;
                }

                if ( passWord == "" ) {
                    alert( "请输入密码" );
                    return false;
                }

                return true;
            }

            function checkLogin(e){
              if (e.keyCode == 13) {
                login();
              }
            }


    </script>

</html>
