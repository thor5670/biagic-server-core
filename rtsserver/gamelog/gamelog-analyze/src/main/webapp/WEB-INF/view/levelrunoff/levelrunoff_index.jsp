<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@include file="../include_top.jsp" %>

<div class="searchParam">
    <jsp:include page="/common/param" flush="true"/>

    <div>
        <input class="submit" id="submit" type="button" value="查询" onclick="search();"/>
    </div>
</div>

<div class="searchResult" id="searchResult">

</div>

<script type="text/javascript">

    document.all.title.innerText = "等级流失分布";
    document.all.nav_levelrunoff.style.color = "red";


    function search() {

        var postData = "startDay=" + getStartDay() + "&endDay=" + getEndDay() + "&" + getServerIds() + "&" + getChannelNames();

        $.ajax({
            url: "/levelrunoff/search",
            type: "post",
            data: postData,
            beforeSend: function () {
                $("#searchResult").html("正在查询...");
                $("#submit").attr("disabled","disabled");
            },
            success:function( result ){
                $("#searchResult").html( result );
                $("#submit").removeAttr("disabled");
            }
        })


    }


</script>

<%@ include file="../include_bottom.jsp" %>