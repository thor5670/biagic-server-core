<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<body>

<table class="commonTable">
    <thead>
    <tr>
        <th>等级</th>
        <th>玩家数</th>
        <th>全服流失率</th>
        <th>累计流失率</th>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${dataList}" var="item">
        <tr>
            <td>${item.level}</td>
            <td>${item.counts}</td>
            <td>${item.runOffRatio}%</td>
            <td>${item.accumulateRunOffRatio}%</td>
        </tr>
    </c:forEach>
    </tbody>

</table>

</body>
</html>