<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<div>
	<label>选服务器:</label>
	<c:forEach items="${serverList}" var="serverId" varStatus="state">
		<input id="chk_server_${ serverId}" name="serverIds" type="checkbox" value="${ serverId}" />
		<label for="chk_server_${ serverId}"> ${ serverId} </label>
	</c:forEach>
</div>

<div id="channelList">
	<label >选择渠道:</label>
	<c:forEach items="${channelList}" var="channel" varStatus="state">
    		<input  id="chk_channel_${ channel}" name="channelNames" type="checkbox" value="${ channel}" />
    		<label  for="chk_channel_${ channel}" > ${channel} </label>
    </c:forEach>
</div>

<div>
	<label style="float:left;" id="startDayLab">选择日期:</label>
	<input style="width:200px;float:left;" class="form-control" type="date" id="startDay" name="startDay" value="${startDay}">
	<label style="float:left;" id="endDayLab">至</label>
	<input style="width:200px;float:left;" class="form-control" type="date" id="endDay"  name="endDay" value="${endDay}">
</div>

<div style="clear:both;"></div>


<script type='text/javascript'>

function getServerIds( ){

	var serverIds = $("input[name='serverIds']:checked").serialize();
	return serverIds;
}

function getChannelNames(){
	var channelNames = $("input[name='channelNames']:checked").serialize();
	return channelNames;
}

function getStartDay(){
	var startDay = $("#startDay").val();
	return startDay;
}

function getEndDay(){
	var endDay = $("#endDay").val();
	return endDay;
}

</script>