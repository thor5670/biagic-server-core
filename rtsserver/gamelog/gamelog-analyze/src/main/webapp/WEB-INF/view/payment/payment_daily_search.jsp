<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<body>

	<table  class="commonTable"  >
		<thead>
			<tr>
				<th>日期</th>
				<th>DAU</th>
				<th>充值金额</th>
				<th>APA</th>
				<th>充值金币</th>
				<th>系统赠送金币</th>
				<th>金币增量合计</th>
				<th>消费金币</th>
				<th>消费人数</th>
				<th>PCU</th>
			</tr>
		</thead>

		<tbody>
		<c:forEach items="${list}" var="viewData">
			<tr>
				<td>${viewData.yearMonthDay}</td>
				<td>${viewData.DAU}</td>
				<td>${viewData.payNum}</td>
				<td>${viewData.APA}</td>
				<td>${viewData.payGoldNum}</td>
				<td>${viewData.systemGoldNum}</td>
				<td>${viewData.totalGoldNum}</td>
				<td>${viewData.costGold}</td>
				<td>${viewData.costPlayerNum}</td>
				<td>${viewData.PCU}</td>
			</tr> 
		</c:forEach>
		</tbody>
		
	</table>

</body>
<!--
<script type="text/javascript">
	
	distinct( "yearMonthDay" );
	distinct( "activityTypeName" );
	

	function distinct( docName ){
		
		var docs = document.getElementsByName( docName );
		var firstValue = "";
		for (var i = 0; i < docs.length; i++) {
			var doc = docs[i];
			if (doc.innerText != firstValue) {
				firstValue = doc.innerText;
			} else {
				doc.innerText = "";
			}
		}

	}

</script>
-->
</html>