<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="../include_top.jsp" %>

		

		<div class="searchParam">

			<jsp:include page="/common/param" flush="true" />

			<input class="submit" id="submit" type="button" value="查 询" onclick="search();">
		</div>

		<hr>

		<div class="searchResult" id="searchResult" >

		</div>

		<hr>

		<div class="searchResult" id="searchResult2">
			
		</div>




	</body>


	<script type="text/javascript">

		// 修改页面标题
		document.all.title.innerText = "充值信息查询";
		// 修改选中的菜单项的颜色
		document.all.nav_payment.style.color = "red";

		document.all.endDay.hidden = true;
		document.all.endDayLab.innerText = "";
		document.all.startDayLab.innerText = "选择日期:";

		function search( ){

			var startDay = $("#startDay").val();
			var endDay = $("#endDay").val();

			var postData = "startDay=" + startDay + "&endDay=" + endDay + "&" + getServerIds() + "&" +getChannelNames();

			$.ajax({
				url:"/payment/daily_search",
				type:"post",
				data:postData,
				beforeSend: function () {
					$("#searchResult").html("正在查询...");
					$("#submit").attr("disabled","disabled");
				},
				success:function( result ){
					$("#searchResult").html( result );
					$("#submit").removeAttr("disabled");
				}
			});

			$.ajax({
				url:"/payment/interval_search",
				type:"post",
				data:postData,
				beforeSend: function () {
					$("#searchResult2").html("正在查询...");
					$("#submit").attr("disabled","disabled");
				},
				success:function( result ){
					$("#searchResult2").html( result );
					$("#submit").removeAttr("disabled");
				}
			});

		}

	</script>


<%@ include file="../include_bottom.jsp" %>