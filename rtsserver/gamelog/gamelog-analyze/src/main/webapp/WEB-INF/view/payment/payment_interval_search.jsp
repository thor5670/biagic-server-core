<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<html>
	<body>

		<table  class="commonTable" >
			<thead>
			<tr>
				<th>区间</th>
				<th>充值人数</th>
				<th>人数占比</th>
				<th>充值额</th>
			</tr>
			</thead>

			<tbody>
			<c:forEach items="${list}" var="viewData">
				<tr>
					<td>${viewData.payInterval}</td>
					<td>${viewData.playerNum}</td>
					<td>${viewData.playerPercent}</td>
					<td>${viewData.payNum}</td>
				</tr> 
			</c:forEach>
			</tbody>

		</table>

	</body>
</html>