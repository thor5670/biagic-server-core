<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<body>

	<table  class="commonTable"  >
		<thead>
			<tr>
				<th>日期</th>
				<th>DNU</th>
				<th>首日APA</th>
				<th>3日APA</th>
				<th>7日APA</th>
				<th>30日APA</th>
				<th>累计APA</th>
				<th>首日LTV</th>
                                                        <th>3日LTV</th>
                                                        <th>7日LTV</th>
                                                        <th>30日LTV</th>
                                                        <th>累计LTV</th>
			</tr>
		</thead>

		<tbody>
		<c:forEach items="${rechargeViewDataList}" var="item">
			<tr>
				<td>${item.date}</td>
				<td>${item.DNU}</td>
				<td>${item.firstDayAPA}</td>
				<td>${item.threeDayAPA}</td>
				<td>${item.sevenDayAPA}</td>
				<td>${item.thirtyDayAPA}</td>
				<td>${item.totalAPA}</td>
				<td>${item.firstDayRec}</td>
				<td>${item.threeDayRec}</td>
				<td>${item.sevenDayRec}</td>
				<td>${item.thirtyDayRec}</td>
				<td>${item.totalRec}</td>
			</tr>
		</c:forEach>
		</tbody>

	</table>

</body>
</html>