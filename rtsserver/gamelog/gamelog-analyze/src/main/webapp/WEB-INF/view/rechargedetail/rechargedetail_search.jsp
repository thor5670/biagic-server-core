<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<body>

	<table  class="commonTable"  >
		<thead>
			<tr>
				<th>服务器ID</th>
				<th>玩家ID</th>
				<th>充值金额</th>
				<th>充值时间</th>
			</tr>
		</thead>

		<tbody>
		<c:forEach items="${rechargeDetailViewDataList}" var="item">
			<tr>
				<td>${item.serverId}</td>
				<td>${item.userId}</td>
				<td>${item.money}</td>
				<td>${item.logTime}</td>
			</tr>
		</c:forEach>
		</tbody>

	</table>

</body>
</html>