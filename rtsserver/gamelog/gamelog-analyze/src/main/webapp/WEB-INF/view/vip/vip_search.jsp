<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<body>
	<table class="commonTable">
	              <thead>
	                            <tr>
	                                          <th>VIP等级</th>
	                                          <th>人数</th>
	                            </tr>
                            </thead>
                            <tbody>
                                          <c:forEach items="${vipViewDatas}" var="item">
                                          <tr>
                                                        <td>${item.vipLevel}</td>
                                                        <td>${item.counts}</td>
                                          </tr>
                                          </c:forEach>
                            </tbody>
              </table>
</body>
</html>