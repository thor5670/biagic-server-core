package com.rtsapp.server.gamelog.api;


public class GamelogHub {

    public static String createSql( String logType ,String log){


        String[] logInfo = log.split("\\|");

        switch ( logType ){

            case "register":{
                String SQL = "INSERT INTO RegisterLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="userId";
                    SQL+=",";
                    SQL+="device";
                    SQL+=",";
                    SQL+="imei";
                    SQL+=",";
                    SQL+="mac";
                    SQL+=",";
                    SQL+="resolution";
                    SQL+=",";
                    SQL+="os";
                    SQL+=",";
                    SQL+="osVersion";
                    SQL+=",";
                    SQL+="openUDID";
                    SQL+=",";
                    SQL+="ADUDID";
                    SQL+=",";
                    SQL+="APPUDID";
                    SQL+=",";
                    SQL+="CENTERID";
                    SQL+=",";
                    SQL+="ip";
                    SQL+=",";
                    SQL+="channel";
                    SQL+=") VALUES (";

                for(int i=0;i<17;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=17-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
            case "login":{
                String SQL = "INSERT INTO LoginLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="userId";
                    SQL+=",";
                    SQL+="roleId";
                    SQL+=",";
                    SQL+="device";
                    SQL+=",";
                    SQL+="imei";
                    SQL+=",";
                    SQL+="mac";
                    SQL+=",";
                    SQL+="resolution";
                    SQL+=",";
                    SQL+="os";
                    SQL+=",";
                    SQL+="osVersion";
                    SQL+=",";
                    SQL+="openUDID";
                    SQL+=",";
                    SQL+="ADUDID";
                    SQL+=",";
                    SQL+="APPUDID";
                    SQL+=",";
                    SQL+="CENTERID";
                    SQL+=",";
                    SQL+="ip";
                    SQL+=",";
                    SQL+="action";
                    SQL+=",";
                    SQL+="level";
                    SQL+=",";
                    SQL+="virtualMoney";
                    SQL+=") VALUES (";

                for(int i=0;i<20;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=20-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
            case "payment":{
                String SQL = "INSERT INTO PaymentLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="userId";
                    SQL+=",";
                    SQL+="roleId";
                    SQL+=",";
                    SQL+="rechrageTimes";
                    SQL+=",";
                    SQL+="orderId";
                    SQL+=",";
                    SQL+="money";
                    SQL+=",";
                    SQL+="currency";
                    SQL+=",";
                    SQL+="result";
                    SQL+=",";
                    SQL+="resultCode";
                    SQL+=",";
                    SQL+="productId";
                    SQL+=",";
                    SQL+="productName";
                    SQL+=",";
                    SQL+="productPrice";
                    SQL+=",";
                    SQL+="getVirualMoney";
                    SQL+=",";
                    SQL+="getVipExp";
                    SQL+=",";
                    SQL+="playerLevel";
                    SQL+=",";
                    SQL+="virualMoney";
                    SQL+=",";
                    SQL+="vipLevel";
                    SQL+=",";
                    SQL+="vipExp";
                    SQL+=",";
                    SQL+="oldVipLevel";
                    SQL+=") VALUES (";

                for(int i=0;i<22;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=22-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
            case "additem":{
                String SQL = "INSERT INTO AddItemLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="userId";
                    SQL+=",";
                    SQL+="roleId";
                    SQL+=",";
                    SQL+="behavior";
                    SQL+=",";
                    SQL+="itemId";
                    SQL+=",";
                    SQL+="itemName";
                    SQL+=",";
                    SQL+="itemNum";
                    SQL+=",";
                    SQL+="resultNum";
                    SQL+=") VALUES (";

                for(int i=0;i<11;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=11-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
            case "costitem":{
                String SQL = "INSERT INTO CostItemLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="userId";
                    SQL+=",";
                    SQL+="roleId";
                    SQL+=",";
                    SQL+="behavior";
                    SQL+=",";
                    SQL+="itemId";
                    SQL+=",";
                    SQL+="itemName";
                    SQL+=",";
                    SQL+="itemNum";
                    SQL+=",";
                    SQL+="resultNum";
                    SQL+=") VALUES (";

                for(int i=0;i<11;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=11-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
            case "guide":{
                String SQL = "INSERT INTO GuideLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="userId";
                    SQL+=",";
                    SQL+="roleId";
                    SQL+=",";
                    SQL+="guideId";
                    SQL+=",";
                    SQL+="guideName";
                    SQL+=") VALUES (";

                for(int i=0;i<8;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=8-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
            case "level":{
                String SQL = "INSERT INTO LevelLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="userId";
                    SQL+=",";
                    SQL+="roleId";
                    SQL+=",";
                    SQL+="oldLevel";
                    SQL+=",";
                    SQL+="newLevel";
                    SQL+=") VALUES (";

                for(int i=0;i<8;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=8-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
            case "action":{
                String SQL = "INSERT INTO ActionLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="userId";
                    SQL+=",";
                    SQL+="roleId";
                    SQL+=",";
                    SQL+="action";
                    SQL+=",";
                    SQL+="actionParam";
                    SQL+=") VALUES (";

                for(int i=0;i<8;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=8-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
            case "activity":{
                String SQL = "INSERT INTO ActivityLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="userId";
                    SQL+=",";
                    SQL+="roleId";
                    SQL+=",";
                    SQL+="activityType";
                    SQL+=",";
                    SQL+="activityTypeName";
                    SQL+=",";
                    SQL+="activityCondition";
                    SQL+=",";
                    SQL+="activityConditionName";
                    SQL+=",";
                    SQL+="activityId";
                    SQL+=") VALUES (";

                for(int i=0;i<11;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=11-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
            case "heartbeat":{
                String SQL = "INSERT INTO HeartbeatLog (";


                SQL+="logTime";
                    SQL+=",";
                    SQL+="gameId";
                    SQL+=",";
                    SQL+="areaId";
                    SQL+=",";
                    SQL+="serverId";
                    SQL+=",";
                    SQL+="onlinePlayerNum";
                    SQL+=",";
                    SQL+="throughput";
                    SQL+=",";
                    SQL+="systemTotalMemory";
                    SQL+=",";
                    SQL+="systemFreeMemory";
                    SQL+=",";
                    SQL+="JVMMaxMemory";
                    SQL+=",";
                    SQL+="JVMTotalMemory";
                    SQL+=",";
                    SQL+="JVMFreeMemory";
                    SQL+=") VALUES (";

                for(int i=0;i<11;i++){

                    if(logInfo.length>i){
                        SQL+="\""+ logInfo[i]+"\"";
                    }else{
                        SQL+="\"\"";
                    }

                    if(i!=11-1){
                        SQL+=",";
                    }
                }
                SQL+=")";
                return SQL;
            }
        }

        return null;
    }

}

