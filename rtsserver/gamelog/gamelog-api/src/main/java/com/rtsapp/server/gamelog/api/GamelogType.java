package com.rtsapp.server.gamelog.api;

public enum  GamelogType {

    Register("Register"),
    Login("Login"),
    Payment("Payment"),
    AddItem("AddItem"),
    CostItem("CostItem"),
    Guide("Guide"),
    Level("Level"),
    Action("Action"),
    Activity("Activity"),
    Heartbeat("Heartbeat"),
    ;

    String name;

    GamelogType(String name) {
        this.name = name;
    }

    public String getName() {
            return name;
    }

    public String getTableName(){
            return this.name + "Log";
    }
}


