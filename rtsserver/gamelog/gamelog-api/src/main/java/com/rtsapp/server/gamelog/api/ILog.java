package com.rtsapp.server.gamelog.api;

/**
 *
 */
public interface ILog {

    String SPLIT_CHAR = "|";


    String getLogTime();

    String getGameId();

    String getAreaId();

    String getServerId();



    void writeTo( StringBuilder sb );

    boolean readFrom( String str );

    void reset();

}
