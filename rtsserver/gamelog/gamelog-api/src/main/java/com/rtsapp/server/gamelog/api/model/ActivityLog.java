package com.rtsapp.server.gamelog.api.model;

import javax.persistence.*;
import com.rtsapp.server.gamelog.api.ILog;

@Entity
@Table
public class ActivityLog implements ILog {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	public long id;
	@Column(columnDefinition="TIMESTAMP")
	public String logTime;
	public String gameId;
	public String areaId;
	public String serverId;
	public String userId;
	public String roleId;
	public String activityType;
	public String activityTypeName;
	public String activityCondition;
	public String activityConditionName;
	public String activityId;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	
	public String getActivityTypeName() {
		return activityTypeName;
	}

	public void setActivityTypeName(String activityTypeName) {
		this.activityTypeName = activityTypeName;
	}
	
	public String getActivityCondition() {
		return activityCondition;
	}

	public void setActivityCondition(String activityCondition) {
		this.activityCondition = activityCondition;
	}
	
	public String getActivityConditionName() {
		return activityConditionName;
	}

	public void setActivityConditionName(String activityConditionName) {
		this.activityConditionName = activityConditionName;
	}
	
	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + ILog.SPLIT_CHAR );
	    if( strs.length < 10 ){
			return false;
		}

		logTime = strs[ 0 ];
		gameId = strs[ 1 ];
		areaId = strs[ 2 ];
		serverId = strs[ 3 ];
		userId = strs[ 4 ];
		roleId = strs[ 5 ];
		activityType = strs[ 6 ];
		activityTypeName = strs[ 7 ];
		activityCondition = strs[ 8 ];
		activityConditionName = strs[ 9 ];

		if( strs.length > 10 ){
			activityId = strs[ 10 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( logTime );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( gameId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( userId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( roleId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( activityType );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( activityTypeName );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( activityCondition );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( activityConditionName );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( activityId );

	}

	public void reset( ){
		logTime = null;
		gameId = null;
		areaId = null;
		serverId = null;
		userId = null;
		roleId = null;
		activityType = null;
		activityTypeName = null;
		activityCondition = null;
		activityConditionName = null;
		activityId = null;
	}
}
