package com.rtsapp.server.gamelog.api.model;

import javax.persistence.*;
import com.rtsapp.server.gamelog.api.ILog;

@Entity
@Table
public class CostItemLog implements ILog {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	public long id;
	@Column(columnDefinition="TIMESTAMP")
	public String logTime;
	public String gameId;
	public String areaId;
	public String serverId;
	public String userId;
	public String roleId;
	public String behavior;
	public String itemId;
	public String itemName;
	public String itemNum;
	public String resultNum;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public String getBehavior() {
		return behavior;
	}

	public void setBehavior(String behavior) {
		this.behavior = behavior;
	}
	
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public String getItemNum() {
		return itemNum;
	}

	public void setItemNum(String itemNum) {
		this.itemNum = itemNum;
	}
	
	public String getResultNum() {
		return resultNum;
	}

	public void setResultNum(String resultNum) {
		this.resultNum = resultNum;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + ILog.SPLIT_CHAR );
	    if( strs.length < 10 ){
			return false;
		}

		logTime = strs[ 0 ];
		gameId = strs[ 1 ];
		areaId = strs[ 2 ];
		serverId = strs[ 3 ];
		userId = strs[ 4 ];
		roleId = strs[ 5 ];
		behavior = strs[ 6 ];
		itemId = strs[ 7 ];
		itemName = strs[ 8 ];
		itemNum = strs[ 9 ];

		if( strs.length > 10 ){
			resultNum = strs[ 10 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( logTime );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( gameId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( userId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( roleId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( behavior );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( itemId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( itemName );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( itemNum );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( resultNum );

	}

	public void reset( ){
		logTime = null;
		gameId = null;
		areaId = null;
		serverId = null;
		userId = null;
		roleId = null;
		behavior = null;
		itemId = null;
		itemName = null;
		itemNum = null;
		resultNum = null;
	}
}
