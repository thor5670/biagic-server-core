package com.rtsapp.server.gamelog.api.model;

import javax.persistence.*;

@Entity
@Table
public class GameInfo {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    public long id;

    public String gameId;
    public String gameName;

    public String areaId;
    public String areaName;

    public String monthIncomeTarget;
    public String exchangeRate;


    public long getId( ) {
        return id;
    }

    public void setId( long id ) {
        this.id = id;
    }

    public String getGameId( ) {
        return gameId;
    }

    public void setGameId( String gameId ) {
        this.gameId = gameId;
    }

    public String getGameName( ) {
        return gameName;
    }

    public void setGameName( String gameName ) {
        this.gameName = gameName;
    }

    public String getAreaId( ) {
        return areaId;
    }

    public void setAreaId( String areaId ) {
        this.areaId = areaId;
    }

    public String getAreaName( ) {
        return areaName;
    }

    public void setAreaName( String areaName ) {
        this.areaName = areaName;
    }

    public String getExchangeRate( ) {
        return exchangeRate;
    }

    public void setExchangeRate( String exchangeRate ) {
        this.exchangeRate = exchangeRate;
    }

    public String getMonthIncomeTarget( ) {
        return monthIncomeTarget;
    }

    public void setMonthIncomeTarget( String monthIncomeTarget ) {
        this.monthIncomeTarget = monthIncomeTarget;
    }
}
