package com.rtsapp.server.gamelog.api.model;

import javax.persistence.*;
import com.rtsapp.server.gamelog.api.ILog;

@Entity
@Table
public class GuideLog implements ILog {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	public long id;
	@Column(columnDefinition="TIMESTAMP")
	public String logTime;
	public String gameId;
	public String areaId;
	public String serverId;
	public String userId;
	public String roleId;
	public String guideId;
	public String guideName;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public String getGuideId() {
		return guideId;
	}

	public void setGuideId(String guideId) {
		this.guideId = guideId;
	}
	
	public String getGuideName() {
		return guideName;
	}

	public void setGuideName(String guideName) {
		this.guideName = guideName;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + ILog.SPLIT_CHAR );
	    if( strs.length < 7 ){
			return false;
		}

		logTime = strs[ 0 ];
		gameId = strs[ 1 ];
		areaId = strs[ 2 ];
		serverId = strs[ 3 ];
		userId = strs[ 4 ];
		roleId = strs[ 5 ];
		guideId = strs[ 6 ];

		if( strs.length > 7 ){
			guideName = strs[ 7 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( logTime );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( gameId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( userId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( roleId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( guideId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( guideName );

	}

	public void reset( ){
		logTime = null;
		gameId = null;
		areaId = null;
		serverId = null;
		userId = null;
		roleId = null;
		guideId = null;
		guideName = null;
	}
}
