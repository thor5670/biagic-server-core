package com.rtsapp.server.gamelog.api.model;

import javax.persistence.*;
import com.rtsapp.server.gamelog.api.ILog;

@Entity
@Table
public class HeartbeatLog implements ILog {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	public long id;
	@Column(columnDefinition="TIMESTAMP")
	public String logTime;
	public String gameId;
	public String areaId;
	public String serverId;
	public String onlinePlayerNum;
	public String throughput;
	public String systemTotalMemory;
	public String systemFreeMemory;
	public String JVMMaxMemory;
	public String JVMTotalMemory;
	public String JVMFreeMemory;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getOnlinePlayerNum() {
		return onlinePlayerNum;
	}

	public void setOnlinePlayerNum(String onlinePlayerNum) {
		this.onlinePlayerNum = onlinePlayerNum;
	}
	
	public String getThroughput() {
		return throughput;
	}

	public void setThroughput(String throughput) {
		this.throughput = throughput;
	}
	
	public String getSystemTotalMemory() {
		return systemTotalMemory;
	}

	public void setSystemTotalMemory(String systemTotalMemory) {
		this.systemTotalMemory = systemTotalMemory;
	}
	
	public String getSystemFreeMemory() {
		return systemFreeMemory;
	}

	public void setSystemFreeMemory(String systemFreeMemory) {
		this.systemFreeMemory = systemFreeMemory;
	}
	
	public String getJVMMaxMemory() {
		return JVMMaxMemory;
	}

	public void setJVMMaxMemory(String JVMMaxMemory) {
		this.JVMMaxMemory = JVMMaxMemory;
	}
	
	public String getJVMTotalMemory() {
		return JVMTotalMemory;
	}

	public void setJVMTotalMemory(String JVMTotalMemory) {
		this.JVMTotalMemory = JVMTotalMemory;
	}
	
	public String getJVMFreeMemory() {
		return JVMFreeMemory;
	}

	public void setJVMFreeMemory(String JVMFreeMemory) {
		this.JVMFreeMemory = JVMFreeMemory;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + ILog.SPLIT_CHAR );
	    if( strs.length < 10 ){
			return false;
		}

		logTime = strs[ 0 ];
		gameId = strs[ 1 ];
		areaId = strs[ 2 ];
		serverId = strs[ 3 ];
		onlinePlayerNum = strs[ 4 ];
		throughput = strs[ 5 ];
		systemTotalMemory = strs[ 6 ];
		systemFreeMemory = strs[ 7 ];
		JVMMaxMemory = strs[ 8 ];
		JVMTotalMemory = strs[ 9 ];

		if( strs.length > 10 ){
			JVMFreeMemory = strs[ 10 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( logTime );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( gameId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( onlinePlayerNum );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( throughput );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( systemTotalMemory );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( systemFreeMemory );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( JVMMaxMemory );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( JVMTotalMemory );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( JVMFreeMemory );

	}

	public void reset( ){
		logTime = null;
		gameId = null;
		areaId = null;
		serverId = null;
		onlinePlayerNum = null;
		throughput = null;
		systemTotalMemory = null;
		systemFreeMemory = null;
		JVMMaxMemory = null;
		JVMTotalMemory = null;
		JVMFreeMemory = null;
	}
}
