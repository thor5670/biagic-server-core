package com.rtsapp.server.gamelog.api.model;

import javax.persistence.*;
import com.rtsapp.server.gamelog.api.ILog;

@Entity
@Table
public class PaymentLog implements ILog {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	public long id;
	@Column(columnDefinition="TIMESTAMP")
	public String logTime;
	public String gameId;
	public String areaId;
	public String serverId;
	public String userId;
	public String roleId;
	public String rechrageTimes;
	public String orderId;
	public String money;
	public String currency;
	public String result;
	public String resultCode;
	public String productId;
	public String productName;
	public String productPrice;
	public String getVirualMoney;
	public String getVipExp;
	public String playerLevel;
	public String virualMoney;
	public String vipLevel;
	public String vipExp;
	public String oldVipLevel;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public String getRechrageTimes() {
		return rechrageTimes;
	}

	public void setRechrageTimes(String rechrageTimes) {
		this.rechrageTimes = rechrageTimes;
	}
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}
	
	public String getGetVirualMoney() {
		return getVirualMoney;
	}

	public void setGetVirualMoney(String getVirualMoney) {
		this.getVirualMoney = getVirualMoney;
	}
	
	public String getGetVipExp() {
		return getVipExp;
	}

	public void setGetVipExp(String getVipExp) {
		this.getVipExp = getVipExp;
	}
	
	public String getPlayerLevel() {
		return playerLevel;
	}

	public void setPlayerLevel(String playerLevel) {
		this.playerLevel = playerLevel;
	}
	
	public String getVirualMoney() {
		return virualMoney;
	}

	public void setVirualMoney(String virualMoney) {
		this.virualMoney = virualMoney;
	}
	
	public String getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}
	
	public String getVipExp() {
		return vipExp;
	}

	public void setVipExp(String vipExp) {
		this.vipExp = vipExp;
	}
	
	public String getOldVipLevel() {
		return oldVipLevel;
	}

	public void setOldVipLevel(String oldVipLevel) {
		this.oldVipLevel = oldVipLevel;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + ILog.SPLIT_CHAR );
	    if( strs.length < 21 ){
			return false;
		}

		logTime = strs[ 0 ];
		gameId = strs[ 1 ];
		areaId = strs[ 2 ];
		serverId = strs[ 3 ];
		userId = strs[ 4 ];
		roleId = strs[ 5 ];
		rechrageTimes = strs[ 6 ];
		orderId = strs[ 7 ];
		money = strs[ 8 ];
		currency = strs[ 9 ];
		result = strs[ 10 ];
		resultCode = strs[ 11 ];
		productId = strs[ 12 ];
		productName = strs[ 13 ];
		productPrice = strs[ 14 ];
		getVirualMoney = strs[ 15 ];
		getVipExp = strs[ 16 ];
		playerLevel = strs[ 17 ];
		virualMoney = strs[ 18 ];
		vipLevel = strs[ 19 ];
		vipExp = strs[ 20 ];

		if( strs.length > 21 ){
			oldVipLevel = strs[ 21 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( logTime );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( gameId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( userId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( roleId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( rechrageTimes );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( orderId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( money );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( currency );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( result );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( resultCode );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( productId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( productName );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( productPrice );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( getVirualMoney );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( getVipExp );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( playerLevel );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( virualMoney );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( vipLevel );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( vipExp );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( oldVipLevel );

	}

	public void reset( ){
		logTime = null;
		gameId = null;
		areaId = null;
		serverId = null;
		userId = null;
		roleId = null;
		rechrageTimes = null;
		orderId = null;
		money = null;
		currency = null;
		result = null;
		resultCode = null;
		productId = null;
		productName = null;
		productPrice = null;
		getVirualMoney = null;
		getVipExp = null;
		playerLevel = null;
		virualMoney = null;
		vipLevel = null;
		vipExp = null;
		oldVipLevel = null;
	}
}
