package com.rtsapp.server.gamelog.api.model;

import javax.persistence.*;
import com.rtsapp.server.gamelog.api.ILog;

@Entity
@Table
public class RegisterLog implements ILog {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	public long id;
	@Column(columnDefinition="TIMESTAMP")
	public String logTime;
	public String gameId;
	public String areaId;
	public String serverId;
	public String userId;
	public String device;
	public String imei;
	public String mac;
	public String resolution;
	public String os;
	public String osVersion;
	public String openUDID;
	public String ADUDID;
	public String APPUDID;
	public String CENTERID;
	public String ip;
	public String channel;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}
	
	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}
	
	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	
	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getOpenUDID() {
		return openUDID;
	}

	public void setOpenUDID(String openUDID) {
		this.openUDID = openUDID;
	}
	
	public String getADUDID() {
		return ADUDID;
	}

	public void setADUDID(String ADUDID) {
		this.ADUDID = ADUDID;
	}
	
	public String getAPPUDID() {
		return APPUDID;
	}

	public void setAPPUDID(String APPUDID) {
		this.APPUDID = APPUDID;
	}
	
	public String getCENTERID() {
		return CENTERID;
	}

	public void setCENTERID(String CENTERID) {
		this.CENTERID = CENTERID;
	}
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + ILog.SPLIT_CHAR );
	    if( strs.length < 16 ){
			return false;
		}

		logTime = strs[ 0 ];
		gameId = strs[ 1 ];
		areaId = strs[ 2 ];
		serverId = strs[ 3 ];
		userId = strs[ 4 ];
		device = strs[ 5 ];
		imei = strs[ 6 ];
		mac = strs[ 7 ];
		resolution = strs[ 8 ];
		os = strs[ 9 ];
		osVersion = strs[ 10 ];
		openUDID = strs[ 11 ];
		ADUDID = strs[ 12 ];
		APPUDID = strs[ 13 ];
		CENTERID = strs[ 14 ];
		ip = strs[ 15 ];

		if( strs.length > 16 ){
			channel = strs[ 16 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( logTime );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( gameId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( userId );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( device );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( imei );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( mac );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( resolution );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( os );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( osVersion );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( openUDID );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( ADUDID );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( APPUDID );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( CENTERID );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( ip );

		sb.append( ILog.SPLIT_CHAR );
		sb.append( channel );

	}

	public void reset( ){
		logTime = null;
		gameId = null;
		areaId = null;
		serverId = null;
		userId = null;
		device = null;
		imei = null;
		mac = null;
		resolution = null;
		os = null;
		osVersion = null;
		openUDID = null;
		ADUDID = null;
		APPUDID = null;
		CENTERID = null;
		ip = null;
		channel = null;
	}
}
