package com.rtsapp.server.gamelog.api.model;

import javax.persistence.*;

@Entity
@Table
public class User{

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	public long id;
	@Column( unique = true )
	public String userName;
	public String passWord;

	public String channels;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getChannels() {
		return channels;
	}

	public void setChannels(String channels) {
		this.channels = channels;
	}
}
