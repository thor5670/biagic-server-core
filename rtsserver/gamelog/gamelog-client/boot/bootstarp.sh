#!/bin/bash

echo "开始启动服务器"

if [ $# -lt 1 ]; then
    echo "please enter server id ,if is dont have id,player enter 0"
    exit
fi


echo "配置 core dump"
ulimit -c unlimited
ulimit -n 10000

echo "参数配置"

BREAK_CONFIG="false"
REMOTE_CONFIG_PATH="/data/tank2d/config.xml"
LOCAL_CONFIG_PATH="cfg/logclient.xml"
JAR_NAME="gamelog-client-0.0.1.jar"

SERVER_TYPE="gamelogClient"

echo "break config : $BREAK_CONFIG"
echo "remote config path : $REMOTE_CONFIG_PATH"
echo "local config path : $LOCAL_CONFIG_PATH"
echo "execute jar name : $JAR_NAME"
echo "current server type : $SERVER_TYPE"

nohup java JVM_OPTION -jar $JAR_NAME -b:$BREAK_CONFIG -c:$REMOTE_CONFIG_PATH -s:$SERVER_TYPE -f:$LOCAL_CONFIG_PATH -i:$1 & > /dev/null 2>&1

sleep 1
exit
