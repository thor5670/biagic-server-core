package com.rtsapp.server.gamelog.client;

import com.rtsapp.server.gamelog.client.socket.ISocketHandler;
import com.rtsapp.server.gamelog.client.socket.LogInHandler;
import com.rtsapp.server.gamelog.client.tail.ILogProcessor;
import com.rtsapp.server.gamelog.client.tail.LogDir;
import com.rtsapp.server.gamelog.client.tail.LogTailMgr;
import com.rtsapp.server.gamelog.cmd.cmd.Commands;
import com.rtsapp.server.gamelog.cmd.cmd.LoginRequest;
import com.rtsapp.server.gamelog.cmd.cmd.SendLogRequest;
import com.rtsapp.server.gamelog.cmd.cmd.StartLogCollectResponse;
import com.rtsapp.server.gamelog.cmd.model.LogInfo;
import com.rtsapp.server.gamelog.cmd.model.ServerInfo;
import com.rtsapp.server.network.client.NettyClient;
import com.rtsapp.server.network.protocol.command.CommandOutHandler;
import com.rtsapp.server.network.protocol.command.ICommand;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelOutboundHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

import java.util.concurrent.ThreadFactory;

/**
 * 日志客户端
 */
public class LogClient {

    private final LogConfig config;
    private final ILogProcessor tailProcessor;
    private final ISocketHandler socketHandler;
    private final LogTailMgr tailMgr;
    private final NettyClient socketClient;


    public LogClient(LogConfig config) {
        this.config = config;

        tailProcessor = new LogTailProcessor();
        socketHandler = new LogClientSocketHandler();

        tailMgr = new LogTailMgr(tailProcessor, config.getDirList());


        //创建网络客户端
        EventLoopGroup eventLoop = new NioEventLoopGroup(1, r -> new Thread(r, "LogEventloop"));

        ChannelInboundHandler inHandler = new LogInHandler(socketHandler);
        ChannelOutboundHandler outHandler = new CommandOutHandler();

        socketClient = new NettyClient(eventLoop, config.getServerIp(), config.getServerPort(), 40960000, inHandler, outHandler);

    }


    public void start() {
        //启动网络连接
        socketClient.connect();
    }


    private class LogClientSocketHandler implements ISocketHandler {

        @Override
        public void onRecieve(ChannelHandlerContext ctx, int commandId, ICommand cmd) {

            switch (commandId) {

                case Commands.StartLogCollect: {//开始采集

                    StartLogCollectResponse response = (StartLogCollectResponse) cmd;
                    tailMgr.startLogCollect(response);

                    break;
                }

                case Commands.StopLogCollect: { //停止采集
                    tailMgr.stopLogCollect();
                    break;
                }
            }
        }

        @Override
        public void onConnect(ChannelHandlerContext ctx) {

            LoginRequest request = new LoginRequest();

            for(LogDir logDir :  config.getDirList() ) {
                ServerInfo info = new ServerInfo();
                info.setGameId( logDir.getGameId() );
                info.setAreaId( logDir.getAreaId() );
                info.setServerId( logDir.getServerId() );

                request.getClientInfo().getServerList().add( info );
            }

            socketClient.send( request );

        }

        @Override
        public void onClose(ChannelHandlerContext ctx) {
            tailMgr.stopLogCollect();
        }
    }


    private class LogTailProcessor implements ILogProcessor {

        @Override
        public void onTailLog( String logType,  String log) {

            SendLogRequest request = new SendLogRequest();

            LogInfo info = new LogInfo();
            info.setLogType(logType);
            info.setLog(log);
            request.getLogList().add(info);

            //2. 消息发送出去
            socketClient.send(request);
        }
    }

}
