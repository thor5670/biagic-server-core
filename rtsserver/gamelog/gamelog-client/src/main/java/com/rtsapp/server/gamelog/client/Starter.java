package com.rtsapp.server.gamelog.client;

import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;
import com.rtsapp.tank2d.config.ConfigHub;

import org.apache.log4j.PropertyConfigurator;

/**
 * Created by admin on 16-7-19.
 */
public class Starter {

    private static final Logger LOG = LoggerFactory.getLogger(Starter.class);


    public static void main(String[] args) {

        if (!LoggerFactory.configure("cfg/log.properties")) {
            System.err.println("日志设置失败");
            System.exit(1);
        }

        ConfigHub.config(args);

        //日志启动
        PropertyConfigurator.configure("cfg/log4j.properties");

        LogConfig config = null;
        try {
            config = LogConfig.parse("cfg/logclient.xml");
        } catch (Exception e) {
            LOG.error("解析配置文件出错", e);
            shutdown();
        }

        if (config == null) {
            shutdown();
            return;
        }

        LogClient client = new LogClient(config);

        client.start();


//        Runtime.getRuntime().addShutdownHook(new Thread() {
//
//            public void run() {
//                shutdown();
//            }
//        });

    }

    private static void shutdown() {
        LOG.error("客户端进程退出");
        LoggerFactory.shutdown();
        System.exit(0);
    }


}
