package com.rtsapp.server.gamelog.client.socket;

import com.rtsapp.server.network.protocol.command.ICommand;
import io.netty.channel.ChannelHandlerContext;

/**
 * 网络接口
 */
public interface ISocketHandler {


    void onRecieve(ChannelHandlerContext ctx, int commandId, ICommand response);

    void onConnect(ChannelHandlerContext ctx);

    void onClose(ChannelHandlerContext ctx);


}
