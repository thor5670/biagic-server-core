package com.rtsapp.server.gamelog.client.tail;


public interface ILogProcessor {

    void onTailLog( String logType,  String log );
}
