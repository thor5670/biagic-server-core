package com.rtsapp.server.gamelog.client.tail;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 某一个日志文件夹
 */
public class LogDir {


    public final File dir;
    private final String gameId;
    private final String areaId;
    private final String serverId;

    public LogDir(File dir, String gameId, String areaId, String serverId) {
        this.dir = dir;
        this.gameId = gameId;
        this.areaId = areaId;
        this.serverId = serverId;
    }

    public String getLogFile(String logType, long logMils) {

        String prefix = getLogPrefix(logType, logMils).toLowerCase();

         String[] files = dir.list();
        for (String fileName : files) {
            if (fileName.toLowerCase().startsWith(prefix)) {
                return fileName;
            }
        }

        return null;
    }

    private String getLogPrefix(String logType, long logMils) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return gameId + ".game." + logType + "." + sdf.format(new Date(logMils));
    }

    public String getGameId() {
        return gameId;
    }

    public String getAreaId() {
        return areaId;
    }

    public String getServerId() {
        return serverId;
    }

    public File getDir() {
        return dir;
    }
}
