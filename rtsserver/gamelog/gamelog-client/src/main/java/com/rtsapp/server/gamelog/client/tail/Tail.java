package com.rtsapp.server.gamelog.client.tail;

import java.io.*;

/**
 * 单个文件的读取
 */
public class Tail {

    private final BufferedReader br;

    public Tail( File dir,  String fileName) throws FileNotFoundException {
        br = new BufferedReader(new InputStreamReader(new FileInputStream(new File( dir, fileName))));
    }


    /**
     * 跳过多少行
     *
     * @param lineSize
     * @return 成功跳过返回true
     */
    public boolean skipLines(int lineSize) throws IOException {

        String line = null;
        for (int i = 0; i < lineSize; i++) {
            line = br.readLine();
            if (line == null) {
                return false;
            }
        }
        return true;
    }


    public String tail() throws IOException {
        return br.readLine();
    }


    /**
     * 关闭流
     * @throws IOException
     */
    public void close() throws IOException {
        br.close();
    }

}
