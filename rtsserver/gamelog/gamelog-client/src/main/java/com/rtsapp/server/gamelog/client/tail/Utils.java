package com.rtsapp.server.gamelog.client.tail;

import com.rtsapp.server.utils.time.DayPeriodCalendar;

/**
 *
 */
public class Utils {

    private static DayPeriodCalendar calendar = new DayPeriodCalendar(0);

    public static DayPeriodCalendar getDayCalendar() {
        return calendar;
    }
}
