package com.rtsapp.server.gamelog.cmd.cmd;


import java.util.*;
import java.lang.reflect.Field;
import com.rtsapp.server.network.protocol.command.*;
import com.rtsapp.server.common.*;
import com.rtsapp.server.profiling.Profilling;
import com.rtsapp.server.gamelog.cmd.model.*;


public class Commands{
//modules
	public static final int MODULE_LOGIN = 0x01;
//commands
	public static final int Login = 0x010001;
	public static final int StartLogCollect = 0x010002;
	public static final int StopLogCollect = 0x010003;
	public static final int SendLog = 0x010004;



	public static ICommand createRequest( int commandId ){

		switch ( commandId ){
		        case Login :{
        		return new LoginRequest();
        	}
                case StartLogCollect :{
        		return new StartLogCollectRequest();
        	}
                case StopLogCollect :{
        		return new StopLogCollectRequest();
        	}
                case SendLog :{
        		return new SendLogRequest();
        	}
        		}

		return null;
	}

	public static ICommand createResponse( int commandId ){

		switch ( commandId ){
				case Login :{
				return new LoginResponse();
			}
				case StartLogCollect :{
				return new StartLogCollectResponse();
			}
				case StopLogCollect :{
				return new StopLogCollectResponse();
			}
				case SendLog :{
				return new SendLogResponse();
			}
				}

		return null;
	}

	public static void initProfilling( int categoryId ){

		try {
			Field[] fields = Commands.class.getFields();

			List<String> nameList = new ArrayList<String>();
			for (Field f : fields) {
				if (!f.getName().startsWith("MODULE_")) {
					try {
						int commandId = f.getInt(Commands.class);
						String commandName = f.getName();
						Profilling.getInstance().getExecuteProfilling( categoryId , commandId).setExecuteName(commandName);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
		}catch( Throwable ex){
			ex.printStackTrace();
		}

	}
}
