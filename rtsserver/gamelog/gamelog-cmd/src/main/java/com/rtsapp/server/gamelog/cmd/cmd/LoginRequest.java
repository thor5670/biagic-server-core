package com.rtsapp.server.gamelog.cmd.cmd;

import java.util.*;
import com.rtsapp.server.network.protocol.command.*;
import com.rtsapp.server.common.*;
import com.rtsapp.server.gamelog.cmd.model.*;

public class  LoginRequest implements ICommand{
		
	public ClientInfo clientInfo = new ClientInfo ( );

	
	public ClientInfo getClientInfo() {
		return clientInfo;
	}

	public void setClientInfo(ClientInfo clientInfo) {
		this.clientInfo = clientInfo;
	}

	public int getCommandId(){
		return Commands.Login;
	}
	
	public void readFromBuffer( IByteBuffer buffer ){
		buffer.readInt();
		if( buffer.readBool( ) ){
			if( clientInfo == null ){
				clientInfo = new ClientInfo(); 
			}
			clientInfo.readFromBuffer( buffer );
		}else{
			clientInfo = null;
		}

	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		buffer.writeInt( this.getCommandId() );
		if( clientInfo != null ){
			buffer.writeBool( true );
			clientInfo.writeToBuffer( buffer );
		}else{
			buffer.writeBool( false );
		}

	}
	
	
}

