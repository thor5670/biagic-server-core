package com.rtsapp.server.gamelog.cmd.cmd;

import java.util.*;
import com.rtsapp.server.network.protocol.command.*;
import com.rtsapp.server.common.*;
import com.rtsapp.server.gamelog.cmd.model.*;


public class  LoginResponse implements ICommand {

	
	public int errorNo;

	
	public int getErrorNo() {
		return errorNo;
	}

	public void setErrorNo(int errorNo) {
		this.errorNo = errorNo;
	}

	public int getCommandId(){
		return Commands.Login;
	}
	
	public void readFromBuffer( IByteBuffer buffer ){
		buffer.readInt();
		errorNo = buffer.readInt( );
	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		buffer.writeInt( this.getCommandId() );
		buffer.writeInt( errorNo);
	}
}





