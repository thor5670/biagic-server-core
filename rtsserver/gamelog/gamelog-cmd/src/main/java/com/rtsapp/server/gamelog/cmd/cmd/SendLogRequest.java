package com.rtsapp.server.gamelog.cmd.cmd;

import java.util.*;
import com.rtsapp.server.network.protocol.command.*;
import com.rtsapp.server.common.*;
import com.rtsapp.server.gamelog.cmd.model.*;

public class  SendLogRequest implements ICommand{
		
	public ArrayList<LogInfo> logList = new ArrayList<LogInfo> ( );

	
	public ArrayList<LogInfo> getLogList() {
		return logList;
	}

	public void setLogList(ArrayList<LogInfo> logList) {
		this.logList = logList;
	}

	public int getCommandId(){
		return Commands.SendLog;
	}
	
	public void readFromBuffer( IByteBuffer buffer ){
		buffer.readInt();
		{
    		int size = buffer.readInt();
    		for( int i = 0; i < size; i++ ){
        		LogInfo model = new LogInfo( );
        		model.readFromBuffer( buffer );
				logList.add( model ); 
    		}
		}
	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		buffer.writeInt( this.getCommandId() );
		{
    		int size;
    		if( logList==null ){
    		    size = 0;
    		}else{
    		    size = logList.size();
    		}
	    	buffer.writeInt( size ); 
    		for( int i = 0; i < size; i++ ){
        		LogInfo model = logList.get( i ) ; 
			if(model==null) {
				model = new LogInfo( );
				System.err.println( this.getClass().getSimpleName() + ".logList 第" + i + "个元素为null"   ); 
			}
        		model.writeToBuffer( buffer );
    		}
		}
	}
	
	
}

