package com.rtsapp.server.gamelog.cmd.cmd;

import java.util.*;
import com.rtsapp.server.network.protocol.command.*;
import com.rtsapp.server.common.*;
import com.rtsapp.server.gamelog.cmd.model.*;


public class  SendLogResponse implements ICommand {

	


	public int getCommandId(){
		return Commands.SendLog;
	}
	
	public void readFromBuffer( IByteBuffer buffer ){
		buffer.readInt();
	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		buffer.writeInt( this.getCommandId() );
	}
}





