package com.rtsapp.server.gamelog.cmd.cmd;

import java.util.*;
import com.rtsapp.server.network.protocol.command.*;
import com.rtsapp.server.common.*;
import com.rtsapp.server.gamelog.cmd.model.*;


public class  StartLogCollectResponse implements ICommand {

	
	public long logStartTime;
	public ArrayList<ServerLogProgress> logProgressList = new ArrayList<ServerLogProgress> ( );

	
	public long getLogStartTime() {
		return logStartTime;
	}

	public void setLogStartTime(long logStartTime) {
		this.logStartTime = logStartTime;
	}
	
	public ArrayList<ServerLogProgress> getLogProgressList() {
		return logProgressList;
	}

	public void setLogProgressList(ArrayList<ServerLogProgress> logProgressList) {
		this.logProgressList = logProgressList;
	}

	public int getCommandId(){
		return Commands.StartLogCollect;
	}
	
	public void readFromBuffer( IByteBuffer buffer ){
		buffer.readInt();
		logStartTime = buffer.readLong( );
		{
    		int size = buffer.readInt();
    		for( int i = 0; i < size; i++ ){
        		ServerLogProgress model = new ServerLogProgress( );
        		model.readFromBuffer( buffer );
				logProgressList.add( model ); 
    		}
		}
	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		buffer.writeInt( this.getCommandId() );
		buffer.writeLong( logStartTime);
		{
    		int size;
    		if( logProgressList==null ){
    		    size = 0;
    		}else{
    		    size = logProgressList.size();
    		}
	    	buffer.writeInt( size ); 
    		for( int i = 0; i < size; i++ ){
        		ServerLogProgress model = logProgressList.get( i ) ; 
			if(model==null) {
				model = new ServerLogProgress( );
				System.err.println( this.getClass().getSimpleName() + ".logProgressList 第" + i + "个元素为null"   ); 
			}
        		model.writeToBuffer( buffer );
    		}
		}
	}
}





