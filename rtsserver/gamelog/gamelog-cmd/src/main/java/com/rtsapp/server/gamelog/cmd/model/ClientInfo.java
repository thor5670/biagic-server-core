package com.rtsapp.server.gamelog.cmd.model;

import java.util.*;
import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.common.IByteBufferSerializable;

@SuppressWarnings("unused")
public class ClientInfo implements IByteBufferSerializable {
	
	public int clientId;
	public String token;
	public ArrayList<ServerInfo> serverList = new ArrayList<ServerInfo> ( );

	
	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public ArrayList<ServerInfo> getServerList() {
		return serverList;
	}

	public void setServerList(ArrayList<ServerInfo> serverList) {
		this.serverList = serverList;
	}

	public void readFromBuffer( IByteBuffer buffer ){
		clientId = buffer.readInt( );
		token = buffer.readUTF8( );
		{
    		int size = buffer.readInt();
    		for( int i = 0; i < size; i++ ){
        		ServerInfo model = new ServerInfo( );
        		model.readFromBuffer( buffer );
				serverList.add( model ); 
    		}
		}
	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		buffer.writeInt( clientId);
		buffer.writeUTF8( token);
		{
    		int size;
    		if( serverList==null ){
    		    size = 0;
    		}else{
    		    size = serverList.size();
    		}
	    	buffer.writeInt( size ); 
    		for( int i = 0; i < size; i++ ){
        		ServerInfo model = serverList.get( i ) ; 
			if(model==null) {
				model = new ServerInfo( );
				System.err.println( this.getClass().getSimpleName() + ".serverList 第" + i + "个元素为null"   ); 
			}
        		model.writeToBuffer( buffer );
    		}
		}
	}
}
