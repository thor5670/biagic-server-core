package com.rtsapp.server.gamelog.cmd.model;

import java.util.*;
import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.common.IByteBufferSerializable;

@SuppressWarnings("unused")
public class LogInfo implements IByteBufferSerializable {
	
	public String logType;
	public String log;

	
	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}
	
	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public void readFromBuffer( IByteBuffer buffer ){
		logType = buffer.readUTF8( );
		log = buffer.readUTF8( );
	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		buffer.writeUTF8( logType);
		buffer.writeUTF8( log);
	}
}
