package com.rtsapp.server.gamelog.cmd.model;

import java.util.*;
import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.common.IByteBufferSerializable;

@SuppressWarnings("unused")
public class LogProgress implements IByteBufferSerializable {
	
	public String logType;
	public long stopLogDay;
	public int stopLogLine;

	
	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}
	
	public long getStopLogDay() {
		return stopLogDay;
	}

	public void setStopLogDay(long stopLogDay) {
		this.stopLogDay = stopLogDay;
	}
	
	public int getStopLogLine() {
		return stopLogLine;
	}

	public void setStopLogLine(int stopLogLine) {
		this.stopLogLine = stopLogLine;
	}

	public void readFromBuffer( IByteBuffer buffer ){
		logType = buffer.readUTF8( );
		stopLogDay = buffer.readLong( );
		stopLogLine = buffer.readInt( );
	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		buffer.writeUTF8( logType);
		buffer.writeLong( stopLogDay);
		buffer.writeInt( stopLogLine);
	}
}
