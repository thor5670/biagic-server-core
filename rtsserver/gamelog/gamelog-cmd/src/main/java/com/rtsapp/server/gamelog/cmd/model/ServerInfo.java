package com.rtsapp.server.gamelog.cmd.model;

import java.util.*;
import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.common.IByteBufferSerializable;

@SuppressWarnings("unused")
public class ServerInfo implements IByteBufferSerializable {
	
	public String gameId;
	public String areaId;
	public String serverId;

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public void readFromBuffer( IByteBuffer buffer ){
		gameId = buffer.readUTF8( );
		areaId = buffer.readUTF8( );
		serverId = buffer.readUTF8( );
	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		buffer.writeUTF8( gameId);
		buffer.writeUTF8( areaId);
		buffer.writeUTF8( serverId);
	}
}
