package com.rtsapp.server.gamelog.cmd.model;

import java.util.*;
import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.common.IByteBufferSerializable;

@SuppressWarnings("unused")
public class ServerLogProgress implements IByteBufferSerializable {
	
	public ServerInfo serverInfo = new ServerInfo ( );
	public ArrayList<LogProgress> progressList = new ArrayList<LogProgress> ( );

	
	public ServerInfo getServerInfo() {
		return serverInfo;
	}

	public void setServerInfo(ServerInfo serverInfo) {
		this.serverInfo = serverInfo;
	}
	
	public ArrayList<LogProgress> getProgressList() {
		return progressList;
	}

	public void setProgressList(ArrayList<LogProgress> progressList) {
		this.progressList = progressList;
	}

	public void readFromBuffer( IByteBuffer buffer ){
		if( buffer.readBool( ) ){
			if( serverInfo == null ){
				serverInfo = new ServerInfo(); 
			}
			serverInfo.readFromBuffer( buffer );
		}else{
			serverInfo = null;
		}

		{
    		int size = buffer.readInt();
    		for( int i = 0; i < size; i++ ){
        		LogProgress model = new LogProgress( );
        		model.readFromBuffer( buffer );
				progressList.add( model ); 
    		}
		}
	}
	
	public void writeToBuffer( IByteBuffer buffer ){
		if( serverInfo != null ){
			buffer.writeBool( true );
			serverInfo.writeToBuffer( buffer );
		}else{
			buffer.writeBool( false );
		}

		{
    		int size;
    		if( progressList==null ){
    		    size = 0;
    		}else{
    		    size = progressList.size();
    		}
	    	buffer.writeInt( size ); 
    		for( int i = 0; i < size; i++ ){
        		LogProgress model = progressList.get( i ) ; 
			if(model==null) {
				model = new LogProgress( );
				System.err.println( this.getClass().getSimpleName() + ".progressList 第" + i + "个元素为null"   ); 
			}
        		model.writeToBuffer( buffer );
    		}
		}
	}
}
