package com.rtsapp.server.gamelog.hub;


import com.rtsapp.server.gamelog.api.GamelogHub;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by apple on 16/12/20.
 */
public class Main {
    /**
     * 辅助日志处理工具
     *
     * -h mysqlhost -p mysqlport -u mysqluser -pa mysqlpass -f filepath
     *
     * @param args
     */

    private static final String HELP = "java -jar gamelog-hub.jar [-h] -h:host -p:port -u:user -d:database -pa:pass -f:filepath" +
            "-help help\n\r" +
            "-h mysqlhost\n\r" +
            "-p mysqlport\n\r" +
            "-u mysqluser\n\r" +
            "-d mysqldatabase\n\r" +
            "-pa mysqlpass\n\r" +
            "-f filepath";

    public static void main(String[] args) {

        Map<String, String> paraMap = new HashMap<>();
        paraMap.put("-h", null);
        paraMap.put("-p", null);
        paraMap.put("-u", null);
        paraMap.put("-d", null);
        paraMap.put("-pa", null);
        paraMap.put("-f", null);


        for (String arg : args) {
            String[] argInfo = arg.split(":");

            if (argInfo.length == 0 || argInfo[0] == null || "".equals(argInfo[0]) || argInfo[0].equals("-help")) {
                System.out.println(HELP);
                System.exit(0);
            }

            if (argInfo.length != 2) {
                System.out.println("the argument least 2");
                System.exit(0);
            }

            if (paraMap.containsKey(argInfo[0])) {
                paraMap.put(argInfo[0],argInfo[1]);
            }else {
                System.out.println("illagel argument"+argInfo[0]);
            }
        }

        for (String key : paraMap.keySet()) {
            if (paraMap.get(key) == null || "".equals(paraMap.get(key))) {
                System.out.println(String.format("param %s cant be null", key));
                System.exit(0);
            }
        }

        File logFilePath = new File(paraMap.get("-f"));
        if (!logFilePath.exists()) {
            System.out.println(String.format("the file path %s is not exist",logFilePath.getAbsolutePath()));
            System.exit(0);
        }

        File[] files =null;
        if (logFilePath.isDirectory()) {
            files = logFilePath.listFiles();
        }else {
            files = new File[1];
            files[0] = logFilePath;
        }
        Connection connection = null;
        try {
            String connectionUrl = String.format("jdbc:mysql://%s:%s/%s?characterEncoding=UTF8",paraMap.get("-h"),paraMap.get("-p"),paraMap.get("-d"));
            connection = DriverManager.getConnection(connectionUrl, paraMap.get("-u"), paraMap.get("-pa"));
        } catch (SQLException e) {
            System.out.println("data base init error");
            e.printStackTrace();
            System.exit(0);
        }

        for (File file : files) {
            String[] fileInfo = file.getName().split("\\.");
            if (fileInfo.length != 5) {
                System.out.println(String.format("the file %s is not avail file ,break ", file.getName()));
                continue;
            }

            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    String SQL = GamelogHub.createSql(fileInfo[2].toLowerCase(), line);
                    System.out.println(SQL);
                    PreparedStatement preparedStatement = connection.prepareStatement(SQL);
                    preparedStatement.execute();
                    System.gc();
                    System.out.println(Runtime.getRuntime().totalMemory());
                }
            } catch (FileNotFoundException e) {
                System.out.println(String.format("file %s not found,break", file.getName()));
            } catch (IOException e) {
                System.out.println(String.format("exception found when read file %s", file.getName()));
                e.printStackTrace();
            } catch (SQLException e) {
                System.out.println("sql exception");
                e.printStackTrace();
            }
        }



        
    }

}
