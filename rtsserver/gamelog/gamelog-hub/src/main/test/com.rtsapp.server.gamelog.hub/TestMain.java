package com.rtsapp.server.gamelog.hub;

import org.junit.Test;

import java.sql.SQLException;

/**
 * Created by apple on 16/12/20.
 */
public class TestMain {

    @Test
    public void testMain() {
        String[] args = new String[6];

        args[0] = "-h:127.0.0.1";
        args[1] = "-p:3306";
        args[2] = "-u:root";
        args[3] = "-pa:root";
        args[4] = "-d:tank2d_gamelog";
        args[5] = "-f:/Users/apple/workspace/rtsapp/data/gateway";

        Main.main(args);

        args[5] = "-f:/Users/apple/workspace/rtsapp/data/gameserver";
        Main.main(args);
    }
}
