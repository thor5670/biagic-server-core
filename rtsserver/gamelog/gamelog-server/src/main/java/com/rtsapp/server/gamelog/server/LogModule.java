package com.rtsapp.server.gamelog.server;

import com.rtsapp.server.domain.EntityBuilder;
import com.rtsapp.server.domain.mysql.sql.MySQLModule;
import com.rtsapp.server.gamelog.server.dao.GamelogDao;
import com.rtsapp.server.gamelog.server.dao.impl.ConfigDao;
import com.rtsapp.server.gamelog.server.entity.ConfigEntity;
import com.rtsapp.server.gamelog.server.handler.CommandInHandler;
import com.rtsapp.server.gamelog.server.service.ILogService;
import com.rtsapp.server.gamelog.server.service.impl.LogServiceImpl;
import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;
import com.rtsapp.server.module.AbstractModule;
import com.rtsapp.server.module.Application;
import com.rtsapp.server.module.Module;

/**
 * Created by admin on 16-7-19.
 */
public class LogModule extends AbstractModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogModule.class);

    private Application application;

    private ILogService logService;

    @Override
    public boolean initialize( Module parentModule ) {

        if ( parentModule == null ) {
            LOGGER.error( getName( ) + " initialize 失败, parentModule is null" );
            return false;
        }

        if ( !( parentModule instanceof Application ) ) {
            LOGGER.error( getName( ) + " initialize 失败, parentModule is not Application " );
            return false;
        }

        this.application = ( Application ) parentModule;

        return true;
    }


    public ILogService getLogService(){
        return this.logService;
    }

    @Override
    public boolean start( ) {

        // 创建 dao
        MySQLModule mysqlModule = application.getModule( MySQLModule.class );
        if ( mysqlModule == null ) {
            LOGGER.error( getName( ) + " start失败,  MySQLModule 服务不存在" );
            return false;
        }

        //!CommandInHandler
        CommandInHandler inHandler = application.getObject( CommandInHandler.class );
        if ( inHandler == null ) {
            LOGGER.error( getName( ) + " start失败,  CommandInHandler 服务不存在" );
            return false;
        }



        EntityBuilder.registerEntity(ConfigEntity.class);

        ConfigDao configDao = null;
        try {

            configDao = (ConfigDao) Class.forName("com.rtsapp.server.gamelog.server.dao.impl.auto.ConfigDao$Impl").newInstance();

        } catch (Exception e) {
            LOGGER.error("创建dao失败", e);
            return false;
        }


        if (!configDao.setWorkpool(mysqlModule.getWorkPool("log_w"), mysqlModule.getWorkPool("log_w"))) {
            return false;
        }


        GamelogDao dao = new GamelogDao( mysqlModule.getWorkPool( "log_w" ) );

        if( ! dao.prepareStatement( dao.getWorkPool() ) ){
            LOGGER.error(  getName( ) + " start失败,  GamelogDao 预编译sql语句失败"  );
            return false;
        }

        //创建Service
        logService = new LogServiceImpl( configDao, dao );


        inHandler.setService(logService);

        return true;
    }



    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
