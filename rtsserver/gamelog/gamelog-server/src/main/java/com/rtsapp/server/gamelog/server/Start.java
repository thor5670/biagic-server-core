package com.rtsapp.server.gamelog.server;

import com.rtsapp.server.common.Timer;
import com.rtsapp.server.domain.mysql.sql.MySQLModule;
import com.rtsapp.server.gamelog.server.service.task.MYSQLPingTask;
import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;
import com.rtsapp.server.module.Application;
import com.rtsapp.tank2d.config.ConfigHub;

import org.apache.log4j.PropertyConfigurator;

import java.util.concurrent.TimeUnit;

public class Start {

    private static final Logger LOGGER = LoggerFactory.getLogger( Start.class );

    public static void main( String[] args ) {

        if (!LoggerFactory.configure("cfg/log.properties")) {
            System.err.println("日志设置失败");
            System.exit(1);
        }

        ConfigHub.config(args);

        PropertyConfigurator.configure( "cfg/log4j.properties" );

        final Application app = new Application( "cfg/gamelog_server.xml" );

        app.startApplication( );

        // mysql ping
        Timer.getInstance( ).start( );
        Timer.getInstance( ).scheduleAtFixedRate( new MYSQLPingTask( app.getModule( MySQLModule.class ) ), MYSQLPingTask.PING_INTERVAL, MYSQLPingTask.PING_INTERVAL, TimeUnit.SECONDS );


        // install a handler to cleanup when the process is killed directly
        Runtime.getRuntime( ).addShutdownHook( new Thread( ) {

            public void run( ) {
                try {
                    LOGGER.info( "服务器进程关闭" );
                    app.stop( );

                } catch ( Throwable ex ) {
                    LOGGER.info( "服务器关闭失败" );
                }
            }

        } );


    }

}
