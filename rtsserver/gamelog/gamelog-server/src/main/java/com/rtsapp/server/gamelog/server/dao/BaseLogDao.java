package com.rtsapp.server.gamelog.server.dao;

import com.rtsapp.server.domain.mysql.sql.DatabaseWorkerPool;

/**
 * Created by admin on 16/7/14.
 */
public abstract class BaseLogDao {

    private final DatabaseWorkerPool workPool;

    public BaseLogDao(DatabaseWorkerPool workPool) {
        this.workPool = workPool;
    }


    public DatabaseWorkerPool getWorkPool() {
        return workPool;
    }


   public abstract boolean prepareStatement( DatabaseWorkerPool db );

}
