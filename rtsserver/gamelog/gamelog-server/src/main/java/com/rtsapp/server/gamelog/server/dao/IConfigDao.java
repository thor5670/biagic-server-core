package com.rtsapp.server.gamelog.server.dao;

import com.rtsapp.server.gamelog.server.entity.ConfigEntity;

/**
 *
 */
public interface IConfigDao {

    void insert( ConfigEntity entity );

    void update( ConfigEntity entity);

    ConfigEntity get( long id );
}
