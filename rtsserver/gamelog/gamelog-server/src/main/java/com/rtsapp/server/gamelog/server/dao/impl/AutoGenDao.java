package com.rtsapp.server.gamelog.server.dao.impl;

import com.rtsapp.server.domain.DaoBuilder;
import com.rtsapp.server.domain.support.MysqlDaoBuilder;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by admin on 15-11-27.
 */
public class AutoGenDao {

    public static void main( String[] args ) throws IOException {
        String prefix = "package com.rtsapp.server.gamelog.server.dao.impl.auto;\n" +
                "\n" +
                "import java.sql.*;\n" +
                "import java.util.*;\n" +
                "\n" +
                "import com.rtsapp.server.domain.*;\n" +
                "import com.rtsapp.server.domain.mysql.sql.*;\n";


        DaoBuilder daoBuilder = new MysqlDaoBuilder();

        Class[ ] daoclasses = { ConfigDao.class };

        for( Class c : daoclasses ){
            StringBuilder code =  daoBuilder.getDaoCode( c );
            code.insert( 0, prefix );

            System.out.println( "====" + c.getName() + "=====");
            System.out.println( code.toString() );
            System.out.println( "\r\n\r\n" );

            File file = new File( "src/main/java/com/rtsapp/server/gamelog/server/dao/impl/auto/" + c.getSimpleName() + "$Impl.java" );

            System.out.println( file.getAbsolutePath() );

            if( !file.getParentFile().exists() ){
                file.getParentFile().mkdirs();
            }

            if( !file.exists() ){
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(  file );
            fw.write( code.toString() );
            fw.flush();
            fw.close();

        }

    }
}
