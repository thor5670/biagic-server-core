package com.rtsapp.server.gamelog.server.dao.impl;

import com.rtsapp.server.domain.mysql.sql.MySQLDao;
import com.rtsapp.server.gamelog.server.dao.IConfigDao;
import com.rtsapp.server.gamelog.server.entity.ConfigEntity;

/**
 * Created by admin on 16-7-21.
 */
public abstract class ConfigDao extends MySQLDao<ConfigEntity,Long> implements IConfigDao {

}
