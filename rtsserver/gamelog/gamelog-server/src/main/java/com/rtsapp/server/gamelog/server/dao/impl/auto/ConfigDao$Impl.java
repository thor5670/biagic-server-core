package com.rtsapp.server.gamelog.server.dao.impl.auto;

import java.sql.*;
import java.util.*;

import com.rtsapp.server.domain.*;
import com.rtsapp.server.domain.mysql.sql.*;
class ConfigDao$ResultHandler implements MySQLResultSetHandler{
 List entityList = new ArrayList() ;public List getEntityList(){
            return entityList;
        }@SuppressWarnings("deprecation")
public void doResultSet(ResultSet rs) {
            try {
                while ( rs.next() ) {
                    com.rtsapp.server.gamelog.server.entity.ConfigEntity entity = EntityBuilder.newEntity( com.rtsapp.server.gamelog.server.entity.ConfigEntity.class );
entity.setId( (rs.getInt(1)) );
entity.setLogStartMils( (rs.getLong(2)) );
                    entityList.add( entity );
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }}
public class ConfigDao$Impl extends com.rtsapp.server.gamelog.server.dao.impl.ConfigDao{
public com.rtsapp.server.gamelog.server.entity.ConfigEntity get( long id ){
 com.rtsapp.server.domain.mysql.sql.DatabaseWorkerPool db = getReadPool(  ); 
 MySQLPreparedStatement stmt =  db.getPreparedStatement( "IConfigDao_get_long" ); 
 stmt.setParams( new Object[]{ id } ); 
ConfigDao$ResultHandler  handler = new ConfigDao$ResultHandler(); 
 db.query( stmt, handler );
 if( handler.getEntityList().size() < 1 ){ return null; }else{
 return (com.rtsapp.server.gamelog.server.entity.ConfigEntity)( handler.getEntityList().get( 0 ) );
 } 
}
public void update( com.rtsapp.server.gamelog.server.entity.ConfigEntity entity ){
 com.rtsapp.server.domain.mysql.sql.DatabaseWorkerPool db = getWritePool(  ); 
 MySQLPreparedStatement stmt =  db.getPreparedStatement( "IConfigDao_update_ConfigEntity" ); 
 stmt.setParams( new Object[]{ entity.getLogStartMils(), entity.getId()} ); 
 db.execute( stmt );
}
public void insert( com.rtsapp.server.gamelog.server.entity.ConfigEntity entity ){
 com.rtsapp.server.domain.mysql.sql.DatabaseWorkerPool db = getWritePool(  ); 
 MySQLPreparedStatement stmt =  db.getPreparedStatement( "IConfigDao_insert_ConfigEntity" ); 
 stmt.setParams( new Object[]{entity.getId(),entity.getLogStartMils()} ); 
 db.execute( stmt );

}
public boolean innerPrepareStatement( com.rtsapp.server.domain.mysql.sql.DatabaseWorkerPool db ) {
if( !db.prepareStatement( "IConfigDao_get_long", " select id,logStartMils from ConfigEntity where id= ? " ) ){ return false; } 
 if( !db.prepareStatement( "IConfigDao_update_ConfigEntity"," update ConfigEntity set logStartMils=?  where id= ? " ) ){ return false; } 
if( !db.prepareStatement( "IConfigDao_insert_ConfigEntity", "insert into ConfigEntity( id,logStartMils ) values ( ?,? ) " ) ){ return false; } 
return true;
}}