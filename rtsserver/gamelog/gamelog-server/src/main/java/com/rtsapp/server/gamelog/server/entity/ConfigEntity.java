package com.rtsapp.server.gamelog.server.entity;

import com.rtsapp.server.domain.IEntity;

import javax.persistence.Entity;
import javax.persistence.Id;


/**
 * 采集日志的配置信息
 */
@Entity
public class ConfigEntity extends IEntity{

    @Id
    private int id;

    //日志开始的时间
    private long logStartMils;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getLogStartMils() {
        return logStartMils;
    }

    public void setLogStartMils(long logStartMils) {
        this.logStartMils = logStartMils;
    }
}
