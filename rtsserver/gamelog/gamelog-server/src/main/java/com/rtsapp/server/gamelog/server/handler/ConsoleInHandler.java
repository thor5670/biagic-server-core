package com.rtsapp.server.gamelog.server.handler;

import com.rtsapp.server.gamelog.server.LogModule;
import com.rtsapp.server.gamelog.server.entity.ConfigEntity;
import com.rtsapp.server.gamelog.server.service.ILogService;
import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;
import com.rtsapp.server.network.protocol.telnet.TelnetServerHandler;
import com.rtsapp.server.network.server.NettyServer;
import io.netty.channel.ChannelHandlerContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;


public class ConsoleInHandler extends TelnetServerHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ConsoleInHandler.class);

    private static final String NEXTLINE = "\r\n";

    @Override
    public void processMessage(ChannelHandlerContext ctx, String request) {


        String[] requestAttr = request.split(" ");

        boolean close = false;
        StringBuilder response = new StringBuilder();

        switch (requestAttr[0].trim().toLowerCase()) {

            case "help": {
                int index = 0;
                response.append( (++index) + ") status : system status" + NEXTLINE );
                response.append( (++index) + ") exit/quit : exit" + NEXTLINE );
                response.append( (++index) + ") logstart [ yyyy-MM-dd]" + NEXTLINE );
                break;
            }
            case "status": {

                response.append(  "#### 控制台在线人数:" + this.sessionManager.getSessionCount() + NEXTLINE );
                NettyServer gameServer = (NettyServer) this.application.getModule("logServer");
                response.append(  "#### 日志采集连接数:" + gameServer.getSessionManager().getSessionCount() + NEXTLINE );

                break;
            }

            case "exit":
            case "quit": {
                response.append(  "byebye, Have a good day!" + NEXTLINE );
                close = true;
                break;
            }

            case "logstart":{
                logStart( requestAttr, response );
                break;
            }

            default:
                response.append(  "command not found" + NEXTLINE );
                break;
        }


        if ( response.length() > 0 ) {
            response.append( NEXTLINE );
            writeResponse(ctx, response.toString(), close);
        }
    }

    private void logStart(String[] requestAttr, StringBuilder response) {

        LogModule module =  application.getObject(LogModule.class);
        ILogService service = module.getLogService();
        ConfigEntity config =  service.getConfig();
        SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd");

        if( requestAttr.length < 2 ){
            response.append("日志采集时间:" + sdf.format(config.getLogStartMils()));
        }else{
            try {
                long logStartMils =  sdf.parse( requestAttr[1] ).getTime();
                config.setLogStartMils( logStartMils );
                service.updateConfig( config );

                response.append("设置成功, 日志采集时间:" + sdf.format(config.getLogStartMils()));
            } catch (ParseException e) {
                response.append( "设置失败" );
            }

        }
    }


}
