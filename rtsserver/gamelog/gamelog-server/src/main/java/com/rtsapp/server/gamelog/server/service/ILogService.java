package com.rtsapp.server.gamelog.server.service;

import com.rtsapp.server.gamelog.server.entity.ConfigEntity;
import com.rtsapp.server.network.protocol.command.ICommand;
import com.rtsapp.server.network.session.Session;

/**
 * Created by admin on 16-7-19.
 */
public interface ILogService {


    void execute(  Session session, int commandId, ICommand request );


    ConfigEntity getConfig();

    void updateConfig( ConfigEntity entity );
}
