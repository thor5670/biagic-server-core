package com.rtsapp.server.gamelog.server.service.task;

import com.rtsapp.server.domain.mysql.sql.MySQLModule;
import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;

/**
 * Created by admin on 15-11-8.
 */
public class MYSQLPingTask implements Runnable  {

    private static final Logger LOGGER = LoggerFactory.getLogger(MYSQLPingTask.class);

    public static final int PING_INTERVAL = 15 * 60 ; //15分钟，秒为单位


    private final MySQLModule mysqlModule;

    public MYSQLPingTask( MySQLModule mysqlModule ){
        this.mysqlModule = mysqlModule;
        if( mysqlModule == null ){
            LOGGER.error( "MySQLModule不存在" );
            System.exit( 0 );
        }
    }

    @Override
    public void run() {

        try{

            mysqlModule.dbPoolKeepAlive();

        }catch( Throwable ex ){
            LOGGER.error( "定时ping数据库出错", ex );
        }

    }
}
