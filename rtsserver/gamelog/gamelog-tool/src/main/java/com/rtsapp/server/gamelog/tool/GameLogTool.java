package com.rtsapp.server.gamelog.tool;

import com.dyt.itool.parser.XmlCommandParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 16-7-11.
 */
public class GameLogTool {

    // excel, command.xml 存放目录
    private static final String inputDir = "cfg/";

    // 输出目录
    private static final String dir_api = "../gamelog-api/src/main/java/com/rtsapp/server/gamelog/api/";

    private static final String dir_storage = "../gamelog-server/src/main/java/com/rtsapp/server/gamelog/server/dao/";

    private static final String dir_cmd = "../gamelog-cmd/src/main/java/com/rtsapp/server/gamelog/cmd/";


    public static void main(String[] args ){

        generateLogApi();

        generateLogInsertDB();

        generateLogCmd();
    }

    private static void generateLogApi(){

        String commandFile = inputDir + "logdef.xml";

        XmlCommandParser parser = new XmlCommandParser();
        parser.parse( commandFile );

        //Commands
        Map<String, String> map = new HashMap<String, String>();


        //model
        map.put( inputDir + "java_log_entity.vm", dir_api + "model/?Log.java");
        parser.generateModelCodeOneByOne(map);



        map.clear();
        map.put(inputDir + "java_log_utils.vm", dir_api + "/GameLogUtils.java");
        parser.generateCommandCode(map);

        map.clear();
        map.put(inputDir + "java_log_types.vm", dir_api + "/GamelogType.java");
        parser.generateCommandCode(map);

        map.clear();;
        map.put(inputDir + "java_log_hub.vm",dir_api+"/GamelogHub.java");
        parser.generateCommandCode(map);

    }


    private static void generateLogInsertDB(){

        String commandFile = inputDir + "logdef.xml";

        XmlCommandParser parser = new XmlCommandParser();
        parser.parse( commandFile );

        //Commands
        Map<String, String> map = new HashMap<String, String>();

        //model

        map.put("cfg/java_log_gamelog_dao.vm", dir_storage + "/GamelogDao.java");
        parser.generateCommandCode( map );

    }



    private static void generateLogCmd() {


        String commandFile = inputDir + "command.xml";

        XmlCommandParser parser = new XmlCommandParser();
        parser.parse( commandFile );

        //Commands
        Map<String, String> map = new HashMap<String, String>();
        map.put("cfg/java_commands.vm", dir_cmd + "cmd/Commands.java");
        parser.generateCommandCode(map);
        map.clear();

        //model
        map.put("cfg/java_command_struct_onebyone.vm", dir_cmd + "model/?.java");
        parser.generateModelCodeOneByOne(map);


        //request, response
        map.clear();
        map.put("cfg/java_command_?.vm", dir_cmd + "cmd/?.java");
        parser.generateCommandCodeOneByOne(map);

    }

}
