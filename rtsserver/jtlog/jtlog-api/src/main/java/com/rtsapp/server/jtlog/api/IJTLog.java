package com.rtsapp.server.jtlog.api;

/**
 *
 */
public interface IJTLog {

    String SPLIT_CHAR = "|";

    String getGameId();


    void writeTo(StringBuilder sb);

    boolean readFrom(String str);

    void reset();

}
