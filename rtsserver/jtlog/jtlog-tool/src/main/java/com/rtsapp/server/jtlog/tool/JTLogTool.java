package com.rtsapp.server.jtlog.tool;

import com.dyt.itool.parser.XmlCommandParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 17-5-5.
 */
public class JTLogTool {

    // excel, command.xml 存放目录
    private static final String inputDir = "cfg/";

    // 输出目录
    private static final String dir_api = "../jtlog-api/src/main/java/com/rtsapp/server/jtlog/api/";

    public static void main(String[] argus){
        generateLogApi();
    }

    private static void generateLogApi(){

        String commandFile = inputDir + "logdef.xml";

        XmlCommandParser parser = new XmlCommandParser();
        parser.parse( commandFile );

        //Commands
        Map<String, String> map = new HashMap<String, String>();


        //model
        map.put( inputDir + "java_log_entity.vm", dir_api + "model/?Log.java");
        parser.generateModelCodeOneByOne(map);



        map.clear();
        map.put(inputDir + "java_log_utils.vm", dir_api + "/JTLogUtils.java");
        parser.generateCommandCode(map);

        map.clear();
        map.put(inputDir + "java_log_types.vm", dir_api + "/JTlogType.java");
        parser.generateCommandCode(map);

    }
}
