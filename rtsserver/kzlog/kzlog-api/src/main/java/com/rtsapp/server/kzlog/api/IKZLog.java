package com.rtsapp.server.kzlog.api;

/**
 *
 */
public interface IKZLog {

    String SPLIT_CHAR = "|";

    String getGameId();


    void writeTo( StringBuilder sb );

    boolean readFrom( String str );

    void reset();

}
