package com.rtsapp.server.kzlog.api;

import com.rtsapp.server.kzlog.api.logger.RollingCalendar;
import com.rtsapp.server.kzlog.api.logger.RollingLogger;
import com.rtsapp.server.kzlog.api.model.*;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by admin on 16-7-11.
 */
public class KZLogUtils {

    private static final ThreadLocal<SimpleDateFormat> threadLocalSDF = new ThreadLocal<>( );
    private static ThreadLocal<StringBuilder> sbLocal = new ThreadLocal();

    private static ThreadLocal<ActiveLog> activeThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger activeLogger;
    private static ThreadLocal<RegisterLog> registerThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger registerLogger;
    private static ThreadLocal<DebitLog> debitThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger debitLogger;
    private static ThreadLocal<LoginLog> loginThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger loginLogger;
    private static ThreadLocal<CreateroleLog> createroleThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger createroleLogger;
    private static ThreadLocal<CreditLog> creditThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger creditLogger;
    private static ThreadLocal<HeartbeatLog> heartbeatThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger heartbeatLogger;
    private static ThreadLocal<AdditemLog> additemThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger additemLogger;
    private static ThreadLocal<CostitemLog> costitemThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger costitemLogger;
    private static ThreadLocal<LeadLog> leadThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger leadLogger;
    private static ThreadLocal<UserphotoLog> userphotoThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger userphotoLogger;
    private static ThreadLocal<PvpLog> pvpThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger pvpLogger;
    private static ThreadLocal<PveLog> pveThreadLocal = new ThreadLocal<>() ;
    private static RollingLogger pveLogger;

    public static void initialize( String logDir, String gameId ){

        if( ! logDir.endsWith( "/" ) ){
            logDir += "/";
        }


        String dateFromat = "yyyy-MM-dd";

        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            activeLogger = new RollingLogger( logDir + gameId + ".game.active.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            registerLogger = new RollingLogger( logDir + gameId + ".game.register.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            debitLogger = new RollingLogger( logDir + gameId + ".game.debit.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            loginLogger = new RollingLogger( logDir + gameId + ".game.login.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            createroleLogger = new RollingLogger( logDir + gameId + ".game.createrole.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            creditLogger = new RollingLogger( logDir + gameId + ".game.credit.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            heartbeatLogger = new RollingLogger( logDir + gameId + ".game.heartbeat.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            additemLogger = new RollingLogger( logDir + gameId + ".game.additem.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            costitemLogger = new RollingLogger( logDir + gameId + ".game.costitem.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            leadLogger = new RollingLogger( logDir + gameId + ".game.lead.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            userphotoLogger = new RollingLogger( logDir + gameId + ".game.userphoto.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            pvpLogger = new RollingLogger( logDir + gameId + ".game.pvp.%s.txt", dateFromat, calendar);
        }
        {
            RollingCalendar calendar = new RollingCalendar();
            calendar.setType(RollingCalendar.TOP_OF_DAY, 0);
            pveLogger = new RollingLogger( logDir + gameId + ".game.pve.%s.txt", dateFromat, calendar);
        }
    }

    public static  ActiveLog getActiveLog(){
        ActiveLog log =  activeThreadLocal.get();
        if( log == null ){
            log = new ActiveLog();
            activeThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  ActiveLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        activeLogger.appendLog(  sb.toString() );
    }


    public static  RegisterLog getRegisterLog(){
        RegisterLog log =  registerThreadLocal.get();
        if( log == null ){
            log = new RegisterLog();
            registerThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  RegisterLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        registerLogger.appendLog(  sb.toString() );
    }


    public static  DebitLog getDebitLog(){
        DebitLog log =  debitThreadLocal.get();
        if( log == null ){
            log = new DebitLog();
            debitThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  DebitLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        debitLogger.appendLog(  sb.toString() );
    }


    public static  LoginLog getLoginLog(){
        LoginLog log =  loginThreadLocal.get();
        if( log == null ){
            log = new LoginLog();
            loginThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  LoginLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        loginLogger.appendLog(  sb.toString() );
    }


    public static  CreateroleLog getCreateroleLog(){
        CreateroleLog log =  createroleThreadLocal.get();
        if( log == null ){
            log = new CreateroleLog();
            createroleThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  CreateroleLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        createroleLogger.appendLog(  sb.toString() );
    }


    public static  CreditLog getCreditLog(){
        CreditLog log =  creditThreadLocal.get();
        if( log == null ){
            log = new CreditLog();
            creditThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  CreditLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        creditLogger.appendLog(  sb.toString() );
    }


    public static  HeartbeatLog getHeartbeatLog(){
        HeartbeatLog log =  heartbeatThreadLocal.get();
        if( log == null ){
            log = new HeartbeatLog();
            heartbeatThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  HeartbeatLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        heartbeatLogger.appendLog(  sb.toString() );
    }


    public static  AdditemLog getAdditemLog(){
        AdditemLog log =  additemThreadLocal.get();
        if( log == null ){
            log = new AdditemLog();
            additemThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  AdditemLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        additemLogger.appendLog(  sb.toString() );
    }


    public static  CostitemLog getCostitemLog(){
        CostitemLog log =  costitemThreadLocal.get();
        if( log == null ){
            log = new CostitemLog();
            costitemThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  CostitemLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        costitemLogger.appendLog(  sb.toString() );
    }


    public static  LeadLog getLeadLog(){
        LeadLog log =  leadThreadLocal.get();
        if( log == null ){
            log = new LeadLog();
            leadThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  LeadLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        leadLogger.appendLog(  sb.toString() );
    }


    public static  UserphotoLog getUserphotoLog(){
        UserphotoLog log =  userphotoThreadLocal.get();
        if( log == null ){
            log = new UserphotoLog();
            userphotoThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  UserphotoLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        userphotoLogger.appendLog(  sb.toString() );
    }


    public static  PvpLog getPvpLog(){
        PvpLog log =  pvpThreadLocal.get();
        if( log == null ){
            log = new PvpLog();
            pvpThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  PvpLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        pvpLogger.appendLog(  sb.toString() );
    }


    public static  PveLog getPveLog(){
        PveLog log =  pveThreadLocal.get();
        if( log == null ){
            log = new PveLog();
            pveThreadLocal.set( log );
        }else{
            log.reset();
        }

        return log;
    }

    public static void appendLog(  PveLog log ){
        StringBuilder sb = getStringBuilder();
        log.writeTo( sb );
        pveLogger.appendLog(  sb.toString() );
    }



    private static StringBuilder getStringBuilder(){
        StringBuilder sb = sbLocal.get();
        if( sb == null ){
            sb = new StringBuilder();
            sbLocal.set( sb );
        }

        sb.setLength( 0 );
        return sb;
    }

     public static String getCurrentLogTime( ) {
        SimpleDateFormat sdf = threadLocalSDF.get();
        if ( sdf == null ) {
            sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
            threadLocalSDF.set( sdf );
        }
        return sdf.format( new Date( ) );
    }

}