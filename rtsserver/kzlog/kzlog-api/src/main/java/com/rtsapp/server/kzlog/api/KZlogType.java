package com.rtsapp.server.kzlog.api;

public enum  KZlogType {

    Active("Active"),
    Register("Register"),
    Debit("Debit"),
    Login("Login"),
    Createrole("Createrole"),
    Credit("Credit"),
    Heartbeat("Heartbeat"),
    Additem("Additem"),
    Costitem("Costitem"),
    Lead("Lead"),
    Userphoto("Userphoto"),
    Pvp("Pvp"),
    Pve("Pve"),
    ;

    String name;

    KZlogType(String name) {
        this.name = name;
    }

    public String getName() {
            return name;
    }

    public String getTableName(){
            return this.name + "Log";
    }
}


