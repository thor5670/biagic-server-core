package com.rtsapp.server.kzlog.api.model;

import javax.persistence.*;
import com.rtsapp.server.kzlog.api.IKZLog;

public class ActiveLog implements IKZLog {

	public long id;
	public String gameId;
	public String activeTime;
	public String os;
	public String osVersion;
	public String deviceSerial;
	public String mac;
	public String activeIp;
	public String openUDID;
	public String ADUDID;
	public String logTime;
	public String channel;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(String activeTime) {
		this.activeTime = activeTime;
	}
	
	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getDeviceSerial() {
		return deviceSerial;
	}

	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}
	
	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}
	
	public String getActiveIp() {
		return activeIp;
	}

	public void setActiveIp(String activeIp) {
		this.activeIp = activeIp;
	}
	
	public String getOpenUDID() {
		return openUDID;
	}

	public void setOpenUDID(String openUDID) {
		this.openUDID = openUDID;
	}
	
	public String getADUDID() {
		return ADUDID;
	}

	public void setADUDID(String ADUDID) {
		this.ADUDID = ADUDID;
	}
	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + IKZLog.SPLIT_CHAR );
	    if( strs.length < 10 ){
			return false;
		}

		gameId = strs[ 0 ];
		activeTime = strs[ 1 ];
		os = strs[ 2 ];
		osVersion = strs[ 3 ];
		deviceSerial = strs[ 4 ];
		mac = strs[ 5 ];
		activeIp = strs[ 6 ];
		openUDID = strs[ 7 ];
		ADUDID = strs[ 8 ];
		logTime = strs[ 9 ];

		if( strs.length > 10 ){
			channel = strs[ 10 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( gameId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( activeTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( os );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( osVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( deviceSerial );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( mac );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( activeIp );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( openUDID );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( ADUDID );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( logTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( channel );

	}

	public void reset( ){
		gameId = null;
		activeTime = null;
		os = null;
		osVersion = null;
		deviceSerial = null;
		mac = null;
		activeIp = null;
		openUDID = null;
		ADUDID = null;
		logTime = null;
		channel = null;
	}
}
