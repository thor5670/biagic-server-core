package com.rtsapp.server.kzlog.api.model;

import javax.persistence.*;
import com.rtsapp.server.kzlog.api.IKZLog;

public class CostitemLog implements IKZLog {

	public long id;
	public String gameId;
	public String areaId;
	public String version;
	public String gameLanguage;
	public String serverId;
	public String userId;
	public String logTime;
	public String behavior;
	public String itemNum;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getGameLanguage() {
		return gameLanguage;
	}

	public void setGameLanguage(String gameLanguage) {
		this.gameLanguage = gameLanguage;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getBehavior() {
		return behavior;
	}

	public void setBehavior(String behavior) {
		this.behavior = behavior;
	}
	
	public String getItemNum() {
		return itemNum;
	}

	public void setItemNum(String itemNum) {
		this.itemNum = itemNum;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + IKZLog.SPLIT_CHAR );
	    if( strs.length < 8 ){
			return false;
		}

		gameId = strs[ 0 ];
		areaId = strs[ 1 ];
		version = strs[ 2 ];
		gameLanguage = strs[ 3 ];
		serverId = strs[ 4 ];
		userId = strs[ 5 ];
		logTime = strs[ 6 ];
		behavior = strs[ 7 ];

		if( strs.length > 8 ){
			itemNum = strs[ 8 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( gameId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( version );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameLanguage );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( userId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( logTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( behavior );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( itemNum );

	}

	public void reset( ){
		gameId = null;
		areaId = null;
		version = null;
		gameLanguage = null;
		serverId = null;
		userId = null;
		logTime = null;
		behavior = null;
		itemNum = null;
	}
}
