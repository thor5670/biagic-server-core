package com.rtsapp.server.kzlog.api.model;

import javax.persistence.*;
import com.rtsapp.server.kzlog.api.IKZLog;

public class CreateroleLog implements IKZLog {

	public long id;
	public String gameId;
	public String areaId;
	public String userRegisterId;
	public String gameName;
	public String gameLanguage;
	public String gameVersion;
	public String registerChannel;
	public String createPlayerDevice;
	public String deviceSerial;
	public String mac;
	public String resolution;
	public String os;
	public String osVersion;
	public String logTime;
	public String createUserIp;
	public String createUserEmail;
	public String openUDID;
	public String ADUDID;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getUserRegisterId() {
		return userRegisterId;
	}

	public void setUserRegisterId(String userRegisterId) {
		this.userRegisterId = userRegisterId;
	}
	
	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	public String getGameLanguage() {
		return gameLanguage;
	}

	public void setGameLanguage(String gameLanguage) {
		this.gameLanguage = gameLanguage;
	}
	
	public String getGameVersion() {
		return gameVersion;
	}

	public void setGameVersion(String gameVersion) {
		this.gameVersion = gameVersion;
	}
	
	public String getRegisterChannel() {
		return registerChannel;
	}

	public void setRegisterChannel(String registerChannel) {
		this.registerChannel = registerChannel;
	}
	
	public String getCreatePlayerDevice() {
		return createPlayerDevice;
	}

	public void setCreatePlayerDevice(String createPlayerDevice) {
		this.createPlayerDevice = createPlayerDevice;
	}
	
	public String getDeviceSerial() {
		return deviceSerial;
	}

	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}
	
	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	
	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getCreateUserIp() {
		return createUserIp;
	}

	public void setCreateUserIp(String createUserIp) {
		this.createUserIp = createUserIp;
	}
	
	public String getCreateUserEmail() {
		return createUserEmail;
	}

	public void setCreateUserEmail(String createUserEmail) {
		this.createUserEmail = createUserEmail;
	}
	
	public String getOpenUDID() {
		return openUDID;
	}

	public void setOpenUDID(String openUDID) {
		this.openUDID = openUDID;
	}
	
	public String getADUDID() {
		return ADUDID;
	}

	public void setADUDID(String ADUDID) {
		this.ADUDID = ADUDID;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + IKZLog.SPLIT_CHAR );
	    if( strs.length < 17 ){
			return false;
		}

		gameId = strs[ 0 ];
		areaId = strs[ 1 ];
		userRegisterId = strs[ 2 ];
		gameName = strs[ 3 ];
		gameLanguage = strs[ 4 ];
		gameVersion = strs[ 5 ];
		registerChannel = strs[ 6 ];
		createPlayerDevice = strs[ 7 ];
		deviceSerial = strs[ 8 ];
		mac = strs[ 9 ];
		resolution = strs[ 10 ];
		os = strs[ 11 ];
		osVersion = strs[ 12 ];
		logTime = strs[ 13 ];
		createUserIp = strs[ 14 ];
		createUserEmail = strs[ 15 ];
		openUDID = strs[ 16 ];

		if( strs.length > 17 ){
			ADUDID = strs[ 17 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( gameId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( userRegisterId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameName );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameLanguage );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( registerChannel );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( createPlayerDevice );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( deviceSerial );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( mac );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( resolution );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( os );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( osVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( logTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( createUserIp );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( createUserEmail );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( openUDID );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( ADUDID );

	}

	public void reset( ){
		gameId = null;
		areaId = null;
		userRegisterId = null;
		gameName = null;
		gameLanguage = null;
		gameVersion = null;
		registerChannel = null;
		createPlayerDevice = null;
		deviceSerial = null;
		mac = null;
		resolution = null;
		os = null;
		osVersion = null;
		logTime = null;
		createUserIp = null;
		createUserEmail = null;
		openUDID = null;
		ADUDID = null;
	}
}
