package com.rtsapp.server.kzlog.api.model;

import javax.persistence.*;
import com.rtsapp.server.kzlog.api.IKZLog;

public class CreditLog implements IKZLog {

	public long id;
	public String gameId;
	public String areaId;
	public String userRegisterId;
	public String gameName;
	public String gameLanguage;
	public String gameVersion;
	public String serverId;
	public String loginDevice;
	public String deviceSerial;
	public String mac;
	public String resolution;
	public String os;
	public String osVersion;
	public String logTime;
	public String creditCause;
	public String moneyType;
	public String creditMoney;
	public String payWay;
	public String exchangeRate;
	public String rechargeGameMoney;
	public String loginIp;
	public String playerLevel;
	public String playerMilitary;
	public String gameMoneyResidual;
	public String gameId1;
	public String areaId1;
	public String userRegisterId1;
	public String openUDID;
	public String playerName;
	public String playerAccount;
	public String payId;
	public String followId;
	public String playerId;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getUserRegisterId() {
		return userRegisterId;
	}

	public void setUserRegisterId(String userRegisterId) {
		this.userRegisterId = userRegisterId;
	}
	
	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	public String getGameLanguage() {
		return gameLanguage;
	}

	public void setGameLanguage(String gameLanguage) {
		this.gameLanguage = gameLanguage;
	}
	
	public String getGameVersion() {
		return gameVersion;
	}

	public void setGameVersion(String gameVersion) {
		this.gameVersion = gameVersion;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getLoginDevice() {
		return loginDevice;
	}

	public void setLoginDevice(String loginDevice) {
		this.loginDevice = loginDevice;
	}
	
	public String getDeviceSerial() {
		return deviceSerial;
	}

	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}
	
	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	
	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getCreditCause() {
		return creditCause;
	}

	public void setCreditCause(String creditCause) {
		this.creditCause = creditCause;
	}
	
	public String getMoneyType() {
		return moneyType;
	}

	public void setMoneyType(String moneyType) {
		this.moneyType = moneyType;
	}
	
	public String getCreditMoney() {
		return creditMoney;
	}

	public void setCreditMoney(String creditMoney) {
		this.creditMoney = creditMoney;
	}
	
	public String getPayWay() {
		return payWay;
	}

	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}
	
	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
	public String getRechargeGameMoney() {
		return rechargeGameMoney;
	}

	public void setRechargeGameMoney(String rechargeGameMoney) {
		this.rechargeGameMoney = rechargeGameMoney;
	}
	
	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	
	public String getPlayerLevel() {
		return playerLevel;
	}

	public void setPlayerLevel(String playerLevel) {
		this.playerLevel = playerLevel;
	}
	
	public String getPlayerMilitary() {
		return playerMilitary;
	}

	public void setPlayerMilitary(String playerMilitary) {
		this.playerMilitary = playerMilitary;
	}
	
	public String getGameMoneyResidual() {
		return gameMoneyResidual;
	}

	public void setGameMoneyResidual(String gameMoneyResidual) {
		this.gameMoneyResidual = gameMoneyResidual;
	}
	
	public String getGameId1() {
		return gameId1;
	}

	public void setGameId1(String gameId1) {
		this.gameId1 = gameId1;
	}
	
	public String getAreaId1() {
		return areaId1;
	}

	public void setAreaId1(String areaId1) {
		this.areaId1 = areaId1;
	}
	
	public String getUserRegisterId1() {
		return userRegisterId1;
	}

	public void setUserRegisterId1(String userRegisterId1) {
		this.userRegisterId1 = userRegisterId1;
	}
	
	public String getOpenUDID() {
		return openUDID;
	}

	public void setOpenUDID(String openUDID) {
		this.openUDID = openUDID;
	}
	
	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	public String getPlayerAccount() {
		return playerAccount;
	}

	public void setPlayerAccount(String playerAccount) {
		this.playerAccount = playerAccount;
	}
	
	public String getPayId() {
		return payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}
	
	public String getFollowId() {
		return followId;
	}

	public void setFollowId(String followId) {
		this.followId = followId;
	}
	
	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + IKZLog.SPLIT_CHAR );
	    if( strs.length < 32 ){
			return false;
		}

		gameId = strs[ 0 ];
		areaId = strs[ 1 ];
		userRegisterId = strs[ 2 ];
		gameName = strs[ 3 ];
		gameLanguage = strs[ 4 ];
		gameVersion = strs[ 5 ];
		serverId = strs[ 6 ];
		loginDevice = strs[ 7 ];
		deviceSerial = strs[ 8 ];
		mac = strs[ 9 ];
		resolution = strs[ 10 ];
		os = strs[ 11 ];
		osVersion = strs[ 12 ];
		logTime = strs[ 13 ];
		creditCause = strs[ 14 ];
		moneyType = strs[ 15 ];
		creditMoney = strs[ 16 ];
		payWay = strs[ 17 ];
		exchangeRate = strs[ 18 ];
		rechargeGameMoney = strs[ 19 ];
		loginIp = strs[ 20 ];
		playerLevel = strs[ 21 ];
		playerMilitary = strs[ 22 ];
		gameMoneyResidual = strs[ 23 ];
		gameId1 = strs[ 24 ];
		areaId1 = strs[ 25 ];
		userRegisterId1 = strs[ 26 ];
		openUDID = strs[ 27 ];
		playerName = strs[ 28 ];
		playerAccount = strs[ 29 ];
		payId = strs[ 30 ];
		followId = strs[ 31 ];

		if( strs.length > 32 ){
			playerId = strs[ 32 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( gameId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( userRegisterId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameName );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameLanguage );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( loginDevice );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( deviceSerial );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( mac );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( resolution );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( os );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( osVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( logTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( creditCause );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( moneyType );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( creditMoney );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( payWay );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( exchangeRate );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( rechargeGameMoney );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( loginIp );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerLevel );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerMilitary );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameMoneyResidual );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameId1 );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( areaId1 );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( userRegisterId1 );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( openUDID );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerName );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerAccount );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( payId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( followId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerId );

	}

	public void reset( ){
		gameId = null;
		areaId = null;
		userRegisterId = null;
		gameName = null;
		gameLanguage = null;
		gameVersion = null;
		serverId = null;
		loginDevice = null;
		deviceSerial = null;
		mac = null;
		resolution = null;
		os = null;
		osVersion = null;
		logTime = null;
		creditCause = null;
		moneyType = null;
		creditMoney = null;
		payWay = null;
		exchangeRate = null;
		rechargeGameMoney = null;
		loginIp = null;
		playerLevel = null;
		playerMilitary = null;
		gameMoneyResidual = null;
		gameId1 = null;
		areaId1 = null;
		userRegisterId1 = null;
		openUDID = null;
		playerName = null;
		playerAccount = null;
		payId = null;
		followId = null;
		playerId = null;
	}
}
