package com.rtsapp.server.kzlog.api.model;

import javax.persistence.*;
import com.rtsapp.server.kzlog.api.IKZLog;

public class DebitLog implements IKZLog {

	public long id;
	public String gameId;
	public String areaId;
	public String userRegisterId;
	public String gameName;
	public String gameLanguage;
	public String gameVersion;
	public String serverId;
	public String loginDevice;
	public String deviceSerial;
	public String mac;
	public String resolution;
	public String os;
	public String osVersion;
	public String debitTime;
	public String debit;
	public String debitMoney;
	public String registerIp;
	public String playerLevel;
	public String playerReputation;
	public String moneyResidual;
	public String openUDID;
	public String playerName;
	public String debitUnits;
	public String playerAccount;
	public String userId;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getUserRegisterId() {
		return userRegisterId;
	}

	public void setUserRegisterId(String userRegisterId) {
		this.userRegisterId = userRegisterId;
	}
	
	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	public String getGameLanguage() {
		return gameLanguage;
	}

	public void setGameLanguage(String gameLanguage) {
		this.gameLanguage = gameLanguage;
	}
	
	public String getGameVersion() {
		return gameVersion;
	}

	public void setGameVersion(String gameVersion) {
		this.gameVersion = gameVersion;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getLoginDevice() {
		return loginDevice;
	}

	public void setLoginDevice(String loginDevice) {
		this.loginDevice = loginDevice;
	}
	
	public String getDeviceSerial() {
		return deviceSerial;
	}

	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}
	
	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	
	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getDebitTime() {
		return debitTime;
	}

	public void setDebitTime(String debitTime) {
		this.debitTime = debitTime;
	}
	
	public String getDebit() {
		return debit;
	}

	public void setDebit(String debit) {
		this.debit = debit;
	}
	
	public String getDebitMoney() {
		return debitMoney;
	}

	public void setDebitMoney(String debitMoney) {
		this.debitMoney = debitMoney;
	}
	
	public String getRegisterIp() {
		return registerIp;
	}

	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}
	
	public String getPlayerLevel() {
		return playerLevel;
	}

	public void setPlayerLevel(String playerLevel) {
		this.playerLevel = playerLevel;
	}
	
	public String getPlayerReputation() {
		return playerReputation;
	}

	public void setPlayerReputation(String playerReputation) {
		this.playerReputation = playerReputation;
	}
	
	public String getMoneyResidual() {
		return moneyResidual;
	}

	public void setMoneyResidual(String moneyResidual) {
		this.moneyResidual = moneyResidual;
	}
	
	public String getOpenUDID() {
		return openUDID;
	}

	public void setOpenUDID(String openUDID) {
		this.openUDID = openUDID;
	}
	
	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	public String getDebitUnits() {
		return debitUnits;
	}

	public void setDebitUnits(String debitUnits) {
		this.debitUnits = debitUnits;
	}
	
	public String getPlayerAccount() {
		return playerAccount;
	}

	public void setPlayerAccount(String playerAccount) {
		this.playerAccount = playerAccount;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + IKZLog.SPLIT_CHAR );
	    if( strs.length < 24 ){
			return false;
		}

		gameId = strs[ 0 ];
		areaId = strs[ 1 ];
		userRegisterId = strs[ 2 ];
		gameName = strs[ 3 ];
		gameLanguage = strs[ 4 ];
		gameVersion = strs[ 5 ];
		serverId = strs[ 6 ];
		loginDevice = strs[ 7 ];
		deviceSerial = strs[ 8 ];
		mac = strs[ 9 ];
		resolution = strs[ 10 ];
		os = strs[ 11 ];
		osVersion = strs[ 12 ];
		debitTime = strs[ 13 ];
		debit = strs[ 14 ];
		debitMoney = strs[ 15 ];
		registerIp = strs[ 16 ];
		playerLevel = strs[ 17 ];
		playerReputation = strs[ 18 ];
		moneyResidual = strs[ 19 ];
		openUDID = strs[ 20 ];
		playerName = strs[ 21 ];
		debitUnits = strs[ 22 ];
		playerAccount = strs[ 23 ];

		if( strs.length > 24 ){
			userId = strs[ 24 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( gameId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( userRegisterId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameName );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameLanguage );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( loginDevice );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( deviceSerial );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( mac );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( resolution );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( os );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( osVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( debitTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( debit );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( debitMoney );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( registerIp );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerLevel );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerReputation );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( moneyResidual );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( openUDID );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerName );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( debitUnits );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerAccount );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( userId );

	}

	public void reset( ){
		gameId = null;
		areaId = null;
		userRegisterId = null;
		gameName = null;
		gameLanguage = null;
		gameVersion = null;
		serverId = null;
		loginDevice = null;
		deviceSerial = null;
		mac = null;
		resolution = null;
		os = null;
		osVersion = null;
		debitTime = null;
		debit = null;
		debitMoney = null;
		registerIp = null;
		playerLevel = null;
		playerReputation = null;
		moneyResidual = null;
		openUDID = null;
		playerName = null;
		debitUnits = null;
		playerAccount = null;
		userId = null;
	}
}
