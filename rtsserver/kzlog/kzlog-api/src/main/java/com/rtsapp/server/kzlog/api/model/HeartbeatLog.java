package com.rtsapp.server.kzlog.api.model;

import javax.persistence.*;
import com.rtsapp.server.kzlog.api.IKZLog;

public class HeartbeatLog implements IKZLog {

	public long id;
	public String gameId;
	public String areaId;
	public String gameName;
	public String gameLanguage;
	public String gameVersion;
	public String serverId;
	public String logTime;
	public String currentOnLineUserNum;
	public String throughput;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	public String getGameLanguage() {
		return gameLanguage;
	}

	public void setGameLanguage(String gameLanguage) {
		this.gameLanguage = gameLanguage;
	}
	
	public String getGameVersion() {
		return gameVersion;
	}

	public void setGameVersion(String gameVersion) {
		this.gameVersion = gameVersion;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getCurrentOnLineUserNum() {
		return currentOnLineUserNum;
	}

	public void setCurrentOnLineUserNum(String currentOnLineUserNum) {
		this.currentOnLineUserNum = currentOnLineUserNum;
	}
	
	public String getThroughput() {
		return throughput;
	}

	public void setThroughput(String throughput) {
		this.throughput = throughput;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + IKZLog.SPLIT_CHAR );
	    if( strs.length < 8 ){
			return false;
		}

		gameId = strs[ 0 ];
		areaId = strs[ 1 ];
		gameName = strs[ 2 ];
		gameLanguage = strs[ 3 ];
		gameVersion = strs[ 4 ];
		serverId = strs[ 5 ];
		logTime = strs[ 6 ];
		currentOnLineUserNum = strs[ 7 ];

		if( strs.length > 8 ){
			throughput = strs[ 8 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( gameId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameName );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameLanguage );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( logTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( currentOnLineUserNum );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( throughput );

	}

	public void reset( ){
		gameId = null;
		areaId = null;
		gameName = null;
		gameLanguage = null;
		gameVersion = null;
		serverId = null;
		logTime = null;
		currentOnLineUserNum = null;
		throughput = null;
	}
}
