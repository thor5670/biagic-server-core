package com.rtsapp.server.kzlog.api.model;

import javax.persistence.*;
import com.rtsapp.server.kzlog.api.IKZLog;

public class LeadLog implements IKZLog {

	public long id;
	public String gameId;
	public String areaId;
	public String version;
	public String language;
	public String serverId;
	public String channelId;
	public String uId;
	public String playerName;
	public String playerId;
	public String playerLevel;
	public String vipLevel;
	public String logTime;
	public String noviceName;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	
	public String getUId() {
		return uId;
	}

	public void setUId(String uId) {
		this.uId = uId;
	}
	
	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	
	public String getPlayerLevel() {
		return playerLevel;
	}

	public void setPlayerLevel(String playerLevel) {
		this.playerLevel = playerLevel;
	}
	
	public String getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}
	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getNoviceName() {
		return noviceName;
	}

	public void setNoviceName(String noviceName) {
		this.noviceName = noviceName;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + IKZLog.SPLIT_CHAR );
	    if( strs.length < 12 ){
			return false;
		}

		gameId = strs[ 0 ];
		areaId = strs[ 1 ];
		version = strs[ 2 ];
		language = strs[ 3 ];
		serverId = strs[ 4 ];
		channelId = strs[ 5 ];
		uId = strs[ 6 ];
		playerName = strs[ 7 ];
		playerId = strs[ 8 ];
		playerLevel = strs[ 9 ];
		vipLevel = strs[ 10 ];
		logTime = strs[ 11 ];

		if( strs.length > 12 ){
			noviceName = strs[ 12 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( gameId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( version );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( language );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( channelId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( uId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerName );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerLevel );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( vipLevel );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( logTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( noviceName );

	}

	public void reset( ){
		gameId = null;
		areaId = null;
		version = null;
		language = null;
		serverId = null;
		channelId = null;
		uId = null;
		playerName = null;
		playerId = null;
		playerLevel = null;
		vipLevel = null;
		logTime = null;
		noviceName = null;
	}
}
