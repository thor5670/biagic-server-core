package com.rtsapp.server.kzlog.api.model;

import javax.persistence.*;
import com.rtsapp.server.kzlog.api.IKZLog;

public class RegisterLog implements IKZLog {

	public long id;
	public String gameId;
	public String areaId;
	public String userRegisterId;
	public String gameName;
	public String gameLanguage;
	public String gameVersion;
	public String registerChannel;
	public String registerDevice;
	public String deviceSerial;
	public String mac;
	public String resolution;
	public String os;
	public String osVersion;
	public String logTime;
	public String registerIp;
	public String registerEmail;
	public String moneyResidual;
	public String openUDID;
	public String ADUDID;
	public String APPUDID;
	public String userCenterUID;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getUserRegisterId() {
		return userRegisterId;
	}

	public void setUserRegisterId(String userRegisterId) {
		this.userRegisterId = userRegisterId;
	}
	
	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	public String getGameLanguage() {
		return gameLanguage;
	}

	public void setGameLanguage(String gameLanguage) {
		this.gameLanguage = gameLanguage;
	}
	
	public String getGameVersion() {
		return gameVersion;
	}

	public void setGameVersion(String gameVersion) {
		this.gameVersion = gameVersion;
	}
	
	public String getRegisterChannel() {
		return registerChannel;
	}

	public void setRegisterChannel(String registerChannel) {
		this.registerChannel = registerChannel;
	}
	
	public String getRegisterDevice() {
		return registerDevice;
	}

	public void setRegisterDevice(String registerDevice) {
		this.registerDevice = registerDevice;
	}
	
	public String getDeviceSerial() {
		return deviceSerial;
	}

	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}
	
	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	
	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}
	
	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getRegisterIp() {
		return registerIp;
	}

	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}
	
	public String getRegisterEmail() {
		return registerEmail;
	}

	public void setRegisterEmail(String registerEmail) {
		this.registerEmail = registerEmail;
	}
	
	public String getMoneyResidual() {
		return moneyResidual;
	}

	public void setMoneyResidual(String moneyResidual) {
		this.moneyResidual = moneyResidual;
	}
	
	public String getOpenUDID() {
		return openUDID;
	}

	public void setOpenUDID(String openUDID) {
		this.openUDID = openUDID;
	}
	
	public String getADUDID() {
		return ADUDID;
	}

	public void setADUDID(String ADUDID) {
		this.ADUDID = ADUDID;
	}
	
	public String getAPPUDID() {
		return APPUDID;
	}

	public void setAPPUDID(String APPUDID) {
		this.APPUDID = APPUDID;
	}
	
	public String getUserCenterUID() {
		return userCenterUID;
	}

	public void setUserCenterUID(String userCenterUID) {
		this.userCenterUID = userCenterUID;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + IKZLog.SPLIT_CHAR );
	    if( strs.length < 20 ){
			return false;
		}

		gameId = strs[ 0 ];
		areaId = strs[ 1 ];
		userRegisterId = strs[ 2 ];
		gameName = strs[ 3 ];
		gameLanguage = strs[ 4 ];
		gameVersion = strs[ 5 ];
		registerChannel = strs[ 6 ];
		registerDevice = strs[ 7 ];
		deviceSerial = strs[ 8 ];
		mac = strs[ 9 ];
		resolution = strs[ 10 ];
		os = strs[ 11 ];
		osVersion = strs[ 12 ];
		logTime = strs[ 13 ];
		registerIp = strs[ 14 ];
		registerEmail = strs[ 15 ];
		moneyResidual = strs[ 16 ];
		openUDID = strs[ 17 ];
		ADUDID = strs[ 18 ];
		APPUDID = strs[ 19 ];

		if( strs.length > 20 ){
			userCenterUID = strs[ 20 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( gameId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( userRegisterId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameName );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameLanguage );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( gameVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( registerChannel );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( registerDevice );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( deviceSerial );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( mac );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( resolution );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( os );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( osVersion );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( logTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( registerIp );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( registerEmail );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( moneyResidual );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( openUDID );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( ADUDID );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( APPUDID );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( userCenterUID );

	}

	public void reset( ){
		gameId = null;
		areaId = null;
		userRegisterId = null;
		gameName = null;
		gameLanguage = null;
		gameVersion = null;
		registerChannel = null;
		registerDevice = null;
		deviceSerial = null;
		mac = null;
		resolution = null;
		os = null;
		osVersion = null;
		logTime = null;
		registerIp = null;
		registerEmail = null;
		moneyResidual = null;
		openUDID = null;
		ADUDID = null;
		APPUDID = null;
		userCenterUID = null;
	}
}
