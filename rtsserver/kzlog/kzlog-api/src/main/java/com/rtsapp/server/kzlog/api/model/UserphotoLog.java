package com.rtsapp.server.kzlog.api.model;

import javax.persistence.*;
import com.rtsapp.server.kzlog.api.IKZLog;

public class UserphotoLog implements IKZLog {

	public long id;
	public String gameId;
	public String areaId;
	public String version;
	public String language;
	public String serverId;
	public String channelId;
	public String uid;
	public String name;
	public String playerId;
	public String level;
	public String vipLevel;
	public String logTime;
	public String generalInfo;

    public long getId(){return id;}
    public void setId(long id){this.id = id;}

	
	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	
	public String getVipLevel() {
		return vipLevel;
	}

	public void setVipLevel(String vipLevel) {
		this.vipLevel = vipLevel;
	}
	
	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}
	
	public String getGeneralInfo() {
		return generalInfo;
	}

	public void setGeneralInfo(String generalInfo) {
		this.generalInfo = generalInfo;
	}

	public boolean readFrom( String sb ){
			    if( sb == null ){
        	return false;
        }

	    String[] strs = sb.split( "\\" + IKZLog.SPLIT_CHAR );
	    if( strs.length < 12 ){
			return false;
		}

		gameId = strs[ 0 ];
		areaId = strs[ 1 ];
		version = strs[ 2 ];
		language = strs[ 3 ];
		serverId = strs[ 4 ];
		channelId = strs[ 5 ];
		uid = strs[ 6 ];
		name = strs[ 7 ];
		playerId = strs[ 8 ];
		level = strs[ 9 ];
		vipLevel = strs[ 10 ];
		logTime = strs[ 11 ];

		if( strs.length > 12 ){
			generalInfo = strs[ 12 ];
		}

		return true;
	}
	
	public void writeTo( StringBuilder sb ){
		sb.append( gameId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( areaId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( version );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( language );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( serverId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( channelId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( uid );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( name );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( playerId );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( level );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( vipLevel );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( logTime );

		sb.append( IKZLog.SPLIT_CHAR );
		sb.append( generalInfo );

	}

	public void reset( ){
		gameId = null;
		areaId = null;
		version = null;
		language = null;
		serverId = null;
		channelId = null;
		uid = null;
		name = null;
		playerId = null;
		level = null;
		vipLevel = null;
		logTime = null;
		generalInfo = null;
	}
}
