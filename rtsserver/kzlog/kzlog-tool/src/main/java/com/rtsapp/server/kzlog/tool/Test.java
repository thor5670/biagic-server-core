package com.rtsapp.server.kzlog.tool;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by admin on 16-7-21.
 */
public class Test {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args ) throws ParseException {

        System.out.println( sdf.parse( "2016-07-21 00:00:00" ).getTime() );
        System.out.println( sdf.parse( "2016-07-22 00:00:00" ).getTime() );

        print("startMils", 1469032166000L);
        print( "stopMils", 1469030400000L );
        print( "curMils", 1469085049870L );


        String log = "2016-07-13 23:23:52 +0800|601|1|1|30000002|30000002|null|null|null|null|null|null|null|null|null|null|127.0.0.1|null|1|0";

        String[] results = log.split( "\\" + "|" );

        for( String s : results ){
            System.out.println( s );
        }

    }

    private static void print(String msg, long mils ){
        System.out.println( msg + ":" + sdf.format(new Date( mils )) );
    }



}
