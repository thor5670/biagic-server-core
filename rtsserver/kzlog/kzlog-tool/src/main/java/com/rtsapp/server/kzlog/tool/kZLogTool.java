package com.rtsapp.server.kzlog.tool;

import com.dyt.itool.parser.XmlCommandParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 16-7-11.
 */
public class kZLogTool {

    // excel, command.xml 存放目录
    private static final String inputDir = "cfg/";

    // 输出目录
    private static final String dir_api = "../kzlog-api/src/main/java/com/rtsapp/server/kzlog/api/";

    public static void main(String[] args ){

        generateLogApi();
    }

    private static void generateLogApi(){

        String commandFile = inputDir + "logdef.xml";

        XmlCommandParser parser = new XmlCommandParser();
        parser.parse( commandFile );

        //Commands
        Map<String, String> map = new HashMap<String, String>();


        //model
        map.put( inputDir + "java_log_entity.vm", dir_api + "model/?Log.java");
        parser.generateModelCodeOneByOne(map);



        map.clear();
        map.put(inputDir + "java_log_utils.vm", dir_api + "/KZLogUtils.java");
        parser.generateCommandCode(map);

        map.clear();
        map.put(inputDir + "java_log_types.vm", dir_api + "/KZlogType.java");
        parser.generateCommandCode(map);

    }


}
