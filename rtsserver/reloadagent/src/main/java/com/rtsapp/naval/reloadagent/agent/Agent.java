package com.rtsapp.naval.reloadagent.agent;

import java.lang.instrument.Instrumentation;
import java.util.Vector;

/**
 * Created by admin on 15-9-10.
 */
public class Agent {

    /** Lists of active Smith agents */
    private static Vector<AgentMonitor> smiths = new Vector<AgentMonitor>();


    /** Called when the agent is initialized via command line */
    public static void premain(String agentArgs, Instrumentation inst) {
        initialize(agentArgs, inst);
    }

    /** Called when the agent is initialized after the jvm startup */
    public static void agentmain(String agentArgs, Instrumentation inst) {
        initialize(agentArgs, inst);
    }


    private static void initialize(String agentArgs, Instrumentation inst) {
        AgentArgs args = new AgentArgs(agentArgs);

        if (!args.isValid()) {
            throw new RuntimeException("agent启动参数不对,请检查");
        }

        AgentMonitor smith = new AgentMonitor(inst, args);
        smiths.add(smith);
    }


    public static void stopAll() {
        for (AgentMonitor smith : smiths) {
            smith.stop();
        }
    }

}
