package com.rtsapp.naval.reloadagent.filemonitor;

public interface FileAddedListener {

    public void fileAdded(FileEvent event);

}
