package com.rtsapp.naval.reloadagent.filemonitor;

public interface FileDeletedListener {

    public void fileDeleted(FileEvent event);

}
