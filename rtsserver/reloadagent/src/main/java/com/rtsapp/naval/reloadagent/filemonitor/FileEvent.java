package com.rtsapp.naval.reloadagent.filemonitor;

import java.io.File;

/**
 * 文件事件
 * 文件 add, delete, modify时触发
 */
public class FileEvent  {

    private final File folder;
    
    private final File file;
    
    
    public FileEvent(File folder, File file) {
        this.file = file;
        this.folder = folder;
    }

    /**
     * @return 事件对应的文件
     */
    public File getFile() {
      return file;
    }
    
    /**
     * @return 对应的文件夹
     */
    public File getFolder() {
      return folder;
    }
}
