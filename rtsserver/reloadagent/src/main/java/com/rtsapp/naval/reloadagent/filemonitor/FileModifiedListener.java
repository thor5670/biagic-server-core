package com.rtsapp.naval.reloadagent.filemonitor;

public interface FileModifiedListener {

    public void fileModified(FileEvent event);

}
