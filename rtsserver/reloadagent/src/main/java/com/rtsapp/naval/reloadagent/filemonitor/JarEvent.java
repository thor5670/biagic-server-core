package com.rtsapp.naval.reloadagent.filemonitor;

import java.util.jar.JarFile;

/**
 * jar包内class改变事件
 */
public class JarEvent {

    private final JarFile jarFile;
    private final String className;
    

    public JarEvent( JarFile file, String className ) {
        jarFile = file;
        this.className = className;
    }

    
    public JarFile getJarFile() {
        return (JarFile) jarFile;
    }

    /**
     * @return the changed class file in jar
     */
    public String getClassName() {
        return className;
    }

}
