package com.rtsapp.naval.reloadagent.filemonitor;


public interface JarModifiedListener {

    void jarModified( JarEvent event );

}
