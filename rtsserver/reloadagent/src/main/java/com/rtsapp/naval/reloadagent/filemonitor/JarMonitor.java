package com.rtsapp.naval.reloadagent.filemonitor;

import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JarMonitor implements FileModifiedListener, FileAddedListener, FileDeletedListener, Runnable {

    private final static Logger log = Logger.getLogger(JarMonitor.class.getName());

    private final FileMonitor fileMonitor;
    private final Map<String, Map<String, Long> > jarsMap;
    private final List< JarModifiedListener > jarModifiedListeners;

    public JarMonitor(String absoluteFolderPath) {
        this.jarsMap = new HashMap<String, Map<String, Long>>();
        this.jarModifiedListeners = new LinkedList<JarModifiedListener>();

        fileMonitor = new FileMonitor(Arrays.asList( absoluteFolderPath ), "jar" );
        fileMonitor.addModifiedListener(this);
        fileMonitor.addAddedListener(this);
        fileMonitor.addDeletedListener(this);
    }

    public void run() {
        fileMonitor.run();
    }

    public void fileModified( FileEvent event ) {
    	
        JarFile file = getJarFile(event);

        if (file != null) {
            Map<String, Long> jarEntries = jarsMap.get( event.getFile().getAbsolutePath() );
            for (Enumeration<JarEntry> entries = file.entries(); entries.hasMoreElements();) {
                JarEntry entry = entries.nextElement();

                if( entry.getName().endsWith( ".class") ){
                	
	                if ( !jarEntries.containsKey(entry.getName()) ) {
	                    jarEntries.put(entry.getName(), Long.valueOf(entry.getTime()));
	                }else{
	                	 if (entry.getTime() != jarEntries.get(entry.getName()).longValue()) {
	 	                    jarEntries.put(entry.getName(), Long.valueOf(entry.getTime()));
	 	                    notifyJarModifiedListeners(new JarEvent(file, entry.getName()));
	 	                }
	                }
                }

            }

        }
    }

    public void fileAdded(FileEvent event) {
    	
        JarFile file = getJarFile(event);

        if ( file != null ) {
            Map<String, Long> jarEntries = new HashMap<String, Long>();
            jarsMap.put( event.getFile().getAbsolutePath(), jarEntries );

            for ( Enumeration<JarEntry> entries = file.entries(); entries.hasMoreElements(); ) {
                JarEntry entry = entries.nextElement();
                if (entry.getName().endsWith(".class") ) {
                    jarEntries.put( entry.getName(), Long.valueOf( entry.getTime() ) );
                    notifyJarModifiedListeners( new JarEvent( file, entry.getName() ) );
                }
            }
        }
    }

    public void fileDeleted(FileEvent event) {
        jarsMap.remove(event.getFile());
    }

    public void addJarModifiedListener(JarModifiedListener listener) {
        jarModifiedListeners.add(listener);
    }

    private void notifyJarModifiedListeners(JarEvent event) {
        for (JarModifiedListener listener : jarModifiedListeners) {
            listener.jarModified(event);
        }
    }

    private JarFile getJarFile(FileEvent event) {
        try {
            return new JarFile( event.getFile() );
        } catch (IOException e) {
            log.log(Level.SEVERE, "error", e);
            return null;
        }
    }

}
