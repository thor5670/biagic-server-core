package com.rtsapp.naval.reloadagent.agent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.jar.JarFile;
import java.util.logging.Level;

import com.rtsapp.naval.reloadagent.filemonitor.FileMonitor;
import com.rtsapp.naval.reloadagent.filemonitor.JarEvent;
import com.rtsapp.naval.reloadagent.filemonitor.JarModifiedListener;
import com.rtsapp.naval.reloadagent.filemonitor.JarMonitor;

public class TestJarMonitor {

	public static void main(String[] args) {
		ScheduledExecutorService	 service = Executors.newScheduledThreadPool(2);

		String jarFolder = "/Users/admin/work/temp/jars";

		JarMonitor jarMonitor = new JarMonitor(jarFolder);
		jarMonitor.addJarModifiedListener( new JarModifiedListener(){

			@Override
			public void jarModified(JarEvent event) {
				System.out.println( event.getClassName() );
			}
			
		});

		service.scheduleWithFixedDelay(jarMonitor, 0,  1000, TimeUnit.MILLISECONDS );

	}
	
	
}