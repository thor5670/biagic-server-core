##RTSSERVER--分布式游戏服务器架构

###1. 服务器架构
1. 分布式, 基于异步IO的网络基础
2. 采用 **容器** + **服务** 方式提供游戏逻辑, 整个代码编写代用模块话方式
	1. 网关服务器
		1. 游戏逻辑( 单服 )
	2. 商队护航服务器
		1. 
	3. 跨服PVP服务器 
		2. 
3. 容器由框架提供, 容器本身由模块组成, 
	1. 最主要的组件是对象管理器 服务器, 异步客户端
	2. 对象管理ObjectManager:
		1. 思路一:
			1. 先实例化所有对象, 设置好依赖关系
			2. Application启动, Application从对象中寻找对应的对象
		1. 

4. 服务分两种
	1. Handler: 被注入容器
		1. 面向玩家的命令处理
	2. Service: 被注入容器
		1. 面向机器rpc的service
		
###2. 示例-海战传奇
1. 服务器架构
	1. 账号负载均衡, 反向代理服务器, 负责代理账号
		1. 账号服务器对外只提供一个入口点
	2. 账号服务器 
		1. 功能
			1. 账号管理
			2. 充值管理
		2. 注册
			1.
			2.
		3. 登陆
			1.s
			2.  
	3. 前端游戏服务器->对应一个游戏服务器数据库
		1. 启动NIO服务器, 加载CommandHandler模块
		2. 加载CommandHandler实例模块
			1. 容器扫描Handler, 实例化, 并注册到CommandHandler 
	4. 后端跨服服务容
		1. 启动NIO服务器,  加载RPCBackendHandler模块
		2. 容器扫描Service, 并实例化, 注册到容器
	5. GM工具服务器
		1.

2. 运行实例
	1. 一个跨服 守矿, 收矿, 被打的例子
		1. 玩家守矿, 指定时间后可收矿
		2. 时间到, 推送可收矿消息
		3. 被别的玩家打, 推送被打的消息
		4. 定义,C1,C2,S1
		
	2. 启动过程
		1. S1后端服务器启动
			1. 启动NIO服务器, 加载RPCBackendHandler模块
			2. 容器扫描Service, 并实例化, 注册到容器
		2. C1, C2启动
			1. 

	3. 守矿操作: 
		1. C1的Handler收到客户端请求
		2. C1->S1 发起一次RPC异步调用, 边设置回调
			1. 创建RPCRequest, 设置
				1. 调用的服务名
				2. 方法名称
				3. 方法参数列表
			2. 调用AsyncClient模块发送请求
				1. 生成该AsyncClient中唯一的SerialNo: 
				2. 从所有Session中选择一个
				3. Session.send( RPCRequest )
					1. RPC编码器编码消息
						1. 序列化
					2. 往通道发送
				4. 创建一个RPCContext, 通过SerialNo关联存入Map
					1. 设置RPCRequest
					2. 设置Session
					3. 设置序号 
			3. S1处理C1RPC消息的过程
				1. 消息到来
				2. RPC解码器解码, 得到RPC请求消息
					1. 调用的服务
					2. 方法名称
					3. 方法参数列表
				3. RPCBackendHandler进行处理
					1. 通过容器找到服务器对象, 调用对应的方法, 获得返回结果
						1. 封装RPCResponse
							1. 设定返回结果
							2. 设定SerialNo
					2. 将RPCRequest写回Session
						2. 通过RPC编码写回
			4. C1的AysncClient 收到S1的消息
				1. 解码消息, 得到RPCResponse
				2. 找到对应的RPCContext, 调用RPCContext执行
					1. 可以是设置Future可用
					2. 也可以是执行Future的回调
				3. 从Map中移除对应的RPCContext
		3. C1可执行其他操作
	
	4. 被抢的推送消息实现 
		1. S1上C1的矿被C2抢, S1推送消息到C1, 过程如下
			1. S1找到userid, 通过userid找到serverId
				1. S1维护者userid->serverid的映射
			2. 找到serverid对应的Session
				1. Session = SessionManager.getSessionByRemoteId( serverId )
			3. 往Session中推送一个消息
				1. 类型: 推送
				2. userid
				3. 内容:xxx
			4. C1接收到消息, 解锁到推送消息
			5. C1处理该推送消息
				1. Session = getSessionByUserId( userId )
				2. 往Session中写入对应的消息
				
###3. 参考项目
1. 服务器通信方式
	1. RPC同步编码方式: 阻塞式
		1. 过程: 发送请求, 等待服务器到来, 然后返回
		2. 特点:
			1. 优点: 同步写法, 写法简单 
			2. 缺点: 阻塞性能不高
	2. actor异步编码方式:
		1. 过程:
			1. 发送请求
			2. 设置回调
			3. 响应到来运行回调
		2. 特点:
			1. 优点: 性能好
			2. 缺点: 异步编码, 编码麻烦
		
	
			
2. 服务器结构
	1. 
	1. 模块( id, name )
		1. 模块
		2. 应用程序
		3. 子模块列表
	
	1. 应用程序
		1. 本地游戏逻辑模块
			1. NetServer: 监听服务器
			2. MysqlDB: mysql队列
			3. 
		2. 商队护航模块
		3. 控制台管理模块
		4. GM管理模块
		
		
		1. GameServer -> NetServer: 绑定端口, 监听连接，使用指定的Handler来处理接收的消息, 可发送消息出去
		2. EscortCaravanclient ->  NetClient: 连接指定IP, 端口, 可发送消息, 使用指定handler来处理消息
		3. MysqlDB -> MysqlDB: MySQL操作队列, 保存数据库功能
		4. LocalGameLogic -> GameLogic: 
		5. EscortCaravancGameLogic -> GameLogic

3. 设计
	1. fronend: 前端
	2. backend: 后端			
	3. 连接数限制, 黑名单限制
	4. Transport:传输
	5. 异步客户端
		* 回调
		* RPC
	6. session管理: 发送消息, 推送pushMessage, 广播broadcast
	7. 负载均衡代理
	8. 消息路由
	9. 心跳
	10. 商店
	11. 应用程序，模块
	13. 序列化，反序列化, protobuf, 自定义序列化
	14. 分布式
	15. 数据落地
	16. mysql存储
	17. redis缓存
	18. 数据库代理服务器
	19. 本地调用, 前后端, 
	20. 协程
		1. 
	30. 异构服务器架构, 多语言编写的必要性?
		1. 
	
	
	
		
3. 术语
	1. 路由
	2. rpc
	3. 异步
	4. 负载均衡
	5. 		
		
		
		
		
独立完成 “通用游戏服务器平台UGS Servers”的设计及编码, 该服务器主要功能包括:
1.并发连接管理及通讯处理.
2.I/O线程组及工作线程组管理.
3.支持多种协议处理(HTTP, 文本协议, 二进制协议).
4.支持报文的加/解密和压缩/解压缩.
5.数据库连接池管理器(支持在主从服务器间自动切换数据库连接).
6.异步互联管理器(用于服务器间的异步通信).
7.同步通信管理器(用于访问缓存服务器)
8.定时器管理器(支持大规模定时器管理).
9.Session 管理器(管理用户在线信息).
10.Memcache缓存管理器(支持多种数据置换策略)
11.私有密码表管理
12.异步写管理器
13.用户锁管理器
14.小型内存池管理器
15.服务器集群管理器
16.提供基础数据结构服务(动态数组, 链表, hash表, 队列, 内存对象,堆,多叉树,红黑树等)
		
		
		
		
		
		
		
		
3. 参考资料
	1. 查看成熟的服务器框架, 而不管他是什么语言
		1. pomelo node.js 的分布式游戏服务器框架
		
		2. kbengine
		
		3. skynet
		
		4. anima

		5. firefly
			* 	
		
		6. NoahGameFrame
			* A fast, scalable, distributed game server framework for C++, include actor library, network library
			* 
		
		7. gabriel
			* seamless distributed game server engine
			* 
		5. ZooKeeper: 分布式协作
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			