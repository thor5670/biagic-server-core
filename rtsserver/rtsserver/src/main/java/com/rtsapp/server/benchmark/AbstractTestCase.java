package com.rtsapp.server.benchmark;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 15-9-11.
 */
public class AbstractTestCase implements ITestCase {

    private Map context;

    /**
     * 延迟执行时间，以毫秒为单位
     */
    private long delayTime;

    protected long startTime;
    protected long endTime;

    private List<ITestCaseListener> listeners;

    public AbstractTestCase( Map context ) {
        this.context = context;
    }

    public AbstractTestCase( Map context , long delayTime ){
        this.context = context;
        this.delayTime = delayTime;
    }


    @Override
    public Map getContext() {
        return this.context;
    }

    @Override
    public long delayTime(){
        return delayTime;
    }

    @Override
    public long executeTime(){
        return endTime - startTime;
    }


    @Override
    public void start() {
        this.startTime = System.nanoTime();

        this.notifyStart();
    }

    @Override
    public void complete() {
        this.endTime = System.nanoTime();

        this.notifyComplete();
    }


    @Override
    public void addListener(ITestCaseListener listener) {

        if( listeners == null ){
            listeners = new ArrayList<>();
        }

        listeners.add(listener);
    }


    protected void notifyStart(){
        if( listeners != null ){
            for( ITestCaseListener listener : listeners ){
                listener.onStart( this );
            }
        }
    }

    protected  void notifyComplete(){
        if( listeners != null ){
            for( ITestCaseListener listener : listeners ){
                listener.onComplete(this);
            }
        }
    }
}
