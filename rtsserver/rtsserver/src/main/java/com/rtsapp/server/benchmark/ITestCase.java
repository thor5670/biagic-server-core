package com.rtsapp.server.benchmark;

import java.util.Map;

/**
 *  测试用例是有状态的
 *
 */
public interface ITestCase {

    /**
     * 开始
     */
    void start();

    /**
     * 完成
     */
    void complete();

    /**
     * 从start到compelete的时间
     * 纳秒为单位
     * @return
     */
    long executeTime( );


    /**
     * 延迟执行的时间
     * @return
     */
    long delayTime( );

    /*
     * start，complete的监听器
     */
    void addListener( ITestCaseListener listener );

    /**
     * 测试用例所在的测试上下文
     * @return
     */
    Map getContext();


}
