package com.rtsapp.server.benchmark;

import java.util.Map;

/**
 * Created by admin on 15-9-17.
 * 测试用例配置
 */
public interface ITestCaseConfig {


    /**
     * 包含测试用例的个数
     * @return
     */
    int getCaseCount();

    /**
     * 所有的测试预计耗时多少毫秒
     * @return
     */
    long getExecuteTimeMills();

    /**
     * 获得测试用例所有用例的迭代器
     * @param ctx
     * @return
     */
    ITestCaseIterator newTestCaseIterator( Map ctx);


    /**
     * 获得测试用例指定组的迭代器
     * @param ctx
     * @param groupName
     * @return
     */
    ITestCaseIterator newTestCaseIterator( Map ctx, String groupName);


}
