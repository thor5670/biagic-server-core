package com.rtsapp.server.benchmark;

/**
 * Created by admin on 15-9-15.
 * 测试用例迭代器, 用于返回要执行的测试用例
 */
public interface ITestCaseIterator {

    /**
     * 是否有下一个测试用例
     * @return
     */
    boolean hasNext( );

    /**
     * 取下一个测试用例
     * @return
     */
    ITestCase next( );


}
