package com.rtsapp.server.benchmark;

/**
 * Created by admin on 15-9-11.
 * 测试用例执行监听器, 监听开始执行, 执行结束
 */
public interface ITestCaseListener {

    /**
     * Case开始后调用
     * @param testCase
     */
    void onStart( ITestCase testCase );

    /**
     * Case完成后调用
     */
    void onComplete( ITestCase testCase );


}
