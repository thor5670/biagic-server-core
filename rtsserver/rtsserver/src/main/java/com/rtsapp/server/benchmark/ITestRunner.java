package com.rtsapp.server.benchmark;

/**
 * 测试用例执行器
 */
public interface ITestRunner {

    /**
     *  启动
     */
    void start();

}
