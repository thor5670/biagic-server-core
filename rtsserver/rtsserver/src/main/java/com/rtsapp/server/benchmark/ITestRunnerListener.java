package com.rtsapp.server.benchmark;

import com.rtsapp.server.profiling.Profilling;

import java.util.Map;

/**
 * 测试框架运行时的监听器
 */
public interface ITestRunnerListener {

    void onTestStart( int sessionId, Map<String, Object> context );

    /**
     * 连接开始的事件,
     * 在这里可以给连接的上下文设置信息, 是该连接独有的
     * @param sessionId sessionId( 从1到最大连接数量  )
     * @param context 该连接独有的上下文
     */
    void onConnectionStart(int sessionId, Map<String, Object> context);


    void onTestComplete( );

}
