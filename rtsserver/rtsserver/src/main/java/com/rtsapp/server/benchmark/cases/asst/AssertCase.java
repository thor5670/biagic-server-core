package com.rtsapp.server.benchmark.cases.asst;

import com.rtsapp.server.benchmark.AbstractTestCase;
import com.rtsapp.server.benchmark.runner.ContextUtils;
import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;

import javax.naming.Context;
import java.util.Map;

/**
 * Created by admin on 15-9-16.
 */
public class AssertCase extends AbstractTestCase {

    private static Logger LOGGER = LoggerFactory.getLogger(  AssertCase.class );

    private AssertInfo assertInfo;

    public AssertCase( Map context , AssertInfo assertInfo ){
        super( context );
        this.assertInfo = assertInfo;
    }

    @Override
    public void start( ){

        super.start();


        try{
            AssertUtils.doAssert( assertInfo, this.getContext()  );
        }catch ( Throwable ex ){
            LOGGER.error(  ex , "[{}]:AssertCase断言出错:assertInfo={}", ContextUtils.getContextId( this.getContext() ), assertInfo );
        }


        complete();
    }


}
