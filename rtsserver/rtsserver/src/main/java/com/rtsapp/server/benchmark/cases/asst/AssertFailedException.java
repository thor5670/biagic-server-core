package com.rtsapp.server.benchmark.cases.asst;

/**
 * Created by admin on 15-11-2.
 */
public class AssertFailedException extends java.lang.RuntimeException {

    public AssertFailedException(String message, Throwable cause) {
        super(message, cause);
    }

}
