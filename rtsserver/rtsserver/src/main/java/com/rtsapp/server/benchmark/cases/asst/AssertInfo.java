package com.rtsapp.server.benchmark.cases.asst;

import java.util.ArrayList;
import java.util.List;

/**
 * 断言的信息
 */
public class AssertInfo {

    public static final String ASSERT_TYPE_EQUALS = "assert.equals";

    /**
     * 断言的类型
     */
    private final int assertType;

    /**
     * 表达式列表
     */
    private final List< AssertExpPair > expValueList = new ArrayList<>();

    
    AssertInfo( int assertType ){
        this.assertType = assertType;
    }

    /**
     * 追加一个测试
     * @param exp 表达式
     * @param exceptValue 期望的值
     */
    public void addAssert( String exp, String exceptValue ){
        expValueList.add( new AssertExpPair( exp, exceptValue ) );
    }


    public int getAssertType() {
        return assertType;
    }

    public List<AssertExpPair> getExpValueList() {
        return expValueList;
    }

    public boolean hasAssert() {
        return expValueList != null && expValueList.size() > 0;
    }

    @Override
    public String toString() {
        return "AssertInfo{" +
                "assertType=" + assertType +
                ", expValueList=" + expValueList +
                '}';
    }



    public static class AssertExpPair{
        /**
         * 实际需要计算的表达式
         */
        private String actualExp;
        /**
         * 期望结果的表达式
         */
        private String expectExp;

        public AssertExpPair(String actualExp, String expectExp) {
            this.actualExp = actualExp;
            this.expectExp = expectExp;
        }

        public String getActualExp() {
            return actualExp;
        }

        public String getExpectExp() {
            return expectExp;
        }


        @Override
        public String toString() {
            return "AssertExpPair{" +
                    "actualExp='" + actualExp + '\'' +
                    ", expectExp='" + expectExp + '\'' +
                    '}';
        }
    }
}
