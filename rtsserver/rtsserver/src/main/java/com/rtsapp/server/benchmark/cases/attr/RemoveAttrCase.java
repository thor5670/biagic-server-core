package com.rtsapp.server.benchmark.cases.attr;

import com.rtsapp.server.benchmark.AbstractTestCase;

import java.util.Map;

/**
 * 从测试上下文中删除一个属性
 */
public class RemoveAttrCase  extends AbstractTestCase {

    private String attrKey;

    public RemoveAttrCase( Map context, String attrKey ){
        super( context );
        this.attrKey = attrKey;
    }

    @Override
    public void start() {
        super.start();

        this.getContext().remove(attrKey);

        complete();
    }

    public String getAttrKey() {
        return attrKey;
    }
}
