package com.rtsapp.server.benchmark.cases.attr;

import com.rtsapp.server.benchmark.AbstractTestCase;

import java.util.Map;

/**
 * 在上下文中设置属性-
 */
public class SetAttrCase extends AbstractTestCase {

    private String attrKey;
    private Object value;

    public SetAttrCase( Map context, String attrKey, Object value ){
        super( context );
        this.attrKey = attrKey;
        this.value = value;
    }

    @Override
    public void start() {
        super.start();

        this.getContext().put(attrKey, value);

        complete();
    }

    public String getAttrKey() {
        return attrKey;
    }
}
