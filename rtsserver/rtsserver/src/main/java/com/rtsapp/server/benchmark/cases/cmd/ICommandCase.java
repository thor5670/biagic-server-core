package com.rtsapp.server.benchmark.cases.cmd;

import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.network.protocol.command.ICommand;

/**
 * Created by admin on 16-7-13.
 */
public interface ICommandCase {
    void setHandler(CommandCaseHandler handler);

    boolean recieve(int commandId, IByteBuffer buffer);

    void continueExecute();

    ICommand getRequest();

    ICommand getResponse();
}
