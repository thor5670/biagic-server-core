package com.rtsapp.server.benchmark.cases.prop;

import com.rtsapp.server.benchmark.AbstractTestCase;
import com.rtsapp.server.benchmark.exp.ExpUtils;
import com.rtsapp.server.benchmark.runner.ContextUtils;
import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;
import com.rtsapp.server.logger.spi.ConfigInvalidException;

import java.util.Map;

/**
 * Created by admin on 15-11-2.
 */
public class SetPropCase  extends AbstractTestCase {

    private static final Logger LOGGER = LoggerFactory.getLogger( SetPropCase.class );

    private String propExp;
    private String valueExp;

    public SetPropCase( Map context, String propExp, String valueExp ){
        super( context );
        this.propExp = propExp;
        this.valueExp = valueExp;
    }

    @Override
    public void start() {
        super.start();

        try {


            int index =  propExp.indexOf( "." );
            if( index >=0 ){

                Object obj = ExpUtils.getValue( this.getContext(), propExp.substring( 0, index ) );
                String prop  = propExp.substring( index +1 );

                Object value = ExpUtils.getValue( this.getContext(), valueExp );

                ExpUtils.setValue( obj, prop, value );

            }



        }catch ( Throwable ex ){

            LOGGER.error(   ex, "[{}]: SetPropCase start error" , ContextUtils.getContextId( getContext() ) );
        }

        complete();
    }


}
