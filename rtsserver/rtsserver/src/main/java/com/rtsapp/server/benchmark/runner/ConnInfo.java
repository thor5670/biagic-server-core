package com.rtsapp.server.benchmark.runner;

/**
 * Created by admin on 15-9-15.
 */
public class ConnInfo {

    private String ip;

    private int port;

    private int threadNum = 8;

    private int connNum = 1000;

    private int connStartTime  = 10;

    public ConnInfo(String ip, int port, int threadNum, int connNum, int connStartTime) {
        this.ip = ip;
        this.port = port;
        this.threadNum = threadNum;
        this.connNum = connNum;
        this.connStartTime = connStartTime;
    }

    public int getThreadNum() {
        return threadNum;
    }

    public void setThreadNum(int threadNum) {
        this.threadNum = threadNum;
    }

    public int getConnNum() {
        return connNum;
    }

    public void setConnNum(int connNum) {
        this.connNum = connNum;
    }

    public int getConnStartTime() {
        return connStartTime;
    }

    public void setConnStartTime(int connStartTime) {
        this.connStartTime = connStartTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }


}
