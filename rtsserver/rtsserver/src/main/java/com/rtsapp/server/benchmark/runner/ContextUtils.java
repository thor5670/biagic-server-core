package com.rtsapp.server.benchmark.runner;

import java.util.Map;

/**
 * Created by admin on 16-3-10.
 */
public class ContextUtils {

    private static final String CONTEXT_ID = "rtsserver_context_id";

    public static Object getContextId( Map<String,Object> context ){
        return context.get( CONTEXT_ID );
    }

}
