package com.rtsapp.server.benchmark.runner;

import com.rtsapp.server.benchmark.cases.cmd.CommandCaseHandler;

/**
 * Created by admin on 16-3-11.
 */
public interface ICommandCaseHandlerFactory {

    CommandCaseHandler createCommandHandler();

}
