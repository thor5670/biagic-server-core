package com.rtsapp.server.benchmark.script;

/**
 * Created by admin on 16-3-7.
 */

import com.rtsapp.server.network.protocol.command.ICommand;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 模板上的帮助类
 */
public abstract class AbstractCaseHelper implements ICaseHelper {

    private static final String NEW_LINE = "\r\n";

    @Override
    public String getCustomCase(ReqInfo reqInfo) {
        return "";
    }

    @Override
    public String getReqAttr(ReqInfo reqInfo) {
        return "";
    }

    @Override
    public String getRespAttr(ReqInfo reqInfo) {
        return "";
    }

    public String getPropString(FieldInfo fInfo)  {
        StringBuilder sb = new StringBuilder( );
        try {
            buildPropString(sb, fInfo.getField().getName(), fInfo.getValue());
        } catch (IllegalAccessException e) {
            throw new RuntimeException( e );
        } catch ( NullPointerException ex ){
            throw new RuntimeException( ex );
        }catch ( Throwable ex ){
            throw new RuntimeException( ex );
        }
        return sb.toString();
    }

    public  String getRespAsserts( ReqInfo reqInfo ){
        return "<assert.equals errorNo=\"0\" />";
    }

    private void buildPropString(StringBuilder sb, String fieldName, Object value) throws IllegalAccessException {

        if( value == null ){
            sb.append("");
            //sb.append("<" + fieldName + " value=\"" + value + "\" />");
            return;
        }

        String className =  value.getClass().getSimpleName();

        switch ( className ){
            case "int":
            case "Integer":
            case "byte":
            case "Byte":
            case "boolean":
            case "Boolean":
            case "float":
            case "Float":
            case "long":
            case "Long": {
                if( fieldName == null ){
                    sb.append("<" + className + " value=\"" + value + "\" />");
                }else {
                    sb.append("<" + fieldName + " value=\"" + value + "\" />");
                }
                break;
            }
            case "String": {
                if( fieldName == null ){
                    sb.append("<" + className + " value=\"'" + value + "'\" />");
                }else {
                    sb.append("<" + fieldName + " value=\"'" + value + "'\" />");
                }
                break;
            }
            case "List":
            case "ArrayList": {
                sb.append( "<"+ fieldName +">" );
                sb.append( NEW_LINE );

                List list  = (List)value;
                for( int i = 0; i < list.size(); i++ ){
                    buildPropString(sb, null, list.get(i) );
                    sb.append( NEW_LINE );
                }

                sb.append("</" + fieldName +">");
                break;
            }
            default: {
                //对象
                sb.append("<" + fieldName + ">");
                sb.append( NEW_LINE );

                Field[] fields =  value.getClass().getFields();
                for( Field f : fields ){
                    buildPropString(sb, f.getName(), f.get(value));
                    sb.append( NEW_LINE );
                }

                sb.append( "</" + fieldName +">" );
                break;
            }
        }
    }


    public String getClassName( Object obj ){
        if( obj == null ){
            return "";
        }else {
            return obj.getClass().getSimpleName();
        }
    }


    public List<FieldInfo> getFields( Object obj ) throws IllegalAccessException {

        List<FieldInfo> list = new ArrayList<>();

        Field[] fields =  obj.getClass().getFields();
        for( Field f : fields ){
            Object value = f.get( obj );
            list.add( new FieldInfo( obj,  f, value  ) );
        }

        return list;
    }


    public String getHex( int value ){
        return "0x" + Integer.toHexString(  value );
    }

    public String getTime( long time ){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date( time ));
    }
}
