package com.rtsapp.server.benchmark.script;

import java.lang.reflect.Field;

/**
 * Created by admin on 16-3-7.
 */
public  class FieldInfo<T> {

    private final T objectInfo;
    private final Field field;
    private final Object value;

    public FieldInfo(T objectInfo, Field field, Object value) {
        this.objectInfo = objectInfo;
        this.field = field;
        this.value = value;
    }

    public Field getField() {
        return field;
    }

    public Object getValue() {
        return value;
    }

    public T getObjectInfo() {
        return objectInfo;
    }
}
