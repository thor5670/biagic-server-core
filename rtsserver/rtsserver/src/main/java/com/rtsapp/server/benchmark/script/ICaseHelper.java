package com.rtsapp.server.benchmark.script;

import java.util.List;

/**
 * Created by admin on 16-3-7.
 */
public interface ICaseHelper {


    /**
     * 从用户的所有命令中解析出请求队列
     *   1. 每一组是一个登陆时长内的操作
     *   2. 每一组命令从登陆开始，或者从断线重登陆开始
     */
    List< List<ReqInfo> >  parseReqPairs( List<ScriptCommand> scriptCommands );

    /**
     * 获得自定义个case类名
     * @param reqInfo
     * @return
     */
    String getCustomCase( ReqInfo reqInfo );

    /**
     * 获得请求需要设置的属性名
     * @param reqInfo
     * @return
     */
    String getReqAttr( ReqInfo reqInfo );

    /**
     * 获得响应需要设置的属性名
     * @param reqInfo
     * @return
     */
    String getRespAttr( ReqInfo reqInfo );

    /**
     * 获得属性的输出字符串
     * @param fInfo
     * @return
     */
    String getPropString( FieldInfo fInfo  );

    /**
     * 获得针对响应的断言信息
     * @param reqInfo
     * @return
     */
    String getRespAsserts( ReqInfo reqInfo );


}
