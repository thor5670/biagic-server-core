package com.rtsapp.server.benchmark.script;

import com.rtsapp.server.network.protocol.command.ICommandFactory;

/**
 * Created by admin on 16-3-7.
 */
public interface IScriptCommandFactory extends ICommandFactory {

    ScriptCommand fixCommand(ScriptCommand command);

}
