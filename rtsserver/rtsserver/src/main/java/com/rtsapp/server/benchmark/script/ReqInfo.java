package com.rtsapp.server.benchmark.script;

/**
 * Created by admin on 16-3-7.
 */

import com.rtsapp.server.network.protocol.command.ICommand;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 表示一个请求的所有信息
 *
 */
public class ReqInfo {

    /**
     * 距离上一个消息的延时
     */
    private final long delay;
    /**
     * 请求消息
     */
    private final ICommand request;

    private ICommand response;

    public ReqInfo(long delay, ICommand request) {
        this.delay = delay;
        this.request = request;
    }

    public long getDelay() {
        return delay;
    }

    public ICommand getRequest() {
        return request;
    }

    public ICommand getResponse() {
        return response;
    }

    public void setResponse(ICommand response) {
        this.response = response;
    }

    public String getCommandName(){
        String className = request.getClass().getSimpleName();
        className = className.substring( 0, className.length() - "Request".length() );
        return className;
    }

    public List<FieldInfo<ReqInfo>> getReqFields( ) throws IllegalAccessException {
        return getFields( request );
    }

    public List<FieldInfo<ReqInfo>> getRespFields( ) throws IllegalAccessException {

        if( response != null ) {
            return getFields(response);
        }else{
            return new ArrayList<>();
        }

    }


    private List<FieldInfo<ReqInfo>> getFields( ICommand command ) throws IllegalAccessException {

        List<FieldInfo<ReqInfo>> list = new ArrayList<>();

        Field[] fields =  command.getClass().getFields( );
        for( Field f : fields ){
            Object value = f.get( command );
            list.add( new FieldInfo<ReqInfo>( this,  f, value  ) );
        }

        return list;
    }

}