package com.rtsapp.server.benchmark.script;

import java.util.Map;

/**
 * Created by admin on 16-3-7.
 */
public interface ScriptGeneratorTemplate {


    /**
     * 使用模板生成测试用例
     * @param saveFile
     * @param context
     */
    void generateFromTemplate( String saveFile, Map<String, Object> context );

}
