package com.rtsapp.server.bytecode;

/**
 * Compiler. (SPI, Singleton, ThreadSafe)
 */
public interface Compiler {

	/**
	 * Compile java source code.
	 * 
	 * @param code Java source code
	 * @param classLoader TODO
	 * @return Compiled class
	 */
	Class<?> compile(String code, ClassLoader classLoader);

}
