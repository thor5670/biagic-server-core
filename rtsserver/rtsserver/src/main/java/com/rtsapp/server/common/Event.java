package com.rtsapp.server.common;

/**
 * 事件对象
 * 每个事件都有一个id, 用于表名自己是什么类型的事件
 * 具体的事件集成Event, 添加自己的数据
 */
public interface Event {

    int getEventId();

}
