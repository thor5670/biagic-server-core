package com.rtsapp.server.common;

/**
 * 事件监听器
 */
public interface EventListener {

    /*
     * 处理发生的事件
     */
    void onEvent( Event event );

}
