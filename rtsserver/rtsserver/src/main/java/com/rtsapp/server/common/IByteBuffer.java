package com.rtsapp.server.common;

import java.nio.ByteOrder;
import java.util.Date;

public interface IByteBuffer {


    public static byte[] getByteArrFromUTF(String str) {
        int strlen = str.length();
        int utflen = 0;
        int c, count = 0;
        /* use charAt instead of copying String to char array */
        for (int i = 0; i < strlen; i++) {
            c = str.charAt(i);
            if ((c >= 0x0001) && (c <= 0x007F)) {
                utflen++;
            } else if (c > 0x07FF) {
                utflen += 3;
            } else {
                utflen += 2;
            }
        }
        byte[] bytearr = new byte[utflen];
        int i = 0;
        for (i = 0; i < strlen; i++) {
            c = str.charAt(i);
            if (!((c >= 0x0001) && (c <= 0x007F))) {
                break;
            }
            bytearr[count++] = (byte) c;
        }
        for (; i < strlen; i++) {
            c = str.charAt(i);
            if ((c >= 0x0001) && (c <= 0x007F)) {
                bytearr[count++] = (byte) c;

            } else if (c > 0x07FF) {
                bytearr[count++] = (byte) (0xE0 | ((c >> 12) & 0x0F));
                bytearr[count++] = (byte) (0x80 | ((c >> 6) & 0x3F));
                bytearr[count++] = (byte) (0x80 | ((c >> 0) & 0x3F));
            } else {
                bytearr[count++] = (byte) (0xC0 | ((c >> 6) & 0x1F));
                bytearr[count++] = (byte) (0x80 | ((c >> 0) & 0x3F));
            }
        }
        return bytearr;
    }

    /**
     * 字节序
     *
     * @return
     */
    public ByteOrder order();

    /**
     * 返回读的index
     *
     * @return
     */
    public int readerIndex();

    /**
     * 设置读的index
     *
     * @param readerIndex
     */
    public void readerIndex(int readerIndex );

    /**
     * 写的位置
     * @return
     */
    int writeIndex();

    /**
     * 设置写的位置
     * @param writeIndex
     */
    void writeIndex( int writeIndex );

    /**
     * 返回ByteBuffer中所有写入的字节数组
     */
    public byte[] array();

    public int capacity();

    /**
     * 返回可读的字节数
     *
     * @return
     */
    public int readableBytes();

    /**
     * 设置指定位置的int值，不改变读写index
     *
     * @param index
     * @param value
     */
    public void setInt(int index, int value);

    /**
     * 获取指定位置的int值，不改变读写索引
     *
     * @param index
     */
    public int getInt(int index);

    public boolean readBool();

    public byte readByte();

    public short readShort();

    public int readInt();

    public long readLong();

    public Date readDate();

    public float readFloat();

    public double readDouble();

    public String readUTF8();

    public void writeBool(boolean value);

    public void writeByte(byte value);

    public void writeShort(short value);

    public void writeInt(int value);

    public void writeLong(long value);

    public void writeDate(Date value);

    public void writeFloat(float value);

    public void writeDouble(double value);

    public void writeUTF8(String value);

    /**
     * 追加写入2字节的数组的长度, 然后继续追加写入数组
     *
     * @param value
     */
    public void writeByteArray(byte[] value);

    /**
     * 追加写入数组,不写入其他信息
     *
     * @param value
     */
    public void writeBytesDirect(byte[] value);

    public byte[] readByteArray();
}
