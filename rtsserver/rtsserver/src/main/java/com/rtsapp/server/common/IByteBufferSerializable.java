package com.rtsapp.server.common;

/**
 * 可与IByteBuffer进行序列化和反序列化的接口
 */
public interface IByteBufferSerializable {

    void  readFromBuffer( IByteBuffer buffer );

    void  writeToBuffer( IByteBuffer buffer );

}
