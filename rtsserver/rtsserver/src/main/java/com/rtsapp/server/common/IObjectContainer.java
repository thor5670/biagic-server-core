package com.rtsapp.server.common;


/**
 * 对象容器
 * @author dengyingtuo
 */
public interface IObjectContainer {

	/**
	 * 按存放的名称获取对象
	 * @param name
	 * @return
	 */
	Object getObject( String name );
	
	
	/**
	 * 按存放的类型获取对象
	 * @param requiredType
	 * @return
	 */
	<T> T getObject( Class<T> requiredType );
	
	
}
