package com.rtsapp.server.common.cache;

/**
 * @author dengyingtuo
 * 缓存
 *
 */
public interface ICache<K,V > {

    /**
     * 缓存中元素的个数
     * @return
     */
    int size();

    /**
     * 返回键关联到的值,如果键没有关联到任何值,返回null
     * @param key
     * @return
     */
    V get(K key);

    /**
     * 将键和值关联起来. 如果之前键已经和一个值关联啦，老的值将被传入的值替换
     * @param key
     * @param value
     * @return 之前该key关联的value, 如果null表示之前没有关联
     * @throws NullPointerException 如果key或者value是null, cache不接收key,value为空
     */
    V put(K key, V value);


    /**
     * 移除 key对应的键值映射,
     * @param key
     * @return
     */
    V remove( K key);

}
