package com.rtsapp.server.common.diskcache;

import java.util.List;
import java.util.Set;

/**
 * 硬盘Cache
 *   使用硬盘来存储数据
 */
public interface DiskCache {

//    static DiskCache newIndexBlockCache(  String cacheName, String dir, int indexSize, int blockSize ){
//        return new IndexBlockDiskCache( cacheName, dir, indexSize, blockSize );
//    }

    static DiskCache newSimpleFileCache( String cacheName, String dir ){
        return new SimpleFileDiskCache( cacheName, dir );
    }


    //所有的key
    Set<String> keys();

    byte[] get(String key );

    void set( String key, byte[] value);

    void append( String key, byte[] values);

    void remove(String key );


}
