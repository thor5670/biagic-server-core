//package com.rtsapp.server.common.diskcache;
//
//import com.rtsapp.server.logger.Logger;
//import com.rtsapp.server.logger.LoggerFactory;
//
//import java.io.IOException;
//import java.io.RandomAccessFile;
//import java.util.Map;
//import java.util.Set;
//
///**
// * Created by admin on 16-6-4.
// */
//class IndexBlockDiskCache implements DiskCache{
//
//    private final IndexBlockMgr indexBlockMgr;
//
//    public IndexBlockDiskCache(  String name, String dir, int indexSize, int blockSize ){
//
//    }
//
//    @Override
//    public Set<String> keys() {
//        return indexBlockMgr.indexKeys();
//    }
//
//
//    public boolean containsKey( String key ){
//        return indexBlockMgr.containsKey( key );
//    }
//
//
//    @Override
//    public byte[] get(String key) {
//        return indexBlockMgr.getBlock( key );
//    }
//
//    @Override
//    public void set(String key, byte[] value) {
//        indexBlockMgr.setBlock(key, value);
//    }
//
//    @Override
//    public void append(String key, byte[] value ) {
//        indexBlockMgr.appendBlock( key , value );
//    }
//
//    @Override
//    public void remove(String key) {
//        indexBlockMgr.remove( key );
//    }
//
//}
//
//
//
///**
// *
// */
//class IndexHeader{
//    byte type;
//    int version;
//    byte block;//一个index最多128字节
//}
//
//
//class Index{
//    String name;
//    int pos;
//    int size;
//    int blockSize;
//}
//
//
//class Block{
//    int pos;
//    int size;
//    Block next;
//}
//
//
//class IndexBlockMgr{
//
//    private static Logger LOG = LoggerFactory.getLogger(IndexBlockMgr.class);
//
//    int defaultIndexSize;
//    int defaultBlockSize;
//
//    IndexHeader header;
//    Map<String, Index> indexMap;
//
//    int allocatedSize;
//    Block freeBlocks;
//
//    RandomAccessFile headFile;
//    RandomAccessFile blockFile;
//
//    public void a( ){
//
//    }
//
//    public Set<String> indexKeys(){
//        return indexMap.keySet();
//    }
//
//    public boolean containsKey( String key ){
//        return indexMap.containsKey(key);
//    }
//
//    public byte[] getBlock(String key) {
//
//        Index index = indexMap.get( key );
//        if( index == null ){
//            return null;
//        }
//
//        try {
//            byte[] bytes = new byte[ index.size ];
//            blockFile.read( bytes, index.pos, index.size );
//            return bytes;
//        } catch (IOException e) {
//            LOG.error(  e );
//            return null;
//        }
//    }
//
//    public void setBlock( String key, byte[] value ){
//
//        Index index = indexMap.get( key );
//
//        //1. 已经有，并占据了足够空间, 重设原有的, 然后return
//        if( index != null && index.blockSize >= value.length ){
//
//            writeBlock( index.pos, value );
//
//            index.size = value.length;
//
//        }else {
//
//            //2. 如果有, 容量不够, 释放原有的
//            if (index != null) {
//                releaseBlock(index.pos, index.blockSize);
//            } else {
//                index = new Index();
//            }
//
//            //3. 新开存储
//            Block block = accquireBlock(value.length);
//            index.pos = block.pos;
//            index.blockSize = block.size;
//            index.size = value.length;
//            index.name = key;
//
//            writeBlock(index.pos, value);
//
//
//        }
//
//    }
//
//    public void appendBlock(String key, byte[] value ){
//
//        Index index = indexMap.get( key );
//
//        if( index == null ){
//            setBlock( key, value );
//        }else{
//
//            //如果空间够
//            if( index.size + value.length <= index.blockSize ){
//
//                writeBlock( index.pos + index.size, value );
//
//                index.size += value.length;
//
//            }else{ // 如果空间不够
//                byte[] bytes = new byte[ index.size + value.length ];
//
//                readBlock( index.pos, bytes, 0, index.size );
//                System.arraycopy(value, 0, bytes, index.size, value.length );
//
//                setBlock( key, value );
//            }
//        }
//
//    }
//
//
//    public void remove(String key) {
//
//        Index index = indexMap.remove(key);
//
//        if( index != null ){
//            releaseBlock( index.pos, index.blockSize );
//        }
//    }
//
//    /**
//     * 释放一个Block, 该块将重新变为可用块
//     * @param pos
//     * @param blockSize
//     */
//    private void releaseBlock( int pos, int blockSize ){
//        headFile.
//    }
//
//    /**
//     * 获得一个Block
//     *  * 从头寻找一个大小满足需求的block
//     * @param size
//     * @return
//     */
//    private Block accquireBlock( int size ){
//
//    }
//
//
//    private void writeBlock( int pos, byte[] value ){
//        blockFile.seek( pos);
//        blockFile.write(value, 0, value.length);
//    }
//
//}
