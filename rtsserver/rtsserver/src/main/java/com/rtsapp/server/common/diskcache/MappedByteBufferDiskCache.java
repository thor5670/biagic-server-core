package com.rtsapp.server.common.diskcache;

import java.util.Set;

/**
 * 基于内存映射的DiskCache
 *    * 速度应该非常快
 *    * 后续再实现吧
 */
public class MappedByteBufferDiskCache implements DiskCache {

    @Override
    public Set<String> keys() {
        return null;
    }

    @Override
    public byte[] get(String key) {
        return new byte[0];
    }

    @Override
    public void set(String key, byte[] value) {

    }

    @Override
    public void append(String key, byte[] values) {

    }

    @Override
    public void remove(String key) {

    }
}
