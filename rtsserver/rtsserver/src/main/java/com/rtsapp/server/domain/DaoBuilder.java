package com.rtsapp.server.domain;

import com.rtsapp.server.domain.support.jpa.ColumnDef;
import com.rtsapp.server.domain.support.jpa.TableDef;
import com.rtsapp.server.logger.Logger;

import javax.persistence.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 15-9-19.
 */
public abstract class DaoBuilder {

    protected static final Logger logger =com.rtsapp.server.logger.LoggerFactory.getLogger( EntityBuilder.class );

    private final Map<Class<?>, Class<?> > daoImplClasses = new HashMap<>( );


    public  StringBuilder getDaoCode( Class<? extends Dao> daoClass  ){
        return getDaoCode( daoClass, null );
    }

    /**
     * 获得生成类的代码
     * @param daoClass
     * @return
     */
    public abstract StringBuilder getDaoCode( Class<? extends Dao> daoClass , String queueKey );


}
