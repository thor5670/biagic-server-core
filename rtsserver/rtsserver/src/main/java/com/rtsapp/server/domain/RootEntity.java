package com.rtsapp.server.domain;

import com.rtsapp.server.domain.IEntity;

/**
 * 聚合根实体
 * 用来注解这个实体关联到了哪些实体, 以便清晰的明白它的级联存储
 */
public @interface RootEntity {

    Class<? extends IEntity>[] entities( );
}
