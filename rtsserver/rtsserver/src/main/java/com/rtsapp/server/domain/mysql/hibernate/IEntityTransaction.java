package com.rtsapp.server.domain.mysql.hibernate;

import java.io.Serializable;
import java.util.*;

public interface IEntityTransaction {

	void begin( );
	void close( );
	void commit( );
	void rollback( );
	
	Object get( Class<?> entityClass, Serializable id );
	List<?> get( Class<?> entityClass, List<Serializable> idList );
	List<?> get( Class<?> entityClass, Serializable...idList );
	
	void insert( Object entity );	
	void insert( List<?> entityList );
	void insert( Object...entityList );
	
	void update( Object entity );
	void update( List<?> entityList );
	void update( Object...entityList );

	void delete( Object entity );
	void delete( List<?> entityList );
	void delete( Object...entityList );
	
	int executeUpdate(String hql, Object... objects);
	
	Object queryUnique(String hql, Object... objects);
	List<?> queryList(String hql, Object... objects) ;
	List<?> queryListLimit(int firstResult, int maxResults, String hql, Object... objects) ;

}