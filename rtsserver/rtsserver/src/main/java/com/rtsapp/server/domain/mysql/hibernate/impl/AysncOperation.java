package com.rtsapp.server.domain.mysql.hibernate.impl;

public interface AysncOperation {
	
	
	enum OperationType{
		INSERT,
		UPDATE,
		DELETE,
	}
	
	
	/**
	 * 异步操作
	 * @param transaction
	 */
	public void execute( DirectTransaction transaction );
	
}
