package com.rtsapp.server.domain.mysql.hibernate.impl.op;

import java.util.*;

import com.rtsapp.server.domain.mysql.hibernate.impl.AysncOperation;
import com.rtsapp.server.domain.mysql.hibernate.impl.DirectTransaction;

public class HibernateTransactionTask implements AysncOperation {

	private List<AysncOperation> oprationList = new ArrayList<AysncOperation>();

	public HibernateTransactionTask(){
		
	}
	
	public void addOperation( AysncOperation opration ){
		this.oprationList.add( opration );
	}
	
	
	@Override
	public void execute(DirectTransaction transaction) {
		
		for( int i = 0; i < oprationList.size(); i++ ){
			AysncOperation operation = oprationList.get( i );
			operation.execute( transaction );
		}
		
		oprationList.clear();
	}

	public boolean isEmpty() {
		return oprationList.isEmpty( );
	}

	public int taskSize() {
		return oprationList.size( );
	}

	public AysncOperation firstTask( ) {
		return oprationList.get(  0 );
	}
	
	
	
	
}
