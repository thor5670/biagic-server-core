package com.rtsapp.server.domain.mysql.hibernate.impl.op;

import java.util.List;
import com.rtsapp.server.domain.mysql.hibernate.impl.AysncOperation;
import com.rtsapp.server.domain.mysql.hibernate.impl.DirectTransaction;


public class InsertUpdateDeleteTask implements AysncOperation {

	private OperationType type;
	private Object obj;
	private Object[] objs;
	private List<?> objList;


	public InsertUpdateDeleteTask( OperationType type, List<?> insertObjList ){
		this.type = type;
		this. objList = insertObjList;
	}

	public InsertUpdateDeleteTask( OperationType type, Object insertObj ){
		this.type = type;
		this.obj = insertObj;
	}

	public InsertUpdateDeleteTask( OperationType type, Object[] insertObjs ){
		this.type = type;
		this.objs = insertObjs;
	}


	@Override
	public void execute(DirectTransaction transaction) {

		if( type == OperationType.INSERT ){
			if( obj != null ){
				transaction.insert(  obj );
			}else if( objs != null ){
				transaction.insert( objs );
			}else if( objList != null ){
				transaction.insert( objList );
			}
		}else if( type == OperationType.UPDATE ){

			if( obj != null ){
				transaction.update(  obj );
			}else if( objs != null ){
				transaction.update( objs );
			}else if( objList != null ){
				transaction.update( objList );
			}

		}else if( type == OperationType.DELETE ){

			if( obj != null ){
				transaction.delete(  obj );
			}else if( objs != null ){
				transaction.delete( objs );
			}else if( objList != null ){
				transaction.delete( objList );
			}
		}
	}


}
