package com.rtsapp.server.domain.mysql.sql;

import com.rtsapp.server.domain.mysql.sql.impl.MySQLConnection;

import java.util.concurrent.Future;

/**
 * 工作线程池, 对外的接口类
 *
 * 1. 设置连接信息, 同步连接数量, 异步连接数量默认为1
 * 2. 开启连接, 预编译sql语句(目前必须提前编译), 关闭
 * 3. 获取预编译sql语句, 执行
 */
public interface DatabaseWorkerPool {

    boolean open();

    boolean prepareStatement( String sqlIndex , String sql );

    void close();
    
    /**
     * 获得预编译的sql语句
     * @param sqlIndex
     * @return
     */
    MySQLPreparedStatement getPreparedStatement( String sqlIndex );

    /**
     * 异步执行sql, 会放入队列
     * @param stmt
     */
    void execute(MySQLPreparedStatement stmt);

    /**
     * 异步执行sql, 会放入队列
     * @param stmt
     * @param queueIndex 使用的异步队列, queueIndex的值应该为非负, 将采取  queueIndex % 队列长度来选择合适的索引
     */
    void execute(MySQLPreparedStatement stmt, int queueIndex );

    /**
     * 同步执行查询, 不使用队列
     * @param stmt
     * @param handler
     */
    void query(MySQLPreparedStatement stmt, MySQLResultSetHandler handler);



    Future<Object> asyncQuery( MySQLPreparedStatement stmt, MySQLResultSetFutureHandler handler  );


    /**
     * 同步执行sql, 不经过队列
     * @param stmt
     * @return 是否执行成功
     */
    boolean directExecute(MySQLPreparedStatement stmt);

    /**
     * 获得事务
     * @return
     */
    MySQLTransaction getTransaction();

    /**
     * 执行事务
     * @param transaction
     */
    void executeTransaction(MySQLTransaction transaction);


    /**
     * 执行事务
     * @param transaction
     * @param queueIndex 使用的队列索引
     */
    void executeTransaction(MySQLTransaction transaction, int queueIndex );

    /**
     * 执行或者追加到事务
     * @param trans
     * @param stmt
     */
    void executeOrAppend(MySQLTransaction trans, MySQLPreparedStatement stmt);


    /**
     * 获取连接, 这个方法除非不得已请不要调用
     * @return
     */
    MySQLConnection getConnection();


    /**
     * 关闭连接, 这个方法和方面的方法一致, 记得一定要释放连接
     * @param conn
     */
    void releaseConnection( MySQLConnection conn );

    /**
     * 发送ping，保持数据库连接
     */
    void keepAlive();



    /**
     * @return 异步队列的任务数量
     */
    int getAsyncQueueSize( );

}
