package com.rtsapp.server.domain.mysql.sql;

public class MySQLConnectionInfo {

    private String host;
    private int port;
    private String database;
    private String characterEncoding;

    private String user;
    private String password;

    //用于同步操作的连接数量
    private int  count;

    //异步写操作的连接数量
    private int asyncCount = 1;

    //用于异步查询的线程数量
    private int queryThread = 1;


    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getCharacterEncoding() {
        return characterEncoding;
    }

    public void setCharacterEncoding(String characterEncoding) {
        this.characterEncoding = characterEncoding;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getAsyncCount() {
        return asyncCount;
    }

    public void setAsyncCount(int asyncCount) {
        if( asyncCount < 0 ){
            throw new RuntimeException( "异步数量的大小不能小于0" );
        }
        this.asyncCount = asyncCount;
    }

    public int getQueryThread() {
        return queryThread;
    }

    public void setQueryThread(int queryThread) {
        if( asyncCount < 0 ){
            throw new RuntimeException( "异步数量的大小不能小于0" );
        }
        this.queryThread = queryThread;
    }


    public String getJDBCURL(){
        String url = "jdbc:mysql://" + this.getHost() + ":" + this.getPort() + "/" + this.getDatabase() + "?characterEncoding=" + this.getCharacterEncoding();
        return url;
    }
}
