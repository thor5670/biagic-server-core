package com.rtsapp.server.domain.mysql.sql;

import com.rtsapp.server.domain.Dao;
import com.rtsapp.server.domain.IEntity;

/**
 * Created by admin on 15-10-7.
 */
public class MySQLDao<T extends IEntity, ID > extends Dao<T,ID > {


    private DatabaseWorkerPool readPool;
    private DatabaseWorkerPool writePool;


    public boolean setWorkpool(  DatabaseWorkerPool readPool, DatabaseWorkerPool writePool ){

        if( readPool == null || writePool == null ){
            return false;
        }

        this.readPool = readPool;
        this.writePool = writePool;

        //预编译自动生成的sql语句
        if( !innerPrepareStatement( this.readPool ) ){
            return false;
        }

        if( this.writePool != this.readPool ){
            if( !innerPrepareStatement( this.writePool ) ){
                return false;
            }
        }

        //预编译生成的sql语句
        if( !doPrepareStatement( this.readPool ) ){
            return false;
        }

        if( this.writePool != this.readPool ){
            if( !doPrepareStatement( this.writePool ) ){
                return false;
            }
        }

        return true;
    }


    public DatabaseWorkerPool getReadPool( ){
        return this.readPool;
    }

    public DatabaseWorkerPool getWritePool( ){
        return this.writePool;
    }


    protected boolean innerPrepareStatement( DatabaseWorkerPool db ){
        return true;
    }

    /**
     * 需要自己的重写
     * @param db
     */
    protected boolean doPrepareStatement( DatabaseWorkerPool db  ){
        return true;
    }



}
