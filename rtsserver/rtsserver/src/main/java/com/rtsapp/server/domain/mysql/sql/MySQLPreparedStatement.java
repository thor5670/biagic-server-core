package com.rtsapp.server.domain.mysql.sql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 封装sql执行的信息
 * 1. index代表一个sql索引
 * 2. parameters代表sql执行的参数
 */
public class MySQLPreparedStatement {

    /**
     * sql的index
     */
    private final String index;

    /**
     * sql执行的参数
     */
    private Object[] parameters;


    public MySQLPreparedStatement( String index) {
        this.index = index;
    }

    public String getIndex() {
        return index;
    }


    public void bindParameters(PreparedStatement stmt) throws SQLException {
        if (parameters != null) {
            for (int i = 0; i < parameters.length; i++) {
                stmt.setObject(i + 1, parameters[i]);
            }
        }
    }

    public void setParams(Object... values) {
        this.parameters = values;
    }


}
