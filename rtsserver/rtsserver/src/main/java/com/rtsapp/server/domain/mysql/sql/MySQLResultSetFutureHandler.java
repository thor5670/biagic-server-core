package com.rtsapp.server.domain.mysql.sql;


public interface MySQLResultSetFutureHandler extends MySQLResultSetHandler{

	Object getResult();

}
