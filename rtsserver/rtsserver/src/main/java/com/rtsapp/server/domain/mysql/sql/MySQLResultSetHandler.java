package com.rtsapp.server.domain.mysql.sql;

import java.sql.ResultSet;


public interface MySQLResultSetHandler {
	
	void doResultSet( ResultSet rs );

}
