package com.rtsapp.server.domain.mysql.sql;

import java.util.ArrayList;
import java.util.List;

/**
 * 封装一组数据库操作为一个整体
 * 在真正执行时会将所有的操作放在一个事务中执行
 */
public class MySQLTransaction {
	
	private List<MySQLPreparedStatement> stmtList = new ArrayList<>();
	
    public MySQLTransaction(){ }
    
    public void append(MySQLPreparedStatement stmt) {
    	this.stmtList.add( stmt );
    }

    public  List<MySQLPreparedStatement> getPreparedStatementList(){
    	return this.stmtList;
    }

    /**
     * 数据库操作对象的个数
     * @return
     */
    public int size( ){
        return this.stmtList.size();
    }

    /**
     * 清除所有的操作对象
     */
    public void cleanup() {
    	this.stmtList.clear();
    }

}
