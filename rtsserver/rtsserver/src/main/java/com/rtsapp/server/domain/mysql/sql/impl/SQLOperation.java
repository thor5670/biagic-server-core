package com.rtsapp.server.domain.mysql.sql.impl;

/**
 * Created by zhangbin on 15/7/8.
 */
public abstract class SQLOperation {
	
    protected MySQLConnection conn = null;

    public void setConnection(MySQLConnection con) {
        this.conn = con;
    }

    public int call() {
        execute();
        return 0;
    }

    public abstract boolean execute();
}
