package com.rtsapp.server.domain.mysql.sql.impl.op;

import com.rtsapp.server.domain.mysql.sql.impl.SQLOperation;

/**
 * Created by zhangbin on 15/7/8.
 */
public class PingTask extends SQLOperation {

    @Override
    public boolean execute() {
        conn.ping();
        return true;
    }
}
