package com.rtsapp.server.domain.mysql.sql.impl.op;

import com.rtsapp.server.domain.mysql.sql.MySQLPreparedStatement;
import com.rtsapp.server.domain.mysql.sql.impl.SQLOperation;


/**
 *
 */
public class PreparedStatementTask extends SQLOperation {

	private final MySQLPreparedStatement stmt ;


	public PreparedStatementTask(MySQLPreparedStatement stmt) {
        this.stmt = stmt;
	}

    public MySQLPreparedStatement getStmt(){
        return stmt;
    }
	
    @Override
    public boolean execute() {
    	return this.conn.execute( stmt );
    }


}
