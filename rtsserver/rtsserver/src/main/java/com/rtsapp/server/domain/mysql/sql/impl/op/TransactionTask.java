package com.rtsapp.server.domain.mysql.sql.impl.op;

import com.rtsapp.server.domain.mysql.sql.MySQLTransaction;
import com.rtsapp.server.domain.mysql.sql.impl.SQLOperation;

/**
 * Created by zhangbin on 15/7/9.
 */
public class TransactionTask extends SQLOperation {
	
	private MySQLTransaction trans;
	
    public TransactionTask( MySQLTransaction transaction ){
    	this.trans = transaction;
    }

    @Override
    public boolean execute() {
    
    	return this.conn.executeTransaction( this.trans );    
    }
}
