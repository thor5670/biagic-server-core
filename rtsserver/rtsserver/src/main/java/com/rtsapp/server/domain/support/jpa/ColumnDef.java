package com.rtsapp.server.domain.support.jpa;

import com.rtsapp.server.utils.StringUtils;

/**
 * Created by admin on 15-9-19.
 * 列的定义
 */
public class ColumnDef {

    //列名
    private String columnName;

    //属性名
    private String fieldName;
    //属性类型
    //如果是外键对象, 类型是关联对象的id类型
    private String fieldType;

    private boolean joinColumn = false;
    private String joinColumnFieldName;
    private String joinClumnFieldType;

    public ColumnDef() {

    }


    public ColumnDef( String columnName, String fieldName , String fieldType ) {
        this.columnName = columnName;
        this.fieldName = fieldName;
        this.fieldType = fieldType;
    }

    public void setJoinColumnInfo( boolean isJoinColumn, String joinColumnFieldName, String joinClumnFieldType ){
        this.joinColumn = isJoinColumn;
        this.joinColumnFieldName = joinColumnFieldName;
        this.joinClumnFieldType = joinClumnFieldType;
    }

    public String getGetterString(){

        if( joinColumn ){
            return getGetter( fieldType, fieldName ) + "." +  getGetter( joinClumnFieldType, joinColumnFieldName );
        }else{
            return getGetter( fieldType, fieldName );
        }
    }

    public String getObjectGetterString(){
        return getGetter( fieldType, fieldName );
    }

    private String getGetter( String type, String fieldName ){
        //只有小写boolean以is开头
        if( "boolean".equals( type ) ){
             return "is" +  StringUtils.upperFirstChar( fieldName ) + "()";
        }else{
            return "get" +  StringUtils.upperFirstChar( fieldName ) + "()";
        }

    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }



    public boolean isJoinColumn() {
        return joinColumn;
    }

    public void setJoinColumn(boolean joinColumn) {
        this.joinColumn = joinColumn;
    }

    public String getJoinColumnFieldName() {
        return joinColumnFieldName;
    }

    public void setJoinColumnFieldName(String joinColumnFieldName) {
        this.joinColumnFieldName = joinColumnFieldName;
    }

    public String getJoinClumnFieldType() {
        return joinClumnFieldType;
    }

    public void setJoinClumnFieldType(String joinClumnFieldType) {
        this.joinClumnFieldType = joinClumnFieldType;
    }
}
