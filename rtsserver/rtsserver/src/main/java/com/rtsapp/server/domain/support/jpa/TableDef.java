package com.rtsapp.server.domain.support.jpa;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 15-9-19.
 * 表的定义
 */
public class TableDef {

    //表名
    private String tableName;
    //实体名
    private String entityName;

    //id
    private ColumnDef id;
    //列
    private List<ColumnDef> columns = new ArrayList<>();


    public TableDef() {

    }

    public TableDef(String tableName, String entityName) {
        this.tableName = tableName;
        this.entityName = entityName;
    }

    public void appendColumn(ColumnDef column) {
        this.columns.add(column);
    }

    public String getTableName() {
        return tableName;
    }

    public String getEntityName() {
        return entityName;
    }

    public ColumnDef getId() {
        return id;
    }

    public void setId(ColumnDef idColumn) {
        this.id = idColumn;
    }

    public List<ColumnDef> getColumns() {
        columns.sort((ColumnDef c1, ColumnDef c2) -> c1.getColumnName().compareTo(c2.getColumnName()));
        return columns;
    }

    public boolean isIdColumn(ColumnDef column) {
        return column == id;
    }

    /**
     * 按列名获取列定义
     * 忽略大小写
     *
     * @param columnName
     * @return
     */
    public ColumnDef getColumnByFieldName(String columnName) {
        for (ColumnDef column : columns) {
            if (column.getFieldName().equalsIgnoreCase(columnName)) {
                return column;
            }
        }

        return null;
    }

}
