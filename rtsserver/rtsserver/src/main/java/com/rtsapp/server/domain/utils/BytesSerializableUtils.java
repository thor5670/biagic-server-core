package com.rtsapp.server.domain.utils;

import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.common.IByteBufferSerializable;

import java.util.DoubleSummaryStatistics;
import java.util.LongSummaryStatistics;

/**
 * Created by admin on 15-11-7.
 */
public class BytesSerializableUtils {


    public static void write( IByteBuffer buffer, Object t  ){
        if( t.getClass() == Integer.class ){
            buffer.writeInt((Integer) t);
        }
        else if( t.getClass() ==  Boolean.class ){
            buffer.writeBool((Boolean) t);
        }
        else if(t.getClass() == String.class ){
            buffer.writeUTF8((String) t);
        }
        else if( t.getClass() == Float.class ){
            buffer.writeFloat((Float) t);
        }
        else if( t.getClass() == Long.class ){
            buffer.writeLong((Long) t);
        }
        else if( t.getClass() == Byte.class ){
            buffer.writeByte((Byte) t);
        }
        else if( t.getClass() == Short.class ){
            buffer.writeShort((Short) t);
        }
        else if( t.getClass() == Double.class ){
            buffer.writeDouble((Double) t);
        }
        else {
            throw new RuntimeException("序列化类型不支持");
        }
    }



    public static <T> T read( IByteBuffer buffer, Class<T> t  ){

        if( t == Integer.class ){
            return (T)( Integer.valueOf( buffer.readInt() ) );
        }
        else if( t == Boolean.class ){
            return (T)( Boolean.valueOf( buffer.readBool() ) );
        }
        else if( t == String.class ){
            return (T)(buffer.readUTF8());
        }
        else if( t == Float.class ){
            return (T)( Float.valueOf(buffer.readFloat()) );
        }
        else if( t == Long.class ){
            return (T)( Long.valueOf( buffer.readLong() ) );
        }
        else if( t == Byte.class ){
            return (T)( Byte.valueOf( buffer.readByte() ) );
        }
        else if( t == Short.class ){
            return (T)( Short.valueOf( buffer.readShort() ) );
        }
        else if( t == Double.class ){
            return (T)( Double.valueOf(buffer.readDouble()) );
        }

        else {
            throw new RuntimeException("序列化类型不支持");
        }
    }

}
