package com.rtsapp.server.domain.utils;

import com.rtsapp.server.common.ByteBuffer;
import com.rtsapp.server.common.IByteBufferSerializable;
import com.rtsapp.server.domain.IEntity;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Created by admin on 15-10-26.
 */
public class EmbedSetProperty<T>  {

    private final IEntity entity;
    private final CopyOnWriteArraySet<T> innerSet = new CopyOnWriteArraySet<>( );
    private final Class<T> valueClass;

    public  EmbedSetProperty( IEntity entity,  Class<T> valueClass ){
        this.entity = entity;
        this.valueClass = valueClass;
    }


    public int size() {
        return innerSet.size();
    }

    public boolean isEmpty() {
        return innerSet.isEmpty();
    }

    public boolean contains(Object o) {
        return innerSet.contains(o);
    }

    public boolean add(T t) {
        boolean isAdd =  innerSet.add( t );
        if( isAdd ){
            entity.setDirty();
        }
        return isAdd;
    }

    public boolean remove(Object o) {
        boolean hasObj =   innerSet.remove(o);
        if( hasObj ){
            entity.setDirty();;
        }
        return hasObj;
    }


    public void clear() {
        if( innerSet.size() > 0 ) {
            innerSet.clear();
            entity.setDirty();
        }
    }

    /**
     * 获得迭代器, 只能读, 不能修改
     * @return
     */
    public Iterator<T> iterator(){
        return innerSet.iterator();
    }



    /**
     * 使用默认的方式序列化为bytes
     * @return
     */
    public byte[] getBytes( ) {

        ByteBuffer buffer = new ByteBuffer();

        buffer.writeInt(0);

        int count = 0; //由于多线程换将，map的大小可能再运行中被改变，因此用count来记数
        for ( Iterator<T> it = iterator(); it.hasNext(); ) {

            count++;

            T value = it.next();

            if (value instanceof IByteBufferSerializable) {
                ((IByteBufferSerializable) (value)).writeToBuffer(buffer);
            } else {
                BytesSerializableUtils.write(buffer, value);
            }
        }

        buffer.setInt(0, count );
        return buffer.toBytes();
    }

    /**
     * 使用默认的方式进行反序列化
     * @param bytes
     */
    public void setBytes(  byte[] bytes) {

        this.clear();

        //bytes不能为空, 且长度大于整数
        if( bytes == null || bytes.length < 4 ){
            return;
        }

        try {
            ByteBuffer buffer = new ByteBuffer(bytes);
            int count = buffer.readInt();
            for (int i = 0; i < count; i++) {

                T value = null;
                if ( IByteBufferSerializable.class.isAssignableFrom(  valueClass ) ) {
                    value = (T) valueClass.newInstance();
                    ((IByteBufferSerializable)value).readFromBuffer( buffer );
                } else {
                    value = BytesSerializableUtils.read(buffer, valueClass);
                }

                this.add( value );
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
