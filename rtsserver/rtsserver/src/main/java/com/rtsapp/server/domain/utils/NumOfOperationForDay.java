package com.rtsapp.server.domain.utils;

import com.rtsapp.server.common.ByteBuffer;
import com.rtsapp.server.domain.IEntity;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by zhangbin on 15/6/26.
 */
public class NumOfOperationForDay extends NumOfOperation {

    public NumOfOperationForDay( IEntity entity  ) {
        super( entity );
    }

    private NumOfOperationForDay( IEntity entity,  int times, long lastOperationTime) {
        super( entity , times, lastOperationTime);
    }

    public NumOfOperationForDay( IEntity entity, ByteBuffer buffer) {
        super( entity, buffer );
    }

    private NumOfOperationForDay( IEntity entity, String timesStr, String lastOperationTimeStr) {
        super( entity, timesStr, lastOperationTimeStr);
    }

    private NumOfOperationForDay( IEntity entity,String[] numOfOperationStrs) {
        super( entity, numOfOperationStrs);
    }

    public static NumOfOperationForDay valueOf(  IEntity entity, int times, long lastOperationTime) {
        return new NumOfOperationForDay( entity, times, lastOperationTime);
    }

    public static NumOfOperationForDay valueOf(  IEntity entity,String timesStr, String lastOperationTimeStr) {
        return new NumOfOperationForDay( entity, timesStr, lastOperationTimeStr);
    }

    public static NumOfOperationForDay valueOf(  IEntity entity, String[] numOfOperationStrs) {
        return new NumOfOperationForDay( entity, numOfOperationStrs );
    }

    protected boolean checkOperationTimesReset( ) {
//        DictService.checkDictVersion();
//        int reset_time = DictService.getInstanceInThread().getGameParamDict().reset_time;
//
//        // 最后一次理论重置时间
//        Calendar lastResetTime = new GregorianCalendar( );
//        Date nowDate = lastResetTime.getTime();
//        lastResetTime.set(Calendar.HOUR_OF_DAY, reset_time);
//        lastResetTime.set(Calendar.MINUTE, 0);
//        lastResetTime.set(Calendar.SECOND, 0);
//        lastResetTime.set(Calendar.MILLISECOND, 0);
//        if (nowDate.getTime() < lastResetTime.getTimeInMillis()) {
//            lastResetTime.add(Calendar.DATE, -1);
//        }
//
//        //如果最后一次操作再重置时间之前, 则应该重置啦
//        if (lastOperationTime < lastResetTime.getTimeInMillis()) {
//            times = 0;
//            lastOperationTime = System.currentTimeMillis();
//            return true;
//        }


        return false;
    }

}
