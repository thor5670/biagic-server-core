package com.rtsapp.server.domain.utils;

import com.rtsapp.server.domain.IEntity;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by zhangbin on 15/6/26.
 */
public class NumOfOperationForMonth extends NumOfOperation {

    public NumOfOperationForMonth(  IEntity entity ) {
        super( entity );
    }

    private NumOfOperationForMonth(  IEntity entity , int times, long lastOperationTime) {
        super( entity, times, lastOperationTime);
    }

    public NumOfOperationForMonth(  IEntity entity , String timesStr, String lastOperationTimeStr) {
        super( entity, timesStr, lastOperationTimeStr);
    }

    public NumOfOperationForMonth(  IEntity entity , String[] numOfOperationStrs) {
        super( entity, numOfOperationStrs);
    }

    public static NumOfOperationForMonth valueOf(  IEntity entity , int times, long lastOperationTime) {
        return new NumOfOperationForMonth( entity, times, lastOperationTime);
    }

    public static NumOfOperationForMonth valueOf(  IEntity entity , String timesStr, String lastOperationTimeStr) {
        return new NumOfOperationForMonth( entity, timesStr, lastOperationTimeStr);
    }

    public static NumOfOperationForMonth valueOf(  IEntity entity, String[] numOfOperationStrs) {
        return new NumOfOperationForMonth( entity, numOfOperationStrs);
    }

    protected boolean checkOperationTimesReset() {


//        DictService.checkDictVersion();
//        int reset_time = DictService.getInstanceInThread().getGameParamDict().reset_time;
//        Calendar lastResetTime = new GregorianCalendar();
//        Date nowDate = lastResetTime.getTime();
//        lastResetTime.set(Calendar.DAY_OF_MONTH, 1);
//        lastResetTime.set(Calendar.HOUR_OF_DAY, reset_time);
//        lastResetTime.set(Calendar.MINUTE, 0);
//        lastResetTime.set(Calendar.SECOND, 0);
//        lastResetTime.set(Calendar.MILLISECOND, 0);
//        if (nowDate.getTime() < lastResetTime.getTimeInMillis()) {
//            lastResetTime.add(Calendar.MONTH, -1);
//        }
//        if (lastOperationTime < lastResetTime.getTimeInMillis()) {
//            times = 0;
//            lastOperationTime = System.currentTimeMillis();
//            return true;
//        }


        return false;
    }

}
