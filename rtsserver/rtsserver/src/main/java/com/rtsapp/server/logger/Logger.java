package com.rtsapp.server.logger;

/**
 * 日志接口
 *   这个接口大部分代码从slf4j复制过来的
 */

public interface Logger {

    String getName();


    boolean isTraceEnabled();

    void trace(String logFormat);

    void trace(String logFormat, Object... params );

    void trace(String logFormat, Throwable ex);



    boolean isDebugEnabled();

    void debug( String logFormat );

    void debug( String logFormat, Object... params );

    void debug( String logFormat, Throwable ex );



    boolean isInfoEnabled();

    void info(String logFormat);

    void info(String logFormat, Object... params);

    void info(String logFormat, Throwable ex);



    boolean isWarnEnabled();

    void warn(String logFormat);

    void warn(String logFormat, Object... params);

    void warn(String logFormat, Throwable ex);



    boolean isErrorEnabled();

    void error(String logFormat);

    void error(String logFormat, Object... params);

    void error(String logFormat, Throwable ex);

    void error(Throwable ex, String logFormat);

    void error(Throwable ex, String logFormat, Object... params);

    void error(Throwable ex);

}
