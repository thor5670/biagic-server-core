package com.rtsapp.server.logger;

import com.rtsapp.server.logger.spi.LoggerManager;

/**
 * Logger工厂
 */
public final class LoggerFactory  {


    private static final LoggerManager loggerMgr = new LoggerManager();


    /**
     * 用于创建日志
     * @param loggerName
     * @return
     */
    public static  Logger getLogger(String loggerName) {
        return loggerMgr.createLogger( loggerName );
    }


    /**
     * 用于创建日志
     * @param claz
     * @return
     */
    public static Logger getLogger(Class claz) {
        return loggerMgr.createLogger( claz.getName() );
    }


    /**
     * 配置日志组件
     * 1. 一般在应用程序启动时进行配置
     * 2. 如果程序运行过程中调用, 相当于重新配置, 在重新加载日志配置的过程中会有短暂的日志暂停, 这段时间输出的日志将被丢弃
     * @param cfgFile
     * @return
     */
    public static boolean configure( String cfgFile ){
        return loggerMgr.configure(cfgFile);
    }


    /**
     * 设置某一日志的级别,
     * 1. setLevel(null, "debug") 为设置root日志为debug
     * 2. setLevel("com.rtsapp.naval", "debug")为设置com.rtsapp.naval日志为debug
     * @param loggerName 配置文件中定义的日志名称  比如:log4j.com.rtsapp.naval名称为com.rtsapp.naval, 另外root用null表示, 如log4j.rootLogger为root日志,用null表示root日志
     * @param level 日志级别字符串 debug, error,info等
     * @return 是否设置成功
     */
    public static boolean setLevel( String loggerName, String level ){
        return loggerMgr.setLevel(loggerName, level );
    }

    /**
     * 日志组件关闭
     * 1. 由于使用了缓存，和异步功能, 日志系统在关闭时必须调用shutdown
     * 2. shutdown确保输出所有缓存的日志, 和异步日志队列, 然后关闭输出流
     */
    public static void shutdown(  ){
        loggerMgr.shutdown( );
    }
}
