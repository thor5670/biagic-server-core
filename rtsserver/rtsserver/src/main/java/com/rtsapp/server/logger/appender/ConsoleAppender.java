package com.rtsapp.server.logger.appender;

/**
 *
 */
public class ConsoleAppender implements ILoggerAppender<String>{

    @Override
    public void appendLog(String log) {
        System.out.print( log );
        System.out.flush();
    }

    @Override
    public void close() {

    }
}
