package com.rtsapp.server.logger.appender;

/**
 * ILoggerAppender 为日志输出器接口
 * 子类: SyncLoggerAppender, AsyncLoggerAppender 是线程安全的, 应该直接使用
 * 其他子类是非线程安全的, 应该封装在SyncLoggerAppender，AsyncLoggerAppender中使用
 */
public interface ILoggerAppender<T> {

    /**
     * 追加日志
     * @param log
     */
    void appendLog( T log );

    /**
     * 关闭
     * 多次close需要没有副作用
     */
    void close();

}
