package com.rtsapp.server.logger.format;

import com.rtsapp.server.logger.spi.LogEvent;

/**
 * 日志格式化类
 * Layout必须是线程安全的
 */
public interface ILayout {

    /**
     * 格式化日志, 将结果存储在传入的StringBuilder中
     * @param sb 结果存储的StringBuilder
     * @param event 日志事件
     */
    void formatLog( StringBuilder sb, LogEvent event );


}
