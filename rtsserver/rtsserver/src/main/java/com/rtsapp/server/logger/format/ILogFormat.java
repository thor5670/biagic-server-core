package com.rtsapp.server.logger.format;

import com.rtsapp.server.logger.spi.LogEvent;


/**
 * 日志格式化
 * 实现必须是线程安全的
 */
public interface ILogFormat {


    /**
     * 获得日志的格式化信息, 并将内容追加到StringBuilder
     * @param sb
     * @param e
     */
    void format( StringBuilder sb, LogEvent e );


}
