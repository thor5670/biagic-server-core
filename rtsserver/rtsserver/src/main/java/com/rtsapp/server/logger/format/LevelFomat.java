package com.rtsapp.server.logger.format;

import com.rtsapp.server.logger.spi.LogEvent;

/**
 * Created by admin on 16-2-29.
 */
public class LevelFomat implements  ILogFormat {

    @Override
    public void format(StringBuilder sb, LogEvent e) {
        sb.append( e.getLevel().getName() );
    }

}
