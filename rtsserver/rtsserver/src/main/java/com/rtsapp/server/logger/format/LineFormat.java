package com.rtsapp.server.logger.format;

import com.rtsapp.server.logger.spi.LogEvent;

/**
 * Created by admin on 16-2-29.
 */
public class LineFormat implements ILogFormat{


    @Override
    public void format(StringBuilder sb, LogEvent e) {

        StackTraceElement[] elements = e.getThread().getStackTrace();

        // 从后往前找, 调用Logger的那个代码，即是 当前日志记录的类, 方法名字, 行号
        for (int i = elements.length - 1; i >= 0; i--) {

            if (e.getLogerClassName().equals(elements[i].getClassName())) {
                int caller = i + 1;
                if (caller < elements.length) {
                    sb.append(elements[caller].getClassName());
                    sb.append(".");
                    sb.append(elements[caller].getMethodName());
                    sb.append(":");
                    sb.append(elements[caller].getLineNumber());
                }
                return;
            }
        }

    }

}
