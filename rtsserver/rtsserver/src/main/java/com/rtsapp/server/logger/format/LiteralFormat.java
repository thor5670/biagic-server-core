package com.rtsapp.server.logger.format;

import com.rtsapp.server.logger.spi.LogEvent;

/**
 * 直接追加固定的文字
 */
public class LiteralFormat implements ILogFormat {

    /**
     * String literal.
     */
    private final String literal;

    public LiteralFormat(String literal) {
        this.literal = literal;
    }

    @Override
    public void format(StringBuilder sb, LogEvent e) {
        sb.append( literal );
    }

}
