package com.rtsapp.server.logger.format;

import java.util.ArrayList;
import java.util.List;

/**
 * 日志字符串的格式化
 * 该类是线程安全的
 */
public class LogStringFormat {

    private static final char LEFT_IDENTIFIER = '{';
    private static final char RIGHT_IDENTIFIER ='}';

    /**
     * 原始的带格式的Log字符串
     */
    private final String formatLog;

    /**
     * 占位符的个数
     */
    private final int paramCount;

    /**
     * 分段开始索引
     * 1. 占位符个数为0时， segmentStartIndexes, segmentEndIndexes 为null
     * 2. 占位符个数大于0时, segmentStartIndexes, segmentEndIndexes 的个数为paramCount+1
     */
    private final int segmentStartIndexes[];

    /**
     * 分段结束索引
     */
    private final int segmentEndIndexes[];



    public LogStringFormat( String formatLog ){

        this.formatLog = formatLog;

        int length = formatLog.length();


        List<Integer> segementIndexes = new ArrayList<Integer>();
        int placeholderSize = 0;
        char ch;
        int lIndex = -1;
        int segmentStartIndex = 0;

        //从头到尾逐个查看{}
        for( int i = 0; i < length; i++ ){
            ch  = formatLog.charAt( i );
            if( ch == LEFT_IDENTIFIER ){
                lIndex = i;
            }else if( ch == RIGHT_IDENTIFIER ){
                //如果配对啦, 一个占位符生成
                if( lIndex >= 0 ){
                    placeholderSize++;
                    segementIndexes.add( segmentStartIndex  );
                    segementIndexes.add( lIndex  );
                    segmentStartIndex = i + 1;
                    lIndex = -1;
                }
            }
        }
        segementIndexes.add( segmentStartIndex );
        segementIndexes.add( length  );

        paramCount = placeholderSize;
        if( paramCount == 0 ){
            segmentStartIndexes = null;
            segmentEndIndexes = null;
        }else{
            segmentStartIndexes = new int[ paramCount + 1 ];
            segmentEndIndexes = new int[ paramCount + 1 ];
            for( int i = 0; i < paramCount + 1; i++ ){
                segmentStartIndexes[ i ] = segementIndexes.get( i * 2 );
                segmentEndIndexes[ i ] = segementIndexes.get( i * 2 + 1 );

            }
        }
    }

    /**
     * 参数的个数
     * @return
     */
    public int getParamCount() {
        return paramCount;
    }

    public void format( StringBuilder sb, Object...params ){

        if( paramCount == 0 || params == null ){
            sb.append( formatLog );
            return;
        }

        if( params.length != paramCount  ){
            sb.append( formatLog );
            return;
        }


        //拼接输出的内容
        for( int i = 0; i < paramCount; i++ ){

            //追加占位符前的内容
            if( segmentStartIndexes[i] < segmentEndIndexes[ i ] && segmentEndIndexes[ i ] <= formatLog.length() ) {
                sb.append(formatLog, segmentStartIndexes[i], segmentEndIndexes[i]);
            }

            //追加参数
            sb.append( params[ i ] );
        }

        if( segmentStartIndexes[paramCount] < segmentEndIndexes[ paramCount ] && segmentEndIndexes[ paramCount ] <= formatLog.length() ) {
            sb.append(formatLog, segmentStartIndexes[paramCount], segmentEndIndexes[paramCount]);
        }
    }


}
