package com.rtsapp.server.logger.format;

import com.rtsapp.server.logger.spi.LogEvent;

/**
 * Pattern格式的Layout
 * 实现必须是线程安全的
 */
public class PatternLayout implements ILayout {

    private final ILogFormat[] formats;

    public PatternLayout(ILogFormat[] formats) {
        this.formats = formats;
    }

    public void formatLog( StringBuilder sb, LogEvent event ){

        //时间, 日志级别, 线程名称, 类名, 方法名, 行号, 消息(消息格式,参数, 异常)
        for( int i = 0; i < formats.length; i++ ){
            ILogFormat format = formats[i];
            format.format(sb, event);
        }

    }

}
