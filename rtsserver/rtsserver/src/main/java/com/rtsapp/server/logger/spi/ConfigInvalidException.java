package com.rtsapp.server.logger.spi;

/**
 * 异常
 */
public class ConfigInvalidException extends Exception{
    public ConfigInvalidException(String message) {
        super(message);
    }
}
