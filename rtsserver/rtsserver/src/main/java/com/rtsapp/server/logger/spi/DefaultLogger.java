package com.rtsapp.server.logger.spi;

import com.rtsapp.server.logger.Logger;

/**
 * 默认日志类
 */
public class DefaultLogger implements Logger {

    private static final String LOGGER_CLASS_NAME = DefaultLogger.class.getName();

    private final String name;
    /**
     * 日志类别
     */
    private volatile  LoggerCategory category = null;

    public DefaultLogger(String name) {
        this.name = name;
    }

    public void setCategory(LoggerCategory category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }


    @Override
    public boolean isTraceEnabled() {
        return isEnabled(LogLevel.TRACE);
    }

    @Override
    public void trace(String logFormat) {
        log(LogLevel.TRACE, null, logFormat, null);
    }

    @Override
    public void trace(String logFormat, Object... params) {
        log(LogLevel.TRACE, null, logFormat, params);
    }

    @Override
    public void trace(String logFormat, Throwable ex) {
        log(LogLevel.TRACE, ex, logFormat, null);
    }

    @Override
    public boolean isDebugEnabled() {
        return isEnabled(LogLevel.DEBUG);
    }

    @Override
    public void debug( String logFormat ) {
        log(LogLevel.DEBUG, null, logFormat, null);
    }

    @Override
    public void debug( String logFormat , Object...params ) {
        log(LogLevel.DEBUG, null, logFormat, params);
    }

    @Override
    public void debug( String logFormat, Throwable ex ) {
        log(LogLevel.DEBUG, ex, logFormat, null);
    }

    @Override
    public boolean isInfoEnabled() {
        return isEnabled(LogLevel.INFO);
    }

    @Override
    public void info(String logFormat) {
        log(LogLevel.INFO, null, logFormat, null);
    }

    @Override
    public void info(String logFormat, Object... params) {
        log(LogLevel.INFO, null, logFormat, params);
    }

    @Override
    public void info(String logFormat, Throwable ex) {
        log(LogLevel.INFO, ex, logFormat, null);
    }

    @Override
    public boolean isWarnEnabled() {
        return isEnabled(LogLevel.WARN);
    }

    @Override
    public void warn(String logFormat) {
        log(LogLevel.WARN, null, logFormat, null);
    }

    @Override
    public void warn(String logFormat, Object... params) {
        log(LogLevel.WARN, null, logFormat, params);
    }

    @Override
    public void warn(String logFormat, Throwable ex) {
        log(LogLevel.WARN, ex, logFormat, null);
    }

    @Override
    public boolean isErrorEnabled() {
        return isEnabled(LogLevel.ERROR);
    }

    @Override
    public void error(String logFormat) {
        log(LogLevel.ERROR, null, logFormat, null);
    }

    @Override
    public void error(String logFormat, Object... params) {
        log(LogLevel.ERROR, null, logFormat, params);
    }

    @Override
    public void error(String logFormat, Throwable ex) {
        log(LogLevel.ERROR, ex, logFormat, null);
    }

    @Override
    public void error(Throwable ex, String logFormat ) {
        log( LogLevel.ERROR, ex, logFormat, null );
    }

    @Override
    public void error(Throwable ex, String logFormat, Object... params) {
        log( LogLevel.ERROR, ex, logFormat, params );
    }

    @Override
    public void error(Throwable ex) {
        log(LogLevel.ERROR, ex, "", null);
    }


    private void log(  LogLevel level,  Throwable e, String logFomat, Object...params ){
        LoggerCategory  c = category;
        if( c != null ) {
            c.log( LOGGER_CLASS_NAME, level, e, logFomat, params );
        }
    }

    private boolean isEnabled( LogLevel level ){
        LoggerCategory  c = category;

        if( c == null ){
            return false;
        }

        return c.isEnabled( level );
    }



}
