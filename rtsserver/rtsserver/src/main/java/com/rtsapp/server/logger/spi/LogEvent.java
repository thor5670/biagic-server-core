package com.rtsapp.server.logger.spi;

import com.rtsapp.server.logger.spi.LogLevel;

/**
 * Created by admin on 16-2-26.
 */
public class LogEvent {

    private String logerClassName;
    private Thread thread;
    private LogLevel level;
    private Throwable ex;
    private String logFomat;
    private Object[] params;


    public void set(String logerClassName, Thread thread, LogLevel level, Throwable ex, String logFomat, Object[] params) {
        this.logerClassName = logerClassName;
        this.thread = thread;
        this.level = level;
        this.ex = ex;
        this.logFomat = logFomat;
        this.params = params;
    }

    public String getLogerClassName() {
        return logerClassName;
    }


    public Thread getThread() {
        return thread;
    }

    public LogLevel getLevel() {
        return level;
    }


    public Throwable getEx() {
        return ex;
    }

    public String getLogFomat() {
        return logFomat;
    }


    public Object[] getParams() {
        return params;
    }


}
