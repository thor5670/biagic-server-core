package com.rtsapp.server.logger.spi;

/**
 * 日志等级
 * 不变类, 线程安全
 */
public final class LogLevel {

    private static final int OFF_INTEGER = 100;
    private static final int FATAL_INTEGER = 6;
    private static final int ERROR_INTEGER = 5;
    private static final int WARN_INTEGER = 4;
    private static final int INFO_INTEGER = 3;
    private static final int DEBUG_INTEGER = 2;
    private static final int TRACE_INTEGER = 1;
    private static final int ALL_INTEGER = -1;

    public static final LogLevel OFF = new LogLevel( OFF_INTEGER, "OFF" );
    public static final LogLevel FATAL = new LogLevel( FATAL_INTEGER, "FATAL" );
    public static final LogLevel ERROR = new LogLevel( ERROR_INTEGER, "ERROR" );
    public static final LogLevel WARN = new LogLevel( WARN_INTEGER, "WARN" );
    public static final LogLevel INFO = new LogLevel( INFO_INTEGER, "INFO" );
    public static final LogLevel DEBUG = new LogLevel( DEBUG_INTEGER, "DEBUG" );
    public static final LogLevel TRACE = new LogLevel( TRACE_INTEGER, "TRACE" );
    public static final LogLevel ALL = new LogLevel( ALL_INTEGER, "ALL" );

    private final int level;
    private final String name;

    private LogLevel(int level, String name) {
        this.level = level;
        this.name = name;
    }

    /**
     * 日志级别大于或等于另外一个日志级别
     * @param r
     * @return
     */
    public boolean isGreaterOrEqual( LogLevel r ){
        return this.level >= r.level;
    }

    public boolean isLessThan( LogLevel r){
        return this.level < r.level;
    }


    /**
     * 日志名字
     * @return
     */
    public String getName() {
        return name;
    }


    public static LogLevel toLevel( String sArg ) {
        String s = sArg.toUpperCase();

        if (s.equals("ALL")) return LogLevel.ALL;
        if (s.equals("DEBUG")) return LogLevel.DEBUG;
        if (s.equals("INFO")) return LogLevel.INFO;
        if (s.equals("WARN")) return LogLevel.WARN;
        if (s.equals("ERROR")) return LogLevel.ERROR;
        if (s.equals("FATAL")) return LogLevel.FATAL;
        if (s.equals("OFF")) return LogLevel.OFF;
        if (s.equals("TRACE")) return LogLevel.TRACE;

        return null;
    }

    
}
