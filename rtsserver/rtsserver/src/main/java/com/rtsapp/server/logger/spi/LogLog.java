package com.rtsapp.server.logger.spi;

/**
 * 用于在日志系统中输出日志
 */
public class LogLog {

    public static void debug( String msg ){
        System.out.println( msg );
        System.out.flush();
    }


    public static void error( String msg ){
        System.err.println( msg );
        System.err.flush();
    }

    public static void error( String msg, Throwable ex ){
        System.err.println( msg );
        ex.printStackTrace();
        System.err.flush();
    }

}
