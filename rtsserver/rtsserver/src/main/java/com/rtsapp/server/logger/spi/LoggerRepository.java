package com.rtsapp.server.logger.spi;

import com.rtsapp.server.logger.async.AsyncLoggerThread;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * LoggerCategory的仓库
 * 这里存着所有的已经创建的LoggerCategory
 */
public class LoggerRepository {


    private final CountDownLatch countDownLatch;
    private final AsyncLoggerThread[] threads;

    private final LoggerCategory rootCategory;

    private final Map<String,LoggerCategory> allLogs ;


    public LoggerRepository(  CountDownLatch countDownLatch, AsyncLoggerThread[] threads, LoggerCategory rootCategory, Map<String, LoggerCategory> allLogs) {
        this.countDownLatch = countDownLatch;
        this.threads = threads;
        this.rootCategory = rootCategory;
        this.allLogs = allLogs;
    }

    /**
     * 启动日志线程
     */
    public void startup( ){

        if( threads != null && threads.length > 0 ) {
            for (int i = 0; i < threads.length; i++) {
                Thread t = new Thread(threads[i], "aysnclogger-" + i);
                t.start();
                LogLog.debug( "日志线程:" +  t.getName() + "启动" );
            }
        }
    }

    /**
     * 关闭所有的线程
     * 关闭所有的日志流
     */
    public void shutdown( ){

        rootCategory.stopLog();
        for( LoggerCategory category : allLogs.values() ){
            category.stopLog();
        }


        if( threads != null && threads.length > 0 ){
            for( AsyncLoggerThread thread : threads ){
                thread.shutdown( );
            }

            try {
                countDownLatch.await( );
            } catch (InterruptedException e) {
               LogLog.error(  "shutdwon等待countDownLatch完成时被中断", e );
            }
        }


        rootCategory.shutdown();
        for( LoggerCategory category : allLogs.values() ){
            category.shutdown();
        }


    }

    /**
     * 获得一个名字对应的logger
     * 名字采用继承策略, 查找过程如下
     * 1. 如果名字直接匹配一个Logger,返回
     * 2. 否则取得名字的上一级名字
     *      1. 如果没有上一级啦, 返回rootLogger
     *      2. 如果直接匹配, 返回该Logger
     *          * 否则重复这个过程
     *
     * getCategory( "com.rtsapp.server.logger.spi.LoggerRepository" )的查找过程
     * com.rtsapp.server.logger.spi.LoggerRepository 是否直接匹配?
     * com.rtsapp.server.logger.spi 是否直接匹配?
     * com.rtsapp.server.logger 是否直接匹配?
     * com.rtsapp.server 是否直接匹配?
     * com.rtsapp 是否直接匹配?
     * com 是否直接匹配?
     * return rootLogger
     *
     * @param loggerName
     * @return
     */
    public LoggerCategory getCategory(String loggerName) {
        return hierarchySearch( loggerName );
    }


    /**
     * 递归查找
     * @param name
     * @return
     */
    private LoggerCategory hierarchySearch( String name ){
        // 默认 rootCategory
        if( name == null ){
            return rootCategory;
        }

        // 直接匹配
        LoggerCategory category =  allLogs.get(name);
        if( category != null ){
            return category;
        }

        // 跳入上一级
        return hierarchySearch( getParentName( name ) );
    }


    private String getParentName( String name ){

        if( name == null || name.length() < 1 ){
            return null;
        }

        int index =  name.lastIndexOf( '.' );
        if( index >= 0 ){
            return name.substring( 0, index );
        }else{
            return null;
        }
    }


}
