package com.rtsapp.server.module;

public abstract class AbstractModule implements Module {

	private String name;
	
	 /**
	 * 设置模块名称
	 * @param moduleName
	 */
	public void setName(String moduleName){
		this.name = moduleName;
	}
	
	/**
	 * 模块注册的名称
	 * @return
	 */
	public String getName( ){
		return this.name;
	}

}
