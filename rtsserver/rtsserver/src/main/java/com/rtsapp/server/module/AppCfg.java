package com.rtsapp.server.module;

import java.util.List;

public class AppCfg {

	private List<String> moduleNames;

	
	
	public List<String> getModuleNames() {
		return moduleNames;
	}

	public void setModuleNames(List<String> moduleNames) {
		this.moduleNames = moduleNames;
	}
	
	
}
