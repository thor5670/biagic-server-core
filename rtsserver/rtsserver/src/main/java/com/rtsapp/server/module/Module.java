package com.rtsapp.server.module;

/**
 * 模块
 * 约定模块生命周期方法
 * @author admin
 */
public interface Module {
	
	/**
	 * 设置模块名称
	 * @param moduleName
	 */
	void setName(String moduleName);
	
	/**
	 * 模块注册的名称
	 * @return
	 */
	String getName( );
	
	/**
	 * 使用父模块作为容器初始化一个模块
	 * @param parentModule
	 * @return 是否初始化成功
	 */
	boolean initialize( Module parentModule );
	
	/**
	 * 启动一个模块
	 */
	boolean start( );
	
	/**
	 * 停止一个模块
	 */
	void stop( );
	
	/**
	 * 销毁一个模块
	 */
	void destroy( );

	
	
}
