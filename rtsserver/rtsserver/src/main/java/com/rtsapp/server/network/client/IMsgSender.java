package com.rtsapp.server.network.client;

/**
 * 消息发送接口
 */
public interface IMsgSender {

    /**
     * 发送消息
     * @param msg
     */
    void send( Object msg );

}
