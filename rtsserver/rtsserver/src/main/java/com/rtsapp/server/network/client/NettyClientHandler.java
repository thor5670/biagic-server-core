package com.rtsapp.server.network.client;

import com.rtsapp.server.logger.Logger;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * Created by admin on 16-7-19.
 */
public class NettyClientHandler extends ChannelInboundHandlerAdapter {


    private final ChannelInboundHandler innerHandler;
    private final NettyClient client;

    public NettyClientHandler(   NettyClient client, ChannelInboundHandler innerHandler ){
        this.client = client;
        this.innerHandler = innerHandler;
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        innerHandler.channelRead( ctx, msg );
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);

        innerHandler.channelActive(ctx);
    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);

        innerHandler.channelInactive(ctx);

        client.closeAndReconnect( this );
    }


    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {

        innerHandler.exceptionCaught( ctx, cause );

    }


}
