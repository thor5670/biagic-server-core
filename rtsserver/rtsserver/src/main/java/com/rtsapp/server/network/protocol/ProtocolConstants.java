package com.rtsapp.server.network.protocol;

public interface ProtocolConstants {

	interface ServerConstants{

		/**
		 * 内置的SessionKey
		 */
		interface SessionKeys {
			/**
			 * 是否验证客户端
			 */
			String isVerifyClient = "SessionKeys.isVerifyClient";

			/**
			 * 是否加密通信数据
			 */
			String isCryptData = "SessionKeys.isCrypt";

			/**
			 * 服务器加密key
			 */
			String serverCryptKey = "SessionKeys.serverCryptKey";

			/**
			 * 客户端加密key
			 */
			String clientCryptKey = "SessionKeys.clientCryptKey";
		}

	}
	

	interface RPCConstants {

		int headerLen = 5;

		interface RPCStatus {
			byte ok = 0;
			byte exception = 1;
			byte unknownError = 2;
		}

		interface RPCType {
			byte normal = 0;
			byte oneway = 1;
			byte async = 2;
		}
		
		
		interface MessageType {
			byte request = 0;
			byte response = 1;
		}

		
		interface RPCSerializer {
			byte kryo = 0;
			byte json = 1;
			byte msgpack = 2;
			byte bson = 3;
		}
		
		
	}
	
	

}
