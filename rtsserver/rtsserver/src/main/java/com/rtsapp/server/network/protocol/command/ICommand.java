package com.rtsapp.server.network.protocol.command;

import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.common.IByteBufferSerializable;


public interface ICommand extends IByteBufferSerializable{
	
	int getCommandId( );

}
