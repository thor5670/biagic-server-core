package com.rtsapp.server.network.protocol.command;

/**
 * 命令工厂
 */
public interface ICommandFactory {

    ICommand createRequest( int commandId );

    ICommand createResponse( int commandId );

}
