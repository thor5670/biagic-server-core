package com.rtsapp.server.network.protocol.command;

import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.network.session.Session;


/**
 * 命令处理类，此类必须无状态
 * @author dengyingtuo
 */
public interface  ICommandHandler {
	
	void execute( int commandId, Session session, IByteBuffer buffer );
	
	
}
