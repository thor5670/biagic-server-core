package com.rtsapp.server.network.protocol.command;

/**
 * Created by admin on 15-11-25.
 */
public interface ICommandHandlerRegister {

    void registerCommandHandler(Integer moduleId, ICommandHandler processor);

    void unregisterCommandHandler( Integer moduleId );

}
