package com.rtsapp.server.network.protocol.crypto;

import io.netty.channel.socket.SocketChannel;

/**
 * 应用程序原始的通道初始化
 */
public interface IAppChannelInitializer {
    void initChannel(SocketChannel ch) throws Exception;
}