package com.rtsapp.server.network.protocol.crypto.cmd;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

/**
 * Created by admin on 16-6-17.
 */
public interface IRSAHandshakeStartCMD {

    int getCommandId();

    void readFromBuffer(ByteBuf buffer);

    void writeToBuffer(ByteBuf buffer);


    /**
     * 将一个协议输出到ByteBuf
     *    写入 长度 + cmd序列化内容
     * @param cmd
     * @return
     */
    static ByteBuf outputByteBuf(IRSAHandshakeStartCMD cmd) {

        ByteBuf buf = Unpooled.buffer();

        int startIdx = buf.writerIndex();
        buf.writeInt(0);
        cmd.writeToBuffer(buf);
        int endIdx = buf.writerIndex();

        buf.setInt(startIdx, endIdx - startIdx - 4);

        return buf;
    }
}
