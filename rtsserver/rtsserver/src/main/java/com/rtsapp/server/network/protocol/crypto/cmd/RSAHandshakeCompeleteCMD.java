package com.rtsapp.server.network.protocol.crypto.cmd;

import io.netty.buffer.ByteBuf;

/**
 * Created by admin on 16-6-17.
 */
public class RSAHandshakeCompeleteCMD implements IRSAHandshakeStartCMD {

    public boolean crypt;

    public boolean isCrypt() {
        return crypt;
    }

    public void setCrypt(boolean crypt) {
        this.crypt = crypt;
    }

    public int getCommandId() {
        return RSAHandshakeCMDType.COMPLETE;
    }

    public void readFromBuffer(ByteBuf buffer) {
        buffer.readInt();
        crypt = buffer.readBoolean();
    }

    public void writeToBuffer(ByteBuf buffer) {
        buffer.writeInt(this.getCommandId());
        buffer.writeBoolean(crypt);
    }

}
