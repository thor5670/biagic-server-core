package com.rtsapp.server.network.protocol.crypto.cmd;

import io.netty.buffer.ByteBuf;

/**
 * 服务器发送给客户端时:
 * * 两个秘钥都是用服务器私钥进行加密
 */
public class RSAHandshakeStartCMD implements IRSAHandshakeStartCMD {

    // rc4 key, 64位
    // S-C： 服务器私钥加密
    // C ： 服务器公钥解密
    // C-S ： 客户端私钥加密
    // S : 客户端公钥解密
    // 对比
    private byte[] serverCryptKey;
    // rc4 key, 64位
    private byte[] clientCryptKey;


    public byte[] getServerCryptKey() {
        return serverCryptKey;
    }

    public void setServerCryptKey(byte[] serverCryptKey) {
        this.serverCryptKey = serverCryptKey;
    }

    public byte[] getClientCryptKey() {
        return clientCryptKey;
    }

    public void setClientCryptKey(byte[] clientCryptKey) {
        this.clientCryptKey = clientCryptKey;
    }


    public int getCommandId() {
        return RSAHandshakeCMDType.START;
    }


    public void readFromBuffer(ByteBuf buffer) {
        buffer.readInt();

        serverCryptKey = new byte[buffer.readShort()];
        buffer.readBytes(serverCryptKey, 0, serverCryptKey.length);

        clientCryptKey = new byte[buffer.readShort()];
        buffer.readBytes(clientCryptKey, 0, clientCryptKey.length);
    }

    public void writeToBuffer(ByteBuf buffer) {
        buffer.writeInt(this.getCommandId());

        buffer.writeShort(serverCryptKey.length);
        buffer.writeBytes(serverCryptKey);

        buffer.writeShort(clientCryptKey.length);
        buffer.writeBytes(clientCryptKey);

    }
}
