
#服务器安全连接与加密流程


##分析

###证书交换过程
 * 证书交换时: 公钥加密, 私钥解密
 *      * 发送时: 服务器使用客户端公钥加密，客户端使用客户端私钥进行解密
 *      * 接收时: 客户端使用服务器公钥加密, 服务器使用服务器私钥进行解密
 *
 *      * 用于验证双方持有对方公钥


###连接状态
1. 建立连接
    * LengthFrameDecoder
    * RSAHandshakeHandler
        *

2. 连接成功后:
    * 应用程序加入自己的Handler
        * LengthFrameDecoder
        * AbstractCommandInHandler
            * 处理逻辑
        * CommandOutHandler

    * 在最前面加入RC4流式加解密
        * RC4InHandler
        * RC4OutHandler


###客户端连接
1. 一个是压力测试客户端
2. 一个是模拟命令客户端

##设计

###核心组件
1. 加密算法
    * RSA
    * RC4
2. 流式加密输入, 输出Handler
    * RC4InHandler
    * RC4OutHandler
3. 连接认证协议
    * RSAHandshakeStart
    * RSAHandshakeStartAck
    * RSAHandshakeComplete
4. 连接秘钥配置
    * RSAHandshakeKey
    * 可以写死, 也可以配置文件配置
5. 连接握手管理
    * RSAHandshake: 握手状态管理
        * 状态零: 加密初始化
            * 初始化加密握手Handlers
        * 状态一: 发送初始化协议
        * 状态二: 接收ACK
        * 状态三: 发送接连成功
        * 状态四: 进入机密通信阶段
            * 初始化通信Handlers

        * 异常终止连接
            *
    * RSAHandshakeHandler: NettyHandler, 通道激活，通道关闭, 通道有消息时回调RSAHandshake
    * IAppChannelInitializer : 应用程序自己的ChannelInitializer

6. 客户端握手管理
    * ClientRSAHandshake
    * ClientRSAHandshakeHandler

###核心流程
1. 自定义的ChannelInitializer 接收到连接
    * 创建RSAHandshake
    * 调用RSAHandshake进入阶段0: 初始化RSAHandshakeHandler

2. RSAHandshakeHandler监听事件
    * 激活事件:
        1. 调用RSAHandshake, 连接激活
        2. RSAHandshake 判断是否加密
            * 机密: 发送握手初始化协议
            * 不加密: 发送握手结束协议

    * 接收消息事件:
        1. 调用RSAHandshake, 接收到消息
        2. 如果处于等待ACK，读取ACK，进行ACK验证
            * 验证通过, 进入阶段3
            * 验证失败, 终止连接

3. RSAHandshake
    1.  进入握手结束阶段
        * 发送结束协议
        * 协议刷新输出后




1. 使用RSAHandshakeInitializer, 进入到连接状态
    *

2. RSAHandshakeHandler:( RSA )
    1. 发送加密初始化协议到channel
    2. 读取返回的结果, 进行解密
    3. 发送加密完成的协议到channel
    4. 发送完毕后
        * 初始化RC4InHandler,RC4OutHandler
        * 连接成功后的状态

    * 任何异常关闭连接

    * 如果不加密直接从第3步开始，发送完毕，加入

