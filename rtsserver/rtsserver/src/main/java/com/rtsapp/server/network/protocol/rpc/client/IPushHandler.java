package com.rtsapp.server.network.protocol.rpc.client;

/**
 * RPC接收服务器的推送接口
 *
 *  PushHandler应该是不带状态的，如果它带状态，需要考虑多线程情况下的推送回调
 */
public interface IPushHandler {


    /**
     * 处理服务器发送过来的推送消息
     * @param message 推送消息
     * @return 处理的结果, 任何处理都必须有结果
     */
    Object processPush( Object message );


}
