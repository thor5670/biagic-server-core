package com.rtsapp.server.network.protocol.rpc.client;

/**
 * RPC客户端操作接口类
 */
public interface IRPCClientProxy {



    /**
     *  设置推送回调接口
     *
     *  这个方法只能在初始化的时候调用一次, 多次调用不生效,clientId,pushHandler都不能为空
     *
     *  请理解一个客户端代理IRPCClientProxy和一个RPCServer的关系，然后再设置ID
     *      1. ID主要用来区分IRPCClientProxy, 相同ID的IRPCClientProxy, 会放在一个列表中
     *      2. 一个RPCServer可提供多个服务, 一个客户端程序可使用多个客户端代理IRPCClientProxy连接同一RPCServer, 示意图如下
     *          RPCServer
     *              service1
     *              service2
     *              service3
     *
     *          RPCClient1
     *              IRPCClientProxy1<-->service1 :   注册ID
     *              IRPCClientProxy2<-->service2 :   注册ID
     *              IRPCClientProxy3<-->service3 :   注册ID
     *
     *          RPCClient2
     *              IRPCClientProxy1<-->service1:   注册ID
     *              IRPCClientProxy2<-->service2:   注册ID
     *              IRPCClientProxy3<-->service3:   注册ID
     *
     *     3. 推荐ID规则为  业务ID + 服务器ID
     *
     *  @param clientId 客户端身份ID: 推荐ID规则为  业务ID + 服务器ID
     *  @param pushHandler 接收推送消息的Handler
     *  @throws IllegalArgumentException
     */
    void setPushHandler( String clientId, IPushHandler pushHandler );


    /**
     * RPC远程调用
     *  如果最后一个是RPCCallback, 则是异步调用
     * @param service
     * @param funName
     * @param args
     * @return
     */
     Object call(  String service, String funName, Object...args );


}
