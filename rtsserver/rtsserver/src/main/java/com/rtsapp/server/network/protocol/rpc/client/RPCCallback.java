package com.rtsapp.server.network.protocol.rpc.client;

public interface RPCCallback {
	
	void onCompleted( Object  result );
	
	void onError( Exception exception );

}
