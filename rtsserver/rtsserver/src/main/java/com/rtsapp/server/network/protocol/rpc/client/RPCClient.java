package com.rtsapp.server.network.protocol.rpc.client;

import com.rtsapp.server.network.protocol.rpc.client.impl.RPCClientImpl;
import com.rtsapp.server.network.protocol.rpc.client.impl.RPCClientProxy;

public interface RPCClient {

	/**
	 * RPC调用
	 * 异步的调用方式:
	 * 		最后一个参数传入RPCCallback, RPC调用完成后，回调RPCCallback执行
	 * 		这种情况下call的返回值没有意义，应该忽略，而以回调为准
	 * 同步调用方式:
	 * 		只传入正常的参数, 最后一个参数不传入回调RPCCallback,
	 * 		RPC回调会等待调用完成，然后返回
	 *
	 * 		* 同步超时时间为30秒，30秒后将抛出异常
	 * 		* 同步调用应该捕获Throwable, 可能是超时或其他调用错误
	 *
	 * 注意
	 * 		1. 不应该是用RPCCallback作为RPC服务的参数，以免和正常的RPC回调冲突
	 *
	 * @param server 服务器名称
	 * @param service 服务名称
	 * @param funName 函数名称
	 * @param args 参数,
	 * @return
	 */
	public abstract Object call( String server,  String service, String funName, Object...args );

	public RPCClientProxy getRPCClientProxy( String serverName );

}
