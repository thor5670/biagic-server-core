package com.rtsapp.server.network.protocol.rpc.client.impl;


import com.rtsapp.server.network.protocol.rpc.codec.ObjectOutHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOutboundHandler;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;


public class RPCClientInitializer extends ChannelInitializer<SocketChannel> {

	//将RPC客户端的大小默认调整到20480
	private int maxFrameLength = 20480;
    private int lengthFieldOffser = 0 ;
    private int lengthFieldLength = 4;


	private RPCClientProxy rpcProxy;

	public RPCClientInitializer(RPCClientProxy rpcProxy, int maxFrameLength) {
		this.rpcProxy = rpcProxy;
		this.maxFrameLength = maxFrameLength;
	}

	private ChannelOutboundHandler outHandler = new ObjectOutHandler();
    
    
	@Override
	protected void initChannel(SocketChannel ch) throws Exception {

		ch.pipeline().addLast( new LengthFieldBasedFrameDecoder( this.maxFrameLength, this.lengthFieldOffser, this.lengthFieldLength ) );
		
		ch.pipeline().addLast( new RPCClientInHandler( this.rpcProxy ) );
		ch.pipeline().addLast( outHandler );

	}
	
	
}
