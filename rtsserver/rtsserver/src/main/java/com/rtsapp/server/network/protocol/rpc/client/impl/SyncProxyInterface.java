package com.rtsapp.server.network.protocol.rpc.client.impl;

/**
 * Created by admin on 15-10-17.
 */
public interface SyncProxyInterface {

    void setSyncRPCProxy(RPCClientProxy proxy);

    RPCClientProxy getSyncRPCProxy();


    public String getServiceName();

    public void setServiceName(String serviceName);


    public static class SyncProxy implements SyncProxyInterface{

        private RPCClientProxy syncRPCProxy;

        private String serviceName;

        @Override
        public RPCClientProxy getSyncRPCProxy() {
            return syncRPCProxy;
        }

        @Override
        public void setSyncRPCProxy(RPCClientProxy syncRPCProxy) {
            this.syncRPCProxy = syncRPCProxy;
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }
    }
}
