//package com.rtsapp.server.network.protocol.rpc.client.impl;
//
//import io.netty.bootstrap.Bootstrap;
//import io.netty.channel.Channel;
//import io.netty.channel.ChannelFuture;
//import io.netty.channel.ChannelFutureListener;
//import io.netty.channel.EventLoopGroup;
//import io.netty.channel.epoll.EpollSocketChannel;
//import io.netty.channel.socket.nio.NioSocketChannel;
//
//import java.net.InetSocketAddress;
//
///**
// * Created by admin on 15-10-17.
// * TODO 还有严重bug, 不能使用
// */
//public class SyncRPCProxy extends RPCClientProxy{
//
//
//
//    private Channel channel;
//
//    SyncRPCProxy( RPCClientImpl.ClientCfg cfg, EventLoopGroup eventLoop, boolean linux) {
//        super(cfg, eventLoop, linux);
//    }
//
//
//    boolean createSyncConnect( ){
//
//        try {
//            initClientInitHandler();
//
//            Bootstrap b = new Bootstrap();
//
//            if (linux) {
//                b.group(eventLoop)
//                        .channel(EpollSocketChannel.class)
//                        .handler(clientInitialHandler);
//            } else {
//                b.group(eventLoop)
//                        .channel(NioSocketChannel.class)
//                        .handler(clientInitialHandler);
//            }
//
//            ChannelFuture channelFuture = b.connect(remotePeer);
//            channelFuture.addListener(new ChannelFutureListener() {
//
//                @Override
//                public void operationComplete(final ChannelFuture channelFuture) throws Exception {
//                    logger.debug( "operationComplete()" );
//                    if (!channelFuture.isSuccess()) {
//                        logger.info("Can't connect to remote server. serverName=" + cfg.getServerName() + "|remote peer=" + remotePeer.toString());
//                    } else {
//                        logger.info("Successfully connect to remote server. objName=" + cfg.getServerName() + "|remote peer=" + remotePeer);
//                        RPCClientInHandler handler = channelFuture.channel().pipeline().get(RPCClientInHandler.class);
//                        addHandler(handler);
//                    }
//                }
//
//            });
//
//
//            channelFuture.await();
//            logger.debug( "channelFuture.await()" );
//
//            this.channel = channelFuture.channel();
//
//            boolean isActive = channelFuture.channel().isActive();
//            return isActive;
//
//        }catch( Exception ex ){
//            logger.error( "createSyncConnect error : ip:"+ cfg.getIp() + ", port:" + cfg.getPort(), ex );
//            return false;
//        }
//    }
//
//    void closeAndRelease(){
//
//        if( this.channel != null && this.channel.isActive() ){
//
//            ChannelFuture closeFuture  = this.channel.close();
//
//            closeFuture.addListener(new ChannelFutureListener() {
//
//                @Override
//                public void operationComplete(final ChannelFuture channelFuture) throws Exception {
//                    if (!channelFuture.isSuccess()) {
//                        logger.info("cannot close channel " + channel );
//                    } else {
//                        logger.info("Successfully close channel" +  channel );
//                    }
//                }
//            });
//
//            this.channel = null;
//        }
//    }
//
//
//}
