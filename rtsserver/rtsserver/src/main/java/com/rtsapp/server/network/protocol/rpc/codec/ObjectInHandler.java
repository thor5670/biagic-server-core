package com.rtsapp.server.network.protocol.rpc.codec;

import com.rtsapp.server.network.protocol.ProtocolInHandler;
import com.rtsapp.server.logger.Logger;

import com.rtsapp.server.network.session.Session;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelHandler.Sharable;


/**
 *  @Sharable 用来表示该Handler可以加入到多个通道中
 */
@Sharable
public class ObjectInHandler   extends ProtocolInHandler {

	private static final Logger LOGGER =com.rtsapp.server.logger.LoggerFactory.getLogger( ObjectInHandler.class );
	
	
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {


		if( msg instanceof  ByteBuf ) {

			ByteBuf buf = (ByteBuf) msg;

			try {
				LOGGER.info("开始处理RPC数据包, 长度:" + buf.readInt());

				Object message = KryoSerializer.read(buf);

				Session session = sessionManager.getSession(ctx.channel());

				if (session != null) {

					processMessage(session, message);

				} else {
					LOGGER.error("sessionManager.getOrAddSession is null ");
				}


			} catch (Throwable ex) {

				LOGGER.error("RPC消息处理异常", ex);
			}finally {
				buf.release();
			}

		}else{
			LOGGER.error("RCP处理错误, msg 不是 ByteBuf" );
		}
    	
    }

    
    public void processMessage( Session session, Object message ){
    	
    	
    }
    
}
