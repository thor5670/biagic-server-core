package com.rtsapp.server.network.protocol.rpc.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.handler.codec.MessageToByteEncoder;
import com.rtsapp.server.logger.Logger;


@Sharable
public class ObjectOutHandler extends MessageToByteEncoder<Object> {

	private static Logger logger =com.rtsapp.server.logger.LoggerFactory.getLogger(ObjectOutHandler.class);

	@Override
	protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out)
			throws Exception {

		int startIdx = out.writerIndex();
		
		out.writeInt( 0 );

		KryoSerializer.write( msg, out );
		
		int endIdx = out.writerIndex();
		out.setInt(startIdx, endIdx - startIdx - 4);


	}
	
}
