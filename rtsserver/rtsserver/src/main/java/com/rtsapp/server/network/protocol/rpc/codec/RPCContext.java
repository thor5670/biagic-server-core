package com.rtsapp.server.network.protocol.rpc.codec;

import java.util.HashMap;

/**
 * Rpc一次请求上下文
 */
public class RPCContext {
	
	private RPCRequest request;
	private RPCResponse response;
	private HashMap<String,Object> attributes = new HashMap<String,Object>();

	public RPCRequest getRequest() {
		return request;
	}
	public void setRequest(RPCRequest request) {
		this.request = request;
	}
	public RPCResponse getResponse() {
		return response;
	}
	public void setResponse(RPCResponse response) {
		this.response = response;
	}
	
	public void setAttribute(String key,Object value){
		attributes.put(key, value);
	}
	public Object getAttribute(String key){
		return attributes.get(key);
	}
}
