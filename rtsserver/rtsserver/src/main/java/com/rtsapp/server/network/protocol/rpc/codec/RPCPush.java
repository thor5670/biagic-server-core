package com.rtsapp.server.network.protocol.rpc.codec;

/**
 * 服务器发给客户端的推送
 */
public class RPCPush {

    //序列号
    private long sequenceNo;

    //推送的消息对象
    private Object message;


    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public long getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(long sequenceNo) {
        this.sequenceNo = sequenceNo;
    }
}
