package com.rtsapp.server.network.protocol.rpc.codec;

/**
 * 服务器发给客户端的推送
 */
public class RPCPushResponse {

    //序列号
    private long sequenceNo;

    //如果错误码是0，代表推送的返回结果, 如果是1，代表异常对象, 2,应该为null
    private Object result;

    //错误码
    private byte errorNo;//0 ok , 1 error, 2 unknown error

    public long getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(long sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public byte getErrorNo() {
        return errorNo;
    }

    public void setErrorNo(byte errorNo) {
        this.errorNo = errorNo;
    }
}
