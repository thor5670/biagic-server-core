package com.rtsapp.server.network.protocol.rpc.codec;

/**
 * RPC客户端向服务器注册自己的身份
 */
public class RPCRegister {

    private String clientId;

    public void setClientId( String clientId){
        this.clientId = clientId;
    }

    public String getClientId(){
        return this.clientId;
    }


}
