package com.rtsapp.server.network.protocol.rpc.codec;


/**
 * Rpc请求
 */
public class RPCRequest {
	
	//序列号
	private long sequenceNo;
	//rpc类型
	private byte rpcType;  //0 normal, 1oneway ,2 async
	
	//对象名
	private String objectName;
	//方法名
	private String methodName;
	//参数数组
	private Object[] parameters;

	public RPCRequest(){

	}

	public long getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(long sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public byte getRpcType() {
		return rpcType;
	}
	public void setRpcType(byte rpcType) {
		this.rpcType = rpcType;
	}
	public String getObjectName() {
		return objectName;
	}
	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public Object[] getParameters() {
		return parameters;
	}
	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}

	
}
