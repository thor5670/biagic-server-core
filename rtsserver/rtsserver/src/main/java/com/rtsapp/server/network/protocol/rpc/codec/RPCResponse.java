package com.rtsapp.server.network.protocol.rpc.codec;

/**
 * RPC响应
 */
public class RPCResponse {
	
	//rpc序列号
	private long sequenceNo;
	//rpc类型
	private byte rpcType; 

	//返回结果
	//在错误码不为0时,result是发生的异常对象
	private Object result;

	//错误码
	private byte errorNo;//0 ok , 1 error, 2 unknown error
	
	public long getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(long sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public byte getRpcType() {
		return rpcType;
	}
	public void setRpcType(byte rpcType) {
		this.rpcType = rpcType;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	public byte getErrorNo() {
		return errorNo;
	}
	public void setErrorNo(byte errorNo) {
		this.errorNo = errorNo;
	}
	
	
	
	
	
}
