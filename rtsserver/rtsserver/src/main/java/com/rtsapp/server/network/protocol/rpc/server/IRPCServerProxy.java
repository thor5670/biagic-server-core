package com.rtsapp.server.network.protocol.rpc.server;

import com.rtsapp.server.network.protocol.rpc.client.RPCCallback;

import java.util.List;

/**
 * RPC客户端管理的接口
 */
public interface IRPCServerProxy {


    /**
     * 获得当前客户端ID
     *     该方法只应该在RPC服务器接口中被使用, 代表执行当前RPC接口来源于哪个客户端的请求
     * @return
     */
    String getCurrentClientId( );



    /**
     * 获得所有的客户端ID
     * @return
     */
    List<String> getAllClientIds( );



    /**
     * 给客户端发一次推送, 并接收回调
     *   回调可能有多种异常, 必须进行处理
     *   1. 连接已关闭
     *   2. 客户端处理异常
     * @param clientId
     * @param message 推送的消息对象
     * @param callback
     * @return true表示找到了客户端并发送了出去，false表示没有找到客户端,没有发送出去
     */
    boolean sendPush(String clientId, Object message, RPCCallback callback );


    //这个方法暂时觉得没必要提供
//    /**
//     * 给客户端发一次推送, 而不管 任何回调,
//     * @param clientId
//     * @param pushObj
//     */
//    void push( String clientId, Object pushObj );


}
