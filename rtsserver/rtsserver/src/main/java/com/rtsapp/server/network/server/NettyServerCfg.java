package com.rtsapp.server.network.server;

public class NettyServerCfg {


	private boolean linux;
	private int port; //端口
	private int bossThread; //连接的事件循环的数量	
	private int workerThread; //IO工作的事件循环数量
	private int ioRatio; //工作线程的IO比
	private int sessionLimit; //session的连接上限
	
	
	public boolean isLinux() {
		return linux;
	}
	public void setLinux(boolean linux) {
		this.linux = linux;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getBossThread() {
		return bossThread;
	}
	public void setBossThread(int bossThread) {
		this.bossThread = bossThread;
	}
	public int getWorkerThread() {
		return workerThread;
	}
	public void setWorkerThread(int workerThread) {
		this.workerThread = workerThread;
	}
	public int getIoRatio() {
		return ioRatio;
	}
	public void setIoRatio(int ioRatio) {
		this.ioRatio = ioRatio;
	}
	public int getSessionLimit() {
		return sessionLimit;
	}
	public void setSessionLimit(int sessionLimit) {
		this.sessionLimit = sessionLimit;
	}
	
	
	
	
	
}
