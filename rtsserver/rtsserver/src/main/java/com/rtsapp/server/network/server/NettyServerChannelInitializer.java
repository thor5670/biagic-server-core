package com.rtsapp.server.network.server;

import com.rtsapp.server.module.Application;
import com.rtsapp.server.network.session.netty.NettySessionManager;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;



public abstract class NettyServerChannelInitializer extends ChannelInitializer<SocketChannel> {

	
	public abstract boolean initialize( Application application, NettySessionManager sessionManager );
	
	
}
