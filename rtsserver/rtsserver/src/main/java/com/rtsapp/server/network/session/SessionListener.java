package com.rtsapp.server.network.session;

public interface SessionListener {

	void onSessionAdded( Session session,  SessionManager sessionManager );
	
	void onSessionRemoved( Session session,  SessionManager sessionManager );

}
