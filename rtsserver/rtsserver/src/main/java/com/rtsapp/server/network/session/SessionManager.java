package com.rtsapp.server.network.session;

import java.util.List;

/**
 * Session的管理类，该类必须是线程安全的
 * 
 * session的加入，断开， 加入的时候包括白名单, 黑名单, 人员上限等限制
 * session的消息推送, 广播等, 后端服务器往前端服务器推送消息等
 * session的查询, 获取对应的Session
 * @author admin
 *
 * 
 */
public interface SessionManager {
	
	/**
	 * 获得Session的总数
	 * @return
	 */
	int getSessionCount( );
	
	
	/**
	 * 能否接受该连接 
	 * 	 对session数量进行限制
	 *   内部服务器使用白名单策略
	 *   对外服务器使用黑名单策略
	 * @param ip 远程ip
	 * @param port 远程端口
	 * @return
	 */
	boolean canAccept( String ip, int port );
	
	
	/**
	 * 按SessionId获取Session
	 * @param sessionId
	 * @return
	 */
	Session getSession( long sessionId );
	
	/**
	 * 
	 * @param remoteId
	 * @return
	 */
	Session getSessionByRemoteId( long remoteId );
	
	
	/**
	 * 获得所有的Session
	 * @return
	 */
	List<Session> getAllSessions();
	
	
	/**
	 * 绑定Session的远端身份, 有时候有单一身份限制， 有时候同一身份的Session数量有限制
	 * @param session
	 * @param remoteId
	 */
	void bindSessionRemoteId( Session session,  long remoteId );
	
	
	/**
	 * 进行消息广播
	 * @param remoteId
	 * @param message
	 */
	void broadcast( long remoteId, Object message );
	
	
	/**
	 * 推送消息
	 * @param sessionId
	 * @param message
	 */
	void pushMessage( long sessionId, Object message );
	
	
}
