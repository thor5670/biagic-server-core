package com.rtsapp.server.profiling;

/**
 * Created by admin on 15-10-13.
 */

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 执行时间的统计信息
 */
public class ExecuteProfilling implements java.lang.Comparable<ExecuteProfilling>{

    /**
     * 执行事件的id
     */
    private final int executeId;

    /**
     * 执行信息的名字
     */
    private String executeName;


    /**
     * 执行失败的次数
     */
    private final AtomicInteger failExecuteCount = new AtomicInteger( 0 );

    /**
     * 成功执行的次数
     */
    private final AtomicInteger successExecuteCount = new AtomicInteger( 0 );
    /**
     * 成功执行的总时间
     */
    private final AtomicLong totalTime = new AtomicLong( 0 );

    private  long minExecuteTime = Long.MAX_VALUE ;
    private  long maxExecuteTime = 0 ;

    public ExecuteProfilling(int executeId ){
        this.executeId = executeId;
    }

    public String getExecuteName() {
        return executeName;
    }

    public void setExecuteName(String executeName) {
        this.executeName = executeName;
    }

    public void logFail( ){
        failExecuteCount.incrementAndGet();
    }

    public void logSuccess(long time){
        if( time < minExecuteTime ){
            minExecuteTime = time;
        }
        if( time > maxExecuteTime ){
            maxExecuteTime = time;
        }

        successExecuteCount.incrementAndGet();
        totalTime.addAndGet(time);
    }


    public int getExecuteId() {
        return executeId;
    }

    public long getMinExecuteTime() {
        return minExecuteTime;
    }

    public long getMaxExecuteTime() {
        return maxExecuteTime;
    }

    public int getSuccessExecuteCount(){
        return successExecuteCount.intValue();
    }


    public int getFailExecuteCount() {
        return failExecuteCount.intValue( );
    }


    public long getAvgExecuteTime(){
        int count =  getSuccessExecuteCount();
        if( count == 0 ){
            return 0;
        }else if( count <= 2 ){
            return totalTime.longValue() / count;
        }else{
            return totalTime.longValue() / count;
        }
    }

    /**
     * 总累计耗时
     * @return
     */
    public long getTotalTime(){
        return totalTime.longValue();
    }

    @Override
    public int compareTo( ExecuteProfilling o ) {
        long avgTime = this.getAvgExecuteTime();
        long avgTimeOther = o.getAvgExecuteTime();

        if( avgTime > avgTimeOther ){
            return -1;
        }else if( avgTime == avgTimeOther ){
            return 0;
        }else{
            return 1;
        }
    }


    private static final String TAB = "\t";
    private static final String NEXTLINE = "\r\n";

    /**
     * 将执行信息格式化成便于查看的字符串并返回
     * @param executeTimeList
     * @return
     */
    public static String formatExecuteTimeList( List<ExecuteProfilling> executeTimeList ) {

        if (executeTimeList != null && executeTimeList.size() > 0) {

            StringBuilder sb = new StringBuilder();

            sb.append(fixToTwoTabLength("id     "));
            sb.append(fixToTwoTabLength("fail   "));
            sb.append(fixToTwoTabLength("success"));
            sb.append(fixToTwoTabLength("avgTime"));
            sb.append(fixToTwoTabLength("minTime"));
            sb.append(fixToTwoTabLength("maxTime"));
            sb.append(fixToTwoTabLength("total"));
            sb.append(fixToTwoTabLength("name"));
            sb.append(NEXTLINE);


            for (ExecuteProfilling executeTime : executeTimeList) {

                String commandHexId = Integer.toHexString(executeTime.getExecuteId()).toUpperCase();
                sb.append(fixToTwoTabLength("0x" + (commandHexId.length() < 6 ? "0" : "") + commandHexId));

                sb.append(fixToTwoTabLength(executeTime.getFailExecuteCount() + ""));
                sb.append(fixToTwoTabLength(executeTime.getSuccessExecuteCount() + ""));
                sb.append(fixToTwoTabLength(executeTime.getAvgExecuteTime() + ""));
                if( executeTime.getSuccessExecuteCount() > 0  ) {
                    sb.append(fixToTwoTabLength(executeTime.getMinExecuteTime() + ""));
                    sb.append(fixToTwoTabLength(executeTime.getMaxExecuteTime() + ""));
                }else{
                    sb.append(fixToTwoTabLength( "-") );
                    sb.append(fixToTwoTabLength( "-") );
                }
                sb.append(fixToTwoTabLength(executeTime.getTotalTime() + ""));
                sb.append(fixToTwoTabLength(executeTime.getExecuteName()));
                sb.append(NEXTLINE);
            }

            return sb.toString();

        }else{
            return "未产生任何网络流量";
        }
    }
    private static String fixToTwoTabLength(String input) {
        if (input.length() >= 16) {
            return input;
        } else if (input.length() >= 8) {
            return input + TAB;
        } else {
            return input + TAB + TAB;
        }
    }

}