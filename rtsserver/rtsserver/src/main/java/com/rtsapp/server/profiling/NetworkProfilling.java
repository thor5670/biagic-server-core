package com.rtsapp.server.profiling;


/**
 * 用于网络统计的帮助类
 */
public class NetworkProfilling {


    private static final NetworkProfilling instance = new NetworkProfilling();

    private final PeriodSummationProfilling inputSumProfilling;
    private final PeriodSummationProfilling outputSumProfilling;

    public NetworkProfilling(){

        ExecuteProfilling inputProfilling = Profilling.getInstance().getExecuteProfilling( Profilling.CategoryIds.NETWORK, 1 );
        ExecuteProfilling outputProfilling = Profilling.getInstance().getExecuteProfilling( Profilling.CategoryIds.NETWORK, 2 );

        inputProfilling.setExecuteName( "in bytes/s" );
        outputProfilling.setExecuteName("out bytes/s");

        PeriodClock periodClock = PeriodClock.newSecondClock();
        inputSumProfilling = new PeriodSummationProfilling( periodClock, inputProfilling );
        outputSumProfilling = new PeriodSummationProfilling( periodClock, outputProfilling );
    }


    public static NetworkProfilling getInstance(){
        return instance;
    }



    public void proflingInput(long bytes){
        inputSumProfilling.sum( bytes );
    }

    public void profillingOutput(long bytes){
        outputSumProfilling.sum( bytes );
    }

}
