package com.rtsapp.server.profiling;


/**
 * 用于请求吞吐率的计算
 */
public class ReqThroughputProfilling {

    private final PeriodSummationProfilling inputSumProfilling;
    public ReqThroughputProfilling(){

        //ID随便定义
        ExecuteProfilling inputProfilling = new ExecuteProfilling( Profilling.CategoryIds.REQ_THROUGHPUT );

        inputProfilling.setExecuteName( "请求吞吐率" );

        PeriodClock periodClock = PeriodClock.newSecondClock();
        inputSumProfilling = new PeriodSummationProfilling( periodClock, inputProfilling );
    }

    /**
     * 完成了请求
     * @param requestAmount 完成请求的数量
     */
    public void completeRequest( long requestAmount ){
            inputSumProfilling.sum( requestAmount );
    }

    /**
     * 获得最后一秒的请求吞吐率
     * @return
     */
    public long getLastReqThroughput() {
        return inputSumProfilling.getLastPeriodAmount();
    }

}
