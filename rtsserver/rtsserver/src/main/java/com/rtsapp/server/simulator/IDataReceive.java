package com.rtsapp.server.simulator;

/**
 * 数据接收接口
 */
public interface IDataReceive {

    /**
     * 读取数据, 返回已经读取完的数据量
     *
     * @param bytes
     * @return
     */
    void receiveData(byte[] bytes);

}

