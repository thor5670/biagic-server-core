package com.rtsapp.server.simulator;

/**
 * Socket操作接口
 * 1. 连接，关闭，发送数据
 * 2.
 */
public interface ITcpSocket {

    /**
     * 设置接收数据的回调类
     * @param receive
     */
    void setReceive(  IDataReceive receive );

    /**
     * 连接
     */
    void connect() throws Exception;

    /**
     * 关闭
     */
    void close();

    /**
     * 发送数据
     * @param bytes
     * @param length
     */
    void send(byte[] bytes, int length);

}
