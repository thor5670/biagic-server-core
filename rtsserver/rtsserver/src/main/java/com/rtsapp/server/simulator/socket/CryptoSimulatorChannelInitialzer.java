package com.rtsapp.server.simulator.socket;

import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;
import com.rtsapp.server.network.protocol.crypto.IAppChannelInitializer;
import com.rtsapp.server.network.protocol.crypto.client.ClientRSAHandshake;
import com.rtsapp.server.network.protocol.crypto.client.ClientRSAHandshakeKey;
import com.rtsapp.server.simulator.IDataReceive;
import io.netty.channel.socket.SocketChannel;

/**
 * Created by admin on 16-6-18.
 */
public class CryptoSimulatorChannelInitialzer extends SimulatorChannelInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(  CryptoSimulatorChannelInitialzer.class );

    //RSA连接握手的Key
    private ClientRSAHandshakeKey handshakeKey;


    public CryptoSimulatorChannelInitialzer(IDataReceive receive, ClientRSAHandshakeKey handshakeKey) {
        super(receive);
        this.handshakeKey = handshakeKey;
    }
    
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {

        try {
            ClientRSAHandshake handshake = new ClientRSAHandshake(ch, new OriginChannelInitializer(), handshakeKey);
            handshake.start();
        }catch ( Throwable ex ){
            LOG.error(ex, "CryptoCommandChannelInitializer int error");
            ch.close();
        }
    }

    private void originInitChannel( SocketChannel ch ) throws Exception{
        super.initChannel(ch);
    }

    public class OriginChannelInitializer implements IAppChannelInitializer {

        @Override
        public void initChannel(SocketChannel ch)  throws Exception {
            originInitChannel(ch);
        }

    }

}
