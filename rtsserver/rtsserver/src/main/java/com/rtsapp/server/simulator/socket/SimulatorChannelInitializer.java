package com.rtsapp.server.simulator.socket;

import com.rtsapp.server.benchmark.cases.cmd.CommandCaseHandler;
import com.rtsapp.server.benchmark.runner.channel.CommandTestOutHandler;
import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;
import com.rtsapp.server.simulator.IDataReceive;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 *
 */
public class SimulatorChannelInitializer extends ChannelInitializer<SocketChannel> {

    private static final Logger LOG = LoggerFactory.getLogger( SimulatorChannelInitializer.class );

    private final IDataReceive receive;

    public SimulatorChannelInitializer(IDataReceive receive) {
        this.receive = receive;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {

        ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(40960, 0, 4));
        ch.pipeline().addLast(new SimulatorInHandler( receive ) );

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);

        System.out.println( "channelActive" );
    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);

        System.out.println( "channelInactive" );
    }


    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {

        LOG.error("CommandCaseChannelInitializer exceptionCaught ", cause);
        super.exceptionCaught( ctx, cause );
    }
}