package com.rtsapp.server.simulator.socket;

import com.rtsapp.server.benchmark.ITestCase;
import com.rtsapp.server.benchmark.ITestCaseIterator;
import com.rtsapp.server.benchmark.ITestCaseListener;
import com.rtsapp.server.benchmark.cases.cmd.CommandCase;
import com.rtsapp.server.benchmark.runner.ContextUtils;
import com.rtsapp.server.common.ByteBuffer;
import com.rtsapp.server.common.IByteBuffer;
import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.network.protocol.command.ICommand;
import com.rtsapp.server.simulator.IDataReceive;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.EventLoopGroup;

import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 连接执行的handler
 */
public class SimulatorInHandler extends ChannelInboundHandlerAdapter  {

    private static Logger LOGGER =com.rtsapp.server.logger.LoggerFactory.getLogger( SimulatorInHandler.class );

    private final  IDataReceive receive;

    public SimulatorInHandler(   IDataReceive receive ){
        this.receive = receive;
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        try {
            ByteBuf buf = (ByteBuf) msg;

            //这一行不能删掉，作用是移动读指针，跳过长度字段
            int length = buf.readInt();

            byte[] bytes = new byte[ buf.readableBytes() ];
            buf.readBytes( bytes );

            receive.receiveData( bytes );

        } catch (Throwable ex) {
            LOGGER.error(ex, "数据包处理错误");
        }


    }

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.fireExceptionCaught(cause);
        LOGGER.error("Command Case Handler出错", cause );
    }


}