package com.rtsapp.server.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * 时间工具
 */
public class DateUtil {

    public static int currentDayOfWeek(){
        return currentDayOfWeek(4);
    }

    public static int currentDayOfWeek(int resetHour) {
        Calendar calendar = new GregorianCalendar();
        if (calendar.get(Calendar.HOUR_OF_DAY) < resetHour) {
            calendar.add(Calendar.DAY_OF_WEEK, -1);
        }
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        day--;
        if (day == 0)
            day = 7;
        return day;
    }

    public static int currentMonth() {
        Calendar calendar = new GregorianCalendar();
        if (calendar.get(Calendar.HOUR_OF_DAY) < 4) {
            calendar.add(Calendar.DAY_OF_WEEK, -1);
        }
        return calendar.get(Calendar.MONTH);
    }

    // 根据Long时间返回一个 服务器标准 的天数
    public static long getServerDay(long time) {
        return ((time + TimeZone.getDefault().getRawOffset()) / 1000 / 60 / 60 - 4) / 24;
    }

    // 返回两个Long时间是不是在 同一天 （服务器标准的时间）
    public static boolean isSameDay(long time1, long time2) {
        return getServerDay(time1) == getServerDay(time2);
    }

    /**
     * 计算时间戳到本时区经过的真实天数
     * @param time
     * @return
     */
    public static long getRealDay(long time) {
        return ((time + TimeZone.getDefault().getRawOffset()) / 1000 / 60 / 60) / 24;
    }

    /**
     * 计算两个时间戳间相隔的真实天数
     * @param before
     * @param after
     * @return
     */
    public static long betweenDays(long before, long after) {
        long diff = getRealDay(before) - getRealDay(after);
        return Math.abs(diff);
    }
}
