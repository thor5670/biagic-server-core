package com.rtsapp.server.utils;

/**
 * Created by admin on 16-3-30.
 */
public class DebugAbleTime {

    private static long offset;

    public static long currentTimeMillis() {
        if (offset != 0) {
            return System.currentTimeMillis() + offset;
        }
        return System.currentTimeMillis();
    }

    public static void appendOffset(long millis) {
        offset += millis;
    }

    public static void setOffset(long millis) {
        offset = millis;
    }

    public static void resetOffset() {
        offset = 0;
    }
}
