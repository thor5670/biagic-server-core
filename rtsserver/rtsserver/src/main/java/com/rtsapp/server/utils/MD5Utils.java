package com.rtsapp.server.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.rtsapp.server.logger.Logger;

public class MD5Utils {
	
	private static final Logger logger =com.rtsapp.server.logger.LoggerFactory.getLogger( MD5Utils.class );
	
	private static char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'}; 
	 
	public static final String MD5( byte[] inputBytes ){
		
		try {
			MessageDigest digest = MessageDigest.getInstance( "MD5" );
			
			digest.update( inputBytes );
			
			byte[] md = digest.digest();
			
			// 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[ j * 2 ];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
			
		} catch (NoSuchAlgorithmException e) {
			logger.error( "md5 出错");
			e.printStackTrace();
			return null;
		}
		
	}
}
