package com.rtsapp.server.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;

/**
 * Created by admin on 15-10-19.
 */
public class PropertyCfg {

    private java.util.Properties properties;

    public PropertyCfg( InputStream stream ){
        properties = new java.util.Properties();
        try {
            properties.load( new InputStreamReader(stream, "UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException( "PropertyCfg创建失败", e );
        }
    }


    public String getString( String key ){
        return properties.getProperty( key );
    }

    public int getInt( String key ) throws PropCfgException{
        return Integer.parseInt(getString(key));
    }

    public double getDouble( String key ) throws PropCfgException{
        return Double.parseDouble(getString(key));
    }

    public boolean getBool( String key ) throws PropCfgException{
        return Boolean.parseBoolean(getString(key));
    }


    public Set<Object> keyset(){
        return properties.keySet();
    }
}
