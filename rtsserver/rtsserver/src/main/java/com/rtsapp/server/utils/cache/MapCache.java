package com.rtsapp.server.utils.cache;

import io.netty.util.internal.ConcurrentSet;
import com.rtsapp.server.logger.Logger;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * 一个Cache
 * 加入缓存对象时，可以指定两个key
 * 可以按两个key查询缓存对象
 *
 * @param <V>
 */
public final class MapCache<V> implements Runnable {
    private static final Logger LOGGER =com.rtsapp.server.logger.LoggerFactory.getLogger(MapCache.class);

    private static final ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(1);


    /**
     * 按key索引的缓存对象的map
     */
    private Map<Integer, CacheObject<V>> cache = new ConcurrentHashMap<>();
    /**
     * 用另外一个key，索引缓存对象的key
     */
    private Map<Integer, Integer> key = new ConcurrentHashMap<>();

    /**
     * 优先玩家id(在线玩家id)集合
     */
    private Set<Integer> favorsPlayerIds = new ConcurrentSet<>();


    /**
     * 缓存超时时间
     */
    private int time;


    /**
     * 通行证集合
     */
    private Map<Integer, Object> pass;


    /**
     * @param time 超时时间
     * @param pass 缓存key需要的通行证map
     */
    public MapCache(int time, Map<Integer, Object> pass) {
        this.time = time * 60000;
        this.pass = pass;

        Calendar date = new GregorianCalendar();
        long current = date.getTimeInMillis();
        date.add(Calendar.HOUR_OF_DAY, 1);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        scheduledExecutorService.scheduleAtFixedRate(this, date.getTimeInMillis() - current, 1200000, TimeUnit.MILLISECONDS);
    }

    /**
     * 加入要缓存的对象
     * @param key1 第一个key
     * @param key2 第二个key
     * @param value 存入的对象
     */
    public void put(Integer key1, Integer key2, V value) {

        //TODO 用于临时修复部分机器人查询问题, 重新创建机器人后(可以)删除此代码
        if (key1 == 0) {
            key1 = 1000000000 + key2;
        }

        pass.putIfAbsent(key1, new Object());
        synchronized (pass.get(key1)) {
            long currentTime = System.currentTimeMillis();
            CacheObject<V> cacheObject = cache.get(key1);
            if (cacheObject == null) {
                cacheObject = new CacheObject<>(value);
            }
            cacheObject.time = currentTime;
            synchronized (cache) {
                cache.put(key1, cacheObject);
            }
            key.put(key2, key1);
            favorsPlayerIds.add(key1);
        }
    }

    /**
     * 按第一个key查询缓存对象
     * 如果已经下线，并且缓存超时，返回null
     * @param key1
     * @return
     */
    public V getByKey1(Integer key1) {

        CacheObject<V> cacheObject = null;
        pass.putIfAbsent(key1, new Object());
        synchronized (pass.get(key1)) {
            cacheObject = cache.get(key1);
            if (cacheObject == null)
                return null;
            long currentTime = System.currentTimeMillis();
            if (!favorsPlayerIds.contains(key1)) {
                long sub = cacheObject.time + time - currentTime;
                if (sub < 0) {
                    LOGGER.info(key1 + "所对应的对象过期" + sub);
                    synchronized (cache) {
                        cache.remove(key1);
                    }
                    return null;
                }
            }
            cacheObject.time = currentTime;
        }
        return cacheObject.obj;
    }

    /**
     * 按第二个key查询缓存对象
     * @param key2
     * @return
     */
    public V getByKey2(Integer key2) {
        Integer key1 = getKey1ByKey2(key2);
        if (key1 == null)
            return null;
        return getByKey1(key1);
    }

    private Integer getKey1ByKey2(Integer key2) {
        return key.get(key2);
    }


    /**
     * 移除在线
     * @param key1
     */
    public void outFavor(Integer key1) {
        favorsPlayerIds.remove(key1);
    }


    public Object[] favoredArray() {
        return (Object[]) favorsPlayerIds.toArray();
    }

    /**
     * 所有在线的玩家id集合
     * @return
     */
    public Set<Integer> favoredSet() {
        return favorsPlayerIds;
    }


    @Override
    public void run() {
        long time = System.nanoTime();
        LOGGER.info("开始检查缓存中的脏数据");
        int cnt = 0;
        synchronized (cache) {
            for (Integer key1 : cache.keySet()) {
                if (getByKey1(key1) == null)
                    cnt++;
            }
        }
        LOGGER.info("检查脏数据结束,共清理数据 " + cnt + " 条,耗时 " + (System.nanoTime() - time) + "ns");
    }



    private static class CacheObject<T> {

        long time;

        T obj;

        public CacheObject(T obj) {
            this.obj = obj;
        }

    }

}
