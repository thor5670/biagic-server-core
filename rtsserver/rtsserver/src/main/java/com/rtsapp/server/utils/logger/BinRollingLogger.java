package com.rtsapp.server.utils.logger;

/**
 *
 */

import com.rtsapp.server.logger.Logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * TODO BinRollingLogger目前和RollingLogger几乎代码一样, 后续修改RollingLogger, 去掉重复的代码
 * 一个周期性的日志记录类
 * 日志记录是同步的
 */
public class BinRollingLogger {

    private static final Logger LOGGER =com.rtsapp.server.logger.LoggerFactory.getLogger( BinRollingLogger.class );
    private final String fileNamePattern ;
    private final String dateFormat;
    private final RollingCalendar calendar;

    private FileOutputStream loggerWriter = null;
    private long nextDayStartMills = 0;

    public BinRollingLogger( String fileNamePattern, String dateFormat, RollingCalendar calendar){
        this.fileNamePattern = fileNamePattern;
        this.dateFormat = dateFormat;
        this.calendar = calendar;
    }


    public void appendLog( byte[] log,  int off, int len  ){

        long timeMillis = System.currentTimeMillis();

        if( timeMillis > nextDayStartMills ){
            synchronized ( this ){
                if( timeMillis > nextDayStartMills ){
                    gotoNextPeriod( );
                }
            }
        }

        if( loggerWriter == null ){
            LOGGER.error( "loggerWriter 为null, 不能记录日志, 下面是要记录的日志" );
            LOGGER.error( log.toString() );
            return;
        }


        synchronized ( this ){
            try {
                loggerWriter.write( log , off, len );
                loggerWriter.flush();
            } catch (IOException e) {
                LOGGER.error( "输出日志有错, 日志如下", e );
                LOGGER.error( log.toString() );
            }

        }
    }


    private void gotoNextPeriod(){
        //如果当前writer不为空, 保存并关闭当前writer
        if( loggerWriter != null ){
            try {
                loggerWriter.flush();
                loggerWriter.close();
                loggerWriter = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Date now = new Date();
        SimpleDateFormat format =  new  SimpleDateFormat( dateFormat );
        String dateStr = format.format( now );

        String fileName = String.format(fileNamePattern, dateStr);
        File f = new File( fileName );

        if( ! f.exists() ){
            try {

                int index = f.getAbsolutePath().lastIndexOf( File.separator );
                File dir = new File( f.getAbsolutePath().substring( 0, index ) );
                if( ! dir.exists() ){
                    boolean createDirOK = dir.mkdirs();
                    if( !createDirOK ){
                        LOGGER.error( "创建日志目录失败:"  + dir.getAbsolutePath() );
                        return;
                    }
                }

                f.createNewFile();
            } catch (IOException e) {
                LOGGER.error("创建日志文件失败:" + f.getAbsolutePath(), e );
                return;
            }
        }

        try {
            loggerWriter = new FileOutputStream( f, true );
        } catch (IOException e) {
            LOGGER.error("创建loggerWriter失败:" + f.getAbsolutePath(), e );
            return;
        }

        //计算下一个周期
        nextDayStartMills = calendar.getNextCheckMillis( now );

    }


}