package com.rtsapp.server.utils.net;

import com.rtsapp.server.network.protocol.command.ICommand;

public interface ICommunicate {

	void init(String host, int post);

	void add(ICommand command);

	void send();

	void receive();

	void destory();

}
