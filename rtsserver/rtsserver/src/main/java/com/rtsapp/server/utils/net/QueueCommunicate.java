package com.rtsapp.server.utils.net;


import com.rtsapp.server.network.protocol.command.ICommand;
import com.rtsapp.server.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueCommunicate implements ICommunicate, Runnable {
	private static Logger logger =com.rtsapp.server.logger.LoggerFactory.getLogger(QueueCommunicate.class);
	private static Thread queueCommunicate = null;
	private long time;
	private ICommunicate communicate;
	private String host;
	private int post;
	private int count;
	private static ICommunicate queue_qc;
	private static QueueCommunicate qc;
	private static BlockingQueue<ICommand> bq = new LinkedBlockingQueue<ICommand>();

	public QueueCommunicate() {
	}

	private QueueCommunicate(ICommunicate communicate, int count, int time) {
		this.time = time;
		this.count = count;
		this.communicate = communicate;
	}

	public static ICommunicate getQueueCommunicate() {
		return queue_qc;
	}

	public static void start(String serverName) {
		logger.info("队列访问操作线程启动");
		queueCommunicate = new Thread(qc, serverName);
		queueCommunicate.start();
	}

	public static ICommunicate build(Class<?> implementClassName, int size,
			int timeout) {
		try {
			qc = new QueueCommunicate(
					(ICommunicate) implementClassName.newInstance(), size,
					timeout * 1000);
		} catch (InstantiationException e) {
			logger.error(
					"start() 队列访问操作实现类" + implementClassName.getName()
							+ "创建异常, 可能是" + implementClassName.getName()
							+ "没有无参数的构造函数", e);
		} catch (IllegalAccessException e) {
			logger.error("start() 队列访问操作实现类" + implementClassName.getName()
					+ "创建异常, 可能是" + implementClassName.getName() + "的构造函数不公开",
					e);
		}
		queue_qc = new QueueCommunicate();
		return queue_qc;
	}

	@Override
	public void init(String host, int post) {
		qc.host = host;
		qc.post = post;
	}

	@Override
	public void add(ICommand command) {
		try {
			bq.put(command);
		} catch (InterruptedException e) {
			logger.warn("交互队列线程休眠被打断");
		}
	}

	@Override
	public void send() {
		logger.info("取出批处理");
		List<ICommand> commands = take(count);
		if (commands != null) {
			communicate.init(host, post);
			for (int i = 0; i < commands.size(); i++) {
				communicate.add(commands.get(i));
			}
			communicate.send();
			communicate.receive();
			communicate.destory();
		} else {
			logger.info("无批处理");
		}
	}

	@Override
	public void receive() {
	}

	@Override
	public void destory() {
	}

	@Override
	public void run() {
		while (queueCommunicate != null) {
			send();
			try {
				Thread.sleep(time);
			} catch (InterruptedException e) {
				logger.warn("交互队列线程休眠被打断");
			}
		}
		logger.warn("交互队列停止工作");
	}

//	@Override
//	public void shutdownGracefully() {
//		Thread tmpThread = queueCommunicate;
//		queueCommunicate = null;
//		if (tmpThread != null) {
//			tmpThread.interrupt();
//		}
//	}


	private List<ICommand> take(int count) {
		List<ICommand> commands = null;
		ICommand command = bq.poll();
		for (int i = 0; i < count && command != null; i++, command = bq.poll()) {
			if (commands == null) {
				commands = new ArrayList<ICommand>();
			}
			commands.add(command);
		}
		return commands;
	}

}
