package com.rtsapp.server.utils.resource;


import com.rtsapp.server.utils.resource.pool.ObjectsPool;
import com.rtsapp.server.utils.resource.pool.PoolFactory;
import com.rtsapp.server.logger.Logger;

/**对象池工厂*/
public class ObjectsPoolFactory extends PoolFactory {
	    private static final Logger logger =com.rtsapp.server.logger.LoggerFactory.getLogger(ObjectsPoolFactory.class);
		private static final ObjectsPoolFactory objFactory = new ObjectsPoolFactory();
	    public static ObjectsPoolFactory getFactoryInstance(){
	    	return objFactory;
	    }
	
		@SuppressWarnings("unchecked")
		@Override
		public <T extends ObjectsPool> T createObjectsPool(Class<T> clz) {
			logger.info("创建"+clz.getName()+"对象池.....");
			T objectsPool = null ;
			try{
				//生产一个对象池
				objectsPool = (T)Class.forName(clz.getName()).newInstance();
			}catch(Throwable e){
				logger.error("对象池创建错误...",e);
			}
			logger.info("创建"+clz.getName()+"对象池完成....");
			return objectsPool;
		}
}
