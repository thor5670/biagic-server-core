//package com.rtsapp.server.utils.resource;
//
//import com.rtsapp.server.logger.Logger;
//import org.quartz.*;
//import org.quartz.impl.StdSchedulerFactory;
//
//import java.util.Collection;
//import java.util.Properties;
//import java.util.TimeZone;
//
//import static org.quartz.CronScheduleBuilder.dailyAtHourAndMinute;
//import static org.quartz.JobBuilder.newJob;
//import static org.quartz.TriggerBuilder.newTrigger;
//
//public class Quartz {
//    public static final Logger logger =com.rtsapp.server.logger.LoggerFactory.getLogger(Quartz.class);
//    public static final Quartz instance = new Quartz();
//    private static final StdSchedulerFactory schedFact = new StdSchedulerFactory();
//    private static final Properties props = new Properties();
//
//    private Quartz() {
//    }
//
//    public static Quartz getInstance() {
//        return instance;
//    }
//
//    public void start(Class<? extends Job> dispatcher, int hour, int min, String id) {
//        //统一时区为东八区
//        TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
//        //定义一个Job
//        JobDetail job = newJob(dispatcher).withIdentity(id + dispatcher.getSimpleName(), "naval_rts")
//                .build();
//        //定义一个时间触发器
//        Trigger trigger = newTrigger()
//                .withIdentity(id + dispatcher.getSimpleName(), "naval")
//                .withSchedule(dailyAtHourAndMinute(hour, min))
//                .forJob(id + dispatcher.getSimpleName(), "naval_rts")
//                .build();
//        Scheduler sched;
//        try {
//
//            props.put("org.quartz.scheduler.instanceName", "naval_rts");
//            props.put(StdSchedulerFactory.PROP_THREAD_POOL_CLASS, "org.quartz.simpl.SimpleThreadPool");
//            props.put("org.quartz.threadPool.threadCount", "1");
//            props.put("org.quartz.threadPool.threadPriority", "5");
//            //初始化
//            schedFact.initialize(props);
//            sched = schedFact.getScheduler();
//            //开启
//            sched.start();
//            sched.scheduleJob(job, trigger);
//            logger.info("资源调度已就绪...调度类" + job.getKey().getName() + ".class");
//        } catch (SchedulerException e) {
//            logger.error("调度开启异常:" + e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    //停止所有调度线程
//    public void stop() {
//        try {
//            Collection<Scheduler> schedulerList = schedFact.getAllSchedulers();
//            if (schedulerList != null) {
//                for (Scheduler s : schedulerList) {
//                    s.shutdown(true);
//                    logger.info("资源调度:" + s.getSchedulerInstanceId() + ":" + s.getSchedulerName() + "停止");
//                }
//            }
//        } catch (SchedulerException e) {
//            logger.error("资源调度关闭异常:" + e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//}
