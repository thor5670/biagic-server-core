package com.rtsapp.server.utils.resource.pool;

public abstract class ObjectsPool {
	private static final int DEAFAULT = 100 ;
	private Object[] objs = null ;
	private int len = 0;
	private long point = 0 ; 
	private int index = - 1;
	/**放对象*/
	public synchronized void put(Object obj){
		if(objs == null){
			//***创建默认大小
			objs = new Object[DEAFAULT];
		}
		//***空间足够
		if(len < objs.length){
			objs[len++] = obj;
		}else{
		  //开辟新空间
		   Object[] tmp = new Object[objs.length * 2];
		   for(int i = 0 ; i< objs.length ; i ++){
			   if(objs[i] != null){
				  tmp[i] = objs[i];
			   }
		   }
		   objs = tmp ;
		   tmp = null ;
		   objs[len++] = obj ;
		}
	}

	/**放入池中并返回当前索引*/
	public int putBackIndex (Object obj){
		put(obj);
		index = len;
		return --index ; 
	}
	
	
	
	/**拿对象*/
    public synchronized Object take(){
    	if(null == objs){
    		return null ;
    	}
    	Object obj = objs[(int)(point)%(len)];
    	point++ ;
    	return obj;
    }
    
    /**根据索引直接拿*/
    public synchronized Object take(Integer index){
    	if(null == objs){
    		return null;
    	}
    	if(index>=this.objs.length || index < 0 || index == null){
    		return null;
    	}
    	return objs[index];
    }

    /**返回对象池中的对象个数*/
    public int size(){
    	return this.len;
    }
}
