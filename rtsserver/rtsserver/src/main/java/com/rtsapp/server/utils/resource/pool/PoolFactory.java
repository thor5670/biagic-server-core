package com.rtsapp.server.utils.resource.pool;

public abstract class PoolFactory {
	/**抽象工厂，创建对象池*/
     public abstract <T extends ObjectsPool> T createObjectsPool(Class<T> clz);
}
