package com.rtsapp.server.utils.time;

/**
 * Created by admin on 16-7-2.
 */

import java.util.Calendar;
import java.util.Date;

/**
 * 每日重置的周期性时间类
 */
public  class DayPeriodCalendar {

    //一天的毫秒数
    private static final int MILLS_A_DAY = 24 * 60 * 60 * 1000;

    private  final int offsetHour;
    private  final int offsetMinute;
    private  final int totalOffset;



    public DayPeriodCalendar(int hourOffset, int minuteOffset) {
        this.offsetHour = hourOffset;
        this.offsetMinute = minuteOffset;
        this.totalOffset = hourOffset * 60 + minuteOffset;
    }

    public DayPeriodCalendar(int offsetHour) {
        this( offsetHour, 0 );
    }


    /**
     * 返回输入的时间所在的周期的开始时间
     * @return
     */
    public long getPeriodStartMils(long time){

        Date date = new Date( time );
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        //如果今日的结算时间在当前时间后，最近的结算应该是上一天的
        if( isTodayMinuteBeforeOffset(c) ){
            c.add( Calendar.DAY_OF_MONTH, -1 );
        }

        c.set( Calendar.HOUR_OF_DAY, offsetHour);
        c.set( Calendar.MINUTE, offsetMinute);
        c.set( Calendar.SECOND,  0  );
        c.set( Calendar.MILLISECOND ,  0 );

        return c.getTimeInMillis();
    }

    /**
     * 返回输入的时间所在周期的下一个周期的开始时间
     * @param time
     * @return
     */
    public  long getNextPeriodStartMils(long time){
        return  increaseAPeriod(getPeriodStartMils(time));
    }

    /**
     * 返回当前时间所在的周期的开始时间
     * @return
     */
    public long getPeriodStartMilsForCurrentTime(){
        return getPeriodStartMils(System.currentTimeMillis());
    }

    /**
     * 返回当前时间所在周期的下一个周期的开始时间
     * @return
     */
    public  long getNextPeriodStartMilsForCurrentTime(){
        return getNextPeriodStartMils(System.currentTimeMillis());
    }

    /**
     * 给定的时间是否在当前的结算周期类
     * @param toTestTime
     * @return
     */
    public  boolean isInCurPeriod(long toTestTime){
        return isInSamePeriod(toTestTime, System.currentTimeMillis());
    }

    /**
     * 是否在同一个时间周期内
     * @param toTestTime
     * @param periodTime
     * @return
     */
    public boolean isInSamePeriod( long toTestTime, long periodTime ){
        long startTime = getPeriodStartMils( periodTime );
        long endTime = increaseAPeriod( startTime );
        return  ( toTestTime >= startTime && toTestTime < endTime );
    }




    /**
     * 一个周期持续的毫秒数
     * @return
     */
    public long getPeriodDurationMils( ){
        return MILLS_A_DAY;
    }

    /**
     * 增加一个时间周期
     * @param inputTime
     * @return
     */
    private long increaseAPeriod( long inputTime ){
        return inputTime + MILLS_A_DAY;
    }

    /**
     * 是否当天的分钟数 在 offset 前
     * @param c
     * @return
     */
    private  boolean isTodayMinuteBeforeOffset(Calendar c){

        int hourNow  = c.get( Calendar.HOUR_OF_DAY );
        int minuteNow = c.get( Calendar.MINUTE );

        int todayOffset = hourNow * 60 + minuteNow;

        return todayOffset < totalOffset;
    }


}