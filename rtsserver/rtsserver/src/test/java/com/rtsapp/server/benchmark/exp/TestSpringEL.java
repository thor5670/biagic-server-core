package com.rtsapp.server.benchmark.exp;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.*;

/**
 * Created by admin on 15-10-30.
 */
public class TestSpringEL {

    public static void main(String[] args ){

        ExpressionParser parser = new SpelExpressionParser( );
        Expression exp =  parser.parseExpression("1+2");

        Integer message =  (Integer)exp.getValue();
        System.out.println(message);

        System.out.println( exp.getValue( new Object( ) ) );


        Simple simple = new Simple();
        simple.booleanList.add( true );
        StandardEvaluationContext ctx = new StandardEvaluationContext( simple );

        Boolean b = (Boolean)parser.parseExpression( "booleanList[0]" ).getValue( ctx );
        System.out.println(b);

        parser.parseExpression( "booleanList[0]" ).setValue(ctx, "false");
        b = (Boolean)parser.parseExpression( "booleanList[0]" ).getValue( ctx );
        System.out.println(b);


        Map m = new HashMap<>( );
        m.put( "name", "zhangsan" );
        m.put("city", "beijing");
        m.put( "girls", new int[]{1,2,3,5} );

        StandardEvaluationContext ctx2 = new StandardEvaluationContext( m );
        System.out.println(  parser.parseExpression( "[name].toUpperCase() + ' in ' + [city] + ' ,has ' + [girls].length +' girls'" ).getValue( ctx2 ) );



        List<Integer> primes = new ArrayList<Integer>();
        primes.addAll(Arrays.asList(2, 3, 5, 7, 11, 13, 17));

// create parser and set variable 'primes' as the array of integers
        StandardEvaluationContext context = new StandardEvaluationContext();
        context.setVariable("primes",primes);

        System.out.println( parser.parseExpression("#primes.?[#this>10]" ).getValue( context ) );

// all prime numbers > 10 from the list (using selection ?{...})
// evaluates to [11, 13, 17]
        List<Integer> primesGreaterThanTen =
                (List<Integer>) parser.parseExpression("#primes.?[#this>10]").getValue(context);

        System.out.println( parser.parseExpression("true").getValue() );

        //parser.parseExpression("#primes.?[#this>10]" ).getValue()
    }

    private static class Simple{
        public List<Boolean> booleanList = new ArrayList<>();

        String name = "myname";
    }

}