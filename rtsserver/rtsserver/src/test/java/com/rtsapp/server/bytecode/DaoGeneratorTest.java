package com.rtsapp.server.bytecode;

import junit.framework.TestCase;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by admin on 15-9-18.
 */
public class DaoGeneratorTest extends TestCase{


    public void testDaoGenerator( ) throws IllegalAccessException, InstantiationException {

        ClassGenerator cg = ClassGenerator.newInstance();
        cg.setClassName(StudentDao.class.getName() + "$MysqlImpl");
        cg.addInterface(StudentDao.class);


        cg.addMethod("public void save(){ System.out.println( \"这里可以根据实体属性拼接save的方法, 后续实现\" ); }");
        cg.addMethod("public void delete(){ System.out.println( \"这里可以根据实体属性拼接delete的方法, 后续实现\" ); }");
        cg.addDefaultConstructor();
        Class<?> clz =  cg.toClass();

        for( int i = 0; i < 10; i++ ){
            StudentDao dao = (StudentDao)clz.newInstance();
            dao.save();
            dao.delete();
        }


    }

    @Entity
    class StudentEntity{

        @Id
        private int id;
        private String name;
        private String password;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

    }


    interface StudentDao {

        void save();
        void delete();

        /*
        void insert( StudentEntity entity );

        void update( StudentEntity entity );

        void delete( StudentEntity entity );

        List<StudentEntity> findAll( );

        StudentEntity find( int id );

        StudentEntity findByUsernamePassword( String username, String password );

*/

    }





}
