package com.rtsapp.server.checkMoney;

import junit.framework.TestCase;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.List;

/**
 * Created by admin on 16/8/12.
 */
public class checkMoneyTest extends TestCase {

    public void testReadFile() {
        File flie = new File("cfg/checkMoney.xml");
        SAXReader saxReader = new SAXReader();
        Document dom;
        try {
            dom = saxReader.read(flie);
        } catch (DocumentException e) {
            e.printStackTrace();
            return;
        }

        Element root = dom.getRootElement();
        String aSwitch = root.element("switch").attributeValue("value"); //开关
        if ("false".equals(aSwitch)) {
            return;
        }
        String isWhiteList = root.element("model").attributeValue("isWhiteList");//是否是白名单

        List<Element> list =root.element("thresholds").elements();
        for(Element element : list){
            System.out.println("id===="+element.attribute("id").getValue());
            System.out.println("value=="+element.attribute("value").getValue());

        }

    }
}
