package com.rtsapp.server.common.diskcache;

/**
 *
 性能测试, 性能在可接受的步骤, 结果还可以
 文件数量多，读取性能下降最明显
 在我的开发机上结果

 1000次,30字节, 写入耗时80705696纳秒,平均耗时:0.080705豪秒
 1000次,30字节, 读取耗时12769869纳秒,平均耗时:0.012769豪秒
 1000次,100字节, 写入耗时55573731纳秒,平均耗时:0.055573豪秒
 1000次,100字节, 读取耗时10805914纳秒,平均耗时:0.010805豪秒
 1000次,1000字节, 写入耗时54515352纳秒,平均耗时:0.054515豪秒
 1000次,1000字节, 读取耗时11248247纳秒,平均耗时:0.011248豪秒


 30000次,30字节, 写入耗时2178595961纳秒,平均耗时:0.072619豪秒
 30000次,30字节, 读取耗时288000184纳秒,平均耗时:0.0096豪秒
 30000次,100字节, 写入耗时1765751244纳秒,平均耗时:0.058858豪秒
 30000次,100字节, 读取耗时282352142纳秒,平均耗时:0.009411豪秒
 30000次,1000字节, 写入耗时1136224627纳秒,平均耗时:0.037874豪秒
 30000次,1000字节, 读取耗时263563947纳秒,平均耗时:0.008785豪秒


 100000次,30字节, 写入耗时8124787573纳秒,平均耗时:0.081247豪秒
 100000次,30字节, 读取耗时7260921869纳秒,平均耗时:0.072609豪秒
 100000次,100字节, 写入耗时6429257398纳秒,平均耗时:0.064292豪秒
 100000次,100字节, 读取耗时7315413544纳秒,平均耗时:0.073154豪秒
 100000次,1000字节, 写入耗时7135868996纳秒,平均耗时:0.071358豪秒
 100000次,1000字节, 读取耗时7386017547纳秒,平均耗时:0.07386豪秒


 100000次,30字节, 移除耗时3171486451纳秒,平均耗时:0.031714豪秒
 100000次,100字节, 移除耗时4274189988纳秒,平均耗时:0.042741豪秒
 100000次,1000字节, 移除耗时2485491029纳秒,平均耗时:0.024854豪秒

 */
public class SimpleFileDiskCacheBenchmark {


    public static void main(String[] args ) {


        final int count = 100000;
        final String[] names = new String[count];
        for (int i = 0; i < count; i++) {
            names[i] = String.valueOf(i);
        }

        final boolean isRead = false;
        final boolean isWrite = false;
        final boolean isRemove = true;

        {
            final byte[] bytes = new byte[30];
            DiskCache cache = DiskCache.newSimpleFileCache("test", "/Users/admin/cache1");

            if (isWrite) {
                long startTime = System.nanoTime();
                for (int i = 0; i < count; i++) {
                    cache.set(names[i], bytes);
                }
                long endTime = System.nanoTime();

                System.out.println(count + "次," + bytes.length + "字节, 写入耗时" + (endTime - startTime) + "纳秒,平均耗时:" + (endTime - startTime) / count / 1000000.0 + "豪秒");
            }

            if (isRead) {
                long startTime = System.nanoTime();
                for (int i = 0; i < count; i++) {
                    cache.get(names[i]);
                }
                long endTime = System.nanoTime();

                System.out.println(count + "次," + bytes.length + "字节, 读取耗时" + (endTime - startTime) + "纳秒,平均耗时:" + (endTime - startTime) / count / 1000000.0 + "豪秒");
            }


            if (isRemove) {
                long startTime = System.nanoTime();
                for (int i = 0; i < count; i++) {
                    cache.remove(names[i]);
                }
                long endTime = System.nanoTime();

                System.out.println(count + "次," + bytes.length + "字节, 移除耗时" + (endTime - startTime) + "纳秒,平均耗时:" + (endTime - startTime) / count / 1000000.0 + "豪秒");
            }

        }

        {
            final byte[] bytes = new byte[100];
            DiskCache cache = DiskCache.newSimpleFileCache("test", "/Users/admin/cache2");

            if (isWrite) {
                long startTime = System.nanoTime();
                for (int i = 0; i < count; i++) {
                    cache.set(names[i], bytes);
                }
                long endTime = System.nanoTime();

                System.out.println(count + "次," + bytes.length + "字节, 写入耗时" + (endTime - startTime) + "纳秒,平均耗时:" + (endTime - startTime) / count / 1000000.0 + "豪秒");
            }

            if (isRead) {
                long startTime = System.nanoTime();
                for (int i = 0; i < count; i++) {
                    cache.get(names[i]);
                }
                long endTime = System.nanoTime();

                System.out.println(count + "次," + bytes.length + "字节, 读取耗时" + (endTime - startTime) + "纳秒,平均耗时:" + (endTime - startTime) / count / 1000000.0 + "豪秒");
            }


            if (isRemove) {
                long startTime = System.nanoTime();
                for (int i = 0; i < count; i++) {
                    cache.remove(names[i]);
                }
                long endTime = System.nanoTime();

                System.out.println(count + "次," + bytes.length + "字节, 移除耗时" + (endTime - startTime) + "纳秒,平均耗时:" + (endTime - startTime) / count / 1000000.0 + "豪秒");
            }
        }

        {
            final byte[] bytes = new byte[1000];
            DiskCache cache = DiskCache.newSimpleFileCache("test", "/Users/admin/cache3");

            if (isWrite) {
                long startTime = System.nanoTime();
                for (int i = 0; i < count; i++) {
                    cache.set(names[i], bytes);
                }
                long endTime = System.nanoTime();

                System.out.println(count + "次," + bytes.length + "字节, 写入耗时" + (endTime - startTime) + "纳秒,平均耗时:" + (endTime - startTime) / count / 1000000.0 + "豪秒");
            }

            if (isRead) {
                long startTime = System.nanoTime();
                for (int i = 0; i < count; i++) {
                    cache.get(names[i]);
                }
                long endTime = System.nanoTime();

                System.out.println(count + "次," + bytes.length + "字节, 读取耗时" + (endTime - startTime) + "纳秒,平均耗时:" + (endTime - startTime) / count / 1000000.0 + "豪秒");
            }


            if (isRemove) {
                long startTime = System.nanoTime();
                for (int i = 0; i < count; i++) {
                    cache.remove(names[i]);
                }
                long endTime = System.nanoTime();

                System.out.println(count + "次," + bytes.length + "字节, 移除耗时" + (endTime - startTime) + "纳秒,平均耗时:" + (endTime - startTime) / count / 1000000.0 + "豪秒");
            }

        }

    }
}
