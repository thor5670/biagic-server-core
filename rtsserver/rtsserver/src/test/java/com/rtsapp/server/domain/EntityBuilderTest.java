package com.rtsapp.server.domain;

import com.rtsapp.server.domain.entity.TestGeneralEntity;
import com.rtsapp.server.domain.entity.TestPackEntity;
import com.rtsapp.server.domain.entity.TestPlayerEntity;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by admin on 15-9-19.
 */
public class EntityBuilderTest extends TestCase{

    public void testNewEntity(){

        EntityBuilder.registerEntity( TestPlayerEntity.class );

        for( int i = 0; i < 100; i++ ) {
            TestPlayerEntity student = EntityBuilder.newEntity( TestPlayerEntity.class );

            Assert.assertNotNull(student);

            System.out.println(student.isDirty());
            Assert.assertFalse(student.isDirty());

            student.setPlayerId(100);
            student.setUserName("张三");

            Assert.assertTrue(student.isDirty());

            boolean b = student.clearDirty( );
            Assert.assertTrue(b);

            Assert.assertFalse(student.isDirty());

            Assert.assertNotNull(student.getPlayerId());
            Assert.assertEquals(100, student.getPlayerId().intValue() );
            Assert.assertEquals("张三", student.getUserName() );
        }

        EntityBuilder.registerEntity(TestGeneralEntity.class);
        EntityBuilder.registerEntity( TestPackEntity.class);

        TestGeneralEntity general = EntityBuilder.newEntity(TestGeneralEntity.class);
        Assert.assertNotNull(general);
        Assert.assertFalse(general.isDirty());
        general.setDictId(100);
        Assert.assertTrue(general.isDirty());

        TestPackEntity pack  = EntityBuilder.newEntity( TestPackEntity.class );
        Assert.assertNotNull( pack );
        Assert.assertFalse(pack.isDirty());
        pack.setItemsBytes(null);
        Assert.assertTrue( pack.isDirty() );

    }

}
