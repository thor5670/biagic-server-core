package com.rtsapp.server.domain.dao;

import com.rtsapp.server.domain.entity.TestGeneralEntity;
import com.rtsapp.server.domain.mysql.sql.MySQLDao;

/**
 * Created by admin on 15-10-7.
 */
public abstract class GeneralDao  extends MySQLDao<TestGeneralEntity, Integer> {

    public abstract void  insert(TestGeneralEntity entity);

    public abstract void update(TestGeneralEntity entity);

    //public abstract ArrayList<GeneralEntity> getBy_player(Integer playerId);


}
