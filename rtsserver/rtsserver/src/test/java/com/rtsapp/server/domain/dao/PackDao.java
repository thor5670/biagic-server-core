package com.rtsapp.server.domain.dao;

import com.rtsapp.server.domain.entity.TestPackEntity;
import com.rtsapp.server.domain.mysql.sql.MySQLDao;

/**
 * Created by admin on 15-10-7.
 */
public abstract class  PackDao extends MySQLDao<TestPackEntity,Integer> {

    public abstract void insert(TestPackEntity entity);

    public abstract void update(TestPackEntity entity);

    public abstract TestPackEntity get(Integer playerId);
    
}
