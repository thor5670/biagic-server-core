package com.rtsapp.server.domain.dao;

import com.rtsapp.server.domain.entity.TestPlayerEntity;
import com.rtsapp.server.domain.mysql.sql.MySQLDao;

/**
 * Created by admin on 15-9-17.
 */
public abstract class PlayerDao extends MySQLDao<TestPlayerEntity, Integer> {

    public abstract void insert(TestPlayerEntity player);

    public abstract void update(TestPlayerEntity player);

    public abstract TestPlayerEntity get(Integer playerId);


}
