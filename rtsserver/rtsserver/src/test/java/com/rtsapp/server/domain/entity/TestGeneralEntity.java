package com.rtsapp.server.domain.entity;

import com.rtsapp.server.domain.IEntity;

import javax.persistence.*;

/**
 * Created by admin on 15-10-9.
 */
@Entity
@Table
public abstract class TestGeneralEntity extends IEntity {

    @Id
    private Integer generalId;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "playerId")
    private TestPlayerEntity player;

    private Integer DictId;

    public Integer getGeneralId() {
        return generalId;
    }

    public void setGeneralId(Integer generalId) {
        this.generalId = generalId;
    }

    public TestPlayerEntity getPlayer() {
        return player;
    }

    public void setPlayer(TestPlayerEntity player) {
        this.player = player;
    }

    public Integer getDictId() {
        return DictId;
    }

    public void setDictId(Integer dictId) {
        DictId = dictId;
    }
}