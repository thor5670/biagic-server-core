package com.rtsapp.server.domain.entity;

import com.rtsapp.server.common.ByteBuffer;
import com.rtsapp.server.domain.IEntity;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by admin on 15-10-9.
 */


@Entity
@Table
public  abstract class TestPackEntity extends IEntity {

    //玩家
    private TestPlayerEntity player;

    // 所有的道具
    private Map<Integer, Integer> itemsMap = new HashMap<>();// 道具字典


    private String remark;



    @Id
    @OneToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "playerId")
    public TestPlayerEntity getPlayer() {
        return player;
    }


    @Column(length = 10000, name = "itemsbytes")
    public byte[] getItemsBytes() {
        ByteBuffer buffer = new ByteBuffer();
        Set<Map.Entry<Integer, Integer>> entrySet = itemsMap.entrySet();
        buffer.writeInt(entrySet.size());
        for (Map.Entry<Integer, Integer> entry : entrySet) {
            buffer.writeInt(entry.getKey());
            buffer.writeInt(entry.getValue());
        }
        return buffer.array();
    }

    public void setItemsBytes(byte[] itemsBytes) {
        if( itemsBytes == null ){
            return;
        }

        itemsMap.clear();
        ByteBuffer buffer = new ByteBuffer(itemsBytes);
        int size = buffer.readInt();
        for (int i = 0; i < size; ++i) {
            Integer key = buffer.readInt();
            Integer value = buffer.readInt();
            itemsMap.put(key, value);
        }
    }

    @Transient
    public Map<Integer, Integer> getItemsMap() {
        return itemsMap;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
