package com.rtsapp.server.domain.entity;

import com.rtsapp.server.domain.IEntity;
import com.rtsapp.server.logger.Logger;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 15-10-9.
 */

@Entity
@Table
public  abstract class TestPlayerEntity extends IEntity {


    private static final Logger logger =com.rtsapp.server.logger.LoggerFactory.getLogger( TestPlayerEntity.class );

    private static final long serialVersionUID = 1L;


    @Id
    private Integer playerId;

    private String userName;


    // 背包
    @OneToOne(mappedBy = "player", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private TestPackEntity packInfo = null;

    // 将军
    @OneToMany(mappedBy = "player", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @MapKey(name = "DictId")
    private Map<Integer, TestGeneralEntity> generals = new HashMap<>();




    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TestPackEntity getPackInfo() {
        return packInfo;
    }

    public void setPackInfo(TestPackEntity packInfo) {
        this.packInfo = packInfo;
    }

    public Map<Integer, TestGeneralEntity> getGenerals() {
        return generals;
    }

    public void setGenerals(Map<Integer, TestGeneralEntity> generals) {
        this.generals = generals;
    }
}