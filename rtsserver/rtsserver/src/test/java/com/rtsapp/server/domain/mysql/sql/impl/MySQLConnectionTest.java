package com.rtsapp.server.domain.mysql.sql.impl;

import com.rtsapp.server.domain.mysql.sql.MySQLConnectionInfo;
import com.rtsapp.server.domain.mysql.sql.MySQLPreparedStatement;
import com.rtsapp.server.domain.mysql.sql.MySQLResultSetHandler;
import junit.framework.Assert;
import junit.framework.TestCase;

import java.sql.ResultSet;

/**
 * Created by admin on 15-10-8.
 */
public class MySQLConnectionTest extends TestCase {

    private boolean testDB = false;

    public void setUp(){
        testDB = false;
    }

    public void testConnection(){

        if( !testDB ){
            return;
        }

        MySQLConnectionInfo connInfo = new MySQLConnectionInfo();

        connInfo.setHost("127.0.0.1");
        connInfo.setPort(3306);
        connInfo.setDatabase("rtsapp");
        connInfo.setCharacterEncoding("UTF-8");
        connInfo.setUser("root");
        connInfo.setPassword("root");

        MySQLConnection conn = new MySQLConnection( connInfo );
        Assert.assertTrue( conn.open( ) );


        String index = "sel_test";
        Assert.assertTrue(conn.prepareStatement( index, "select * from test"));
        Assert.assertNotNull(conn.getPreparedStatement( index ));

        MySQLPreparedStatement pst = new MySQLPreparedStatement( index );
        conn.query( pst, new MySQLResultSetHandler( ) {
            @Override
            public void doResultSet(ResultSet rs) {
                Assert.assertTrue( true );
            }
        });


        conn.close();


    }

}
