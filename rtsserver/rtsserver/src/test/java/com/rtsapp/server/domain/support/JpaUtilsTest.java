package com.rtsapp.server.domain.support;

import com.rtsapp.server.domain.support.jpa.ColumnDef;
import com.rtsapp.server.domain.support.jpa.TableDef;
import junit.framework.Assert;
import junit.framework.TestCase;

import com.rtsapp.server.domain.entity.*;

/**
 * Created by admin on 15-10-8.
 */
public class JpaUtilsTest extends TestCase {


    public void testGetTableDef(){

        //PlayerEntity
        TableDef tableDef =  JpaUtils.getTableDef(TestPlayerEntity.class);
        Assert.assertNotNull(tableDef);

        Assert.assertEquals("TestPlayerEntity", tableDef.getEntityName());
        Assert.assertEquals("TestPlayerEntity", tableDef.getTableName());
        Assert.assertEquals(2, tableDef.getColumns().size());

        ColumnDef idColumn = tableDef.getId();
        Assert.assertNotNull(idColumn);
        Assert.assertEquals("playerId", idColumn.getFieldName());

        ColumnDef column =  tableDef.getColumnByFieldName("playerId");
        Assert.assertNotNull(column);
        Assert.assertEquals("java.lang.Integer", column.getFieldType());
        Assert.assertEquals("playerId", column.getFieldName());
        Assert.assertEquals("playerId", column.getColumnName());
        Assert.assertFalse(column.isJoinColumn());

        column = tableDef.getColumnByFieldName("userName");
        Assert.assertNotNull(column);
        Assert.assertEquals("java.lang.String", column.getFieldType());
        Assert.assertEquals("userName", column.getColumnName());
        Assert.assertEquals("userName", column.getFieldName());
        Assert.assertFalse(column.isJoinColumn());



        //PackEntity
        tableDef = JpaUtils.getTableDef(TestPackEntity.class);
        Assert.assertNotNull(tableDef);
        Assert.assertEquals("TestPackEntity", tableDef.getEntityName());
        Assert.assertEquals(3, tableDef.getColumns().size());

        idColumn = tableDef.getId();
        Assert.assertNotNull(idColumn);
        Assert.assertEquals("playerId", idColumn.getColumnName());

        Assert.assertEquals("player", idColumn.getFieldName());
        Assert.assertEquals("com.rtsapp.server.domain.entity.TestPlayerEntity", idColumn.getFieldType());
        Assert.assertTrue(idColumn.isJoinColumn());
        Assert.assertEquals("java.lang.Integer", idColumn.getJoinClumnFieldType());
        Assert.assertEquals("playerId", idColumn.getJoinColumnFieldName());
        Assert.assertEquals( "getPlayer().getPlayerId()", idColumn.getGetterString() );

        column = tableDef.getColumnByFieldName("itemsBytes");
        Assert.assertNotNull(column);
        Assert.assertEquals("itemsBytes", column.getFieldName());
        Assert.assertEquals("byte[]", column.getFieldType());
        Assert.assertEquals("itemsbytes", column.getColumnName());
        Assert.assertFalse(column.isJoinColumn());

        column = tableDef.getColumnByFieldName("remark");
        Assert.assertNotNull(column);
        Assert.assertEquals("remark", column.getFieldName());
        Assert.assertEquals("java.lang.String", column.getFieldType());
        Assert.assertEquals("remark", column.getColumnName());
        Assert.assertFalse(column.isJoinColumn());


        //GeneralEntity
        tableDef = JpaUtils.getTableDef(TestGeneralEntity.class);
        Assert.assertNotNull( tableDef );
        Assert.assertEquals("TestGeneralEntity", tableDef.getEntityName());
        Assert.assertEquals(3, tableDef.getColumns().size());

        idColumn = tableDef.getId();
        Assert.assertNotNull(idColumn);
        Assert.assertEquals("generalId", idColumn.getFieldName());
        Assert.assertEquals("java.lang.Integer", idColumn.getFieldType());
        Assert.assertEquals("generalId", idColumn.getColumnName());
        Assert.assertFalse(idColumn.isJoinColumn());


        column = tableDef.getColumnByFieldName("DictId");
        Assert.assertNotNull( column );
        Assert.assertEquals("DictId", column.getFieldName());
        Assert.assertEquals("DictId", column.getColumnName() );
        Assert.assertEquals( "java.lang.Integer", column.getFieldType() );
        Assert.assertFalse(column.isJoinColumn());


        column = tableDef.getColumnByFieldName( "player" );
        Assert.assertNotNull( column );
        Assert.assertEquals("player", column.getFieldName());
        Assert.assertEquals("com.rtsapp.server.domain.entity.TestPlayerEntity", column.getFieldType() );
        Assert.assertEquals( "playerId", column.getColumnName() );
        Assert.assertTrue(column.isJoinColumn());
        Assert.assertEquals("java.lang.Integer", column.getJoinClumnFieldType() );
        Assert.assertEquals( "playerId", column.getJoinColumnFieldName() );

    }


}
