package com.rtsapp.server.logger.file;


import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by admin on 16-2-26.
 */
public class FilePerformanceTest {


    public static void main( String[] args ) throws IOException {
       //String log = "2016-02-25 23:53:15 [ DEBUG ] - [ main:24677 ] com.rtsapp.server.domain.mysql.sql.impl.MySQLConnection.prepareStatement() : 163 - 预编译sql: index=IPlayerActivityDao_insert_PlayerActivityEntity, sql=insert into PlayerActivityEntity( playerId,activityProgressBytes ) values ( ?,? )";
        String log = "2016-02-25 23:53:15 [ DEBUG ] - [ main:24677 ] com.rtsapp.server.domain.mysql.sql.impl.MySQLConnection.prepareStatement() : 163 - 预编译sql: index=IPlayerActivityDao_inse";

        //String log = "2016-02-25 23:53:15 [ DEBUG ] - [ main:24677 ] com.rtsapp.server.domain";

        log.getBytes();


        int count = 1000000;
        long startTime = System.nanoTime();
        for( int i = 0; i < count; i++ ) {
            byte[] bytes = log.getBytes();
        }
        long endTime = System.nanoTime();
        System.out.println( "共耗时:" + (endTime-startTime) + ",平均耗时:" + (endTime-startTime) / count );


        {
            // FileWriter
            FileWriter fw = new FileWriter("/Users/admin/a.log");
            startTime = System.nanoTime();
            for (int i = 0; i < count; i++) {
                fw.write(log);
                if (i % 500 == 0) {
                    fw.flush();
                }
            }
            fw.flush();
            fw.close();
            endTime = System.nanoTime();
            System.out.println("共耗时:" + (endTime - startTime) + ",平均耗时:" + (endTime - startTime) / count);
        }


        {
            FileOutputStream fw = new FileOutputStream("/Users/admin/b.log");
            startTime = System.nanoTime();
            for (int i = 0; i < count; i++) {
                fw.write( log.getBytes() );
                if (i % 100 == 0) {
                    fw.flush();
                }
            }
            fw.flush();
            fw.close();
            endTime = System.nanoTime();
            System.out.println("共耗时:" + (endTime - startTime) + ",平均耗时:" + (endTime - startTime) / count);
        }

        {
            //
            FileChannel fileChannel = new FileOutputStream("/Users/admin/c.log").getChannel();
            ByteBuffer bw = ByteBuffer.allocateDirect(1024 * 1024);

            startTime = System.nanoTime();

            for (int i = 0; i < count; i++) {
                bw.clear();
                bw.put(log.getBytes());
                bw.flip();

                while (bw.hasRemaining()) {
                    fileChannel.write(bw);
                }

            }

            fileChannel.close();
            endTime = System.nanoTime();

            System.out.println("共耗时:" + (endTime - startTime) + ",平均耗时:" + (endTime - startTime) / count);

        }

    }


}
