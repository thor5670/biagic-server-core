package com.rtsapp.server.logger.format;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by admin on 16-2-25.
 */
public class LogStringFormatTest extends TestCase {


    public void testLogStringFormat(){

        foo( "abc{}efg{}hij{}lmn", "abc中efg国hij人lmn", "中", "国", "人" );
        foo( "abc{ 0 }efg{ 1 }hij{ 2 }lmn", "abc中efg国hij人lmn", "中", "国", "人" );
        foo( "{}abc{}hij{}", "中abc国hij人", "中", "国", "人" );
        foo( "{}abc{}{}", "中abc国人", "中", "国", "人" );
        foo( "{}{}{}", "中国人", "中", "国", "人" );
        foo( "{{}abc{}{}", "{中abc国人", "中", "国", "人" );
        foo( "{{}abc{}{}}", "{中abc国人}", "中", "国", "人" );
        foo( "{{{}}}abc{}{}", "{{中}}abc国人", "中", "国", "人" );
    }

    private void foo( String formatLog, String expect, Object... params ){

        LogStringFormat stringFormat = new LogStringFormat( formatLog );
        StringBuilder sb = new StringBuilder();
        stringFormat.format(sb, params );

        Assert.assertEquals( expect, sb.toString());
    }



}
