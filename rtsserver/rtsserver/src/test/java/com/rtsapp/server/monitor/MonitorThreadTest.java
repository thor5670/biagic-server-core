package com.rtsapp.server.monitor;

import com.rtsapp.server.monitor.MonitorThread;
import com.rtsapp.server.monitor.TimeoutMonitorTask;

import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *  线程测试
 *  测试检测线程死循环
 *  测试kill线程，再重启
 *
 *  如何检测一个线程有问题
 *      1. 如果检测他是死循环，而不是正常的运行
 *      2. 如何检测他是阻塞,
 *
 */
public class MonitorThreadTest {

    /**
     * 死循环线程
     */
    static class DeadlockTask implements Runnable{

        int times = 0;
        int lockTimes = 0;

        int deadLockCondition = 0;

        @Override
        public void run() {

            String threadName = Thread.currentThread().getName();

            System.out.println( threadName + ": DeadlockTask running" );

            while( true ) {

                if (times <= 0) {
                    deadLockCondition = new Random().nextInt(60) + 30;
                }

                try {

                    //进入业务线程
                    TimeoutMonitorTask.startProcess();

                    times++;
                    System.out.println( threadName + ": times=" + times);
                    Thread.sleep(1000);

                    if (times >= deadLockCondition) {
                        times = 0;
                        lockTimes = 0;
                        while (true) {
                            lockTimes++;
                            System.out.println( threadName + ": deadlock: deadlocktimes=" + lockTimes );
                            Thread.sleep( 1000 );
                        }
                    }

                } catch (InterruptedException e) {
                    times = 0;
                    e.printStackTrace();
                }finally {
                    //退出业务线程
                    TimeoutMonitorTask.endProcess();
                }
            }
        }




    }



    public static void main( String[] args ){

        //监控线程启动
        MonitorThread.start();

        //创建N个业务线程，进行处理
        int n = 6;
        for( int i = 1; i <= n ; i++ ){

            Thread t = new Thread( new DeadlockTask() , "线程-" + i );
            t.start();

            try {
                Thread.sleep( 1000 / n  );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
