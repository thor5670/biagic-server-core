package com.rtsapp.server.network.crypto;

import com.rtsapp.server.network.protocol.crypto.RC4;
import com.rtsapp.server.network.protocol.crypto.KeyUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.util.Random;

/**
 * Created by admin on 16-6-17.
 */
public class RC4Benchmark {


    public static void main( String[] args ){


        final Random r = new Random();
        final RC4 rc4 = new RC4( KeyUtils.generateRC4CrpytKey() );

        int length = 10;
        long startTime , endTime ;

        for( int i = 0; i < 20; i++ ) {

            System.out.println( "---进行长度为:" + length + "的加密测试---" );

            byte[] bytes = new byte[ length ];
            r.nextBytes( bytes );

            {
                ByteBuf buffer = Unpooled.buffer();
                buffer.writeBytes( bytes );

                startTime = System.nanoTime();
                rc4.crypt0(buffer, buffer.readerIndex(), buffer.writerIndex());
                endTime = System.nanoTime();

                System.out.println( "crypt0 耗时:" + (endTime - startTime)  + "纳秒" );
            }


            {
                ByteBuf buffer = Unpooled.buffer();
                buffer.writeBytes( bytes );

                startTime = System.nanoTime();
                rc4.crypt1(buffer, buffer.readerIndex(), buffer.writerIndex() );
                endTime = System.nanoTime();

                System.out.println("crypt1 耗时:" + (endTime - startTime) + "纳秒");
            }

            length = length * 2;
        }

    }

}
