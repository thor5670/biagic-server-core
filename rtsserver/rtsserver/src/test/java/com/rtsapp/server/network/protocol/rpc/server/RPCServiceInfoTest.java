package com.rtsapp.server.network.protocol.rpc.server;

import junit.framework.TestCase;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;

import static junit.framework.Assert.*;


public class RPCServiceInfoTest extends TestCase{

    interface IFoo{
        void hello1();
    }

    static class FooA implements IFoo{

        @Override
        public void hello1() {
            System.out.println( "hello A" );
        }

    }

    static class FooB extends FooA{

        @Override
        public void hello1( ) {
            System.out.println("hello B");
        }

    }


    public static class HelloService{

        public int add( int a, int b ){
            System.out.println( "a+b=" + ( a + b ) );
            return (a+b);
        }


        public void add( String name ){
            System.out.println( "say add = " + name );
        }

        public void foo( IFoo foo ){
            foo.hello1();
        }
    }


    public void testTypeCompare(){

        {
            ITypeCompare compare1 = TypeCompareFactory.getTypeCompare(int.class);
            assertTrue(compare1.getClass() == BasicTypeCompare.class);
            assertTrue(compare1.isTypeCompatible(new Integer(10)));
            assertFalse(compare1.isTypeCompatible(new Long(10L)));
            assertFalse(compare1.isTypeCompatible(new FooB()));
        }

        {
            ITypeCompare compare2 = TypeCompareFactory.getTypeCompare(IFoo.class);
            assertTrue(compare2.getClass() == ObjectTypeCompare.class);
            assertTrue(compare2.isTypeCompatible(new FooA()));
            assertTrue(compare2.isTypeCompatible(new FooB()));

            assertTrue(compare2.isTypeCompatible(new IFoo() {
                @Override
                public void hello1() {

                }
            }));

            assertFalse(compare2.isTypeCompatible(new Long(10)));
        }
    }


    public void testInvokeObjectParams(){

        HelloService service = new HelloService();
        RPCServiceInfo serviceInfo = new RPCServiceInfo( service );
        try {

            serviceInfo.invoke( "foo", new Object[]{ new FooA( ) } );

            serviceInfo.invoke( "foo", new Object[]{ new FooB( ) } );


        } catch (Throwable e) {
            e.printStackTrace();
            assertFalse("不应该抛出异常的", true);
        }

    }


    public void testInvokeBasicParams(){

        HelloService service = new HelloService();

        RPCServiceInfo serviceInfo = new RPCServiceInfo( service );
        try {

            int a = (Integer)serviceInfo.invoke( "add", new Object[]{ 1, 2 } );
            System.out.println( "result 1=" + a );

            int b = (Integer)serviceInfo.invoke( "add", new Object[]{ new Integer(10), new Integer(20) } );
            System.out.println( "result 2=" + b );


        } catch (Throwable e) {
            e.printStackTrace();
            assertFalse("不应该抛出异常的", true);
        }

    }


}
