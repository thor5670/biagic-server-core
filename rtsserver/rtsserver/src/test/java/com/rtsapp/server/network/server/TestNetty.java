package com.rtsapp.server.network.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import org.hibernate.c3p0.internal.C3P0MessageLogger_$logger;

/**
 * Created by admin on 15-11-3.
 */
public class TestNetty {

    public static void main(String[] args ){

        NioEventLoopGroup loopGroup = new NioEventLoopGroup(  1 );

        ServerBootstrap server = new ServerBootstrap();
        server.group( loopGroup, loopGroup ).channel(NioServerSocketChannel.class);
        server.option(ChannelOption.SO_BACKLOG, 128);
        server.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) throws Exception {

                ChannelPipeline pipeline = ch.pipeline();

                pipeline.addLast(new ChannelInboundHandlerAdapter() {
                    @Override
                    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                        ctx.writeAndFlush(msg);
                    }

                });
            }

        });

        try {

            server.bind(9000).sync();

        }catch( Throwable e ){
           e.printStackTrace();
        }
    }


}
