package com.rtsapp.server.utils;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by zhangbin on 16/5/30.
 */
public class StringUtilsTest extends TestCase {

    public void testI18nConversion() {

        String template = "say ${hellow} ${world} ${world} ${hellow}!!!";

        String result = StringUtils.i18nConversion(template,
                new String[]{"${hellow}", "${world}"},
                new String[]{"Hellow", "World"},
                "%s", "^%&");

        Assert.assertEquals(result, "say %s %s %s %s!!!^%&Hellow^%&World^%&World^%&Hellow");

    }

    public void testsimilarDegree() {
        StringUtils.similarDegree("哈哈霍霍嘿嘿和", "哈哈霍霍嘿嘿和我是一个测试你不是是不是是哈哈哈哈哈哈哈你不是");
    }

}
