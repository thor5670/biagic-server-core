package com.rtsapp.server.utils.collections;


import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 */
public class IntListTest extends TestCase{

    public void testAdd( ){

        innerTestAdd( 0, 10 );
        innerTestAdd( 1, 10 );
        innerTestAdd( 5, 10 );
        innerTestAdd( 7, 10 );
        innerTestAdd( 10, 10 );

    }

    private void innerTestAdd( int initLen, int maxLen ){

        IntList list = new IntList( initLen , maxLen);

        Assert.assertTrue(list.isEmpty());
        Assert.assertFalse(list.isFull());
        Assert.assertEquals(0, list.size());


        for (int i = 0; i < maxLen; i++) {
            list.add(i);
            Assert.assertEquals(i + 1, list.size());
        }

        for (int i = 0; i < maxLen; i++) {
            Assert.assertEquals( i , list.get(i) );
        }

    }

}
