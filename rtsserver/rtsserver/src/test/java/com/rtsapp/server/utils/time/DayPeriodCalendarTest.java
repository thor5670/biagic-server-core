package com.rtsapp.server.utils.time;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DayPeriodCalendarTest extends TestCase {


    public void test() throws ParseException {

        DayPeriodCalendar calendar = new DayPeriodCalendar(0);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        {
            Date date_20160601 = sdf.parse("2016-06-01 00:00:00");
            Date date_20160601_00_00_01 = sdf.parse("2016-06-01 00:00:01");
            Date date_20160601_12_00_00 = sdf.parse("2016-06-01 12:00:00");
            Date date_20160601_23_59_59 = sdf.parse("2016-06-01 23:59:59");

            Date date_20160602 = sdf.parse("2016-06-02 00:00:00");


            Assert.assertEquals(date_20160601.getTime(), calendar.getPeriodStartMils(date_20160601.getTime()));
            Assert.assertEquals(date_20160601.getTime(), calendar.getPeriodStartMils(date_20160601_00_00_01.getTime()));
            Assert.assertEquals(date_20160601.getTime(), calendar.getPeriodStartMils(date_20160601_12_00_00.getTime()));
            Assert.assertEquals(date_20160601.getTime(), calendar.getPeriodStartMils(date_20160601_23_59_59.getTime()));

            Assert.assertEquals(date_20160602.getTime(), calendar.getNextPeriodStartMils(date_20160601.getTime()));
            Assert.assertEquals(date_20160602.getTime(), calendar.getNextPeriodStartMils(date_20160601_00_00_01.getTime()));
            Assert.assertEquals(date_20160602.getTime(), calendar.getNextPeriodStartMils(date_20160601_12_00_00.getTime()));
            Assert.assertEquals(date_20160602.getTime(), calendar.getNextPeriodStartMils(date_20160601_23_59_59.getTime()));

            Assert.assertEquals(date_20160602.getTime(), calendar.getPeriodStartMils(date_20160602.getTime()));




            Assert.assertTrue(calendar.isInSamePeriod(date_20160601_00_00_01.getTime(), date_20160601.getTime()));
            Assert.assertTrue( calendar.isInSamePeriod( date_20160601_12_00_00.getTime() , date_20160601.getTime() ) );
            Assert.assertTrue( calendar.isInSamePeriod( date_20160601_23_59_59.getTime() , date_20160601.getTime() ) );

            Assert.assertFalse(calendar.isInSamePeriod(date_20160602.getTime(), date_20160601.getTime()));

        }


        {
            Date date_20160630 = sdf.parse("2016-06-30 00:00:00");
            Date date_20160630_00_00_01 = sdf.parse("2016-06-30 00:00:01");
            Date date_20160630_12_00_00 = sdf.parse("2016-06-30 12:00:00");
            Date date_20160630_23_59_59 = sdf.parse("2016-06-30 23:59:59");

            Date date_20160701 = sdf.parse("2016-07-01 00:00:00");



            Assert.assertEquals(date_20160630.getTime(), calendar.getPeriodStartMils(date_20160630.getTime()));
            Assert.assertEquals(date_20160630.getTime(), calendar.getPeriodStartMils(date_20160630_00_00_01.getTime()));
            Assert.assertEquals(date_20160630.getTime(), calendar.getPeriodStartMils(date_20160630_12_00_00.getTime()));
            Assert.assertEquals(date_20160630.getTime(), calendar.getPeriodStartMils(date_20160630_23_59_59.getTime()));

            Assert.assertEquals(date_20160701.getTime(), calendar.getNextPeriodStartMils(date_20160630.getTime()));
            Assert.assertEquals(date_20160701.getTime(), calendar.getNextPeriodStartMils(date_20160630_00_00_01.getTime()));
            Assert.assertEquals(date_20160701.getTime(), calendar.getNextPeriodStartMils(date_20160630_12_00_00.getTime()));
            Assert.assertEquals(date_20160701.getTime(), calendar.getNextPeriodStartMils(date_20160630_23_59_59.getTime()));

            Assert.assertEquals(date_20160701.getTime(), calendar.getPeriodStartMils(date_20160701.getTime()));

            Assert.assertTrue(calendar.isInSamePeriod(date_20160630_00_00_01.getTime(), date_20160630.getTime()));
            Assert.assertTrue(calendar.isInSamePeriod(date_20160630_12_00_00.getTime(), date_20160630.getTime()));
            Assert.assertTrue(calendar.isInSamePeriod(date_20160630_23_59_59.getTime(), date_20160630.getTime()));

            Assert.assertFalse(calendar.isInSamePeriod(date_20160630_23_59_59.getTime(), date_20160701.getTime()));
        }



        //测试4点


    }



    public void testHourOffset() throws ParseException {

        DayPeriodCalendar calendar = new DayPeriodCalendar( 4 );

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        {


            Date date_20160601_04_00_00 = sdf.parse("2016-06-01 04:00:00");


            Date date_20160601_00_00_00 = sdf.parse("2016-06-01 00:00:00");
            Date date_20160601_00_00_01 = sdf.parse("2016-06-01 00:00:01");
            Date date_20160601_03_59_59 = sdf.parse("2016-06-01 03:59:59");


            Date date_20160601_04_00_01 = sdf.parse("2016-06-01 04:00:01");
            Date date_20160601_23_59_59 = sdf.parse("2016-06-01 23:59:59");

            Date date_20160602_00_00_00 = sdf.parse("2016-06-02 00:00:00");
            Date date_20160602_03_59_59 = sdf.parse("2016-06-02 03:59:59");

            Date date_20160602_04_00_00 = sdf.parse("2016-06-02 04:00:00");



            Assert.assertEquals(date_20160601_04_00_00.getTime(), calendar.getPeriodStartMils(date_20160601_04_00_00.getTime()));

            Date date_20160531_04_00_00 = sdf.parse("2016-05-31 04:00:00");
            Assert.assertEquals(date_20160531_04_00_00.getTime(), calendar.getPeriodStartMils(date_20160601_00_00_00.getTime()));
            Assert.assertEquals(date_20160531_04_00_00.getTime(), calendar.getPeriodStartMils(date_20160601_00_00_01.getTime()));
            Assert.assertEquals(date_20160531_04_00_00.getTime(), calendar.getPeriodStartMils(date_20160601_03_59_59.getTime()));


            Assert.assertEquals(date_20160601_04_00_00.getTime(), calendar.getPeriodStartMils(date_20160601_04_00_01.getTime()));
            Assert.assertEquals(date_20160601_04_00_00.getTime(), calendar.getPeriodStartMils(date_20160601_23_59_59.getTime()));


            Assert.assertEquals(date_20160601_04_00_00.getTime(), calendar.getPeriodStartMils(date_20160602_00_00_00.getTime()));
            Assert.assertEquals(date_20160601_04_00_00.getTime(), calendar.getPeriodStartMils(date_20160602_03_59_59.getTime()));


            Assert.assertEquals(date_20160602_04_00_00.getTime(), calendar.getPeriodStartMils(date_20160602_04_00_00.getTime()));

        }
    }

    public void testHourMinuteOffset() throws ParseException {

        DayPeriodCalendar calendar = new DayPeriodCalendar( 4 , 20 );

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        {
            Date date_20160601_04_20_00 = sdf.parse("2016-06-01 04:20:00");

            Date date_20160601_00_00_00 = sdf.parse("2016-06-01 00:00:00");
            Date date_20160601_00_00_01 = sdf.parse("2016-06-01 00:00:01");
            Date date_20160601_04_19_59 = sdf.parse("2016-06-01 04:19:59");


            Date date_20160601_04_20_01 = sdf.parse("2016-06-01 04:20:01");
            Date date_20160601_23_59_59 = sdf.parse("2016-06-01 23:59:59");

            Date date_20160602_00_00_00 = sdf.parse("2016-06-02 00:00:00");
            Date date_20160602_04_19_59 = sdf.parse("2016-06-02 04:19:59");

            Date date_20160602_04_20_00 = sdf.parse("2016-06-02 04:20:00");

            Date date_20160602 = sdf.parse("2016-06-02 00:00:00");


            Assert.assertEquals(date_20160601_04_20_00.getTime(), calendar.getPeriodStartMils(date_20160601_04_20_00.getTime()));

            Date date_20160531_04_20_00 = sdf.parse("2016-05-31 04:20:00");
            Assert.assertEquals(date_20160531_04_20_00.getTime(), calendar.getPeriodStartMils(date_20160601_00_00_00.getTime()));
            Assert.assertEquals(date_20160531_04_20_00.getTime(), calendar.getPeriodStartMils(date_20160601_00_00_01.getTime()));
            Assert.assertEquals(date_20160531_04_20_00.getTime(), calendar.getPeriodStartMils(date_20160601_04_19_59.getTime()));


            Assert.assertEquals(date_20160601_04_20_00.getTime(), calendar.getPeriodStartMils(date_20160601_04_20_01.getTime()));
            Assert.assertEquals(date_20160601_04_20_00.getTime(), calendar.getPeriodStartMils(date_20160601_23_59_59.getTime()));


            Assert.assertEquals(date_20160601_04_20_00.getTime(), calendar.getPeriodStartMils(date_20160602_00_00_00.getTime()));
            Assert.assertEquals(date_20160601_04_20_00.getTime(), calendar.getPeriodStartMils(date_20160602_04_19_59.getTime()));


            Assert.assertEquals(date_20160602_04_20_00.getTime(), calendar.getPeriodStartMils(date_20160602_04_20_00.getTime()));

        }
    }

}
