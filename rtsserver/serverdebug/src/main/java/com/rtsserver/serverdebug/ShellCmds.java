package com.rtsserver.serverdebug;

/**
 * Created by admin on 16-11-1.
 */
public class ShellCmds {

    public static String killProcess( String processName ){

        String cmd =  "proc_num=`ps -ef | grep "+processName+" | grep -v grep | wc -l`\n" +
                "        if [ $proc_num -gt 0 ]\n" +
                "        then\n" +
                "        proc_id=`ps -ef | grep "+processName+" | grep -v grep | awk '{print $2}'`\n" +
                "        echo 'kill pid=' $proc_id\n" +
                "\n" +
                "        kill $proc_id\n" +
                "        fi";
        return cmd;
    }

    public static String waitProcessShutDown( String processName ){

        String cmd =
                "for (( ; ; ))\n" +
                "  do\n" +
                "    proc_num=`ps -ef | grep "+processName+" | grep -v grep | wc -l`\n" +
                "    if [ $proc_num -gt 0 ]                                                                            \n" +
                "    then\n" +
                "      echo '休眠1秒,等待进程结束'\n" +
                "      sleep 1\n" +
                "    else\n" +
                "      echo '进程已关闭'\n" +
                "      break\n" +
                "    fi\n" +
                "  done";

        return cmd;
    }


    public static String executeJar( String jarPath, String jarName ){
//        String cmd=" cd "+jarPath+" \n" +
//                " nohup java -jar " +jarName+ "&";

        String cmd=" cd "+jarPath+" \n" +
                "  dtach -n `mktemp -u /tmp/dtach.XXXX` java -jar " +jarName+ "&";

        return cmd;
    }

}
