<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.rtsserver.serverdebug.*" %>
<%@ page import="com.rtsserver.serverdebug.ShellUtils.CommandResult" %>

<%

 try{
  String dateStr = request.getParameter( "date" );
  Date date =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse( dateStr );

  CommandResult result = ShellUtils.execCommand( "date -s \"" +dateStr+ "\"" , true );

  out.println( result.result );
  out.println( result.successMsg );
  out.println( result.errorMsg );

 }catch( Exception ex ){
    ex.printStackTrace( response.getWriter() );
 }
%>