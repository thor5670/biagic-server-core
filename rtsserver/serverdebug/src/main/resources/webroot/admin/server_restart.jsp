<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.rtsserver.serverdebug.*" %>
<%@ page import="com.rtsserver.serverdebug.ShellUtils.CommandResult" %>

<%

 try{

   String jarName = request.getParameter( "jarName" );
   String jarPath = request.getParameter( "jarPath" );

   out.println( "游戏名称:"+ jarName+ "<br/>" );
   out.println( "游戏目录:"+ jarPath+  "<br/>" );

  {
   CommandResult result = ShellUtils.execCommand( ShellCmds.killProcess( jarName ) , false );
   out.println( result.result + "<br/>" );
   out.println( result.successMsg + "<br/>" );
   out.println( result.errorMsg + "<br/>"  );
  }


  {
     CommandResult result = ShellUtils.execCommand( ShellCmds.waitProcessShutDown( jarName ) , false );
     out.println( result.result + "<br/>" );
     out.println( result.successMsg + "<br/>" );
     out.println( result.errorMsg + "<br/>"  );
    }


    {
         CommandResult result = ShellUtils.execCommand( ShellCmds.executeJar( jarPath, jarName ) , false );
         out.println( result.result + "<br/>" );
         out.println( result.successMsg + "<br/>" );
         out.println( result.errorMsg + "<br/>"  );
    }


 }catch( Exception ex ){
    ex.printStackTrace( response.getWriter() );
 }
%>