package com.rtsapp.version.builder;

import com.rtsapp.version.builder.utils.FileUtils;

import java.io.File;

/**
 * Created by zhangbin on 16/4/19.
 */
public class MigrateTool {

    public static void main(String[] args) {

        File oldDir = new File("/Users/zhangbin/working/naval/naval_version/version-client/version-tool-old-2/");
        File newDir = new File("/Users/zhangbin/working/naval/naval_version/version-client/version-tool/");

        for (File file : oldDir.listFiles()) {
            if (!file.getName().equals("Android") && !file.getName().equals("IOS")) {
                continue;
            }
            for (File file1 : file.listFiles()) {
                if (!file1.getName().startsWith("version_")) {
                    continue;
                }
                File file2 = new File(file1.getAbsolutePath().replace("\\", "/") + "/version.txt");
                if (file2.exists()) {

                    File fileDir = new File(file2.getAbsolutePath().replace("\\", "/").replace(oldDir.getAbsolutePath().replace("\\", "/"), newDir.getAbsolutePath().replace("\\", "/")).replace(file2.getParentFile().getName() + "/version.txt", file2.getParentFile().getName() + ".txt"));
                    if (!fileDir.getParentFile().exists()){
                        fileDir.getParentFile().mkdir();
                    }

                    FileUtils.copyFile(file2, new File(file2.getAbsolutePath().replace("\\", "/").replace(oldDir.getAbsolutePath().replace("\\", "/"), newDir.getAbsolutePath().replace("\\", "/")).replace(file2.getParentFile().getName() + "/version.txt", file2.getParentFile().getName() + ".txt")));

                }
            }
        }

    }

}
