package com.rtsapp.version.builder.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangbin on 15-10-25.
 */
public class VersionInfo {

    public String platform;                                         //平台
    public int versionNo;                                           //版本号
    public String description;                                      //描述
    public String buildTime;                                        //构建时间
    public boolean milestone;                                       //里程碑版本
    public String downloadSite;                                     //整包下载网址
    public String md5 = "";                                         //更新包Md5
    public int size = 0;                                            //更新包大小
    public boolean invalid;                                         //作废版本
    public Map<String, Resource> resourceMap = new HashMap<>();     //全文件列表
    public Map<String, String> channelDownloadSite = new HashMap<>();      //各渠道整包下载网址

}
