package com.rtsapp.version.builder.utils;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;


public class CopyPutUtil implements IPutFile {

    private Element copy;

    private Element getRoot() {
        if (copy == null) {
            try {
                SAXReader saxReader = new SAXReader();
                Document dom = saxReader.read(new File("cfg/cdn.xml"));
                copy = dom.getRootElement().element("copy");

            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }
        return copy;
    }

    public boolean putFiles(String keyPrefix, String pathPrefix, String path) {

        getRoot();

        File file = new File(copy.element("targetDir").attributeValue("value") + keyPrefix + path);

        if (file.exists())//判断是否已上传
        {
            return false;
        }

        FileUtils.newFolder(file.getParent());

        FileUtils.copyFile(new File(pathPrefix + path), file);

        return true;
    }
}
