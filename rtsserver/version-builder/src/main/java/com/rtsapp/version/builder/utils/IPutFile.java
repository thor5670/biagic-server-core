package com.rtsapp.version.builder.utils;

/**
 * Created by zhangbin on 16/8/6.
 */
public interface IPutFile {
    boolean putFiles(String keyPrefix, String pathPrefix, String path);
}
