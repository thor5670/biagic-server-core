package com.rtsapp.version.builder.utils;

import cn.ucloud.ufile.UFileClient;
import cn.ucloud.ufile.UFileRequest;
import cn.ucloud.ufile.UFileResponse;
import cn.ucloud.ufile.sender.GetSender;
import cn.ucloud.ufile.sender.PutSender;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;

public class UFilePutUtil implements IPutFile {

    private Element ufile;

    private Element getRoot() {
        if (ufile == null) {
            try {
                SAXReader saxReader = new SAXReader();
                Document dom = saxReader.read(new File("cfg/cdn.xml"));
                ufile = dom.getRootElement().element("ufile");
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }
        return ufile;
    }

    private UFileClient getNewClient() {
        getRoot();

        UFileClient ufileClient = new UFileClient();
        ufileClient.setUcloudPublicKey(ufile.element("publicKey").attributeValue("value"));
        ufileClient.setUcloudPrivateKey(ufile.element("privateKey").attributeValue("value"));
        ufileClient.setProxySuffix(ufile.element("proxySuffix").attributeValue("value"));
        ufileClient.setDownloadProxySuffix(ufile.element("downloadProxySuffix").attributeValue("value"));
        HttpClient httpClient = new DefaultHttpClient();
        ufileClient.setHttpClient(httpClient);
        return ufileClient;
    }

    public boolean putFiles(String keyPrefix, String pathPrefix, String path) {

        UFileClient ufileClient = getNewClient();
        UFileRequest request = new UFileRequest();
        request.setBucketName(ufile.element("bucketName").attributeValue("value"));
        request.setKey(keyPrefix + path);
        request.setFilePath(pathPrefix + path);

        if (checkHasFile(ufileClient, request)) {
            return false;
        }
        ufileClient.getHttpClient().getConnectionManager().shutdown();

        ufileClient = getNewClient();

        PutSender sender = new PutSender();
        sender.makeAuth(ufileClient, request);

        UFileResponse response = sender.send(ufileClient, request);
        if (response == null) {
            throw new RuntimeException("put file has an error, lost connect.");

        }
        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("put file has an error, status number: " + response.getStatusLine().getStatusCode());
        }

        ufileClient.getHttpClient().getConnectionManager().shutdown();

        return true;

    }

    private boolean checkHasFile(UFileClient ufileClient, UFileRequest request) {
        GetSender sender = new GetSender();
        sender.makeAuth(ufileClient, request);

        UFileResponse response = sender.send(ufileClient, request);
        if (response == null) {
            return false;
        }

        //handler error response
        if (response.getStatusLine().getStatusCode() != 200) {
            return false;
        }
        if (response.getContent() == null) {
            return false;
        }
        return true;
    }
}
