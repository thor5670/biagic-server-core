package com.rtsapp.naval.version.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.rtsapp.naval.version")
public class AppConfig {
}