package com.rtsapp.naval.version.context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rtsapp.naval.version.model.RemotePlatform;
import com.rtsapp.naval.version.model.RemoteVersion;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by zhangbin on 15/12/8.
 */
public class VersionContext {

    private static final ThreadLocal<VersionContext> DEBUG_VERSION_CONTEXT = new ThreadLocal<>();
    private static final ThreadLocal<VersionContext> RELEASE_VERSION_CONTEXT = new ThreadLocal<>();
    private static Set<String> debugHosts = new ConcurrentSkipListSet<>();
    private static VersionContext debugInstance = new VersionContext();
    private static VersionContext releaseInstance = new VersionContext();

    private Map<Integer, RemotePlatform> versionMap = new HashMap<>();

    public static void refreshVersionContext() {
        VersionContext currentVersionContext = DEBUG_VERSION_CONTEXT.get();
        if (currentVersionContext == null || currentVersionContext != debugInstance) {
            DEBUG_VERSION_CONTEXT.set(debugInstance);
        }

        currentVersionContext = RELEASE_VERSION_CONTEXT.get();
        if (currentVersionContext == null || currentVersionContext != releaseInstance) {
            RELEASE_VERSION_CONTEXT.set(releaseInstance);
        }
    }

    public static VersionContext getInstanceInThread(boolean isDebug) {
        if (isDebug) {
            return DEBUG_VERSION_CONTEXT.get();
        }
        return RELEASE_VERSION_CONTEXT.get();
    }

    public static boolean checkDebugUser(String host) {
        for (String debugHost : debugHosts) {
            if (host.startsWith(debugHost)) {
                return true;
            }
        }
        return false;
    }

    public static boolean genInstance(boolean isDebug, Integer platform, RemotePlatform remotePlatform) {
        if (isDebug) {
            synchronized (VersionContext.class) {
                RemotePlatform oldRemotePlatform = debugInstance.getVersionMap().get(platform);
                if (oldRemotePlatform != null && remotePlatform.getMaxVersionNo() <= oldRemotePlatform.getMaxVersionNo()) {
                    return false;
                }
                if (oldRemotePlatform != null && oldRemotePlatform.getPkgVersion() != null) {
                    if (remotePlatform.getVersionMap().size() > 1) {
                        remotePlatform.getPkgVersion().setDownloadSite(oldRemotePlatform.getPkgVersion().getDownloadSite());
                        remotePlatform.getPkgVersion().setChannelDownloadSite(oldRemotePlatform.getPkgVersion().getChannelDownloadSite());
                    }
                }
                debugInstance.getVersionMap().put(platform, remotePlatform);
                return true;
            }
        } else {
            synchronized (VersionContext.class) {
                RemotePlatform debugRemotePlatform = debugInstance.getVersionMap().get(platform);
                releaseInstance.getVersionMap().put(platform, debugRemotePlatform);
                return true;
            }
        }
    }

    public static void saveVersionInfo() {

        synchronized (VersionContext.class) {


            try {
                ObjectMapper mapper = new ObjectMapper();

                File debugVersionFile = new File("debug_version.txt");
                debugVersionFile.createNewFile();
                mapper.writeValue(debugVersionFile, debugInstance);

                File releaseVersionFile = new File("release_version.txt");
                releaseVersionFile.createNewFile();
                mapper.writeValue(releaseVersionFile, releaseInstance);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void initVersionInfo() {

        ObjectMapper mapper = new ObjectMapper();

        File debugVersionFile = new File("debug_version.txt");
        if (debugVersionFile.exists()) {
            try {
                debugInstance = mapper.readValue(debugVersionFile, VersionContext.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File releaseVersionFile = new File("release_version.txt");
        if (debugVersionFile.exists()) {
            try {
                releaseInstance = mapper.readValue(releaseVersionFile, VersionContext.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static Map<String, Map<Integer, RemotePlatform>> getAllVersion() {

        Map<String, Map<Integer, RemotePlatform>> objects = new HashMap<>();
        synchronized (VersionContext.class) {

            objects.put("debugVersion", debugInstance.versionMap);
            objects.put("releaseVersion", releaseInstance.versionMap);

        }
        return objects;

    }

    public static Set<String> getDebugHosts() {
        return debugHosts;
    }

    public RemoteVersion getAppVersion(int platform, int versionNo) {
        RemotePlatform remotePlatform = versionMap.get(platform);
        if (remotePlatform == null) {
            return new RemoteVersion(platform, 0, false, "", null, "", 0, "", null);
        }
        if (versionNo < remotePlatform.getMinVersionNo()) {
            return remotePlatform.getPkgVersion();
        }
        if (versionNo > remotePlatform.getMaxVersionNo()) {
            return remotePlatform.getVersionMap().get(remotePlatform.getMaxVersionNo());
        }
        if (!remotePlatform.getVersionMap().containsKey(versionNo)) {
            if (remotePlatform.getVersionMap().containsKey(0)) {
                versionNo = 0;
            } else {
                versionNo = remotePlatform.getMinVersionNo();
            }
        }
        return remotePlatform.getVersionMap().get(versionNo);
    }

    public Map<Integer, RemotePlatform> getVersionMap() {
        return versionMap;
    }
}
