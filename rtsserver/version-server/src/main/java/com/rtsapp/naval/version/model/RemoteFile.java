package com.rtsapp.naval.version.model;

/**
 * Created by zhangbin on 15/12/8.
 */
public class RemoteFile {
    private String resMd5;                                             //MD5
    private int resSize;                                               //文件大小
    private String resPath;                                            //短路径

    public RemoteFile() {
    }

    public RemoteFile(String resMd5, int resSize, String resPath) {
        this.resMd5 = resMd5;
        this.resSize = resSize;
        this.resPath = resPath;
    }

    public String getResMd5() {
        return resMd5;
    }

    public int getResSize() {
        return resSize;
    }

    public String getResPath() {
        return resPath;
    }
}