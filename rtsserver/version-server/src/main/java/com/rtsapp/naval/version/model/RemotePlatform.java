package com.rtsapp.naval.version.model;

import java.util.Map;

/**
 * Created by zhangbin on 15/12/8.
 */
public class RemotePlatform {

    private int minVersionNo;
    private int maxVersionNo;
    private RemoteVersion pkgVersion;
    private Map<Integer, RemoteVersion> versionMap;

    public RemotePlatform() {
    }

    public RemotePlatform(int minVersionNo, int maxVersionNo, Map<Integer, RemoteVersion> versionMap, RemoteVersion pkgVersion) {
        this.minVersionNo = minVersionNo;
        this.maxVersionNo = maxVersionNo;
        this.pkgVersion = pkgVersion;
        this.versionMap = versionMap;
    }

    public int getMinVersionNo() {
        return minVersionNo;
    }

    public int getMaxVersionNo() {
        return maxVersionNo;
    }

    public RemoteVersion getPkgVersion() {
        return pkgVersion;
    }

    public Map<Integer, RemoteVersion> getVersionMap() {
        return versionMap;
    }
}
