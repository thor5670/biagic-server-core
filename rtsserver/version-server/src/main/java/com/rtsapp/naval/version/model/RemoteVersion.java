package com.rtsapp.naval.version.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangbin on 15/12/8.
 */
public class RemoteVersion {
    private int platform;                                           //平台
    private int versionNo;                                          //版本号
    private boolean milestone;                                      //里程碑版本
    private Map<String, String> channelDownloadSite = new HashMap<>();      //各渠道整包下载网址
    private String downloadSite;                                    //整包下载网址
    private String md5;                                             //整包MD5
    private int size;                                               //整包文件大小
    private String filesUrlPrefix;                                  //文件列表地址前缀
    private List<RemoteFile> files;                                     //文件列表

    public RemoteVersion() {
    }

    public RemoteVersion(int platform, int versionNo, boolean milestone, String downloadSite, Map<String, String> channelDownloadSite, String md5, int size, String filesUrlPrefix, List<RemoteFile> files) {
        this.platform = platform;
        this.versionNo = versionNo;
        this.milestone = milestone;
        this.downloadSite = downloadSite;
        this.channelDownloadSite = channelDownloadSite;
        this.md5 = md5;
        this.size = size;
        this.filesUrlPrefix = filesUrlPrefix;
        this.files = files;
    }

    public int getPlatform() {
        return platform;
    }

    public int getVersionNo() {
        return versionNo;
    }

    public boolean isMilestone() {
        return milestone;
    }

    public String getDownloadSite() {
        return downloadSite;
    }

    public void setDownloadSite(String downloadSite) {
        this.downloadSite = downloadSite;
    }

    public Map<String, String> getChannelDownloadSite() {
        return channelDownloadSite;
    }

    public void setChannelDownloadSite(Map<String, String> channelDownloadSite) {
        this.channelDownloadSite = channelDownloadSite;
    }

    public String getMd5() {
        return md5;
    }

    public int getSize() {
        return size;
    }

    public String getFilesUrlPrefix() {
        return filesUrlPrefix;
    }

    public void setFilesUrlPrefix(String filesUrlPrefix) {
        this.filesUrlPrefix = filesUrlPrefix;
    }

    public List<RemoteFile> getFiles() {
        return files;
    }
}
