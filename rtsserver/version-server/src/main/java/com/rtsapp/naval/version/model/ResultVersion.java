package com.rtsapp.naval.version.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhangbin on 15/12/8.
 */
public class ResultVersion {
    private int platform;                                           //平台
    private int versionNo;                                          //版本号
    private boolean milestone;                                      //里程碑版本
    private Map<String, String> channelDownloadSite = new HashMap<>();      //各渠道整包下载网址
    private String downloadSite;                                    //整包下载网址
    private String md5;                                             //整包MD5
    private int size;                                               //整包文件大小
    private String filesUrlPrefix;                                  //文件列表地址前缀
    private List<RemoteFile> files;                                     //文件列表

    public ResultVersion() {
    }

    public ResultVersion(RemoteVersion remoteVersion) {
        this.platform = remoteVersion.getPlatform();
        this.versionNo = remoteVersion.getVersionNo();
        this.milestone = remoteVersion.isMilestone();
        this.downloadSite = remoteVersion.getDownloadSite();
        this.channelDownloadSite = remoteVersion.getChannelDownloadSite();
        this.md5 = remoteVersion.getMd5();
        this.size = remoteVersion.getSize();
        this.filesUrlPrefix = remoteVersion.getFilesUrlPrefix();
        this.files = remoteVersion.getFiles();
    }

    public int getPlatform() {
        return platform;
    }

    public int getVersionNo() {
        return versionNo;
    }

    public boolean isMilestone() {
        return milestone;
    }

    public String getDownloadSite() {
        return downloadSite;
    }

    @JsonProperty("versionPath")
    public String getVersionPath() {
        return downloadSite;
    }

    public Map<String, String> getChannelDownloadSite() {
        return channelDownloadSite;
    }

    public String getMd5() {
        return md5;
    }

    public int getSize() {
        return size;
    }

    public String getFilesUrlPrefix() {
        return filesUrlPrefix;
    }

    public List<RemoteFile> getFiles() {
        return files;
    }
}
