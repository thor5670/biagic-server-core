package com.rtsapp.server.app;

import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.utils.PropertyCfg;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by admin on 17-6-27.
 */
public class AppConfig {
    private static final AppConfig appConfig = new AppConfig();
    public static PropertyCfg cfg;
    private static Logger LOGGER = com.rtsapp.server.logger.LoggerFactory.getLogger(AppConfig.class);

    static {
        try {
            cfg = new PropertyCfg(new FileInputStream("cfg/sdk.properties"));
        } catch (FileNotFoundException e) {
            LOGGER.error("解析项目配置文件失败", e);
        }
    }
    private AppConfig() {
    }

    public static void loadCfg(String cfgPath) {
        try {
            cfg = new PropertyCfg(new FileInputStream(cfgPath));
        } catch (FileNotFoundException e) {
            LOGGER.error("重新加载配置文件错误!", e);
        }

    }

    public static AppConfig getInstance() {
        return appConfig;
    }

    public String getYiJieSdkUrl() {
        return cfg.getString("sdk.yijie.url");
    }

    public String getAnQuUrl() {
        return cfg.getString("sdk.anqu.url");
    }

    public String getAnQuHongJingAppId() {
        return cfg.getString("sdk.anqu.app.id.HongJing");
    }

    public String getAnQuHongJingAppSecret() {
        return cfg.getString("sdk.anqu.app.secret.HongJing");
    }

    public String getAnQuTanKeZhanAppId() {
        return cfg.getString("sdk.anqu.app.id.TanKeZhan");
    }

    public String getAnQuTanKeZhanAppSecret() {
        return cfg.getString("sdk.anqu.app.secret.TanKeZhan");
    }

    public String getFaceBookUrl() {
        return cfg.getString("sdk.facebook.url");
    }

    public String getHMUrl() {
        return cfg.getString("sdk.hm.url");
    }

    public String getHMAppId() {
        return cfg.getString("sdk.hm.app.id");
    }

    public String getHMAppKey() {
        return cfg.getString("sdk.hm.app.key");
    }

    public String getI4Url() {
        return cfg.getString("sdk.i4.url");
    }

    public String getPPUrl() {
        return cfg.getString("sdk.pp.url");
    }

    public String getPPAppId() {
        return cfg.getString("sdk.pp.app.id");
    }

    public String getPPAppKey() {
        return cfg.getString("sdk.pp.app.key");
    }

    public String getXunLeAppId() {
        return cfg.getString("sdk.xunle.app.id");
    }

    public String getXunLeAppSecret() {
        return cfg.getString("sdk.xunle.app.secret");
    }

    public String getXYUrl() {
        return cfg.getString("sdk.xy.url");
    }

    public String getXYAppId() {
        return cfg.getString("sdk.xy.app.id");
    }

    public String getXYAppKey() {
        return cfg.getString("sdk.xy.app.key");
    }

    public String getXYPayKey() {
        return cfg.getString("sdk.xy.pay.key");
    }

    public String getGoogleAppId() {
        return cfg.getString("sdk.google.app.id");
    }

    public String getFacebookAppId() {
        return cfg.getString("sdk.facebook.app.id");
    }

    public String getFacebookAppSecret() {
        return cfg.getString("sdk.facebook.app.secret");
    }

    public String getGoogleAndroidAppId() {
        return cfg.getString("sdk.google.android.app.id");
    }

    public String getAndroidFacebookAppId() {
        return cfg.getString("sdk.facebook.android.app.id");
    }

    public String getAndroidFacebookAppSecret() {
        return cfg.getString("sdk.facebook.android.app.secret");
    }

}
