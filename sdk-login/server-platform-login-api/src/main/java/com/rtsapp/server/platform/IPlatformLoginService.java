package com.rtsapp.server.platform;

/**
 * Created by admin on 15-12-14.
 */
public interface IPlatformLoginService {

    /**
     * 验证登陆的Token, 成功返回 deviceType_平台_UserId, 失败返回null
     * @param token
     * @param uid 某些登陆sdk返回的uid
     * @return 成功返回平台的UserId, 失败返回null
     */
    String validateLoginToken(  String token, String uid );


}
