package com.rtsapp.server.platform.gamecenter;

import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;
import com.rtsapp.server.platform.IPlatformLoginService;


/**
 * ICC登陆验证
 */
public class GameCenterPlatformLoginService implements IPlatformLoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger( GameCenterPlatformLoginService.class );

    @Override
    public String validateLoginToken(String token, String uid) {

        if ( token == null ) {
            return null;
        }

        LOGGER.info( "gamecenter 登录: sdk_token: " + token );

        return token.trim();
    }
}
