package com.iccgame.sdk;

/**
 *
 * @author Administrator
 */
public class Main {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		// 如何验证数据
		WhatVerifyData();
		// 如何签名数据
		WhatSignData();
		// 如何验证通知
		WhatVerify2Data();
	}

	/**
	 * 如何验证数据
	 */
	public static void WhatVerifyData() {
		try {
			// 需要验证的数据
			String sdk_token = "acct_is_persisted=0&acct_game_user_id=645&acct_login_time=1441155591&sign_type=rsa&sign=ppLsBmO9JgylicC2%2B8VZfTw5MuqsLnU8e5G6hND5J8vHWDNWjO9ZBWicmVV72BbrixQ8hHumbLQa4eLC%2BxptuQ%3D%3D";
			// 还原结构
			SignSerialization data = SignSerialization.unserialize(sdk_token);
			// 设置验证需要的公钥
			data.setPublicKey("MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAPb6KnYMnx1IyXlx6z3fv9ojQ7cx8wpiWdRZ4rbMg2b/Ww0wRF1bSx4SvwEd8/c04YVRo+FiIL8ijSNUT3apQXMCAwEAAQ==");
			// 验证结果
			boolean result = data.verify();
			// 显示结果
			System.out.println("验证数据范例(模拟登录)");
			System.out.println("验证结果: " + String.valueOf(result));
			System.out.println("账号标识: " + data.getItem("acct_game_user_id"));
			System.out.println();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * 如何签名数据
	 */
	public static void WhatSignData() {
		try {
			
			String raw_str = "content=%5B%7B%22name%22%3A%22100%E9%87%91%E5%AD%90%22%2C%22count%22%3A1%2C%22price%22%3A1000.0%7D%5D&game_user_id=994&notify_url=http%3A%2F%2Fuctest.good321.net%3A18081%2Fu1&out_trade_no=4500771&service=2193001&total_fee=1000.0&sign_type=rsa&sign=niFtcn2dkQOO5VpwTlL3jJwryQb%2FVeejfL%2FzARCfgnoAFNS%2BIkf7Wf9ygYysu38Ygo0cYLiOZTf9r%2BMplUQjOg%3D%3D";
			SignSerialization raw = SignSerialization.unserialize(raw_str);
			// 属性赋值
			raw.setPublicKey("MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBANNVk3u/HkcwRL8H7mf4GubQrzVdRxlvNECogJnvjlOPFeLHiSlkefan3VDLrHSNEs0hAtMBeM6MuoCYF/CAaS0CAwEAAQ==");
			raw.setPrivateKey("MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEA01WTe78eRzBEvwfuZ/ga5tCvNV1HGW80QKiAme+OU48V4seJKWR59qfdUMusdI0SzSEC0wF4zoy6gJgX8IBpLQIDAQABAkAL/ueINqj5UXwVe9XEgQjF4UKQvOK0RuOoZ+gk1Lw3ceVIVHfsubNdTa+9NnFcl7DHM/CQ999HK/7+wTcXCtHhAiEA+yl7UE144ZMXWjJqJ1qRYx83GEtNQw53AyPn0lKttssCIQDXZ7Kaxcl0rCnD4fJo0LBxi3Sq4WlR5L7bghxTXLpo5wIhALhl+Dc46esThy2dnfpOsDdKD7UydAMrd41Cq0zrjsaJAiASI8JvtD5N2/28aDewyfpB5ZuSlF75LYlBFQjroEB2ewIhAIEScIfZzGSYeC5Pfhi5N0c+o/F3lUme5UDMyBtuXUQn");
			System.out.println("lin");
			System.out.println(raw.getItem("content"));
			System.out.println(raw.verify());
			System.out.println(raw_str);
			System.out.println(raw.serialize());
			System.out.println();
			
			
			// 还原结构
			SignSerialization data = new SignSerialization();
			
			// 属性赋值
			data.setItem("content", "[{\"name\":\"10个玄石\",\"count\":1,\"price\":1}]");
			data.setItem("game_user_id", "1001");
			data.setItem("notify_url", "http://192.168.0.167:8080/sdkdemo/payComplete.jsp");
			data.setItem("out_trade_no", "1509111748");
			data.setItem("service", "2193001");
			data.setItem("total_fee", "1");
			// 设置签名使用的私钥
			data.setPrivateKey("MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEA01WTe78eRzBEvwfuZ/ga5tCvNV1HGW80QKiAme+OU48V4seJKWR59qfdUMusdI0SzSEC0wF4zoy6gJgX8IBpLQIDAQABAkAL/ueINqj5UXwVe9XEgQjF4UKQvOK0RuOoZ+gk1Lw3ceVIVHfsubNdTa+9NnFcl7DHM/CQ999HK/7+wTcXCtHhAiEA+yl7UE144ZMXWjJqJ1qRYx83GEtNQw53AyPn0lKttssCIQDXZ7Kaxcl0rCnD4fJo0LBxi3Sq4WlR5L7bghxTXLpo5wIhALhl+Dc46esThy2dnfpOsDdKD7UydAMrd41Cq0zrjsaJAiASI8JvtD5N2/28aDewyfpB5ZuSlF75LYlBFQjroEB2ewIhAIEScIfZzGSYeC5Pfhi5N0c+o/F3lUme5UDMyBtuXUQn");
			// 串化数据
			String result = data.serialize();
			// 显示结果
			System.out.println("签名数据案例(模拟支付)");
			System.out.println(result);
			System.out.println();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * 如何验证通知
	 */
	public static void WhatVerify2Data() {
		try {
			// 数据签名
			String sign = "W4tcCyEC+lmRQb6Gq1BwBuzJC5RaXqdHpf+1QsN5irIs80bktnfSsc6X08k0wWt+J6Cp5rD46ITS7k1X7T/+RA==";
			// 还原结构
			SignSerialization data = new SignSerialization();
			// 属性赋值
			data.setItem("game_user_id", "30");
			data.setItem("out_trade_no", "7bad600f23bd442c9307bd9c67839046");
			data.setItem("service", "2193001");
			data.setItem("total_fee", "1");
			data.setItem("trade_no", "20150825237P21930417330200000036");
			data.setItem("completed", "2015-08-25T17:35:28+08:00");
			// 设置验证需要的公钥
			data.setPublicKey("MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKwfufThInBNc5BC6kMNfZhnHzc8YTCakAcmkZYeaSA2FpVHySDwpQZ3EDv4psOJd7YFV6Po/l7mDIgx8I+9RXsCAwEAAQ==");
			// 验证结果
			boolean result = data.verify(sign);
			// 显示结果
			System.out.println("验证数据范例(模拟通知)");
			System.out.println("验证结果: " + String.valueOf(result));
			System.out.println("交易订单: " + data.getItem("trade_no"));
			System.out.println("对比参数: " + data.getSignData());
			System.out.println();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

// End Class
}
