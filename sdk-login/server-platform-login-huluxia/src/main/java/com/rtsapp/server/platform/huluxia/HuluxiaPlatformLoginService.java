package com.rtsapp.server.platform.huluxia;

import com.iccgame.sdk.SignSerialization;
import com.rtsapp.server.platform.IPlatformLoginService;
import com.rtsapp.server.utils.StringUtils;
import com.rtsapp.server.logger.Logger;


/**
 * Created by admin on 15-12-14.
 */
public class HuluxiaPlatformLoginService implements IPlatformLoginService {

    private static final Logger LOGGER = com.rtsapp.server.logger.LoggerFactory.getLogger(HuluxiaPlatformLoginService.class);

    @Override
    public String validateLoginToken(String sdk_token, String uid ) {
        // 验证结果
        boolean result = false;
        String sdk_user_id = null;
        try {
            //sdk_token = "acct_is_persisted=0&acct_game_user_id=645&acct_login_time=1441155591&sign_type=rsa&sign=ppLsBmO9JgylicC2%2B8VZfTw5MuqsLnU8e5G6hND5J8vHWDNWjO9ZBWicmVV72BbrixQ8hHumbLQa4eLC%2BxptuQ%3D%3D";

            // 还原结构
            SignSerialization data = SignSerialization.unserialize(sdk_token);
            // 设置验证需要的公钥
            data.setPublicKey("MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAPb6KnYMnx1IyXlx6z3fv9ojQ7cx8wpiWdRZ4rbMg2b/Ww0wRF1bSx4SvwEd8/c04YVRo+FiIL8ijSNUT3apQXMCAwEAAQ==");

            result = data.verify();
            sdk_user_id = data.getItem("acct_game_user_id");

            LOGGER.info("验证结果: " + String.valueOf(result));
            LOGGER.info("账号标识: " + data.getItem("acct_game_user_id"));

        } catch (Exception e) {
            e.printStackTrace();
        }


        // 显示结果
        if (!result || StringUtils.isEmpty(sdk_user_id)) {
            LOGGER.error("token解析失败,登录失败....");
            return null;
        }

        return sdk_user_id;
    }


}
