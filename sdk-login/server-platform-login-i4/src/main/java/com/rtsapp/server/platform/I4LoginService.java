package com.rtsapp.server.platform;

import com.alibaba.fastjson.JSONObject;
import com.rtsapp.server.app.AppConfig;
import com.rtsapp.server.platform.util.HttpUtils;
import com.rtsapp.server.logger.Logger;

/**
 * 爱思登陆
 */
public class I4LoginService implements IPlatformLoginService {

    private static final Logger LOGGER = com.rtsapp.server.logger.LoggerFactory.getLogger( I4LoginService.class );

    //    https://pay.i4.cn/member_third.action
    private static final String url = AppConfig.getInstance().getI4Url();

    //String token = "b6ed01d7414945aa81e17cf607b3f5fb";

    @Override
    public String validateLoginToken(String token, String uid) {

        LOGGER.info( "i4SDK登陆验证:token=" + token );

        String params = "token=" + token;
        int status = -1;
        int userid = 0;

        String retJson = HttpUtils.postHttps(url, params);
        LOGGER.info( "i4SDK登陆验证返回:" + retJson );

        try {

            JSONObject jsonObject =  JSONObject.parseObject(retJson);
            status = jsonObject.getIntValue("status");
            userid = jsonObject.getIntValue("userid");
            String username  = jsonObject.getString("username");
        } catch (Exception e) {
            LOGGER.error( "I4登陆验证异常:", e );
        }


        if(status == 0){// 登录验证成功
            LOGGER.info("I4登陆成功");
            return "i4_" + userid;
        } else {// 登录验证失败
            LOGGER.info("I4登陆失败");
            return null;
        }


    }






}
