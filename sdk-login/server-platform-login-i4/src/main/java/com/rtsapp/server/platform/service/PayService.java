package com.rtsapp.server.platform.service;

import com.rtsapp.server.platform.conf.PayConfig;
import com.rtsapp.server.platform.util.PayCore;

import java.util.Map;


/**
 * 
 * @author Jonny
 *
 */
public class PayService{
	
	
	
    /**
     * 异步通知消息验证
     * @param para 异步通知消息
     * @return 验证结果
     */
    public static boolean verifySignature(Map<String, String> para) {
        try {
			String respSignature = para.get(PayConfig.SIGNATURE);
			// 除去数组中的空值和签名参数
			Map<String, String> filteredReq = PayCore.paraFilter(para);
			Map<String, String> signature = PayCore.parseSignature(respSignature);
			for (String key : filteredReq.keySet()) {
			    String value = filteredReq.get(key);
			    String signValue = signature.get(key);
			    if (!value.equals(signValue)) {
			    	return false;
			    }
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
        return true;
    }
	
}
	