package com.rtsapp.server.platform.jituo;

import com.rtsapp.server.platform.IPlatformLoginService;
import com.rtsapp.server.utils.StringUtils;
import noumena.payment.userverify.ChannelVerify;

/**
 * Created by admin on 17-8-14.
 */
public class JiTuoPlatformLoginService implements IPlatformLoginService {
    @Override
    public String validateLoginToken(String token, String uid) {

        String result =  ChannelVerify.verify2( token, true);
        if(StringUtils.isEmpty( result ) ){
            return null;
        }else{
            return result;
        }
    }
}
