package com.rtsapp.server.platform.kongzhong;

import com.rtsapp.server.platform.IPlatformLoginService;
import com.rtsapp.server.utils.StringUtils;
import noumena.payment.userverify.ChannelVerify;

/**
 * Created by admin on 15-12-14.
 */
public class KongzhongAndroidLoginService implements IPlatformLoginService {

    @Override
    public String validateLoginToken(String token, String uid  ) {

//        ios_token = "0d019d11-7f22-44aa-a768-bf0aa7a3f755";
//
//        String token = "{ sdk:\"new\",type:\"kongzhong\",token:\""+ ios_token +"\"}";

//        //TODO 测试版
//        ChannelVerify.kongTest=true;

        String result =  ChannelVerify.verify2( token, true);
        if(StringUtils.isEmpty( result ) ){
            return null;
        }else{
            return result;
        }

    }


}
