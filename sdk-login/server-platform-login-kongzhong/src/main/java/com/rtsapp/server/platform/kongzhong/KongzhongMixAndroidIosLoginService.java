package com.rtsapp.server.platform.kongzhong;

import com.rtsapp.server.platform.IPlatformLoginService;

/**
 *  空中IOS，Android混服登陆
 */
public class KongzhongMixAndroidIosLoginService  implements IPlatformLoginService {

    private static  final String DEVICE_TYPE_IOS = "ios";
    private static  final String DEVICE_TYPE_ANDROID = "android";


    private final KongzhongIOSLoginService iosLogin = new KongzhongIOSLoginService( );
    private final KongzhongAndroidLoginService andoirdLogin = new KongzhongAndroidLoginService( );

    @Override
    public String validateLoginToken(String token, String uid) {

        String type_userid = null;
        String device = null;

        if( uid != null &&  DEVICE_TYPE_IOS.equalsIgnoreCase(uid.trim()) ){

            type_userid = iosLogin.validateLoginToken( token, uid );
            device = DEVICE_TYPE_IOS;

        }else{

            type_userid = andoirdLogin.validateLoginToken( token, uid );
            device = DEVICE_TYPE_ANDROID;
        }

        if(  type_userid != null && type_userid.trim().length() > 0 ){
            //return device + "_" + type_userid;

            // 混服策略采用: android, ios同一账号, 共用同一游戏服
            return type_userid;
        }else{
            return null;
        }

    }
}
