##混服多SDK支持
1. 多SDK只需要根据前端传过来的token，调用不同的SDK进行登录即可
    * token格式: { sdk:"new",type:"kongzhong",token:"xxxxx"}
2. 多SDK的支付，服务器不用管，由空中支付中心统一进行处理，并回调我们的服务器
    *
##SDK的type标识
    * type: 可标识的走特定的去掉, 其他的走android统计入口
        1. ios_kongzhong: 空中的ios登录
        2. ios_pp: pp助手
        3. ios_xy: xy助手
        4. ios_i4: 爱思渠道
        5. ios_hm: 海马
    * 其他的走android统接入口
        1.