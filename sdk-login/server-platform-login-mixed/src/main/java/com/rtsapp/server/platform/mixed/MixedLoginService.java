package com.rtsapp.server.platform.mixed;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rtsapp.server.platform.I4LoginService;
import com.rtsapp.server.platform.XYLoginService;
import com.rtsapp.server.platform.facebook.AndroidFacebookPlatformLoginService;
import com.rtsapp.server.platform.facebook.FacebookPlatformLoginService;
import com.rtsapp.server.platform.gamecenter.GameCenterPlatformLoginService;
import com.rtsapp.server.platform.google.GooglePlayPlatformAndroidLoginService;
import com.rtsapp.server.platform.google.GooglePlayPlatformLoginService;
import com.rtsapp.server.platform.jituo.JiTuoPlatformLoginService;
import com.rtsapp.server.platform.xunle.XunleLoginService;
import com.rtsapp.server.platform.IPlatformLoginService;
import com.rtsapp.server.platform.hm.HMLoginService;
import com.rtsapp.server.platform.kongzhong.AnquLoginService;
import com.rtsapp.server.platform.kongzhong.KongzhongAndroidLoginService;
import com.rtsapp.server.platform.kongzhong.KongzhongIOSLoginService;
import com.rtsapp.server.platform.onesdk.OneSdkLoginService;
import com.rtsapp.server.platform.pp.PPLoginService;
import com.rtsapp.server.platform.tourist.TouristLoginService;
import com.rtsapp.server.utils.StringUtils;
import com.rtsapp.server.logger.Logger;

/**
 * 混服登录
 */
public class MixedLoginService   implements IPlatformLoginService {

    private static final Logger LOGGER = com.rtsapp.server.logger.LoggerFactory.getLogger( MixedLoginService.class );

    private static  final String CHANNEL_IOS_KONGZHONG = "ios_kongzhong";
    private static  final String CHANNEL_IOS_PP = "ios_pp";
    private static  final String CHANNEL_IOS_XY = "ios_xy";
    private static  final String CHANNEL_IOS_HM = "ios_hm";
    private static  final String CHANNEL_IOS_I4 = "ios_i4";
    private static  final String CHANNEL_ANDROID_1SDK = "android_1sdk";

    private static  final String CHANNEL_IOS_TOURIST = "ios_tourist";
    private static  final String CHANNEL_ANDROID_TOURIST = "android_tourist";

    private static final String CHANNEL_GAMECENTER = "ios_gamecenter";

    private static final String CHANNEL_FACEBOOK = "facebook";
    //中东Facebook
    private static final String CHANNEL_MIDEAST_FACEBOOK = "mideast_fb";

    private static final String CHANNEL_IOS_ANQU = "ios_anqu";

    private static final String CHANNEL_XUNLE = "xunle";

    private static final String CHANNEL_GOOGLE_PLAY = "google";

    private static final String CHANNEL_MIDEAST_GOOGLE = "mideast_gs";

    private static final String CHANNEL_MIDEAST_ANDROID_GOOGLE = "mideast_gs_android";

    private static final String CHANNEL_MIDEAST_ANDROID_FACEBOOK = "mideast_fb_android";

    private static final String CHANNEL_JITUO = "jituo";

    // 空中android 统接SDK
    private final KongzhongAndroidLoginService andoirdLogin = new KongzhongAndroidLoginService( );

    // 空中 ios
    private final KongzhongIOSLoginService iosLogin = new KongzhongIOSLoginService( );

    //pp助手
    private final PPLoginService ppLogin = new PPLoginService( );

    //i4
    private final I4LoginService i4Login = new I4LoginService( );

    //xy
    private final XYLoginService xyLogin = new XYLoginService( );

    //海马
    private final HMLoginService hmLogin = new HMLoginService();

    //易接
    private final OneSdkLoginService oneSdkLoginService = new OneSdkLoginService();

    //游客
    private final TouristLoginService touristLoginService = new TouristLoginService();

    //gamecenter
    private final GameCenterPlatformLoginService gameCenterService = new GameCenterPlatformLoginService();

    //facebook
    private final FacebookPlatformLoginService facebookPlatformLoginService = new FacebookPlatformLoginService();

    //安趣
    private final AnquLoginService anquLoginService = new AnquLoginService();

    //讯乐
    private final XunleLoginService xunleLoginService = new XunleLoginService();

    //google
    private final GooglePlayPlatformLoginService googlePlayPlatformLoginService = new GooglePlayPlatformLoginService();

    //既拓
    private final JiTuoPlatformLoginService jiTuoPlatformLoginService = new JiTuoPlatformLoginService();

    //中东谷歌安卓版
    private final GooglePlayPlatformAndroidLoginService googlePlayPlatformAndroidLoginService = new GooglePlayPlatformAndroidLoginService();

    //中东facebook安卓版
    private final AndroidFacebookPlatformLoginService androidFacebookPlatformLoginService = new AndroidFacebookPlatformLoginService();

    @Override
    public String validateLoginToken(String token, String deviceType) {

        try {
            JSONObject jsonObject = JSON.parseObject(token);
            if (jsonObject == null) {
                return null;
            }

            String type =  jsonObject.getString("type");
            if( StringUtils.isEmpty( type )) {
                return null;
            }

            type = type.trim().toLowerCase();
            switch ( type ){
                case CHANNEL_IOS_KONGZHONG:{
                    String tokenStr = jsonObject.getString( "token" );
                    return iosLogin.validateLoginToken( tokenStr, null );
                }
                case CHANNEL_JITUO:{
                    return jiTuoPlatformLoginService.validateLoginToken(token, null);
                }
                case CHANNEL_IOS_PP:{
                    String tokenStr = jsonObject.getString( "token" );
                    return ppLogin.validateLoginToken( tokenStr, null );
                }
                case CHANNEL_IOS_HM:{
                    String tokenStr = jsonObject.getString( "token" );
                    String uid = jsonObject.getString( "uid" );
                    return hmLogin.validateLoginToken( tokenStr, uid );
                }
                case CHANNEL_IOS_XY:{
                    String tokenStr = jsonObject.getString( "token" );
                    String uid = jsonObject.getString( "uid" );
                    return xyLogin.validateLoginToken( tokenStr, uid );
                }
                case CHANNEL_IOS_I4:{
                    String tokenStr = jsonObject.getString( "token" );
                    return i4Login.validateLoginToken( tokenStr, null );
                }
                case CHANNEL_ANDROID_1SDK:{
                    return oneSdkLoginService.validateLoginToken(token, null);
                }
                case CHANNEL_IOS_TOURIST:{
                    return touristLoginService.validateLoginToken(token, null);
                }
                case CHANNEL_ANDROID_TOURIST:{
                    return touristLoginService.validateLoginToken(token, null);
                }
                case CHANNEL_GAMECENTER:{
                    String tokenStr = jsonObject.getString( "token" );
                    return gameCenterService.validateLoginToken(tokenStr, null);
                }
                case CHANNEL_FACEBOOK:{
                    String tokenStr = jsonObject.getString( "token" );
                    return facebookPlatformLoginService.validateLoginToken(tokenStr, null);
                }
                case CHANNEL_IOS_ANQU:{
                    return anquLoginService.validateLoginToken(token, null);
                }
                case CHANNEL_XUNLE:{
                    return xunleLoginService.validateLoginToken(token, null);
                }
                case CHANNEL_GOOGLE_PLAY:{
                    String tokenStr = jsonObject.getString( "token" );
                    return googlePlayPlatformLoginService.validateLoginToken(tokenStr, null);
                }
                case CHANNEL_MIDEAST_FACEBOOK:{
                    String tokenStr = jsonObject.getString("token");
                    return facebookPlatformLoginService.validateLoginToken(tokenStr, null);
                }
                case CHANNEL_MIDEAST_GOOGLE:{
                    String tokenStr = jsonObject.getString( "token" );
                    return googlePlayPlatformLoginService.validateLoginToken(tokenStr, null);
                }
                case CHANNEL_MIDEAST_ANDROID_GOOGLE:{
                    String tokenStr = jsonObject.getString( "token" );
                    return googlePlayPlatformAndroidLoginService.validateLoginToken(tokenStr, null);
                }
                case CHANNEL_MIDEAST_ANDROID_FACEBOOK:{
                    String tokenStr = jsonObject.getString("token");
                    return androidFacebookPlatformLoginService.validateLoginToken(tokenStr, null);
                }
                default:{
                    return andoirdLogin.validateLoginToken( token, null );
                }
            }
        }catch( Throwable ex ){
            LOGGER.error("SDK 登录时出现异常",ex);
            return null;
        }


    }
}

