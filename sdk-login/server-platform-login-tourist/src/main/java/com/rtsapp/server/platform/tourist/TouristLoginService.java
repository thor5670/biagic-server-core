package com.rtsapp.server.platform.tourist;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rtsapp.server.platform.IPlatformLoginService;
import com.rtsapp.server.logger.Logger;
import com.rtsapp.server.logger.LoggerFactory;

/**
 * Created by admin on 16-3-10.
 */
public class TouristLoginService implements IPlatformLoginService {

    private static final String privateKey = "bo76&^B*&(FYT%@#I&FI";
    private static final Long overtime = Long.valueOf(1 * 60 * 60 * 1000);
    private static final Logger LOGGER = com.rtsapp.server.logger.LoggerFactory.getLogger(TouristLoginService.class);

    public static void main(String[] args) {


        if (!LoggerFactory.configure("cfg/log.properties")) {
            System.err.println("日志设置失败");
            System.exit(1);
        }


        TouristLoginService t = new TouristLoginService();

        t.validateLoginToken("{sdk:\"new\",type:\"ios_tourist\",token:\"406d62de2c006438992d1ba2c\",deviceid:\"D631A8E4-DCB7-4B18-9C30-2B6B99ED7B8C\",timestamp:\"1467169277\"}", null);
    }


    @Override
    public String validateLoginToken(String token, String uid) {

        JSONObject jsonObject = JSON.parseObject(token);
        String token2 = jsonObject.getString("token");
        String deviceId = jsonObject.getString("deviceid");
        String timestamp = jsonObject.getString("timestamp");

        if (deviceId == null || deviceId.replace("-", "").replace("0", "").trim().length() == 0) {
            return null;
        }

        return "tourist_"+deviceId.trim();
    }

}
