package com.rtsapp.server.platform.xunle;

import com.alibaba.fastjson.JSONObject;
import com.rtsapp.server.app.AppConfig;
import com.rtsapp.server.platform.IPlatformLoginService;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by apple on 17/6/9.
 */
public class XunleLoginService implements IPlatformLoginService {

    //    1000049
    private static final String APP_ID = AppConfig.getInstance().getXunLeAppId();
    //    62deb00f7ebd963a0ceb3f57d8e61fde
    private static final String APP_SECRET = AppConfig.getInstance().getXunLeAppSecret();

    @Override
    public String validateLoginToken(String token, String uid) {

        JSONObject info = JSONObject.parseObject(token);
        String sign = info.getString("sign");
        String userid = info.getString("uid");
        String timestamp = info.getString("timestamp");

        String md5Source = userid + "|" + timestamp + "&" + APP_ID + "|" + APP_SECRET;
        System.out.println(md5Source);
        String md5 = md5(md5Source);
        return md5.equals(sign)?"xunle_"+userid:null;
    }

    private String md5(String datastr) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(datastr.getBytes());
            return new BigInteger(1, messageDigest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
